<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_controller extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		// $this->load->helper(array('email'));
		// $this->load->model(array());

		// $this->site->side = 'backend';
		// $this->site->template = 'default/production';

		$this->template->location = $this->sConfig->_template['backend_folder'];
		$this->template->template_name = $this->sConfig->_template['backend_template_name'];
		
		$this->data['page_title'] = 'default login';
		$this->load->library(array('form_validation'));

		// if (is_backend_privilege() && $this->uri->segment(2) != 'logout')
        // {
        //     redirect('/admin/dashboard');
        // }

        // $this->data = array(
        //     'web_info' => $this->profile_model->web_info('web_name, web_description, web_logo')
		// );
	}
}