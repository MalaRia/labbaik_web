<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_controller extends MY_Controller
{
	function __construct()
	{
        parent::__construct();

        $this->uri_1 = $this->uri->segment(1);
        $this->uri_2 = $this->uri->segment(2);
        $this->uri_3 = $this->uri->segment(3);
        $this->uri_4 = $this->uri->segment(4);

        // if api key not set return forbidden
        $api_key = $this->input->get_request_header('key', TRUE);
        if (empty($api_key)) {
            $data = array(
                'statusCode' => 403,
                'message' => 'Access Forbidden.',
            );
            $this->response($data);
        } else {
            if ($api_key != 'f83b7b6c3564ffce685155bedc7c0093') {
                $data = array(
                    'statusCode' => 403,
                    'message' => 'Access Forbidden.',
                );
                $this->response($data);
            }
        }
    }
    
    public function response($data)
    {
        $this->output
        ->set_status_header(200)
        ->set_content_type('application/json', 'utf-8')
        ->set_output(json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
        ->_display();
        exit;
    }
}