<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->sConfig = new SConfig;

		$this->load->helper(array('template_helper','visitor_helper','auth_helper','flash_helper','image_helper','text','time_helper'));
		$this->load->library(array('Site','Breadcrumb','Upload_file','User','template','user_agent'));
		$this->load->model(array('user_model','profile_model'));
        $this->current_datetime = current_datetime();

        $this->is_ajax_request = $this->input->is_ajax_request();

        $this->errors = FALSE;

		$this->data = array(
			'page_title' => 'cms amp' 
		);
	}
 

	protected function render($the_view = NULL, $template = 'index')
    {
        if ($template == 'json' || $this->is_ajax_request) // Ajax
        {
            $this->output
            ->set_status_header(200)
            ->set_content_type('application/json', 'utf-8')
            ->set_output(json_encode($this->data['data'], JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        }
        elseif(is_null($template)) // Static Page
        {
            $template_name = $this->template->location.'/'.$this->template->template_name.'/';
            $this->load->view($template_name.$the_view,$this->data);
        }
        else // Dynamic Page
        {
            // echo $the_view;
            // $this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view('public/default/'.$the_view, $this->data, TRUE);
            // $this->load->view('public/default/'.$template.'_view', $this->data);

			$template_name = $this->template->location.'/'.$this->template->template_name.'/';
            $this->data['layout'] = (is_null($the_view)) ? '' : $this->load->view($template_name.$the_view, $this->data, TRUE); // show as data if view not null
            $this->template->view($template, $this->data);
        }
    }

}