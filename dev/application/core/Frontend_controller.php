<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_controller extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('seo'));
		$this->load->library(array());
		$this->load->model(array('setting_model','visitor_model','post_model'));

		// if ($this->session->userdata('id'))
		// 	$this->data['user_data'] = $this->user_model->get_by(array('id' => $this->session->userdata('id')), NULL, NULL, TRUE, 'id, username, role, avatar, created_at');

		$this->uri_1 = $this->uri->segment(1);
        $this->uri_2 = $this->uri->segment(2);
        $this->uri_3 = $this->uri->segment(3);
        $this->uri_4 = $this->uri->segment(4);
        $this->uri_5 = $this->uri->segment(5);

		// $this->site->side = 'frontend';
		// $this->site->template = 'default';
		// // $this->data['site_name'] = 'contoh web';

		// $this->breadcrumb->_include_home = 'Home';
		// $this->folder_controller = $sConfig->_template['admin_controller'];
		
		// get active template
		$setting = $this->setting_model->get_by(NULL, NULL, NULL, FALSE, 'content,type'); // sorting by desc
		$this->data['social_media'] = json_decode($setting[0]->content); // social media row
		$this->data['web_info'] = json_decode($setting[2]->content); // web info row
		$this->data['seo'] = json_decode($setting[1]->content); // seo row

		$this->upload_location = base_url('assets/uploads/');

	
		$this->limit = $this->data['web_info']->site_pagination;
		$this->offset = 0;
		$template = json_decode($setting[3]->content); // template row

        $this->template->location = $this->sConfig->_template['frontend_folder'];
		$this->template->template_name = ($template->template) ? $template->template : $this->sConfig->_template['frontend_template_name'];
		
		$this->data['page_title'] = 'default frontend';
	}

	public function insert_visitor($uri_2 = NULL)
    {
        $post_id = '';
        if ($uri_2 != null)
        {
            $post = $this->post_model->get_by(array('slug' => $uri_2), NULL, NULL, TRUE, 'id');
            $post_id = ($post) ? $post->id : '';
        }
        // insert into visitor table
        $data_visitor = array(
            'url' => current_url(),
            'post_id' => $post_id,
            'browser' => $this->agent->browser(),
            'platform' => $this->agent->platform(),
            'ip_address' => $this->input->ip_address(),
            'created_at' => $this->current_datetime
        );
        if (!empty($this->agent->mobile())) {
            $data_visitor['mobile'] = $this->agent->mobile();
        }
        $this->visitor_model->insert($data_visitor);
	}
	
	public function meta_tag($data = NULL)
    { 
        return array(
            'title' => ($data) ? $data['title'] : (($this->data['seo']) ? $this->data['seo']->title : $this->data['web_info']->site_title),
            'description' => ($data) ? strip_tags($data['description']) : (($this->data['seo']->description) ? strip_tags($this->data['seo']->description) : strip_tags($this->data['web_info']->site_description)),
            'keywords' => ($data) ? $data['keywords'] : (($this->data['seo']->keywords) ? $this->data['seo']->keywords : ''),
            'site_title' => $this->data['web_info']->site_title,
            'published_at' => ($data) ? $data['published_at'] : '',
            'url' => current_url(),
            'image' => ($data) ? $this->upload_location.$data['image'] : '',
            'favicon' => (isset($this->data['web_info']->icon)) ? $this->data['web_info']->icon->image : '',
            'ga_verify' => (isset($this->data['seo']->ga_verify)) ? $this->data['seo']->ga_verify : '',
            'ga_analytics' => (isset($this->data['seo']->ga_analytics)) ? $this->data['seo']->ga_analytics : '',
            'rss' => (isset($this->data['seo']->rss)) ? $this->data['seo']->rss : ''
        );
    }
}