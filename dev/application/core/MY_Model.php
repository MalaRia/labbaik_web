<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model
{
	protected $_table_name;
	protected $_order_by;
	protected $_order_by_type;
	protected $_primary_filter = 'intval';
	protected $_primary_key;
	protected $_post_type;
	public $rules; 

	function __construct()
	{
		parent::__construct();
	}

	public function insert($data, $batch = FALSE)
	{
		if ($batch == TRUE)
		{
			return $this->db->insert_batch('{PRE}'.$this->_table_name, $data);
		}
		else
		{
			$this->db->set($data);
			$this->db->insert('{PRE}'.$this->_table_name);
			$id = $this->db->insert_id();
			return $id;
		}
	}

	public function update($data, $where = array())
	{
		if (!$where)
			return false;

		$this->db->set($data);
		$this->db->where($where);
		return $this->db->update('{PRE}'.$this->_table_name);
	}

	public function get($id = NULL, $single = FALSE)
	{
		if (!empty($id))
		{
			$filter = $this->_primary_filter;
			$id = $filter($id);
			$this->db->where($this->_primary_key, $id);
			$method = 'row';
		}
		elseif ($single == TRUE) 
		{
			$method = 'row';
		}
		else
		{
			$method = 'result';
		}
		
		if ($this->_order_by_type)
			$this->db->order_by($this->_order_by, $this->_order_by_type);
		else
			$this->db->order_by($this->_order_by);

		return $this->db->get('{PRE}'.$this->_table_name)->$method();
	}

	public function get_by($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = NULL)
	{
		if ($select != NULL)
			$this->db->select($select);

		if ($where)
			$this->db->where($where);

		if ($limit)
			$this->db->limit($limit);
		if ($offset)
			$this->db->offset($offset);

		return $this->get(NULL,$single);
	}

	public function delete($id)
	{
		$filter = $this->_primary_filter;
		$id = $filter($id);

		if (!$id)
			return FALSE;

		$this->db->where($this->_primary_key, $id);
		return $this->db->delete('{PRE}'.$this->_table_name);
	}

	public function delete_by($where = NULL)
	{
		if ($where)
			$this->db->where($where);

		return $this->db->delete('{PRE}'.$this->_table_name);
	}

	// public function count($where = NULL)
	// {
	// 	if (!empty($this->_post_type))
	// 	{
	// 		$where['post_type'] = $this->_post_type;
	// 	}

	// 	if ($where)
	// 	{
	// 		$this->db->where($where);
	// 	}

	// 	$this->db->from('{PRE}'.$this->_table_name);
	// 	return $this->db->count_all_results();
	// }

	public function count_post($where, $select)
	{
		$this->db->select($select);
		$this->db->where($where);
		$query = $this->db->get('{PRE}'.$this->_table_name);
		return $query->num_rows();
	}
}