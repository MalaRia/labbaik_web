<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend_controller extends MY_Controller
{
	function __construct()
	{
        parent::__construct();

        $this->uri_1 = $this->uri->segment(1);
        $this->uri_2 = $this->uri->segment(2);
        $this->uri_3 = $this->uri->segment(3);

        if (!is_backend_privilege())
        { // if not has privilige
            redirect($this->uri_1.'/auth/login');
        }

        // $this->load->helper(array());
        $this->load->library(array('form_validation'));

        $this->template->location = $this->sConfig->_template['backend_folder'];
		$this->template->template_name = $this->sConfig->_template['backend_template_name'];
        $this->data['page_title'] = 'CMS AMP';

        $this->data['roles'] = array('subscriber','user','editor','administrator');

        /* generate csrf token */
        $this->data['csrf_token'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );

        // filemanager token
        $this->filemanager = array(
            'ckeditor' => base_url('templates/assets/filemanager/dialog.php?type=1&editor=ckeditor&relative_url=0&fldr=images&akey=masnat'),
            'standalone' => base_url('templates/assets/filemanager/dialog.php?type=2&field_id=fieldID4&fldr=&relative_url=1&akey=masnat')
        );

        $this->data['user_data'] = $this->user_model->get_by(array('id' => $this->session->userdata('id')), NULL, NULL, TRUE, 'id, username, role, avatar, created_at');
	}
}