<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends MY_Model
{
	protected $_table_name = 'categories';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'name' => array(
				'field' => 'name',
				'label' => 'Category Name',
				'rules' => 'trim|required',
			),
			'choose_action' => array(
				'field' => 'choose_action',
				'label' => 'Action',
				'rules' => 'trim|required|in_list[create]',
				// 'errors' => array(
				// 	'in_list' => 'This %s is not valid!'
				// ),
			)
		),
		'update' => array(
			'choose_action' => array(
				'field' => 'choose_action',
				'label' => 'Action',
				'rules' => 'trim|required|in_list[update]',
				// 'errors' => array(
				// 	'in_list' => 'This %s is not valid!'
				// ),
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	public function having_posts($where = NULL, $single = TRUE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		if ($select)
			$this->db->select($select);
		
		if ($where)
			$this->db->where($where);
		// Category
		$this->db->join('post_category as pc','amp_cat.id = pc.category_id', 'LEFT');
		$this->db->join('categories as cat1','cat.parent_id = cat1.id', 'LEFT');
		$this->db->group_by('amp_cat.slug');
		$this->db->order_by($this->_order_by, $this->_order_by_type);

		return $this->db->get($this->_table_name. ' as amp_cat')->result();
	}

	public function have_posts($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = NULL)
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		if ($select)
			$this->db->select($select);
		
		if ($where)
			$this->db->where($where);
		if ($limit)
			$this->db->limit($limit);
		if ($offset)
			$this->db->offset($offset);
			
		// Category
		$this->db->join('post_category as pc','amp_cat.id = pc.category_id');
		$this->db->join('posts as p','p.id = pc.post_id');
		$this->db->order_by('p.'.$this->_order_by, $this->_order_by_type);

		return $this->db->get($this->_table_name. ' as amp_cat')->$method();
	}

	public function where_in($category_id, $select)
	{
		$this->db->select($select);
		$this->db->where_in('id', $category_id);
		$query = $this->db->get('{PRE}'.$this->_table_name);
		return $query->result();
	}
}