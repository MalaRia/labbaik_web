<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_model extends MY_Model
{
	protected $_table_name = 'pages';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'title' => array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'trim|required|is_unique[pages.title]',
			),
			'status' => array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|in_list[draft,schedule,publish]',
			)
		),
		'update' => array(
			'title' => array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'trim|required',
			),
			'status' => array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|in_list[draft,schedule,publish]',
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	// public function page_status($where = array(), $single = FALSE, $select = '')
	// {
	// 	if ($single)
	// 		$method = 'row';
	// 	else
	// 		$method = 'result';
	// 	$this->db->select($select);
	// 	$this->db->where($where);
	// 	$this->db->join('{PRE}status','status.id = {PRE}'.$this->_table_name.'.status');
	// 	return $this->db->get($this->_table_name)->$method();
	// } 

	public function where_like($where='', $single=TRUE, $select='')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';
		$this->db->select($select);
		$this->db->like($where);
		return $this->db->get($this->_table_name)->$method();
	}

	public function where_in($post_id, $select)
	{
		$this->db->select($select);
		$this->db->where_in('id', $post_id);
		$query = $this->db->get($this->_table_name);
		return $query->result();
	}

	function get_order($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = NULL, $order = NULL)
	{
		$this->_order_by = $order['order_by'];
		$this->_order_by_type = $order['order_by_type'];
		return $this->get_by($where, $limit, $offset, $single, $select);
	}
}