<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends MY_Model
{
	protected $_table_name = 'contacts';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'full_name' => array(
				'field' => 'full_name',
				'label' => 'Name',
				'rules' => 'trim|required',
			),
			'email' => array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|valid_email',
			),
			'description' => array(
				'field' => 'description',
				'label' => 'Message',
				'rules' => 'trim|required',
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}
}