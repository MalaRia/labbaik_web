<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_model extends MY_Model
{
	protected $_table_name = 'tags';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'name' => array(
				'field' => 'name',
				'label' => 'Tag Name',
				'rules' => 'trim|required|is_unique[amp_tags.name]',
			),
			'choose_action' => array(
				'field' => 'choose_action',
				'label' => 'Action',
				'rules' => 'trim|required|in_list[create]',
				// 'errors' => array(
				// 	'in_list' => 'This %s is not valid!'
				// ),
			)
		),
		'update' => array(
			'choose_action' => array(
				'field' => 'choose_action',
				'label' => 'Action',
				'rules' => 'trim|required|in_list[update]',
				// 'errors' => array(
				// 	'in_list' => 'This %s is not valid!'
				// ),
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	public function having_posts($where = NULL, $single = TRUE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		if ($select)
			$this->db->select($select);
		
		if ($where)
			$this->db->where($where);
		// Category
		$this->db->join('post_tag as pt','amp_tag.id = pt.tag_id', 'LEFT');
		$this->db->group_by('amp_tag.slug');
		$this->db->order_by($this->_order_by, $this->_order_by_type);

		return $this->db->get($this->_table_name. ' as amp_tag')->result();
	}

	public function where_in($tag_id, $select)
	{
		$this->db->select($select);
		$this->db->where_in('id', $tag_id);
		$query = $this->db->get('{PRE}'.$this->_table_name);
		return $query->result();
	}
}