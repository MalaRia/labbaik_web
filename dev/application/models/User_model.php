<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends MY_Model
{
	protected $_table_name = 'users';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'username' => array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'trim|required|min_length[4]|is_unique[users.username]',
			),
			'email' => array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|valid_email|is_unique[users.email]',
			),
			'first_name' => array(
				'field' => 'first_name',
				'label' => 'First name',
				'rules' => 'trim|required',
			),
			'last_name' => array(
				'field' => 'last_name',
				'label' => 'Last name',
				'rules' => 'trim|required',
			),
			'role' => array(
				'field' => 'role',
				'label' => 'Role',
				'rules' => 'trim|required|in_list[user,subscriber,editor,administrator]',
			),
			'password' => array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'trim|required|min_length[6]',
			),
			'password_confirmation' => array(
				'field' => 'password_confirmation',
				'label' => 'Password Confirmation',
				'rules' => 'trim|required|matches[password]',
			),
			'facebook' => array(
				'field' => 'facebook',
				'label' => 'Facebook',
				'rules' => 'trim',
			),
			'twitter' => array(
				'field' => 'twitter',
				'label' => 'Twitter',
				'rules' => 'trim',
			),
			'gplus' => array(
				'field' => 'gplus',
				'label' => 'Google Plus',
				'rules' => 'trim',
			),
			'instagram' => array(
				'field' => 'instagram',
				'label' => 'Instagram',
				'rules' => 'trim',
			),
			'linkedin' => array(
				'field' => 'linkedin',
				'label' => 'Linkedin',
				'rules' => 'trim',
			),
			'description' => array(
				'field' => 'description',
				'label' => 'Biographical Info',
				'rules' => 'trim',
			),
		),
		'create_contact' => array(
			'email' => array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|valid_email',
			),
			'first_name' => array(
				'field' => 'first_name',
				'label' => 'First name',
				'rules' => 'trim|required',
			)
		),
		'update' => array(
			'first_name' => array(
				'field' => 'first_name',
				'label' => 'First name',
				'rules' => 'trim|required',
			),
			'last_name' => array(
				'field' => 'last_name',
				'label' => 'Last name',
				'rules' => 'trim|required',
			),
			'role' => array(
				'field' => 'role',
				'label' => 'Role',
				'rules' => 'trim|required|in_list[user,subscriber,editor,administrator]',
			),
		),
		'password_change' => array(
			'password' => array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'trim|required|min_length[6]',
			),
			'password_confirmation' => array(
				'field' => 'password_confirmation',
				'label' => 'Password Confirmation',
				'rules' => 'trim|required|matches[password]',
			)
		),
		'login' => array(
			'username' => array(
				'field' => 'username',
				'label' => 'Username',
				'rules' => 'trim|required',
			),
			'password' => array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'trim|required',
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}
}