<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_tag_model extends MY_Model
{
	protected $_table_name = 'post_tag';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array();

	function __construct()
	{
		parent::__construct();
	}
}