<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Widget_model extends MY_Model
{
	protected $_table_name = 'widget';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'ads' => array(
            'create' => array(
				'type' => array(
					'field' => 'type',
					'label' => 'Type',
					'rules' => 'trim|required|in_list[embedded-script,non-embedded-script]',
				),
				'position' => array(
					'field' => 'position',
					'label' => 'Position',
					'rules' => 'trim|required|in_list[long-top,long-bottom,long-left-side,long-right-side,short-left-side,short-right-side,short-post,long-post]',
				),
				'choose_action' => array(
					'field' => 'choose_action',
					'label' => 'Choose Action',
					'rules' => 'trim|required|in_list[create]',
				)
			),
			'embedded_script' => array(
				'field' => 'embedded_script',
				'label' => 'Embedded Script',
				'rules' => 'trim|required'
			),
			'link' => array(
				'field' => 'link',
				'label' => 'Link',
				'rules' => 'trim|required'
			)
        ),
		'featured_post' => array(
			'create' => array(
				'name' => array(
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required',
				),
				'name_id' => array(
					'field' => 'name_id',
					'label' => 'Name ID',
					'rules' => 'trim|required',
				),
				'choose_action' => array(
					'field' => 'choose_action',
					'label' => 'Choose Action',
					'rules' => 'trim|required|in_list[create]',
				)
			),
			'update' => array(
				'name' => array(
					'field' => 'name',
					'label' => 'Name',
					'rules' => 'trim|required',
				),
				'name_id' => array(
					'field' => 'name_id',
					'label' => 'Name ID',
					'rules' => 'trim|required',
				),
				'choose_action' => array(
					'field' => 'choose_action',
					'label' => 'Choose Action',
					'rules' => 'trim|required|in_list[update]',
				)
			)
		),
		'slider' => array(
			'create' => array(
				'title' => array(
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required',
				),
				'order' => array(
					'field' => 'order',
					'label' => 'Order',
					'rules' => 'trim|required',
				),
				'choose_action' => array(
					'field' => 'choose_action',
					'label' => 'Choose Action',
					'rules' => 'trim|required|in_list[create]',
				)
			),
			'update' => array(
				'title' => array(
					'field' => 'title',
					'label' => 'Title',
					'rules' => 'trim|required',
				),
				'order' => array(
					'field' => 'order',
					'label' => 'Order',
					'rules' => 'trim|required',
				),
				'choose_action' => array(
					'field' => 'choose_action',
					'label' => 'Choose Action',
					'rules' => 'trim|required|in_list[update]',
				)
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	public function having_posts($where = NULL, $single = TRUE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		if ($select)
			$this->db->select($select);
		
		if ($where)
			$this->db->where($where);
		// Category
		$this->db->join('post_category as pc','amp_cat.id = pc.category_id', 'LEFT');
		$this->db->join('categories as cat1','cat.parent_id = cat1.id', 'LEFT');
		$this->db->group_by('amp_cat.slug');
		$this->db->order_by($this->_order_by, $this->_order_by_type);

		return $this->db->get($this->_table_name. ' as amp_cat')->result();
	}
}