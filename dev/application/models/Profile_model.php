<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile_model extends MY_Model
{
	protected $_table_name = 'profile';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'web_name' => array(
			'field' => 'web_name',
			'label' => 'Web Name',
			'rules' => 'trim|required',
		),
		'web_email' => array(
			'field' => 'web_email',
			'label' => 'Web Email',
			'rules' => 'trim|required',
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	public function web_info($select = NULL)
	{
		$this->db->select($select);
		return $this->db->get('{PRE}'.$this->_table_name)->row();
	}
}