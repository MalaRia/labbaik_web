<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment_model extends MY_Model
{
	protected $_table_name = 'comments';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'comment' => array(
				'field' => 'comment',
				'label' => 'Comment',
				'rules' => 'trim|required',
			),
			'name' => array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'trim|required',
			),
			'email' => array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|valid_email',
			),
			'url' => array(
				'field' => 'url',
				'label' => 'URL',
				'rules' => 'trim',
			),
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	public function get_comment_post($where = array(), $single = FALSE, $select)
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('contents', 'contents.id = comments.content_id');
		return $this->db->get($this->_table_name)->$method();
	}

	public function comment_by_post_id($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('post_comment as pc', 'pc.comment_id = amp_co.id');
		$this->db->join('amp_posts as po', 'po.id = pc.post_id');
		return $this->db->get($this->_table_name. ' as amp_co')->$method();
	}

	public function in_post($where = array(), $single = FALSE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		$this->db->select($select);
		$this->db->where($where);
		$this->db->join('post_comment as pc', 'pc.comment_id = amp_co.id');
		$this->db->join('amp_posts as po', 'po.id = pc.post_id');
		return $this->db->get($this->_table_name. ' as amp_co')->$method();
	}
}