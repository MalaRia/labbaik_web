<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_category_model extends MY_Model
{
	protected $_table_name = 'post_category';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		// 'create' => array(
		// 	'name' => array(
		// 		'field' => 'name',
		// 		'label' => 'Category Name',
		// 		'rules' => 'trim|required',
		// 	),
		// 	'choose_action' => array(
		// 		'field' => 'choose_action',
		// 		'label' => 'Action',
		// 		'rules' => 'trim|required|in_list[create]',
		// 		// 'errors' => array(
		// 		// 	'in_list' => 'This %s is not valid!'
		// 		// ),
		// 	)
		// ),
		// 'update' => array(
		// 	'choose_action' => array(
		// 		'field' => 'choose_action',
		// 		'label' => 'Action',
		// 		'rules' => 'trim|required|in_list[update]',
		// 		// 'errors' => array(
		// 		// 	'in_list' => 'This %s is not valid!'
		// 		// ),
		// 	)
		// )
	);

	function __construct()
	{
		parent::__construct();
	}
}