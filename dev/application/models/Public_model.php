<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Public_model extends MY_Model
{
	protected $_table_name = 'user';
	protected $_order_by = 'ID';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'ID';
}