<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends MY_Model
{
	protected $_table_name = 'settings';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
        'social_media' => array(
            'facebook' => array(
                'field' => 'facebook',
                'label' => 'Facebook',
                'rules' => 'trim|valid_url',
            ),
            'twitter' => array(
                'field' => 'twitter',
                'label' => 'Twitter',
                'rules' => 'trim|valid_url',
            ),
            'gplus' => array(
                'field' => 'gplus',
                'label' => 'Google+',
                'rules' => 'trim|valid_url',
            ),
            'instagram' => array(
                'field' => 'instagram',
                'label' => 'Instagram',
                'rules' => 'trim|valid_url',
            ),
            'linkedin' => array(
                'field' => 'linkedin',
                'label' => 'LinkedIn',
                'rules' => 'trim|valid_url',
            )
        ),
        'seo' => array(
            'ga_analytics' => array(
                'field' => 'ga_analytics',
                'label' => 'Google Analytics',
                'rules' => 'trim',
            ),
            'keywords' => array(
                'field' => 'keywords',
                'label' => 'Keywords',
                'rules' => 'trim',
            ),
            'rss' => array(
                'field' => 'rss',
                'label' => 'RSS',
                'rules' => 'trim|valid_url',
            ),
            'sitemap' => array(
                'field' => 'sitemap',
                'label' => 'SITEMAP',
                'rules' => 'trim|valid_url',
            )
        ),
        'web' => array(
            'site_title' => array(
                'field' => 'site_title',
                'label' => 'Site Title',
                'rules' => 'trim|required',
            ),
            'site_url' => array(
                'field' => 'site_url',
                'label' => 'Site Url',
                'rules' => 'trim|required|valid_url',
            ),
            'site_description' => array(
                'field' => 'site_description',
                'label' => 'Site Description',
                'rules' => 'trim',
            ),
            'site_email' => array(
                'field' => 'site_email',
                'label' => 'Site Email',
                'rules' => 'trim|valid_email',
            ),
            'site_telephone' => array(
                'field' => 'site_telephone',
                'label' => 'Site Telephone',
                'rules' => 'trim|integer',
            ),
            'site_address' => array(
                'field' => 'site_address',
                'label' => 'Site Address',
                'rules' => 'trim',
            ),
            'site_pagination' => array(
                'field' => 'site_pagination',
                'label' => 'Site Pagination',
                'rules' => 'trim|integer',
            )
        ),
        'theme' => array(
            'template' => array(
                'field' => 'template',
                'label' => 'Theme',
                'rules' => 'trim|required',
            )
        )
    );

	function __construct()
	{
		parent::__construct();
	}
}