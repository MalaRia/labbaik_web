<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post_model extends MY_Model
{
	protected $_table_name = 'posts';
	protected $_order_by = 'id';
	protected $_order_by_type = 'DESC';
	protected $_primary_key = 'id';
	public $rules = array(
		'create' => array(
			'title' => array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'trim|required|is_unique[posts.title]',
			),
			'page' => array(
				'field' => 'page',
				'label' => 'Page',
				'rules' => 'trim|required',
			),
			'status' => array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|in_list[draft,schedule,publish]',
			)
		),
		'update' => array(
			'title' => array(
				'field' => 'title',
				'label' => 'Title',
				'rules' => 'trim|required',
			),
			'page' => array(
				'field' => 'page',
				'label' => 'Page',
				'rules' => 'trim|required',
			),
			'status' => array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|in_list[draft,schedule,publish]',
			)
		)
	);

	function __construct()
	{
		parent::__construct();
	}

	// public function post_status($where = array(), $single = FALSE, $select = '')
	// {
	// 	if ($single)
	// 		$method = 'row';
	// 	else
	// 		$method = 'result';
	// 	$this->db->select($select);
	// 	$this->db->where($where);
	// 	$this->db->join('{PRE}status','status.id = {PRE}'.$this->_table_name.'.status');
	// 	return $this->db->get($this->_table_name)->$method();
	// }

	// public function popular_post($where)
	// {
	// 	// SELECT count(hd_visitors.id) as visitors, hd_contents.title FROM `hd_visitors` join hd_contents on hd_contents.id = hd_visitors.content_id where hd_contents.type = 2 and hd_contents.parent_id = 2 GROUP by title order by visitors DESC limit 5
	// 	$this->db->select('count(hd_visitors.id) as visitors, hd_contents.title');
	// 	$this->db->from('hd_contents');
	// 	$this->db->join('hd_visitors', 'hd_visitors.content_id = hd_contents.id');
	// 	$this->db->where('hd_contents.type', 2);
	// 	$this->db->where($where);
	// 	$this->db->group_by('title');
	// 	$this->db->order_by('visitors', 'DESC');
	// 	$this->db->limit(5);
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }
	public function where_like($where='', $single=TRUE, $select='')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';
		$this->db->select($select);
		$this->db->like($where);
		return $this->db->get($this->_table_name)->$method();
	}

	public function where_in($post_id, $select)
	{
		$this->db->select($select);
		$this->db->where_in('id', $post_id);
		$query = $this->db->get($this->_table_name);
		return $query->result();
	}

	public function get_comments($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		if ($select)
			$this->db->select($select);
		
		if ($where)
			$this->db->where($where);

		if ($limit)
			$this->db->limit($limit);
		if ($offset)
			$this->db->offset($offset);
			
		$this->db->join('post_comment as pc','amp_post.id = pc.post_id', 'LEFT');
		$this->db->join('comments as c','pc.comment_id = c.id', 'LEFT');
		// $this->db->group_by('amp_tag.slug');
		// $this->db->order_by($this->_order_by, $this->_order_by_type);

		return $this->db->get($this->_table_name. ' as amp_post')->$method();
	}

	public function get_by_user($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = '')
	{
		if ($single)
			$method = 'row';
		else
			$method = 'result';

		if ($select)
			$this->db->select($select);
		
		if ($where)
			$this->db->where($where);
		
		if ($limit)
			$this->db->limit($limit);
		if ($offset)
			$this->db->offset($offset);
		
		$this->db->join('users as u','u.id = amp_post.created_by');
		$this->db->join('pages as p','p.id = amp_post.page_id');

		return $this->db->get($this->_table_name. ' as amp_post')->$method();
	}

	public function get_search($query = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = NULL) {
		if ($single)
			$method = 'row';
		else
			$method = 'result';
		$this->db->select($select);
		// $this->db->where("title LIKE '%$query%' OR content LIKE '%$query%'");
		$this->db->like("title", $query);
		$this->db->or_like("content", $query);
		if ($limit)
			$this->db->limit($limit);
		if ($offset)
			$this->db->offset($offset);
		return $this->db->get($this->_table_name)->$method();
	}

	public function get_cat($where = NULL, $limit = NULL, $offset = NULL, $single = TRUE, $select = NULL) {
		if ($single)
			$method = 'row';
		else
			$method = 'result';
		$this->db->select($select);
		$this->db->join("post_category as pc", 'pc.post_id = amp_post.id');
		$this->db->join("categories as c", 'c.id = pc.category_id');
		if ($where)
			$this->db->where($where);
		if ($limit)
			$this->db->limit($limit);
		if ($offset)
			$this->db->offset($offset);
		return $this->db->get($this->_table_name . ' as amp_post')->$method();
	}
} 