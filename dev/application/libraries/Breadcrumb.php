<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
* Breadcrumb for the application
*/
class Breadcrumb
{
	private $_breadcrumb = array();
	public $_include_home = 'Home';
	public $_container_open = '<div class="breadcrumb">';
	public $_container_close = '</div>';
	public $_divider = '&nbsp;&#8250;&nbsp;';
	public $_crumb_open = '';
	public $_crumb_close = '';

	function __construct()
	{
		$CI =& get_instance();
		$CI->load->helper(array('url','template_helper'));
	}

	/**
	* Add breadcrumb data
	* @param $title The title of the breadcrumb
	* @param $href The link of the breadcrumb
	* @param $segment The bolean segment of the breadcrumb
	* @return void The breadcrumb array
	*/
	public function add($title = NULL, $href = '', $segment = FALSE)
	{
		// if the $title is null, it won't do anything
		if (is_null($title)) return;

		// if has href
		if (isset($href) && strlen($href) > 0)
		{
			// if $segment is not false then add with previous crumb
			if ($segment)
			{
				$count_breadcrumb = count($this->_breadcrumb);
				if ($count_breadcrumb > 0) 
				{
					$previous = $this->_breadcrumb[$count_breadcrumb - 1]['href'];
					$href = $previous . '/' . $href;
				}
			}
			// elseif the $href is not absolute path
			elseif (!filter_var($href, FILTER_VALIDATE_URL)) 
			{
				$href = set_url($href);
			}
		}

		// add crumb to the end of the breadcrumb
		$this->_breadcrumb[] = array(
			'title'	=>	$title,
			'href'	=>	$href
		);
	}

	/**
	* Output breadcrumb data
	* @return $output String
	*/
	public function output()
	{
		$output = $this->_container_open;

		// set the homepage
		$output .= anchor(rtrim(site_url(),'/'), $this->_include_home);

		$count_breadcrumb = count($this->_breadcrumb);
		if ($count_breadcrumb > 0)
		{
			// set homepage divider
			$output .= $this->_divider;

			// fetch the breadcrumb
			foreach ($this->_breadcrumb as $key => $crumb)
			{
				$output .= $this->_crumb_open;
				$length_crumb = strlen($crumb['href']);
				if ($length_crumb > 0)
				{
					$output .= anchor($crumb['href'], $crumb['title']);
				}
				else
				{
					$output .= $crumb['title'];
				}
				$output .= $this->_crumb_close;

				// add divider if not the last crumb
				if ($key < $count_breadcrumb - 1)
				{
					$output .= $this->_divider;
				}
			}
		}
		// close the container's tag
		$output .= $this->_container_close;
		return $output;
	}
}