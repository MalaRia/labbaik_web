<?php

class Template 
{
    public $folder_controller;
    public $view_folder;
    public $location;
    public $template_name = 'default';

    public function __construct()
    {
        $this->CI =& get_instance();
        $this->CI->load->helper('url');

        $view_folder = explode('/', VIEWPATH);
        end($view_folder); // get last array ""
        $this->view_folder = prev($view_folder); // get folder before last
    }

    public function view($file = NULL, $data = NULL, $plain = false)
    {
        if ($plain)
            return $this->CI->load->view($this->location . '/' . $this->template_name . '/' . $file, $data, $plain);
        else
            $this->CI->load->view($this->location . '/' . $this->template_name . '/' . $file, $data);
    }

    public function assets($file)
    {
        // var_dump($this->view_folder);
        // return base_url($this->view_folder . '/' . $this->location . '/' . $this->template_name . '/assets/' . $file);
        return base_url($this->view_folder . '/' . $this->location . '/' . $this->template_name . '/assets/' . $file);
        // echo base_url($this->view_folder . '/' . $this->location . '/' . $this->template_name);
    }

    public function link($segment)
    {
        return site_url($this->folder_controller . '/' . $segment);
    }
}