<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
*	Upload_file Class
*
*	This Class to handling upload image file
*/
class Upload_file {

	public function __construct()
	{
		$this->CI =& get_instance();
		
		date_default_timezone_set('Asia/Jakarta');
		$this->current_date = mdate('%Y-%m-%d %H:%i:%s');

		// $this->images_path = './assets/uploads/images/'; // images folder
		// $this->thumbs_path = './assets/uploads/thumbs/'; // thumbnails folder
		$this->images_path = './assets/uploads/'; // images folder
		$this->thumbs_path = './assets/uploads/'; // thumbnails folder
		$this->files_path = './assets/uploads/files/'; // files folder
		$this->allowed_types = 'gif|jpg|jpeg|png|bmp'; // image types
		$this->crop = TRUE; // crop true
		$this->max_size = 1024; // max size 1Mb
		$this->file_name = sha1($this->current_date);
		$this->post_name = 'upload_file'; // name of the field form

		// resize for image
		$this->create_thumb = TRUE;
		$this->image_library = 'gd2';
		$this->maintain_ratio = TRUE;
		$this->width = 400;
		$this->height = 400;
		$this->quality = '100%';

		// crop  for image
		$this->x_axis = 0;
		$this->y_axis = 0;
	}

	/**
	* Upload Image
	* 
	* @param $info information of file
	*
	* post_name the name of the field form file
	* file_name rename file when upload
	* file_type type of the file (image and file)
	* max_size maximum size when upload file
	* is_overwrite is boolean
	* allowed_types is type of file when upload
	* is_csrf_token is boolean
	* @return $data array 
	*/
	public function _do_upload($info = array())
	{
		// var_dump($info);
		// return;
		$post_name = empty($info['post_name']) ? $this->post_name : $info['post_name']; // The name of input file
		$file_name = $this->file_name; // The name of image file
		$upload_path = (!empty($info['file_type']) && $info['file_type'] == 'file') ? $this->files_path : $this->images_path; // upload path
		$max_size = empty($info['max_size']) ? $this->max_size : $info['max_size']; //  maximize file
		$allowed_types = empty($info['allowed_types']) ? $this->allowed_types : $info['allowed_types']; // types of file
		$is_csrf_token = empty($info['is_csrf_token']) ? FALSE : TRUE; // boolean csrf token

		// set config file
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] = $allowed_types;
		$config['max_size'] = $max_size;
		$config['file_name'] = $file_name;
		$config['is_csrf_token'] = $is_csrf_token;

		// load upload library
		$this->CI->load->library('upload');
		$this->CI->upload->initialize($config);
		if (!$this->CI->upload->do_upload($post_name)) {
			$data = array(
				'error' => true,
				'content' => $this->CI->upload->display_errors('','!')
			);
		} else { // if image has been upload
			$file_data = $this->CI->upload->data();
			$content = array('file' => $file_data['file_name']);

			// IF IMAGE 
			if ($info['file_type'] == 'image') {
				$content['image'] = $file_data['file_name'];
				// IF RESIZE
				if (isset($info['resize']) && $info['resize'] == TRUE) {
					$info['file_name'] = $file_data['file_name'];
					$this->_do_resize($info);
					$content['thumb'] = $file_data['raw_name'].'_thumb'.$file_data['file_ext'];
				} else {
					// IF IMAGE CROP
					$info['file_name'] = $file_data['file_name'];
					$this->_do_crop($info);

					$content['thumb'] = $file_data['raw_name'].'_thumb'.$file_data['file_ext'];
				}
			}

			// SET DATA
			$data = array(
				'error' => false,
				// 'type' => $file_data['file_ext'],
				// 'file_name' => $file_data,
				// 'real_path' => $upload_path,
				'content' => $content
			);
		}

		if ($this->CI->input->is_ajax_request()) { // ajax request

			if ($is_csrf_token) { // set csrf token
				$data['csrf'] = array(
					'name' => $this->CI->security->get_csrf_token_name(),
					'hash' => $this->CI->security->get_csrf_hash()
				);
			}
			// json encode the image data array
			echo json_encode($data);
			return true;

		} else {
			return $data;
		}
	}

	/**
	* Crop image after upload the image
	* 
	* @param $info  array information of image
	*
	* file_name is the file of the name
	* width is the width of the name when crop
	* height is the height of the name when crop
	* x_axis is the x_axis of the name when crop
	* y_axis is the y_axis of the name when crop
	*/
	public function _do_crop($info = array())
	{
		$file_name = empty($info['file_name']) ? $this->file_name : $info['file_name'];
		$width = empty($info['width']) ? $this->width : $info['width']; // crop width
		$height = empty($info['height']) ? $this->height : $info['height']; // crop height
		$x_axis = empty($info['x_axis']) ? $this->x_axis : $info['x_axis']; //  crop x start
		$y_axis = empty($info['y_axis']) ? $this->y_axis : $info['y_axis']; // crop y start


		$config['image_library'] = $this->image_library;
		$config['source_image'] = $this->images_path . $file_name;
		$config['new_image'] = $this->thumbs_path;
		$config['create_thumb'] = $this->create_thumb;
		$config['quality'] = $this->quality.'%';
		$config['width'] = floor($width);
		$config['height'] = floor($height);
		$config['x_axis'] = floor($x_axis);
		$config['y_axis'] = floor($y_axis);


		$this->CI->load->library('image_lib', $config);
		$this->CI->image_lib->crop();

		// Clear image library settings so we can do some more image manipulations if we have to
		$this->CI->image_lib->clear();
		unset($config);
	}

	/**
	* Resize image after upload the image
	* 
	* @param $info  array information of image
	*
	* file_name is the file of the name
	* width is the width of the name when resize
	* height is the height of the name when resize
	*
	*/
	public function _do_resize($info = array())
	{
		$file_name = empty($info['file_name']) ? $this->file_name : $info['file_name']; // file name
		$width = empty($info['width']) ? $this->width : $info['width']; // resize width
		$height = empty($info['height']) ? $this->height : $info['height']; // resize height

		$config['image_library'] = $this->image_library;
		$config['source_image'] = $this->images_path . $file_name;
		$config['new_image'] = $this->thumbs_path;
		$config['create_thumb'] = $this->create_thumb;
		$config['maintain_ratio'] = $this->maintain_ratio;
		$config['width'] = floor($width);
		$config['height'] = floor($height);
		$config['quality'] = $this->quality.'%';

		$this->CI->load->library('image_lib', $config);
		$this->CI->image_lib->resize();

		// Clear image library settings so we can do some more image manipulations if we have to
		$this->CI->image_lib->clear();
		unset($config);
	}

	/**
	* Remove File
	* 
	* @param $info an associative array. $info[file_type], $info[file_name], $info[file_thumb]
	*
	* file_type is the type of the file, The value is (image and file)
	* file_name is the name of file
	* file_thumb is the name of thumbnail (image only)
	*
	* @return $data array
	*/
	public function _do_remove($info = array())
	{
		$file_path = (!empty($info['file_type']) && $info['file_type'] == 'file') ? $this->files_path : $this->images_path; // file path
		
		$filename = $file_path . $info['file_name'];

		// SET Default Message
		$data = array(
			'error' => true,
			'content' => 'Gagal Dihapus!'
		);

		if (file_exists($file_path . $info['file_name'])) {
			unlink($filename); // remove the file

			// if image remove thumbnail
			if ($info['file_type'] == 'image' && file_exists($this->thumbs_path . $info['file_thumb'])) {
				$filethumb = $this->thumbs_path . $info['file_thumb'];
				unlink($filethumb); // remove thumbnail
			}

			// SET Message
			$data = array(
				'error' => false,
				'content' => 'Berhasil Dihapus!'
			);
		}

		return $data;
	}
}
