<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
*	User Class
*
*	This Class to get user information
*/
class User {

	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->model('user_model');

		$this->id = $this->CI->session->userdata('id');
		$this->role = $this->CI->session->userdata('role');
	}

	/*
	*	User Info
	*
	*	@param string $select field name that want to get.
	*	@return user data
	*/
	public function user_info($select = NULL)
	{
		$where = array(
			'id' => $this->id
		);
		return $this->CI->user_model->get_by($where, NULL, NULL, TRUE, $select);
	}

	/*
	*	Check Admin
	*
	*	@return boolean
	*/
	public function is_admin()
	{
		if ($this->role != 1)
		{
			return false;
		}
		return true;
	}

	/*
	*	Check Author
	*
	*	@return boolean
	*/
	public function is_author()
	{
		if ($this->role != 2)
		{
			return false;
		}
		return true;
	}

	/*
	*	Check User
	*
	*	@return boolean
	*/
	public function is_user()
	{
		if ($this->role != 3)
		{
			return false;
		}
		return true;
	}

	/*
	*	Check Subscriber
	*
	*	@return boolean
	*/
	public function is_subscriber()
	{
		if ($this->role != 4)
		{
			return false;
		}
		return true;
	}
}