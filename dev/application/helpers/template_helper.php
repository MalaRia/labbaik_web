<?php
	
	function get_template_directory($path, $dir_file)
	{
		global $SConfig;

		$replace_path = str_replace('\\', '/', $path);
		$get_digit_doc_root = strlen($SConfig->_document_root);
		$full_path = substr($replace_path, $get_digit_doc_root);

		return $SConfig->_site_url.$full_path.'/'.$dir_file;
	}

	function get_template($view)
	{
		$_this =& get_instance();
		$_this->load->library('template');
		return $_this->template->view($view);
	}

	function set_url($sub)
	{
		$_this =& get_instance();
		if ($_this->site->side == 'backend')
		{
			if (strpos($sub, 'auth') > 0)
				return site_url($sub);
			else
				return site_url('admin/'.$sub);
		}
		else
		{
			return site_url($sub);
		}
	}

	function get_list_directory($dir = NULL)
	{
		if ($dir == NULL)
			$dir = VIEWPATH . '/frontend';
			
		if ($handle = opendir($dir)) {
	
			$dir = Array();
		
			/* This is the correct way to loop over the directory. */
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != ".." && $entry != '.htaccess.htaccess' && $entry != '.DS_Store' && $entry != 'robots.php' && $entry != 'rss.php' && $entry != 'sitemap.php') {
				// if (is_dir($entry)) {
					$dir[] = $entry;
				}
			}
		
			closedir($handle);
		}
		return $dir;
	}

	function active_menu($segment2, $segment3 = NULL)
	{
		$_this =& get_instance();
		$class = array(
			'parent' => '',
			'child' => ''
		);
		if ($segment2 == $_this->uri->segment(2) && $segment3 == NULL)
			$class['parent'] = 'active';
		else if ($segment2 == $_this->uri->segment(2) && $segment3 == $_this->uri->segment(3))
			$class['child'] = 'active';
		
		return $class;
	}

	function current_menu($segment_2 = NULL, $segment_3 = NULL, $sub = FALSE)
	{
		$_this =& get_instance();
		
		if ($segment_2 == $_this->uri->segment(2) && $sub == FALSE)
			return 'active';
		if ($segment_2 == $_this->uri->segment(2) && $segment_3 == $_this->uri->segment(3) && $sub == TRUE)
			return 'active';
		
	}

	// function is_active_page($page, $class)
	// {
	// 	$_this =& get_instance();
	// 	if ($_this->site->side == 'backend' && $page == $_this->uri->segment('2'))
	// 	{
	// 		return $class;
	// 	}
	// }

	// function title()
	// {
	// 	$_this =& get_instance();
	// 	global $SConfig;

	// 	$_array_backend_page = array(
	// 		'dashboard' => 'Halaman Dashboard',
	// 		'my-story' => 'Halaman My-Story',
	// 		'blog' => 'Halaman Blog',
	// 		'gallery' => 'Halaman Gallery',
	// 		'schedule' => 'Halaman Schedule',
	// 		'comment' => 'Halaman Komentar',
	// 		'contact' => 'Halaman Kontak',
	// 		'setting' => 'Halaman Setting',
	// 	);

	// 	// $title = 
	// 	if ($_this->site->side == 'backend' && (array_key_exists($_this->uri->segment(2), $_array_backend_page)))
	// 	{
	// 		return $_array_backend_page[$_this->uri->segment(2)];
	// 	}
	// }

	// function current_page()
	// {
	// 	$_this =& get_instance();

	// 	$url = explode('/', current_url());
	// 	$count_slash = count($url);

	// 	if (strpos(current_url(), 'edit') > 1 || strpos(current_url(), 'view') > 1) 
	// 	{
	// 		$count_slash = $count_slash - 2;
	// 	}
	// 	else
	// 	{
	// 		$count_slash = $count_slash - 1;
	// 	}
		
	// 	return ucwords(str_replace('-', ' ', $url[$count_slash]));

	// }

?>