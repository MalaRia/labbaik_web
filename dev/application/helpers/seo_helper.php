<?php
	
	function meta_google_plus($data = array())
	{
		$metatag = array(
			'title' => (isset($data['title'])) ? $data['title'] : '',
			'image' => (isset($data['image'])) ? $data['image'] : '',
			'description' => (isset($data['description'])) ? $data['description'] : '',
		);

		return '<!-- Schema.org markup for Google+ -->
			<meta itemprop="name" content="'.$metatag['title'].'">
			<meta itemprop="description" content="'.$metatag['description'].'">
			<meta itemprop="image" content="'.$metatag['image'].'">';
	}

	function meta_twitter($data = array())
	{
		$metatag = array(
			'title' => (isset($data['title'])) ? $data['title'] : '',
			'image' => (isset($data['image'])) ? $data['image'] : '',
			'description' => (isset($data['description'])) ? $data['description'] : '',
			'author' => (isset($data['author'])) ? $data['author'] : '',
		);
		return '<!-- Twitter Card data -->
			<meta name="twitter:card" content="summary_large_image">
			<meta name="twitter:site" content="@publisher_handle">
			<meta name="twitter:title" content="'.$metatag['title'].'">
			<meta name="twitter:description" content="'.$metatag['description'].'">
			<meta name="twitter:creator" content="@author_handle">
			<!-- Twitter summary card with large image must be at least 280x150px -->
			<meta name="twitter:image:src" content="'.$metatag['image'].'">';
	}

	function meta_facebook($data = array())
	{
		$metatag = array(
			'title' => (isset($data['title'])) ? $data['title'] : '',
			'type' => (isset($data['type'])) ? $data['type'] : '',
			'image' => (isset($data['image'])) ? $data['image'] : '',
			'description' => (isset($data['description'])) ? $data['description'] : '',
			'author' => (isset($data['author'])) ? $data['author'] : '',
			'published_time' => (isset($data['published_time'])) ? $data['published_time'] : '',
			'section' => (isset($data['section'])) ? $data['section'] : '', // sub-page
			'tag' => (isset($data['tag'])) ? $data['tag'] : '', // tag
			'admins' => (isset($data['admins'])) ? $data['admins'] : '',
		);

		return '<!-- Open Graph data -->
			<meta property="og:title" content="'.$metatag['title'].'" />
			<meta property="og:type" content="'.$metatag['type'].'" />
			<meta property="og:url" content="'.current_url().'" />
			<meta property="og:image" content="'.$metatag['image'].'" />
			<meta property="og:description" content="'.$metatag['description'].'" />
			<meta property="article:author" content="'.$metatag['author'].'" />
			<meta property="og:site_name" content="'.site_url().'" />
			<meta property="article:published_time" content="'.$metatag['published_time'].'" />
			<meta property="article:section" content="'.$metatag['section'].'" />
			<meta property="article:tag" content="'.$metatag['tag'].'" />
			<meta property="fb:admins" content="'.$metatag['admins'].'" />';
	}

	function seo($json_seo)
	{
		$decode_seo = json_decode($json_seo);
		$seo = array(
			'title' => (isset($decode_seo[0])) ? $decode_seo[0] : '',
			'content' => (isset($decode_seo[1])) ? $decode_seo[1] : '',
			'keywords' => (isset($decode_seo[2])) ? $decode_seo[2] : '',
		);
		return $seo;
	}

	function profile_seo($json_seo)
	{
		$seo = json_decode($json_seo);
		if (!empty($seo))
		{
			$so['ga_analytics'] = $seo->ga_analytics;
			$so['title'] = $seo->title;
			$so['description'] = $seo->description;
			$so['keywords'] = $seo->keywords;
		}
		else
		{
			$so['ga_analytics'] = '';
			$so['title'] = '';
			$so['description'] = '';
			$so['keywords'] = '';
		}
		return $so;
	}

	function profile_socmed($json_seo)
	{
		$seo = json_decode($json_seo);
		if (!empty($seo))
		{
			$soc['facebook'] = $seo->facebook;
			$soc['twitter'] = $seo->twitter;
			$soc['gplus'] = $seo->gplus;
			$soc['linkedin'] = $seo->linkedin;
			$soc['instagram'] = $seo->instagram;
		}
		else
		{
			$soc['facebook'] = '';
			$soc['twitter'] = '';
			$soc['gplus'] = '';
			$soc['linkedin'] = '';
			$soc['instagram'] = '';
		}
		return $soc;
	}

?>