<?php

	function errors()
	{
		$_this =& get_instance();
		$errors = $_this->session->flashdata('errors');
		if (isset($errors))
		{
			return $errors;
		}
	}

	function set_flash($name = 'errors', $values = '', $form_validation = FALSE, $del_open = '<div class="error">', $del_close = '</div>')
	{
		$_this =& get_instance();
		
		if (!$form_validation) // custom error
		{
			if (is_array($values))
			{
				foreach ($values as $key => $val)
				{
					$_this->session->set_flashdata($key, $del_open.$val.$del_close);	
				}
			}
			else
			{
				$_this->session->set_flashdata($name, $del_open.$values.$del_close);
			}
		}
		else // form_validation error
		{
			$_this->session->set_flashdata($name, $values);
		}
	}

	function get_flash($name = 'errors')
	{
		$_this =& get_instance();
		return $_this->session->flashdata($name);
	}

	function repopulate_post_field($post_field)
	{
		foreach ($post_field as $k => $v) {
            set_flash($k, $v, FALSE, '', '');
        }
	}

	function redirect_back()
	{
		if (isset($_SERVER['HTTP_REFERER']))
			redirect($_SERVER['HTTP_REFERER']);
	}

?>
	