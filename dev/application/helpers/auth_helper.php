<?php 

	/*
    *   Check Backend Privilege
    *
    *   @return boolean
    */
    function is_backend_privilege()
    {
        $_this =& get_instance();
        return $_this->session->userdata('backend_privilege');
    }

	/*Use to generate hash password */
	function generate_hash($password)
	{
		$options = [
            'cost' => 10
        ];

        return password_hash($password, PASSWORD_DEFAULT, $options);
	}

	/*Use to verify hash password */
	function verify_hash($password, $hash)
	{
		return password_verify($password, $hash);
	}

	/*Use to regenerate new hash if cost or algorithm has changed*/
	function regenerate_hash($hash, $password)
	{
		$options = [
            'cost' => 10
        ];

		if (password_needs_rehash($hash, PASSWORD_DEFAULT, $options))
		{
			return password_hash($password, PASSWORD_DEFAULT, $options);
		}
		return false;
	}

	/*Use for login validation*/
	function login_validation()
	{
		return array(
            'username' => array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required',
            ),
            'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
        );
	}

	/*Use for password validation*/
	function password_validation()
	{
		return array(
            'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
            'password_confirmation' => array(
                'field' => 'password_confirmation',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|matches[password]',
            )
        );
	}

	/*Use for register validation*/
	function register_validation()
	{
		return array(
            'username' => array(
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'trim|required|is_unique[hd_users.username]',
            ),
            'email' => array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'trim|required|is_unique[hd_users.email]',
            ),
            'password' => array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|required',
            ),
            'password_confirmation' => array(
                'field' => 'password_confirmation',
                'label' => 'Password Confirmation',
                'rules' => 'trim|required|matches[password]',
            ),
            'gender' => array(
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'trim|required|in_list[0,1]',
            ),
            'role' => array(
                'field' => 'role',
                'label' => 'Role',
                'rules' => 'trim|required|in_list[1,2,3,4]',
            ),
        );
	}

	/*Use for show message*/
	function error_message()
	{
		$_this =& get_instance();
		$_this->form_validation->set_message('required', '%s kosong, harap diisi!'); // required
        $_this->form_validation->set_message('integer', '%s harus berupa angka!'); // integer
        $_this->form_validation->set_message('is_unique', '%s sudah ada!'); // is_unique
        $_this->form_validation->set_message('min_length', '%s minimal harus 3 karakter!'); // min_length
        $_this->form_validation->set_message('max_length', '%s maksimal 100 karakter!'); // max_length
        $_this->form_validation->set_message('in_list', '%s tidak ada di dalam daftar!'); // in_list
        $_this->form_validation->set_message('valid_email', '%s tidak valid!');
	}
?>