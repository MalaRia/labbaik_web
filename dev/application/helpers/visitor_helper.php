<?php
	
	// Get The user Agent
	function user_agent()
	{
		$CI =& get_instance();
		$CI->load->helper('url');


		$data = array(
			'platform' => $CI->agent->platform,
			'browser' => $CI->agent->browser,
			'version' => $CI->agent->version,
			'mobile' => $CI->agent->mobile,
			'robot' => $CI->agent->robot,
			'target' => current_url(),
			'referer' => $CI->agent->referer,
			'is_browser' => $CI->agent->is_browser,
			'is_robot' => $CI->agent->is_robot,
			'is_mobile' => $CI->agent->is_mobile,
			'ip_address' => $_SERVER['REMOTE_ADDR'],
			'created_at' => mdate('%Y-%m-%d %h:%i:%a')
		);

		return $data;
	}

?>