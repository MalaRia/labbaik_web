<?php

	function get_image($images = NULL, $thumb = TRUE)
	{
		$avatar = json_decode($images);
		if ($thumb)
			$avatar = 'thumbs/'.$avatar->thumb;
		else
			$avatar = 'images/'.$avatar->file;

		return site_url('assets/uploads/'.$avatar);
	}

	function set_image($images = NULL, $multiple = FALSE)
	{
		$_this =& get_instance();
		$_this->sConfig = new SConfig;
		$meta_images = array();
		if ($images) 
		{
			$image = json_decode($images);
			if (!$multiple)
			{
				// undefined
				$meta_images = array (
					'fullImage' => base_url('assets/uploads/'.$image->image),
					'bigImage' => $image->image,
					'thumbImage' => $image->thumb,
					// 'titleImage' => ($image->title != undefined) ? $image->title : '',
					// 'altImage' => ($image->alt != undefined) ? $image->alt : ''
				);
			}
			else
			{
				foreach ($image as $img)
				{
					$meta_image = array (
						'fullImage' => base_url('assets/uploads/'.$img->image),
						'bigImage' => $img->image,
						'thumbImage' => $img->thumb,
						// 'titleImage' => ($image->title != undefined) ? $image->title : '',
						// 'altImage' => ($image->alt != undefined) ? $image->alt : ''
					);
					array_push($meta_images, $meta_image);
				}
			}
		}
		else
		{
			$meta_image = array (
				'fullImage' => (get_flash('fullImage')) ? get_flash('fullImage') : base_url('templates/') . $_this->sConfig->_template['backend_folder'].'/'.$_this->sConfig->_template['backend_template_name'].'/dist/img/default.png',
				'bigImage' => (get_flash('image_big')) ? get_flash('image_big') : '',
				'thumbImage' => (get_flash('image_thumb')) ? get_flash('image_thumb') : '',
				// 'titleImage' => (get_flash('image_title')) ? get_flash('image_title') : '',
				// 'altImage' => (get_flash('image_alt')) ? get_flash('image_alt') : ''
			);
			if (!$multiple)
				$meta_images = $meta_image;
			else
				array_push($meta_images, $meta_image);
		}
		return $meta_images;
	}

?>