<?php
	
	function current_datetime()
	{
		return mdate("%Y-%m-%d %H:%i:%s");
	}

	function format_date($date, $format = "Y-m-d")
	{
		$date = new DateTime($date);
		return $date->format($format);
		// return date($format, strtotime($date));
	}

	function indonesian_date($date)
	{
		$date = date("Y-m-d", strtotime($date));
		$month = array (
			1 =>   'Januari',
			'Februari',
			'Maret',
			'April',
			'Mei',
			'Juni',
			'Juli',
			'Agustus',
			'September',
			'Oktober',
			'November',
			'Desember'
		);
		$new_date = explode('-', $date);
		
		// variabel pecahkan 0 = tanggal
		// variabel pecahkan 1 = bulan
		// variabel pecahkan 2 = tahun
	 
		return $new_date[2] . ' ' . $month[ (int)$new_date[1] ] . ' ' . $new_date[0];
	}

	function time_ago($datetime)
	{
		$CI =& get_instance();
		$CI->load->helper('date');
		$post_date = strtotime($datetime);
		$now = time();

		// will echo "2 hours ago" (at the time of this post)
		return timespan($post_date, $now) . ' ago';
	}

?>