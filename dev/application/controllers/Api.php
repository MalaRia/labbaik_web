<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends Api_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('post_model'));
        $this->load->helper(array('text'));

        $this->limit = 5;
        $this->offset = 0;
        $this->page = 1;
    }

    public function index()
    {
        switch ($this->uri_3) {
            case 'articles':
                $this->_articles();
                break;
            case 'popular':
                $this->_popular();
                break;
            default:
                # code...
                break;
        }
    }
    private function _articles()
    {
        // check if there 4 is segment
        if (!empty($this->uri_4)) {
            $this->_article_detail();
            return;
        }
        
        $where = array(
            'amp_post.status' => 'publish',
            'amp_post.feed' => 1,
        );
        $limit = ($this->input->get('limit')) ? $this->input->get('limit') : $this->limit;
        $page = ($this->input->get('page')) ? $this->input->get('page') : $this->page;
        $offset = ($page == 1) ? 0 : $limit * ($page - 1);
        
        $get_data = $this->post_model->get_cat($where, $limit, $offset, FALSE, 'amp_post.id, amp_post.title, amp_post.slug, amp_post.excerpt, amp_post.image, amp_post.feed, amp_post.published_at, c.name as category');
        $count_data = $this->post_model->get_cat($where, NULL, NULL, TRUE, 'count(amp_post.id) as total_baris');

        // var_dump($get_data);
        // return;
        // pageLeft = Math.ceil(resultCount[0].total_semua_baris / limit);
        $pageLeft = floor($count_data->total_baris / $limit);
        $data = array(
            'statusCode' => 200,
            'message' => 'success',
            'data' => array(),
            'limit' => $limit,
            'page' => $page,
            'page_total' => $pageLeft
        );
        foreach ($get_data as $d) {
            // var_dump($d->image);
            $dt = array(
                'id' => $d->id,
                'title' => $d->title,
                'slug' => $d->slug,
                'url' => site_url($d->slug),
                'category' => $d->category,
                'excerpt' => character_limiter(strip_tags($d->excerpt), 50),
                'image' => base_url('assets/uploads/').json_decode($d->image)->image,
                'published_at' => $d->published_at
            );
            array_push($data['data'], $dt);
        }
        // var_dump($data);
        $this->response($data);
    }

    private function _article_detail()
    {
        // check if there 4 is segment
        $where = array(
            'amp_post.id' => $this->uri_4,
            'amp_post.status' => 'publish',
            // 'amp_post.feed' => 1
        );
        
        $get_data = $this->post_model->get_cat($where, NULL, NULL, TRUE, 'amp_post.id, amp_post.title, amp_post.slug, amp_post.content, amp_post.image, amp_post.feed, amp_post.published_at, c.name as category');
        
        $data = array(
            'statusCode' => 200,
            'message' => 'success',
            'data' => []
        );
        if ($get_data) {
            $data['data'] = array(
                'id' => $get_data->id,
                'title' => $get_data->title,
                'slug' => $get_data->slug,
                'url' => site_url($get_data->slug),
                'category' => $get_data->category,
                'content' => $get_data->content,
                'image' => base_url('assets/uploads/').json_decode($get_data->image)->image,
                'published_at' => $get_data->published_at
            );
        }
        $this->response($data);
    }

    private function _popular()
    {
        if (!empty($this->uri_4)) {
            $this->_popular_detail();
            return;
        }
        // check if there 4 is segment
        $where = array(
            'amp_post.status' => 'publish',
            'amp_post.popular' => 1
        );
        $limit = ($this->input->get('limit')) ? $this->input->get('limit') : $this->limit;
        $page = ($this->input->get('page')) ? $this->input->get('page') : $this->page;
        $offset = ($page == 1) ? 0 : $limit * ($page - 1);
        
        $get_data = $this->post_model->get_cat($where, $limit, $offset, FALSE, 'amp_post.id, amp_post.title, amp_post.slug, amp_post.excerpt, amp_post.image, amp_post.feed, amp_post.published_at, c.name as category');
        $count_data = $this->post_model->get_cat($where, NULL, NULL, TRUE, 'count(amp_post.id) as total_baris');

        // var_dump($count_data->total_baris);
        // pageLeft = Math.ceil(resultCount[0].total_semua_baris / limit);
        $pageLeft = floor($count_data->total_baris / $limit);
        $data = array(
            'statusCode' => 200,
            'message' => 'success',
            'data' => array(),
            'limit' => $limit,
            'page' => $page,
            'page_total' => $pageLeft
        );
        foreach ($get_data as $d) {
            // var_dump($d->image);
            $dt = array(
                'id' => $d->id,
                'title' => $d->title,
                'slug' => $d->slug,
                'url' => site_url($d->slug),
                'category' => $d->category,
                'excerpt' => character_limiter(strip_tags($d->excerpt), 50),
                'image' => base_url('assets/uploads/').json_decode($d->image)->image,
                'published_at' => $d->published_at
            );
            array_push($data['data'], $dt);
        }
        // var_dump($data);
        $this->response($data);
    }

    private function _popular_detail()
    {
        // check if there 4 is segment
        $where = array(
            'amp_post.id' => $this->uri_4,
            'amp_post.status' => 'publish',
            'amp_post.popular' => 1
        );
        
        $get_data = $this->post_model->get_cat($where, NULL, NULL, TRUE, 'amp_post.id, amp_post.title, amp_post.slug, amp_post.content, amp_post.image, amp_post.feed, amp_post.published_at, c.name as category');
        
        $data = array(
            'statusCode' => 200,
            'message' => 'success',
            'data' => []
        );
        if ($get_data) {
            $data['data'] = array(
                'id' => $get_data->id,
                'title' => $get_data->title,
                'slug' => $get_data->slug,
                'url' => site_url($get_data->slug),
                'category' => $get_data->category,
                'content' => $get_data->content,
                'image' => base_url('assets/uploads/').json_decode($get_data->image)->image,
                'published_at' => $get_data->published_at
            );
        }
        $this->response($data);
    }
}
