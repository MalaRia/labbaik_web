<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('post_model','page_model','category_model','tag_model','post_category_model','post_tag_model'));
        $this->load->helper(array('email','seo'));
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'feed':
                $this->feed();
                break;
            case 'all-feed':
                $this->all_feed();
                break;
            case 'feed-popular':
                $this->feed_popular();
                break;
            case 'all-popular-feed':
                $this->all_popular_feed();
                break;

            default:
                $this->index();
                break;
        }
    }

    // POSTS
	public function index()
	{
        $this->data['page_title'] = 'Posts';
        $this->breadcrumb->add('Posts');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete'),
                'preview' => site_url('preview/')
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all')
        );

        $this->render('pages/posts');
    }
    
    /*
    * POST: Create
    */
    public function create()
    {
        $post_field = $this->input->post();

        // POST SUBMIT
        if (isset($post_field['submit']))
        {
            // check rules validation
            $rules = $this->post_model->rules['create'];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            if ($this->errors == FALSE) // if not error insert data
            {
                if ($_FILES['upload_file']['tmp_name'] != '') {
                    $this->load->library('upload_file');
                    $config = array(
                        'post_name' => 'upload_file', // name upload field
                        'file_type' => 'image', // type of the file want to upload (image or file)
                        'max_size' => 1000, // maximal size upload
                        'is_csrf_token' => FALSE, // when csrf needed
                        'resize' => TRUE
                    );
    
                    $images = $this->upload_file->_do_upload($config);
                    if ($images['error'] == false) {
                        $this->errors = FALSE;
                    } else {
                        $this->errors = TRUE;
                        $message = $images['content'];
                    }
                }
                // var_dump($images);
                // return;
                // if image upload not error
                if ($this->errors == false) {
                    // Check status
                    $published_at = NULL;
                    if ($post_field['status'] == 'publish')
                        $published_at = $this->current_datetime;
                    elseif ($post_field['status'] == 'schedule')
                        $published_at = format_date($post_field['schedule_date']);

                    // if image exist
                    $image = '';
                    if ($images['content']) {
                        $image = json_encode($images['content']);
                    }

                    // gallery
                    $gallery = NULL;
                    $video = NULL;
                    $type = NULL;
                    if (!empty($post_field['video_gallery'])) {
                        $video = $post_field['video_gallery'];
                        $type = 'video';
                    }

                    // SEO
                    $seo = array(
                        'title' => $post_field['seo_title'],
                        'description' => $post_field['seo_description'],
                        'keywords' => $post_field['seo_keywords'],
                    );

                    // CREATE HERE
                    $page_id = explode('-', $post_field['page']);
                    $temp_data = array(
                        'title' => $post_field['title'],
                        'slug' => url_title($post_field['title'], '-', TRUE),
                        'content' => $post_field['content'],
                        'excerpt' => character_limiter($post_field['content'], 200),
                        'page_id' => $page_id[0],
                        'status' => $post_field['status'],
                        'image' => $image,
                        'seo' => json_encode($seo),
                        'additional' => ($gallery) ? $gallery : $video,
                        'type' => $type,
                        // 'comment' => ($post_field['allow_comments'] == 1) ? 1 : 0,
                        'feed' => ($post_field['api_feed'] == 1) ? 1 : 0,
                        'popular' => ($post_field['api_popular'] == 1) ? 1 : 0,
                        'published_at' => $published_at,
                        'created_at' => $this->current_datetime,
                        'created_by' => $this->session->userdata('id'),
                    );
                    // XSS CLEAN
                    $data = $this->security->xss_clean($temp_data);
                    
                    if ($id = $this->post_model->insert($data))
                    {
                        // Insert Batch Category or tag
                        if ($post_field['category'] != NULL)
                        {
                            $cat_data = array();

                            foreach ($post_field['category'] as $cat)
                            {
                                $cat_id = explode('-', $cat);
                                $cat_temp_data = array(
                                    'post_id' => $id,
                                    'category_id' => $cat_id[0]
                                );
                                array_push($cat_data, $cat_temp_data);
                            }
                            $this->post_category_model->insert($cat_data, TRUE);
                        }
                        // Tag
                        if ($post_field['tag'] != NULL)
                        {
                            $tag_data = array();

                            foreach ($post_field['tag'] as $tag)
                            {
                                $tag_id = explode('-', $tag);
                                $tag_temp_data = array(
                                    'post_id' => $id,
                                    'tag_id' => $tag_id[0]
                                );
                                array_push($tag_data, $tag_temp_data);
                            }
                            $this->post_tag_model->insert($tag_data, TRUE);
                        }

                        $message = 'Post berhasil dibuat!';
                        $this->errors = FALSE;
                    }
                    else
                    {
                        $message = 'Post gagal dibuat. Silahkan ulangi lagi!';
                        $this->errors = TRUE;
                    }
                } else { // if upload image error;
                    $message = $images['content'];
                    $this->errors = TRUE;
                }
            }

            if ($this->errors == FALSE) // insert
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field); // repopulate posf field
            }
            redirect_back();
        }

        // GET PAGE
        $this->data['page_title'] = 'New Post';
        $this->breadcrumb->add('Posts', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('New Post');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'all_posts' => array(
                'page' => site_url('admin/page/all?select=TRUE&field=amp_pages.id,amp_pages.title'),
                'category' => site_url('admin/category/all?select=TRUE&field=amp_cat.id,amp_cat.name'),
                'tag' => site_url('admin/tag/all?select=TRUE&field=amp_tag.id,amp_tag.name')
            ),
            'filemanager' => $this->filemanager,
        );
        $this->render('pages/new-post');
    }

    /*
    * POST: Update Post by ID
    */
    public function update()
    {
        // check if empty id
        $id = $this->uri->segment(4);
        
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->post_model->get_by($where, NULL, NULL, TRUE, 'id, title, content, page_id, image, seo, additional, type, comment, feed, popular, published_at, status');
        if (!$data_check)
            redirect($this->uri_1 . '/' . $this->uri_2);

        // set all posts
        $post_field = $this->input->post();

        // POST SUBMIT
        if (isset($post_field['submit']))
        {
            // Check status
            $published_at = NULL;
            if ($post_field['status'] == 'publish')
                $published_at = $this->current_datetime;
            elseif ($post_field['status'] == 'schedule')
                $published_at = format_date($post_field['schedule_date']);

            // gallery
            $gallery = NULL;
            $video = NULL;
            $type = NULL;
            if (!empty($post_field['video_gallery'])) {
                $video = $post_field['video_gallery'];
                $type = 'video';
            }

            // SEO
            $seo = array(
                'title' => $post_field['seo_title'],
                'description' => $post_field['seo_description'],
                'keywords' => $post_field['seo_keywords'],
            );

            $page_id = explode('-', $post_field['page']);
            $temp_data = array(
                'title' => $post_field['title'],
                'slug' => url_title($post_field['title'], '-', TRUE),
                'content' => $post_field['content'],
                'excerpt' => character_limiter($post_field['content'], 200),
                'page_id' => $page_id[0],
                'status' => $post_field['status'],
                'seo' => json_encode($seo),
                'additional' => ($gallery) ? $gallery : $video,
                'type' => $type,
                // 'comment' => ($post_field['allow_comments'] == 1) ? 1 : 0,
                'feed' => ($post_field['api_feed'] == 1) ? 1 : 0,
                'popular' => ($post_field['api_popular'] == 1) ? 1 : 0,
                'published_at' => $published_at,
                'updated_at' => $this->current_datetime,
                'updated_by' => $this->session->userdata('id'),
            );
            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            // check rules validation
            $rules = $this->post_model->rules['update'];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            // only current id can change the name
            $where_check = array(
                'id <> ' => $id,
                'title' => $data['title']
            );
            $data_check = $this->post_model->get_by($where_check, NULL, NULL, TRUE, 'id');
            if ($data_check)
            {
                $message = 'Nama Judul tidak boleh sama!';
                $this->errors = TRUE;
            }

            if ($this->errors == FALSE) // if not error update data
            {
                if ($_FILES['upload_file']['tmp_name'] != '') {
                    $this->load->library('upload_file');
                    $config = array(
                        'post_name' => 'upload_file', // name upload field
                        'file_type' => 'image', // type of the file want to upload (image or file)
                        'max_size' => 1000, // maximal size upload
                        'is_csrf_token' => FALSE, // when csrf needed
                        'resize' => TRUE
                    );
    
                    $images = $this->upload_file->_do_upload($config);
                    if ($images['error'] == false) {
                        $this->errors = FALSE;
                    } else {
                        $this->errors = TRUE;
                        $message = $images['content'];
                    }
                }
                if ($this->errors == false) {
                    if ($images['content']) {
                        $data['image'] = json_encode($images['content']);
                    }
                    if ($this->post_model->update($data, $where)) // update
                    {
                        // Insert Batch Category or tag
                        if ($post_field['category'] != NULL)
                        {
                            $cat_data = array();

                            foreach ($post_field['category'] as $cat)
                            {
                                $cat_id = explode('-', $cat);
                                $cat_temp_data = array(
                                    'post_id' => $id,
                                    'category_id' => $cat_id[0]
                                );
                                array_push($cat_data, $cat_temp_data);
                            }
                            $this->post_category_model->delete_by(array('post_id' => $id)); // Remove category by post id
                            $this->post_category_model->insert($cat_data, TRUE);
                        }
                        // Tag
                        if ($post_field['tag'] != NULL)
                        {
                            $tag_data = array();

                            foreach ($post_field['tag'] as $tag)
                            {
                                $tag_id = explode('-', $tag);
                                $tag_temp_data = array(
                                    'post_id' => $id,
                                    'tag_id' => $tag_id[0]
                                );
                                array_push($tag_data, $tag_temp_data);
                            }
                            $this->post_tag_model->delete_by(array('post_id' => $id)); // remove tag by post id
                            $this->post_tag_model->insert($tag_data, TRUE); 
                        }

                        $message = 'Post berhasil dirubah!';
                        $this->errors = FALSE;
                    }
                    else
                    {
                        $message = 'Post gagal dirubah. Silahkan ulangi lagi!';
                        $this->errors = TRUE;
                    }
                } else {
                    $message = $message;
                    $this->errors = TRUE;
                }
            }
            
            if ($this->errors == FALSE) // success
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else // error
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field);
            }
            redirect_back();            
        }

        // GET PAGE
        $this->data['page_title'] = 'Edit Post';
        $this->breadcrumb->add('Posts', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('Edit Post');
        $this->data['breadcrumb'] = $this->breadcrumb->output();

        // Get Selected Category
        $category = $this->post_category_model->get_by(array('post_id' => $data_check->id), NULL, NULL, FALSE, 'category_id');

        $category_id = array();
        foreach ($category as $cat)
        {
            array_push($category_id, $cat->category_id);
        }

        // Get Selected Tag
        $tag = $this->post_tag_model->get_by(array('post_id' => $data_check->id), NULL, NULL, FALSE, 'tag_id');

        $tag_id = array();
        foreach ($tag as $tg)
        {
            array_push($tag_id, $tg->tag_id);
        }

        $this->data['data'] = array(
            'all_posts' => array(
                'single' => $data_check,
                'page_detail' => $this->page_model->get_by(array('id' => $data_check->page_id), NULL, NULL, TRUE, 'id,title'),
                'page' => site_url('admin/page/all?select=TRUE&field=amp_pages.id,amp_pages.title'),
                'category_detail' => ($category_id) ? $this->category_model->where_in($category_id, 'id,name') : '',
                'category' => site_url('admin/category/all?select=TRUE&field=amp_cat.id,amp_cat.name'),
                'tag_detail' => ($tag_id) ? $this->tag_model->where_in($tag_id, 'id,name') : '',
                'tag' => site_url('admin/tag/all?select=TRUE&field=amp_tag.id,amp_tag.name')
            ),
            'filemanager' => $this->filemanager,
        );
        
        $this->render('pages/edit-post');
    }

    /*
    * POST: Delete Post by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->post_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->post_model->delete_by($where)) // delete
        {
            // Delete Post Category
            $this->post_category_model->delete_by(array('post_id' => $id));
            // Delete Post Tag
            $this->post_tag_model->delete_by(array('post_id' => $id));

            $message = 'Post berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Post gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
        }
        redirect_back();        
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'post_model',
            'where' => ($search != NULL) ? array('title' => $search) : array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'amp_post.id, amp_post.title, amp_post.status, u.first_name, u.last_name, p.slug as p_slug,amp_post.published_at'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->post_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        if ($search != NULL)
            $records = $this->post_model->where_like($data['where'], $data['single'], $data['select']); // post_model
        else
            $records = $this->post_model->get_by_user($data['where'], $data['limit'], $data['offset'], $data['single'], $data['select']); // post_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "title" => $record->title,
                        "author" => ucfirst($record->first_name) .' '. ucfirst($record->last_name),
                        "page_slug" => $record->p_slug,
                        "status" => $record->status,
                        "published" => $record->published_at
                    )
                );
            }

            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            $this->render(); // ajax
        }
    }

    // Feed
	public function feed()
	{
        $this->data['page_title'] = 'Feed';
        $this->breadcrumb->add('Posts');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'action' => array(
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all-feed')
        );

        $this->render('pages/posts');
    }

    // Feed popular
	public function feed_popular()
	{
        $this->data['page_title'] = 'Popular Feed';
        $this->breadcrumb->add('Posts');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'action' => array(
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all-popular-feed')
        );

        $this->render('pages/posts');
    }

    public function all_feed($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'post_model',
            'where' => array('amp_post.feed' => 1, 'amp_post.status' => 'publish'),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'amp_post.id, amp_post.title, amp_post.status, u.first_name, u.last_name, p.slug as p_slug,amp_post.published_at'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->post_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        if ($search != NULL)
            $records = $this->post_model->where_like($data['where'], $data['single'], $data['select']); // post_model
        else
            $records = $this->post_model->get_by_user($data['where'], $data['limit'], $data['offset'], $data['single'], $data['select']); // post_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "title" => $record->title,
                        "author" => ucfirst($record->first_name) .' '. ucfirst($record->last_name),
                        "page_slug" => $record->p_slug,
                        "status" => $record->status,
                        "published" => $record->published_at
                    )
                );
            }

            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            $this->render(); // ajax
        }
    }

    public function all_popular_feed($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'post_model',
            'where' => array('amp_post.popular' => 1, 'amp_post.status' => 'publish'),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'amp_post.id, amp_post.title, amp_post.status, u.first_name, u.last_name, p.slug as p_slug,amp_post.published_at'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->post_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        if ($search != NULL)
            $records = $this->post_model->where_like($data['where'], $data['single'], $data['select']); // post_model
        else
            $records = $this->post_model->get_by_user($data['where'], $data['limit'], $data['offset'], $data['single'], $data['select']); // post_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "title" => $record->title,
                        "author" => ucfirst($record->first_name) .' '. ucfirst($record->last_name),
                        "page_slug" => $record->p_slug,
                        "status" => $record->status,
                        "published" => $record->published_at
                    )
                );
            }

            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            $this->render(); // ajax
        }
    }
}