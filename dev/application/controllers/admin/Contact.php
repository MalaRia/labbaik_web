<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('contact_model'));
        $this->load->helper(array('email'));
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            // case 'information':
            //     $this->information();
            //     break;
            case 'single':
                $this->single();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // contacts
	public function index()
	{
        $this->data['page_title'] = 'Messages';
        $this->breadcrumb->add('Contacts', current_url(), true);
        $this->breadcrumb->add('Messages');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'action' => array(
                // 'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all'),
            'single' => site_url($this->uri_1 . '/' . $this->uri_2 . '/single?id=')
        );

        $this->render('pages/messages');
    }
    
    /*
    * POST: Create Contacts
    */
    // public function create()
    // {
    //     $post_field = $this->input->post();

    //     if (!isset($post_field['submit']))
    //     {
    //         $this->breadcrumb->add('Users', $this->uri_1 . '/' . $this->uri_2);
    //         $this->breadcrumb->add('New User');
    //         $this->data['breadcrumb'] = $this->breadcrumb->output();
    //         $this->data['data'] = array(
    //             'all_users' => array(
    //                 // 'page' => site_url('admin/page/all?select=TRUE&field=amp_pages.id,amp_pages.title'),
    //                 // 'category' => site_url('admin/category/all?select=TRUE&field=amp_cat.id,amp_cat.name'),
    //                 // 'tag' => site_url('admin/tag/all?select=TRUE&field=amp_tag.id,amp_tag.name')
    //             ),
    //             'filemanager' => $this->filemanager,
    //         );
    //         $this->render('pages/new-user');
    //         return;
    //     }

    //     // image
    //     $image = NULL;
    //     if (!empty($post_field['image_big'])) {
    //         $image = array(
    //             'image' => $post_field['image_big'],
    //             'thumb' => $post_field['image_thumb'],
    //             'title' => $post_field['image_title'],
    //             'alt' => $post_field['image_alt']
    //         );
    //         $image = json_encode($image);
    //     }

    //     // CREATE HERE
    //     $temp_data = array(
    //         'username' => $post_field['username'],
    //         'email' => $post_field['email'],
    //         'first_name' => $post_field['first_name'],
    //         'last_name' => $post_field['last_name'],
    //         'website' => $post_field['website'],
    //         'role' => $post_field['role'],
    //         'avatar' => $image,
    //         'password' => generate_hash($post_field['password']),
    //         'created_at' => $this->current_datetime,
    //         'created_by' => $this->session->userdata('id'),
    //     );
    //     // XSS CLEAN
    //     $data = $this->security->xss_clean($temp_data);

    //     // check rules validation
    //     $rules = $this->user_model->rules['create'];
    //     $this->form_validation->set_rules($rules);
    //     if ($this->form_validation->run() == FALSE)
    //     {
    //         $message = validation_errors();
    //         $this->errors = TRUE;
    //     }

    //     if ($this->errors == FALSE) // if not error insert data
    //     {

    //         if ($this->user_model->insert($data))
    //         {
    //             $message = 'User berhasil dibuat!';
    //             $this->errors = FALSE;
    //         }
    //         else
    //         {
    //             $message = 'User gagal dibuat. Silahkan ulangi lagi!';
    //             $this->errors = TRUE;
    //         }
    //     }

    //     if ($this->errors == FALSE) // insert
    //     {
    //         set_flash('class', 'success', TRUE);
    //         set_flash('status', 'success', TRUE);
    //         set_flash('success', $message, TRUE);
    //     }
    //     else
    //     {
    //         set_flash('class', 'danger', TRUE);
    //         set_flash('status', 'errors', TRUE);
    //         set_flash('errors', $message, TRUE);
    //         repopulate_post_field($post_field); // repopulate posf field
    //     }
    //     redirect_back();
    // }

    /*
    * GET: Information // deprecated soon
    */
    // public function information()
    // {
    //     // check if data exist
    //     $where = array(
    //         'id' => 1,
    //     );
    //     $data_check = $this->contact_model->get_by($where, NULL, NULL, TRUE, 'id, description,image');
    //     if (!$data_check)
    //         redirect($this->uri_1 . '/' . $this->uri_2);

    //     // set all posts
    //     $post_field = $this->input->post();

    //     if (!isset($post_field['submit']))
    //     {
    //         $this->breadcrumb->add('Contacts', $this->uri_1 . '/' . $this->uri_2);
    //         $this->breadcrumb->add('Information');
    //         $this->data['breadcrumb'] = $this->breadcrumb->output();
    //         $this->data['data'] = array(
    //             'all_contacts' => array(
    //                 'single' => $data_check,
    //             ),
    //             'filemanager' => $this->filemanager,
    //         );
            
    //         $this->render('pages/edit-information');
    //         return;
    //     }

    //     // image
    //     $image = $data_check->avatar;
    //     if (!empty($post_field['image_big'])) {
    //         $image = array(
    //             'image' => $post_field['image_big'],
    //             'thumb' => $post_field['image_thumb'],
    //             'title' => $post_field['image_title'],
    //             'alt' => $post_field['image_alt']
    //         );
    //         $image = json_encode($image);
    //     } 

    //     $temp_data = array(
    //         'description' => $post_field['description'],
    //         'image' => $image,
    //         'updated_at' => $this->current_datetime,
    //         'updated_by' => $this->session->userdata('id'),
    //     );
    //     // XSS CLEAN
    //     $data = $this->security->xss_clean($temp_data);

    //     // check rules validation
    //     $rules = $this->user_model->rules['update'];

    //     if ($this->errors == FALSE) // if not error update data
    //     {
    //         if ($this->contact_model->update($data, $where)) // update
    //         {
    //             $message = 'Contact berhasil dirubah!';
    //             $this->errors = FALSE;
    //         }
    //         else
    //         {
    //             $message = 'Contact gagal dirubah. Silahkan ulangi lagi!';
    //             $this->errors = TRUE;
    //         }
    //     }
        
    //     if ($this->errors == FALSE) // success
    //     {
    //         set_flash('class', 'success', TRUE);
    //         set_flash('status', 'success', TRUE);
    //         set_flash('success', $message, TRUE);
    //         redirect_back();
    //     }
    //     else // error
    //     {
    //         set_flash('class', 'danger', TRUE);
    //         set_flash('status', 'errors', TRUE);
    //         set_flash('errors', $message, TRUE);
    //         repopulate_post_field($post_field);
    //         redirect_back();
    //     }
    // }

    public function single()
    {
        // set all get
        $get_field = $this->input->get();
        // check if data exist
        $where = array(
            'id' => $get_field['id'],
        );
        $data_check = $this->contact_model->get_by($where, NULL, NULL, TRUE, 'id, email, full_name, description');
        if (!$data_check)
            redirect($this->uri_1 . '/' . $this->uri_2);
    
        $this->data['data'] = array(
            'email' => $data_check->email,
            'full_name' => $data_check->full_name,
            'description' => $data_check->description
        );
        $this->render(); // ajax
    }

    /*
    * POST: Delete contact by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->contact_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->contact_model->delete_by($where)) // delete
        {
            $message = 'Kontak berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Kontak gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
        }
        redirect_back();
    }

    public function all()
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');

        $data = array(
            'model' => 'contact_model',
            'where' => array('id <>' => 1),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'id, email, full_name, description'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->contact_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->contact_model->get_by($data['where'], NULL, NULL, $data['single'], $data['select']); // tag_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "email" => $record->email,
                        "fullname" => $record->full_name,
                        "description" => strip_tags($record->description),
                    )
                );
            }

            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );

            $this->render(); // ajax
        }
    }
}