<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('visitor_model','page_model','post_model','user_model','tag_model','category_model','contact_model','comment_model'));
    }

	public function index()
	{
		$this->data['page_title'] = 'Dashboard';
		$this->data['breadcrumb'] = '';
		$this->data['data'] = array(
			'current' => 'dashboard',
			'content' => array(
				'posts' => array(
					'total' => $this->post_model->count_post(array(),'id'),
					'title' => 'Posts',
					'detail' => site_url('admin/post')
				),
				'users' => array(
					'total' => $this->user_model->count_post(array(),'id'),
					'title' => 'Users',
					'detail' => site_url('admin/user')
				),
				'pages' => array(
					'total' => $this->page_model->count_post(array(),'id'),
					'title' => 'Pages',
					'detail' => site_url('admin/page')
				),
				'comments' => array(
					'total' => $this->comment_model->count_post(array(),'id'),
					'title' => 'Comments',
					'detail' => site_url('admin/comment')
				),
				'messages' => array(
					'total' => $this->contact_model->count_post(array('id <>' => 1),'id'),
					'title' => 'Messages',
					'detail' => site_url('admin/contact/message')
				),
				'visitors' => array(
					'total' => $this->visitor_model->count_post(array(),'id'),
					'title' => 'Visitors',
					'detail' => '#'
				),
				'tags' => array(
					'total' => $this->tag_model->count_post(array(),'id'),
					'title' => 'Tags',
					'detail' => site_url('admin/tag')
				),
				'categories' => array(
					'total' => $this->category_model->count_post(array(),'id'),
					'title' => 'Categories',
					'detail' => site_url('admin/category')
				),
			)
        );
        $this->render('pages/dashboard');
	}

}