<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Setting extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('setting_model'));
        $this->load->helper(array('email','seo'));
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'theme':
                $this->update('theme');
                break;
            case 'web':
                $this->update('web');
                break;
            case 'seo':
                $this->update('seo');
                break;
            case 'social-media':
                $this->update('social_media');
                break;

            default:
                // $this->index();
                show_404();
                break;
        }
    }

    /*
    * POST: Update Setting by type
    */
    public function update($type = NULL)
    {   
        if (empty($type))
            redirect_back();

        // check if data exist
        $where = array(
            'type' => $type,
        );
        $data_check = $this->setting_model->get_by($where, NULL, NULL, TRUE, 'id, content, type');
        if (!$data_check)
            redirect($this->uri_1 . '/' . $this->uri_2);

        // set all posts
        $post_field = $this->input->post();
        // POST TYPE
        if (isset($post_field['submit']))
        {
            switch ($type)
            {
                case 'theme':
                    $this->theme();
                    break;
                case 'social_media':
                    $this->social_media();
                    break;

                case 'seo':
                    $this->seo();
                    break;

                case 'web':
                    $this->web();
                    break;
            }
        }

        // GET TYPE
        $title = str_replace('_', ' ', ucfirst($type));
        $this->data['page_title'] = $title;
        $this->breadcrumb->add('Setting', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add($title);
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'setting' => array(
                $type => $data_check,
            ),
            'filemanager' => $this->filemanager,
        );
        
        $this->render('pages/'.$type);
    }

    private function theme()
    {
        $post_field = $this->input->post();
        // check rules validation
        $rules = $this->setting_model->rules['theme'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error update data
        {
            $where = array(
                'type' => 'theme'
            );
            $theme = array(
                'template' => $post_field['template']
            );
            $data = array(
                'content' => json_encode($theme),
                'updated_at' => $this->current_datetime,
                'updated_by' => $this->session->userdata('id')
            );

            if ($this->setting_model->update($data, $where))
            {
                $message = 'Theme berhasil dirubah!';
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else
            {
                $message = 'Theme gagal dirubah. Silahkan ulangi lagi!';
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field);
            }
        }
        redirect_back();
    }

    private function social_media()
    {
        $post_field = $this->input->post();
        // check rules validation
        $rules = $this->setting_model->rules['social_media'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error update data
        {
            $where = array(
                'type' => 'social_media'
            );
            $social_media = array(
                'facebook' => $post_field['facebook'],
                'twitter' => $post_field['twitter'],
                'gplus' => $post_field['gplus'],
                'instagram' => $post_field['instagram'],
                'linkedin' => $post_field['linkedin']
            );
            $data = array(
                'content' => json_encode($social_media),
                'updated_at' => $this->current_datetime,
                'updated_by' => $this->session->userdata('id')
            );

            if ($this->setting_model->update($data, $where))
            {
                $message = 'Social Media berhasil dirubah!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Social Media gagal dirubah. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field);
        }
        redirect_back();
    }

    private function seo()
    {
        $post_field = $this->input->post();
        // check rules validation
        $rules = $this->setting_model->rules['seo'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error update data
        {
            $where = array(
                'type' => 'seo'
            );
            $seo = array(
                'title' => $post_field['title'],
                'description' => $post_field['description'],
                'rss' => $post_field['rss'],
                'sitemap' => $post_field['sitemap'],
                'ga_verify' => $post_field['ga_verify'],
                'ga_analytics' => $post_field['ga_analytics'],
                'keywords' => $post_field['keywords']
            );
            $data = array(
                'content' => json_encode($seo),
                'updated_at' => $this->current_datetime,
                'updated_by' => $this->session->userdata('id')
            );

            if ($this->setting_model->update($data, $where))
            {
                $message = 'Seo berhasil dirubah!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Seo gagal dirubah. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field);
        }
        redirect_back();
    }

    private function web()
    {
        $post_field = $this->input->post();
        // var_dump($post_field);
        // return;
        // check rules validation
        $rules = $this->setting_model->rules['web'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error update data
        {
            $where = array(
                'type' => 'web'
            );

            // image
            $images = NULL;
            $this->load->library('upload_file');
            if (isset($_FILES['upload_file']['tmp_name'])) {
                $config = array(
                    'post_name' => 'upload_file', // name upload field
                    'file_type' => 'image', // type of the file want to upload (image or file)
                    'max_size' => 1000, // maximal size upload
                    'is_csrf_token' => FALSE, // when csrf needed
                    'resize' => TRUE
                );

                $images = $this->upload_file->_do_upload($config);
                // var_dump($images);
                // return;
                if ($images['error'] == false) {
                    $this->errors == FALSE;
                } else {
                    $this->errors == TRUE;
                    $message = $images['content'];
                }
            }

            // icon
            $icons = NULL;
            if (isset($_FILES['upload_file_icon']['tmp_name'])) {
                $config = array(
                    'post_name' => 'upload_file_icon', // name upload field
                    'file_type' => 'image', // type of the file want to upload (image or file)
                    'max_size' => 1000, // maximal size upload
                    'is_csrf_token' => FALSE, // when csrf needed
                    'resize' => TRUE
                );
    
                $icons = $this->upload_file->_do_upload($config);
                if ($icons['error'] == false) {
                    $this->errors == FALSE;
                } else {
                    $this->errors == TRUE;
                    $message = $icons['content'];
                }
            }

            // logo
            $logos = NULL;
            if (isset($_FILES['upload_file_logo']['tmp_name'])) {
                $config = array(
                    'post_name' => 'upload_file_logo', // name upload field
                    'file_type' => 'image', // type of the file want to upload (image or file)
                    'max_size' => 1000, // maximal size upload
                    'is_csrf_token' => FALSE, // when csrf needed
                    'resize' => TRUE
                );
    
                $logos = $this->upload_file->_do_upload($config);
                if ($logos['error'] == false) {
                    $this->errors == FALSE;
                } else {
                    $this->errors == TRUE;
                    $message = $logos['content'];
                }
            }
            
            if ($this->errors == false) {
                $web = array(
                    'site_title' => $post_field['site_title'],
                    'site_url' => $post_field['site_url'],
                    'site_description' => $post_field['site_description'],
                    'site_email' => $post_field['site_email'],
                    'site_telephone' => $post_field['site_telephone'],
                    'site_address' => $post_field['site_address'],
                    'site_pagination' => $post_field['site_pagination'],
                );

                if ($images != NULL) {
                    $web['image'] = $images['content'];
                }
                if ($icons != NULL) {
                    $web['icon'] = $icons['content'];
                }
                if ($logos != NULL) {
                    $web['logo'] = $logos['content'];
                }

                $data = array(
                    'content' => json_encode($web),
                    'updated_at' => $this->current_datetime,
                    'updated_by' => $this->session->userdata('id')
                );

                if ($this->setting_model->update($data, $where))
                {
                    $message = 'Web Setting berhasil dirubah!';
                    $this->errors = FALSE;
                }
                else
                {
                    $message = 'Web Setting gagal dirubah. Silahkan ulangi lagi!';
                    $this->errors = TRUE;
                }
            } else {
                $message = $message;
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field);
        }
        redirect_back();
    }
}