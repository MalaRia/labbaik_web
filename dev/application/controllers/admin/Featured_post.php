<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Featured_post extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('widget_model','post_model'));
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'single':
                $this->single();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // Featured Post
	public function index()
	{
        $this->data['page_title'] = 'Featured-Post';
        $this->breadcrumb->add('Widgets', current_url(), TRUE);
        $this->breadcrumb->add('Featured-Post');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $where = array();
        $select = 'id,title';
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'all_featured' => array(
                'posts' => site_url('admin/post/all?select=TRUE&field=amp_posts.id,amp_posts.title'),
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all?type=featured-post'),
            'single' => site_url($this->uri_1 . '/' . $this->uri_2 . '/single?id='),
        );
        $this->render('pages/featured-post');
	}

    /*
    * POST: Create Featured Post
    */
    public function create()
    {
        $post_field = $this->input->post();
        
        // check rules validation
        $rules = $this->widget_model->rules['featured_post']['create'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error insert data
        {
            $temp_featured = array(
                'name' => $post_field['name'],
                'name_id' => $post_field['name_id'],
                'post_id' => $post_field['post_id'],
            );

            $temp_data = array(
                'content' => json_encode($temp_featured),
                'additional' => json_encode($temp_featured['name_id']),
                'type' => 'featured-post',
                'created_at' => $this->current_datetime,
                'created_by' => $this->session->userdata('id'),
            );

            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            if ($this->widget_model->insert($data))
            {
                $message = 'Featured post berhasil dibuat!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Featured post gagal dibuat. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // insert
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field); // repopulate posf field
        }
        redirect_back();
    }

    /*
    * GET: Single by ID
    */
    public function single()
    {
        // check if empty id
        $id = $this->input->get('id');
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id,content');
        if (!$data_check)
            redirect_back();

        $content = json_decode($data_check->content);
        $this->data['data'] = array(
            'id' => $data_check->id,
            'name' => $content->name,
            'name_id' => $content->name_id,
            'post_id' => $content->post_id,
        );

        $this->render(NULL, 'json');
    }

    /*
    * POST: Update Featured Post by ID
    */
    public function update()
    {
        // set all posts
        $post_field = $this->input->post();
        // check if empty id
        $id = $post_field['featured_id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        // check rules validation
        $rules = $this->widget_model->rules['featured_post']['update'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE)
        {
            $temp_featured = array(
                'name' => $post_field['name'],
                'name_id' => $post_field['name_id'],
                'post_id' => $post_field['post_id'],
            );
    
            $temp_data = array(
                'content' => json_encode($temp_featured),
                'additional' => json_encode($temp_featured['name_id']),
                'type' => 'featured-post',
                'created_at' => $this->current_datetime,
                'created_by' => $this->session->userdata('id'),
            );
    
            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);
    
            if ($this->errors == FALSE) // if not error update data
            {
                if ($this->widget_model->update($data, $where)) // update
                {
                    $message = 'Featured Post berhasil dirubah!';
                    $this->errors = FALSE;
                }
                else
                {
                    $message = 'Featured Post gagal dirubah. Silahkan ulangi lagi!';
                    $this->errors = TRUE;
                }
            }
        }
        
        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field);
        }
        redirect_back();
    }

    /*
    * POST: Delete by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->widget_model->delete_by($where)) // delete
        {
            $message = 'Featured Post berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Featured Post gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
        }
        redirect_back();
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'widget_model',
            'where' => ($type != NULL) ? array('type' => $type) : array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'id,content'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->widget_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->widget_model->get_by($data['where'], NULL, NULL,$data['single'], $data['select']); // category_model

        if ($select)
        {
            echo json_encode($records);
        }
        else
        {
            $record_data = array();
            foreach ($records as $record)
            {
                $content = json_decode($record->content);
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "name" => (isset($content->name)) ? $content->name : '',
                        "name_id" => (isset($content->name_id)) ? $content->name_id : '',
                        // "count" => $record->count
                    )
                );
            }

            $results = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            if ($return)
                return $results;
            else
                echo json_encode($results);
        }
    }
}