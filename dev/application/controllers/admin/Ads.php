<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ads extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('widget_model'));

        /* generate csrf token */
        $this->data['csrf_token'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );
        $this->filemanager = array(
            'ckeditor' => base_url('templates/assets/filemanager/dialog.php?type=1&editor=ckeditor&relative_url=0&fldr=images&akey=masnat'),
            'standalone' => base_url('templates/assets/filemanager/dialog.php?type=2&field_id=fieldID4&fldr=&relative_url=1&akey=masnat')
        );

        $this->errors = FALSE;
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'single':
                $this->single();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // Ads
	public function index()
	{
        $this->breadcrumb->add('Dashboard', 'admin/dashboard', TRUE);
        $this->breadcrumb->add('Ads');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $where = array();
        $select = 'id,title';
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all?type=ads'),
            'single' => site_url($this->uri_1 . '/' . $this->uri_2 . '/single?id='),
            'filemanager' => $this->filemanager,
        );

        // var_dump($this->data['data']['all_post'])

        $this->render('pages/ads');
	}

    /*
    * POST: Create Category
    */
    public function create()
    {
        $post_field = $this->input->post();
        
        // check rules validation
        $rules = $this->widget_model->rules['ads']['create'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error insert data
        {
            if ($post_field['type'] == 'embedded-script')
            {
                $temp_ads = array(
                    'type' => $post_field['type'],
                    'position' => $post_field['position'],
                    'embedded_script' => $post_field['embedded_script'],
                );
            }
            else if ($post_field['type'] == 'non-embedded-script')
            {
                // image
                $image = NULL;
                if (!empty($post_field['image_big'])) {
                    $image = array(
                        'image' => $post_field['image_big'],
                        'thumb' => $post_field['image_thumb'],
                        'title' => $post_field['image_title'],
                        'alt' => $post_field['image_alt']
                    );
                    // $image = json_encode($image);
                }

                $temp_ads = array(
                    'type' => $post_field['type'],
                    'position' => $post_field['position'],
                    'link' => $post_field['link'],
                    'image' => $image,
                );
            }

            $temp_data = array(
                'content' => json_encode($temp_ads),
                'additional' => $temp_ads['position'],
                'type' => 'ads',
                'created_at' => $this->current_datetime,
                'created_by' => $this->session->userdata('id'),
            );

            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            if ($this->widget_model->insert($data))
            {
                $message = 'Ads berhasil dibuat!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Ads gagal dibuat. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // insert
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field); // repopulate posf field
        }
        redirect_back();
    }

    /*
    * GET: Single Ads by ID
    */
    public function single()
    {
        // check if empty id
        $id = $this->input->get('id');
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id,content');
        if (!$data_check)
            redirect_back();

        $content = json_decode($data_check->content);
        $data = array(
            'id' => $data_check->id,
            'type' => $content->type,
            'position' => $content->position,
            // 'position' => $content->position,
        );
        if ($data['type'] == 'embedded-script')
        {
            $data['embedded_script'] = $content->embedded_script;
        }
        else if ($data['type'] == 'non-embedded-script')
        {
            $data['link'] = $content->link;
            $data['image'] = $content->image;
        }
        echo json_encode($data);
    }

    /*
    * POST: Update Ads by ID
    */
    public function update()
    {
        // check if empty id
        $id = $this->uri->segment(4);
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        // set all posts
        $post_field = $this->input->post();

        if ($post_field['type'] == 'embedded-script')
        {
            $temp_ads = array(
                'type' => $post_field['type'],
                'position' => $post_field['position'],
                'embedded_script' => $post_field['embedded_script'],
            );
        }
        else if ($post_field['type'] == 'non-embedded-script')
        {
            // image
            $image = NULL;
            if (!empty($post_field['image_big'])) {
                $image = array(
                    'image' => $post_field['image_big'],
                    'thumb' => $post_field['image_thumb'],
                    'title' => $post_field['image_title'],
                    'alt' => $post_field['image_alt']
                );
                // $image = json_encode($image);
            }

            $temp_ads = array(
                'type' => $post_field['type'],
                'position' => $post_field['position'],
                'link' => $post_field['link'],
                'image' => $image,
            );
        }

        $temp_data = array(
            'content' => json_encode($temp_ads),
            'additional' => $temp_ads['position'],
            'type' => 'ads',
            'created_at' => $this->current_datetime,
            'created_by' => $this->session->userdata('id'),
        );

        // XSS CLEAN
        $data = $this->security->xss_clean($temp_data);

        if ($this->widget_model->update($data, $where))
        {
            $message = 'Ads berhasil dirubah!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Ads gagal dirubah. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }
        
        
        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
            redirect_back();
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field);
            redirect_back();
        }
    }

    /*
    * POST: Delete Category by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $cat_id = $post_field['delete-id'];
        if (empty($cat_id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $cat_id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->widget_model->delete_by($where)) // delete
        {
            $message = 'Ads berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Ads gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
            redirect_back();
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            redirect_back();
        }
    }

    public function all($return = FALSE)
    {
        // echo 'lists';
        // return;
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'widget_model',
            'where' => ($type != NULL) ? array('type' => $type) : array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'id, content'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->widget_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->widget_model->get_by($data['where'], NULL, NULL,$data['single'], $data['select']); // category_model

        // var_dump($records);
        // return;
        if ($select)
        {
            echo json_encode($records);
        }
        else
        {
            $record_data = array();
            foreach ($records as $record)
            {
                $content = json_decode($record->content);
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "type" => $content->type,
                        "position" => $content->position,
                        // "count" => $record->count
                    )
                );
            }

            $results = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            if ($return)
                return $results;
            else
                echo json_encode($results);
        }
    }
}