<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model('page_model');
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // page
	public function index()
	{
        $this->data['page_title'] = 'Pages';
        $this->breadcrumb->add('Pages');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete'),
                'preview' => site_url('preview/page/')
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all')
        );

        $this->render('pages/pages');
	}

    /*
    * POST: Create
    */
    public function create()
    {
        $post_field = $this->input->post();

        // POST SUBMIT
        if (isset($post_field['submit']))
        {
           // Check status
            $published_at = NULL;
            if ($post_field['status'] == 'publish')
                $published_at = $this->current_datetime;
            elseif ($post_field['status'] == 'schedule')
                $published_at = format_date($post_field['schedule_date']);

            // image
            $image = NULL;
            if (!empty($post_field['image_big'])) {
                $image = array(
                    'image' => $post_field['image_big'],
                    'thumb' => $post_field['image_thumb'],
                    'title' => $post_field['image_title'],
                    'alt' => $post_field['image_alt']
                );
                $image = json_encode($image);
            }

            // SEO
            $seo = array(
                'title' => $post_field['seo_title'],
                'description' => $post_field['seo_description'],
                'keywords' => $post_field['seo_keywords'],
            );

            // CREATE HERE
            $temp_data = array(
                'title' => $post_field['title'],
                'slug' => url_title($post_field['title'], '-', TRUE),
                'content' => $post_field['content'],
                'excerpt' => character_limiter($post_field['content'], 200),
                'parent_id' => $post_field['parent'],
                'status' => $post_field['status'],
                'image' => $image,
                'seo' => json_encode($seo),
                'comment' => ($post_field['allow_comments'] == 1) ? 1 : 0,
                'published_at' => $published_at,
                'created_at' => $this->current_datetime,
                'created_by' => $this->data['user_data']->id,
            );
            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            // check rules validation
            $rules = $this->page_model->rules['create'];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            if ($this->errors == FALSE) // if not error insert data
            {
                if ($this->page_model->insert($data))
                {
                    $message = 'Page berhasil dibuat!';
                    $this->errors = FALSE;
                }
                else
                {
                    $message = 'Page gagal dibuat. Silahkan ulangi lagi!';
                    $this->errors = TRUE;
                }
            }

            if ($this->errors == FALSE) // insert
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field); // repopulate posf field
            }
            redirect_back();
        }

        $this->data['page_title'] = 'New Page';
        $this->breadcrumb->add('Pages', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('New Page');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'all_pages' => array(
                'data' => $this->page_model->get_by(NULL, NULL, NULL, FALSE, 'id, title')
            ),
            'filemanager' => $this->filemanager,
        );
        $this->render('pages/new-page');
    }

    /*
    * POST: Update by ID
    */
    public function update()
    {
        // check if empty id
        $id = $this->uri->segment(4);
        
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->page_model->get_by($where, NULL, NULL, TRUE, 'id, title, content, parent_id, image, seo, comment, published_at, status');
        if (!$data_check)
            redirect($this->uri_1 . '/' . $this->uri_2);

        // set all posts
        $post_field = $this->input->post();

        // POST SUBMIT
        if (isset($post_field['submit']))
        {
            // Check status
            $published_at = NULL;
            if ($post_field['status'] == 'publish')
                $published_at = $this->current_datetime;
            elseif ($post_field['status'] == 'schedule')
                $published_at = format_date($post_field['schedule_date']);

            // image
            $image = $data_check->image;
            if (!empty($post_field['image_big'])) {
                $image = array(
                    'image' => $post_field['image_big'],
                    'thumb' => $post_field['image_thumb'],
                    'title' => $post_field['image_title'],
                    'alt' => $post_field['image_alt']
                );
                $image = json_encode($image);
            }

            // SEO
            $seo = array(
                'title' => $post_field['seo_title'],
                'description' => $post_field['seo_description'],
                'keywords' => $post_field['seo_keywords'],
            );

            $excerpt = ($post_field['content']) ? explode("==", $post_field['content']) : '';
            $temp_data = array(
                'title' => $post_field['title'],
                // 'slug' => url_title($post_field['title'], '-', TRUE),
                'content' => $post_field['content'],
                'excerpt' => character_limiter(strip_tags($excerpt[1]), 200),
                'parent_id' => $post_field['parent'],
                'status' => $post_field['status'],
                'image' => $image,
                'seo' => json_encode($seo),
                'comment' => ($post_field['allow_comments'] == 1) ? 1 : 0,
                'published_at' => $published_at,
                'updated_at' => $this->current_datetime,
                'updated_by' => $this->data['user_data']->id,
            );
            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            // check rules validation
            $rules = $this->page_model->rules['update'];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            // only current id can change the name
            $where_check = array(
                'id <> ' => $id,
                'title' => $data['title']
            );
            $data_check = $this->page_model->get_by($where_check, NULL, NULL, TRUE, 'id');
            if ($data_check)
            {
                $message = 'Nama Judul tidak boleh sama!';
                $this->errors = TRUE;
            }

            if ($this->errors == FALSE) // if not error update data
            {
                if ($this->page_model->update($data, $where)) // update
                {
                    $message = 'Page berhasil dirubah!';
                    $this->errors = FALSE;
                }
                else
                {
                    $message = 'Page gagal dirubah. Silahkan ulangi lagi!';
                    $this->errors = TRUE;
                }
            }
            
            if ($this->errors == FALSE) // success
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else // error
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field);
            }
            redirect_back();
        }

        // GET PAGE
        $this->data['page_title'] = 'Edit Page';
        $this->breadcrumb->add('Pages', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('Edit Page');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'all_pages' => array(
                'single' => $data_check,
                'data' => $this->page_model->get_by($where, NULL, NULL, FALSE, 'id, title')
            ),
            'filemanager' => $this->filemanager,
        );
        
        $this->render('pages/edit-page');
    }

    /*
    * POST: Delete by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->page_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->page_model->delete_by($where)) // delete
        {
            $message = 'Page berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Page gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
        }
        redirect_back();
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'page_model',
            'where' => ($search != NULL) ? array('title' => $search) : array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'id, title, published_at, status, slug'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->page_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        if ($search != NULL)
            $records = $this->page_model->where_like($data['where'], $data['single'], $data['select']); // tag_model
        else
            $records = $this->page_model->get_by($data['where'], $data['limit'], $data['offset'], $data['single'], $data['select']); // tag_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "title" => $record->title,
                        "slug" => $record->slug,
                        "published" => $record->published_at,
                        "status" => $record->status,
                    )
                );
            }

            $results = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            
            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            $this->render(); // ajax
        }
    }
}