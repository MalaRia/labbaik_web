<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('user_model'));
        $this->load->helper(array('email','auth_helper'));
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;
            case 'profile':
                $this->profile();
                break;

            default:
                $this->index();
                break;
        }
    }

    // posts
	public function index()
	{
        $this->data['page_title'] = 'Users';
        $this->breadcrumb->add('Users');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all')
        );

        $this->render('pages/users');
    }
    
    /*
    * POST: Create User
    */
    public function create()
    {
        $post_field = $this->input->post();

        // POST SUBMIT
        if (isset($post_field['submit']))
        {
            // CREATE HERE
            $temp_data = array(
                'username'    => $post_field['username'],
                'email'       => $post_field['email'],
                'first_name'  => $post_field['first_name'],
                'last_name'   => $post_field['last_name'],
                'facebook'    => $post_field['facebook'],
                'twitter'     => $post_field['twitter'],
                'gplus'       => $post_field['gplus'],
                'instagram'   => $post_field['instagram'],
                'linkedin'    => $post_field['linkedin'],
                'description' => $post_field['description'],
                'website'     => $post_field['website'],
                'role'        => $post_field['role'],
                'avatar'      => null,
                'password'    => generate_hash($post_field['password']),
                'created_at'  => $this->current_datetime,
                'created_by'  => $this->data['user_data']->id,
            );
            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            // check rules validation
            $rules = $this->user_model->rules['create'];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            if ($this->errors == FALSE) // if not error insert data
            {
                // image
                $this->load->library('upload_file');
                if (isset($_FILES['upload_file']['tmp_name'])) {
                    $config = array(
                        'post_name' => 'upload_file', // name upload field
                        'file_type' => 'image', // type of the file want to upload (image or file)
                        'max_size' => 1000, // maximal size upload
                        'is_csrf_token' => FALSE, // when csrf needed
                        'resize' => TRUE
                    );

                    $images = $this->upload_file->_do_upload($config);
                    if ($images['error'] == false) {
                        $this->errors = FALSE;
                    } else {
                        $this->errors = TRUE;
                        $message = $images['content'];
                    }
                }
                if ($this->errors == false) {
                    if ($images['content']) {
                        $data['avatar'] = json_encode($images['content']);
                    }

                    if ($this->user_model->insert($data))
                    {
                        $message = 'User berhasil dibuat!';
                        $this->errors = FALSE;
                    }
                    else
                    {
                        $message = 'User gagal dibuat. Silahkan ulangi lagi!';
                        $this->errors = TRUE;
                    }
                }
            }

            if ($this->errors == FALSE) // insert
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field); // repopulate posf field
            }
            redirect_back();
        }

        $this->data['page_title'] = 'New User';
        $this->breadcrumb->add('Users', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('New User');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'all_users' => array(),
            'filemanager' => $this->filemanager,
        );
        $this->render('pages/new-user');
    }

    /*
    * POST: Update by ID
    */
    public function update()
    {
        // check if empty id
        $id = $this->uri->segment(4);
        
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->user_model->get_by($where, NULL, NULL, TRUE, 'id, username, email, first_name, last_name, avatar, role, website, description, facebook, twitter, gplus, instagram, linkedin');
        if (!$data_check)
            redirect($this->uri_1 . '/' . $this->uri_2);

        // set all posts
        $post_field = $this->input->post();

        // POST
        if (isset($post_field['submit']))
        {
            $temp_data = array(
                'username'    => $post_field['username'],
                'email'       => $post_field['email'],
                'first_name'  => $post_field['first_name'],
                'last_name'   => $post_field['last_name'],
                'website'     => $post_field['website'],
                'facebook'    => $post_field['facebook'],
                'twitter'     => $post_field['twitter'],
                'gplus'       => $post_field['gplus'],
                'instagram'   => $post_field['instagram'],
                'linkedin'    => $post_field['linkedin'],
                'description' => $post_field['description'],
                'role'        => $post_field['role'],
                'updated_at'  => $this->current_datetime,
                'updated_by'  => $this->data['user_data']->id,
            );

            // check rules validation
            $rules = $this->user_model->rules['update'];

            // if password has change
            if ($post_field['password'] != NULL)
            {
                $temp_data['password'] = generate_hash($post_field['password']);
                $rules['password'] = $this->user_model->rules['password_change']['password'];
                $rules['password_confirmation'] = $this->user_model->rules['password_change']['password_confirmation'];
            }

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            // only current id can change the name
            $where_check = array(
                'id <> ' => $id,
                'email' => $temp_data['email']
            );
            $data_check = $this->user_model->get_by($where_check, NULL, NULL, TRUE, 'id');
            if ($data_check)
            {
                $message = 'Email tidak boleh sama!';
                $this->errors = TRUE;
            }

            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            if ($this->errors == FALSE) // if not error update data
            {
                // image
                $this->load->library('upload_file');
                if (isset($_FILES['upload_file']['tmp_name']) && $_FILES['upload_file']['tmp_name'] != "") {
                    $config = array(
                        'post_name' => 'upload_file', // name upload field
                        'file_type' => 'image', // type of the file want to upload (image or file)
                        'max_size' => 1000, // maximal size upload
                        'is_csrf_token' => FALSE, // when csrf needed
                        'resize' => TRUE
                    );

                    $images = $this->upload_file->_do_upload($config);
                    if ($images['error'] == false) {
                        $this->errors = FALSE;
                    } else {
                        $this->errors = TRUE;
                        $message = $images['content'];
                    }
                }

                if ($this->errors == FALSE) {
                    if ($images['content']) {
                        $data['avatar'] = json_encode($images['content']);
                    }

                    if ($this->user_model->update($data, $where)) // update
                    {
                        $message = 'User berhasil dirubah!';
                        $this->errors = FALSE;
                    }
                    else
                    {
                        $message = 'User gagal dirubah. Silahkan ulangi lagi!';
                        $this->errors = TRUE;
                    }
                }
            }
            
            if ($this->errors == FALSE) // success
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else // error
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field);
            }
            redirect_back();
        }

        // GET PAGE
        $this->data['page_title'] = 'Edit User';
        $this->breadcrumb->add('Users', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('Edit User');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'all_users' => array(
                'single' => $data_check
            ),
            'filemanager' => $this->filemanager,
        );
        
        $this->render('pages/edit-user');
    }

    /*
    * POST: Delete by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->user_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->user_model->delete_by($where)) // delete
        {
            $message = 'User berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'User gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
        }
        redirect_back();
    }

    public function profile()
    {
        // check if data exist
        $where = array(
            'id' => $this->data['user_data']->id,
        );
        // POST
        $post_field = $this->input->post();
        if (isset($post_field['submit']))
        {
            // var_dump($post_field);
            // return;
            $temp_data = array(
                // 'username'    => $post_field['username'],
                // 'email'       => $post_field['email'],
                'first_name'  => $post_field['first_name'],
                'last_name'   => $post_field['last_name'],
                'website'     => $post_field['website'],
                'updated_at'  => $this->current_datetime,
                'updated_by'  => $this->data['user_data']->id,
            );

            // check rules validation
            $rules = $this->user_model->rules['update'];

            // if password has change
            if ($post_field['password'] != NULL)
            {
                $temp_data['password'] = generate_hash($post_field['password']);
                $rules['password'] = $this->user_model->rules['password_change']['password'];
                $rules['password_confirmation'] = $this->user_model->rules['password_change']['password_confirmation'];
            }

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $message = validation_errors();
                $this->errors = TRUE;
            }

            // only current id can change the name
            // $where_check = array(
            //     'id <> ' => $user_id,
            //     'email' => $data['email']
            // );
            // $data_check = $this->user_model->get_by($where_check, NULL, NULL, TRUE, 'id');
            // if ($data_check)
            // {
            //     $message = 'Email tidak boleh sama!';
            //     $this->errors = TRUE;
            // }

            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            if ($this->errors == FALSE) // if not error update data
            {
                // image
                $this->load->library('upload_file');
                if (isset($_FILES['upload_file']['tmp_name'])) {
                    $config = array(
                        'post_name' => 'upload_file', // name upload field
                        'file_type' => 'image', // type of the file want to upload (image or file)
                        'max_size' => 1000, // maximal size upload
                        'is_csrf_token' => FALSE, // when csrf needed
                        'resize' => TRUE
                    );

                    $images = $this->upload_file->_do_upload($config);
                    if ($images['error'] == false) {
                        $this->errors = FALSE;
                    } else {
                        $this->errors = TRUE;
                        $message = $images['content'];
                        // var_dump($this->errors);
                    }
                }
                // var_dump($this->errors);
                // var_dump($images);
                // var_dump($message);
                // return;

                if ($this->errors == FALSE) {
                    if ($images['content']) {
                        $data['avatar'] = json_encode($images['content']);
                    }

                    if ($this->user_model->update($data, $where)) // update
                    {
                        $message = 'User berhasil dirubah!';
                        $this->errors = FALSE;
                    }
                    else
                    {
                        $message = 'User gagal dirubah. Silahkan ulangi lagi!';
                        $this->errors = TRUE;
                    }
                }
            }
            
            if ($this->errors == FALSE) // success
            {
                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', $message, TRUE);
            }
            else // error
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', $message, TRUE);
                repopulate_post_field($post_field);
            }
            redirect_back();
        }

        $data_check = $this->user_model->get_by($where, NULL, NULL, TRUE, 'id, username, email, first_name, last_name, avatar, role, website, description, facebook, twitter, gplus, instagram, linkedin');

        $this->data['page_title'] = 'Profile';
        $this->breadcrumb->add('Users', $this->uri_1 . '/' . $this->uri_2);
        $this->breadcrumb->add('Profile');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $this->data['data'] = array(
            'all_users' => array(
                'single' => $data_check,
            ),
            'filemanager' => $this->filemanager,
        );
        
        $this->render('pages/profile');
        return;
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');

        $data = array(
            'model' => 'user_model',
            'where' => array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'id, username, first_name, last_name, email, role, description, facebook, twitter, gplus, instagram, linkedin'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->user_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->user_model->get_by($data['where'], NULL, NULL, $data['single'], $data['select']); // tag_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "username" => $record->username,
                        "name" => $record->first_name . ' ' . $record->last_name,
                        "email" => $record->email,
                        "role" => $record->role
                    )
                );
            }

            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            $this->render(); // ajax
        }
    }
}