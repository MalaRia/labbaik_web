<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('category_model'));

        /* generate csrf token */
        $this->data['csrf_token'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );

        $this->errors = FALSE;
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // category
	public function index()
	{
        // $this->breadcrumb->add('Dashboard', 'admin/dashboard', TRUE);
        $this->breadcrumb->add('Categories');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $where = array();
        $select = 'id,name,slug,icon,description,created_at';
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'all_categories' => array(
                'data' => $this->category_model->get_by($where, NULL, NULL, FALSE, $select)
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all')
        );

        $this->render('pages/category');
	}

    /*
    * POST: Create Category
    */
    public function create()
    {
        $post_field = $this->input->post();
        $temp_data = array(
            'name' => $post_field['name'],
            'slug' => url_title($post_field['name'], '-', TRUE),
            'icon' => htmlentities($post_field['icon']),
            // 'type' => $this->category_type,
            'description' => $post_field['description'],
            'parent_id' => $post_field['parent'],
            'created_at' => $this->current_datetime,
            'created_by' => $this->session->userdata('id'),
        );
        // XSS CLEAN
        $data = $this->security->xss_clean($temp_data);

        // check rules validation
        $rules = $this->category_model->rules['create'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error insert data
        {

            if ($this->category_model->insert($data))
            {
                $message = 'Kategori berhasil dibuat!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Kategori gagal dibuat. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // insert
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field); // repopulate posf field
        }
        redirect_back();
    }

    /*
    * POST: Update Category by ID
    */
    public function update()
    {
        // check if empty id
        $cat_id = $this->uri->segment(4);
        if (empty($cat_id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $cat_id,
        );
        $data_check = $this->category_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        // set all posts
        $post_field = $this->input->post();

        $temp_data = array(
            'name' => $post_field['name'],
            // 'slug' => ($post_field['slug']) ? $post_field['slug'] : url_title($post_field['category_name'], '-', TRUE),
            'slug' => url_title($post_field['name'], '-', TRUE),
            'icon' => htmlentities($post_field['icon']),
            // 'type' => $this->category_type,
            'description' => $post_field['description'],
            'parent_id' => $post_field['parent'],
            'updated_at' => $this->current_datetime,
            'updated_by' => $this->session->userdata('id'),
        );
        // XSS CLEAN
        $data = $this->security->xss_clean($temp_data);

        // check rules validation
        $rules = $this->category_model->rules['update'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        // only current id can change the name
        $where_check = array(
            'id <> ' => $cat_id,
            'name' => $data['name']
        );
        $data_check = $this->category_model->get_by($where_check, NULL, NULL, TRUE, 'id');
        if ($data_check)
        {
            $message = 'Nama Kategori tidak boleh sama!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error update data
        {
            // var_dump($data);
            // return;
            if ($this->category_model->update($data, $where)) // update
            {
                $message = 'Kategori berhasil dirubah!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Kategori gagal dirubah. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }
        
        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
            redirect_back();
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field);
            redirect_back();
        }
    }

    /*
    * POST: Delete Category by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $cat_id = $post_field['delete-id'];
        if (empty($cat_id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $cat_id,
        );
        $data_check = $this->category_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->category_model->delete_by($where)) // delete
        {
            $message = 'Kategori berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Kategori gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
            redirect_back();
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            redirect_back();
        }
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');

        $data = array(
            'model' => 'category_model',
            'where' => array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'amp_cat.id, amp_cat.name, amp_cat.icon, amp_cat.description, COUNT(pc.category_id) as count, cat1.name as parent_name, cat1.id as parent_id'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->category_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->category_model->having_posts($data['where'], $data['single'], $data['select']); // category_model

        // var_dump($records);
        // return;
        if ($select)
        {
            echo json_encode($records);
        }
        else
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "name" => $record->name,
                        "icon" => $record->icon,
                        "description" => $record->description,
                        "count" => $record->count,
                        "parent_name" => ($record->parent_id) ? $record->parent_name .'('.$record->parent_id.')' : '-',
                    )
                );
            }

            $results = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            if ($return)
                return $results;
            else
                echo json_encode($results);
        }
    }
}