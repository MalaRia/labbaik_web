<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->helper(array('email','seo'));

        /* get method from uri segment */
        $this->method = $this->uri->segment(3);

        /* generate csrf token */
        $this->data['csrf'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );

        $this->errors = FALSE;
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->method)
        {
            case 'insert':
                $this->index();
                break;

            case 'remove':
                $this->delete();
                break;

            default:
                show_404();
                break;
        }
    }

    // all-pages
	public function index()
	{
        $this->load->library('Upload_file'); // load upload's library
        $config = array(
            'post_name' => 'userfile', // name from upload field
            'file_type' => 'image', // type of the file want to upload (image or file)
            'max_size' => 1000, // maximal size upload
            'allowed_types' => 'jpg|jpeg|png|gif', // file that allowed to upload
            'is_csrf_token' => FALSE, // when csrf needed
            'y_axis' => 100, //  y_axis for image crop (for image only)
            'x_axis' => 100, // x_axis for image crop (for image only)
            'crop' => TRUE, // crop the image (for image only)
            'width' => 200, // width resize or crop image (for image only)
            'height' => 200, // height resize or crop image (for image only)
        );
        $image = $this->upload_file->_do_upload($config);

        $images['csrf'] = $this->data['csrf'];
        $images['error'] = $image['error'];
        $images['content'] = $image['content'];

        echo json_encode($images);
        return;
	}
}