<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('comment_model'));
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'single':
                $this->single();
                break;
            // case 'create':
            //     $this->create();
            //     break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // Comment
	public function index()
	{
        // $this->breadcrumb->add('Dashboard', 'admin/dashboard', TRUE);
        $this->data['page_title'] = 'Comments';
        $this->breadcrumb->add('Comment');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $where = array();
        $select = 'id,name,email,comment,created_at';
        $this->data['data'] = array(
            'action' => array(
                // 'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update/'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'all_comments' => array(
                'data' => $this->comment_model->get_by($where, NULL, NULL, FALSE, $select)
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all'),
            'single' => site_url($this->uri_1 . '/' . $this->uri_2 . '/single?id=')
        );

        $this->render('pages/comment');
	}

    /*
    * POST: Create
    */
    // public function create()
    // {
        // $post_field = $this->input->post();
        // $temp_data = array(
        //     'name' => $post_field['name'],
        //     'slug' => url_title($post_field['name'], '-', TRUE),
        //     'icon' => htmlentities($post_field['icon']),
        //     // 'type' => $this->category_type,
        //     'description' => $post_field['description'],
        //     'parent_id' => $post_field['parent'],
        //     'created_at' => $this->current_datetime,
        //     'created_by' => $this->session->userdata('id'),
        // );
        // // XSS CLEAN
        // $data = $this->security->xss_clean($temp_data);

        // // check rules validation
        // $rules = $this->category_model->rules['create'];
        // $this->form_validation->set_rules($rules);
        // if ($this->form_validation->run() == FALSE)
        // {
        //     $message = validation_errors();
        //     $this->errors = TRUE;
        // }

        // if ($this->errors == FALSE) // if not error insert data
        // {

        //     if ($this->category_model->insert($data))
        //     {
        //         $message = 'Komentar berhasil dibuat!';
        //         $this->errors = FALSE;
        //     }
        //     else
        //     {
        //         $message = 'Komentar gagal dibuat. Silahkan ulangi lagi!';
        //         $this->errors = TRUE;
        //     }
        // }

        // if ($this->errors == FALSE) // insert
        // {
        //     set_flash('class', 'success', TRUE);
        //     set_flash('status', 'success', TRUE);
        //     set_flash('success', $message, TRUE);
        // }
        // else
        // {
        //     set_flash('class', 'danger', TRUE);
        //     set_flash('status', 'errors', TRUE);
        //     set_flash('errors', $message, TRUE);
        //     repopulate_post_field($post_field); // repopulate posf field
        // }
        // redirect_back();
    // }

    /*
    * POST: Update by ID
    */
    public function update()
    {
        // set all posts
        $post_field = $this->input->post();
        // check if empty id
        $id = $this->uri->segment(4);
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->comment_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        $temp_data = array(
            'status' => $post_field['status'],
            'updated_at' => $this->current_datetime,
            'updated_by' => $this->session->userdata('id'),
        );
        // XSS CLEAN
        $data = $this->security->xss_clean($temp_data);

        // check rules validation
        // $rules = $this->comment_model->rules['update']['status'];
        // $this->form_validation->set_rules($rules);
        // if ($this->form_validation->run() == FALSE)
        // {
        //     $message = validation_errors();
        //     $this->errors = TRUE;
        // }

        if ($this->errors == FALSE) // if not error update data
        {
            if ($this->comment_model->update($data, $where)) // update
            {
                $message = 'Komentar berhasil dirubah!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Komentar gagal dirubah. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        // AJAX REQUEST
        if ($this->is_ajax_request)
        {
            $this->data['data'] = array(
                'csrf_token' => $this->data['csrf_token'],
                'status' => $message
            );
            $this->render();
        }
        // else
        // {
        //     if ($this->errors == FALSE) // success
        //     {
        //         set_flash('class', 'success', TRUE);
        //         set_flash('status', 'success', TRUE);
        //         set_flash('success', $message, TRUE);
        //     }
        //     else // error
        //     {
        //         set_flash('class', 'danger', TRUE);
        //         set_flash('status', 'errors', TRUE);
        //         set_flash('errors', $message, TRUE);
        //         repopulate_post_field($post_field);
        //     }
        //     redirect_back();
        // }
    // }
    }

    // GET DETAIL
    public function single()
    {
        // set all get
        $get_field = $this->input->get();
        // check if data exist
        $where = array(
            'id' => $get_field['id'],
        );
        $data_check = $this->comment_model->get_by($where, NULL, NULL, TRUE, 'id, name, email, status, comment');
        if (!$data_check)
            redirect($this->uri_1 . '/' . $this->uri_2);

        $this->data['data'] = array(
            'id' => $data_check->id,
            'name' => $data_check->name,
            'email' => $data_check->email,
            'status' => $data_check->status,
            'comment' => $data_check->comment,
        );
        $this->render();
    }

    /*
    * POST: Delete Category by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->comment_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->comment_model->delete_by($where)) // delete
        {
            $message = 'Komentar berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Komentar gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
        }
        redirect_back();
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');

        $data = array(
            'model' => 'comment_model',
            'where' => array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'amp_co.id, amp_co.name, amp_co.email, amp_co.status, po.title as article'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->comment_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->comment_model->in_post($data['where'], $data['single'], $data['select']); // category_model

        if ($select) // custom select
        {
            $this->data['data'] = $records;
            $this->render(); // ajax
        }
        else // datatable
        {
            $record_data = array();
            foreach ($records as $record)
            {
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "name" => $record->name,
                        "email" => $record->email,
                        "article" => $record->article,
                        "status" => $record->status,
                    )
                );
            }

            $this->data['data'] = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );

            $this->render(); // ajax
        }
    }
}