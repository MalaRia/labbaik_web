<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends Backend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('widget_model','post_model'));

        /* generate csrf token */
        $this->data['csrf_token'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );
        $this->filemanager = array(
            'ckeditor' => base_url('templates/assets/filemanager/dialog.php?type=1&editor=ckeditor&relative_url=0&fldr=images&akey=masnat'),
            'standalone' => base_url('templates/assets/filemanager/dialog.php?type=2&field_id=fieldID4&fldr=&relative_url=1&akey=masnat')
        );
        $this->errors = FALSE;
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->uri_3)
        {
            case 'all':
                $this->all();
                break;
            case 'create':
                $this->create();
                break;
            case 'single':
                $this->single();
                break;
            case 'update':
                $this->update();
                break;
            case 'delete':
                $this->delete();
                break;

            default:
                $this->index();
                break;
        }
    }

    // Slider
	public function index()
	{
        $this->breadcrumb->add('Slider');
        $this->data['breadcrumb'] = $this->breadcrumb->output();
        $where = array();
        $select = 'id,title';
        $this->data['data'] = array(
            'action' => array(
                'create' => site_url($this->uri_1 . '/' . $this->uri_2 . '/create'),
                'update' => site_url($this->uri_1 . '/' . $this->uri_2 . '/update'),
                'delete' => site_url($this->uri_1 . '/' . $this->uri_2 . '/delete')
            ),
            'lists' => site_url($this->uri_1 . '/' . $this->uri_2 . '/all?type=slider'),
            'single' => site_url($this->uri_1 . '/' . $this->uri_2 . '/single?id='),
            'filemanager' => $this->filemanager,
        );

        $this->render('pages/slider');
	}

    /*
    * POST: Create Slider
    */
    public function create()
    {
        $post_field = $this->input->post();
        
        // check rules validation
        $rules = $this->widget_model->rules['slider']['create'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error insert data
        {
            // image
            $image = NULL;
            if (!empty($post_field['image_big'])) {
                $image = array(
                    'image' => $post_field['image_big'],
                    'thumb' => $post_field['image_thumb'],
                    'title' => $post_field['image_title'],
                    'alt' => $post_field['image_alt']
                );
                // $image = json_encode($image);
            }

            $temp_featured = array(
                'title' => $post_field['title'],
                'image' => $image,
            );

            // check order
            // $this->widget_model->get_by

            $temp_data = array(
                'content' => json_encode($temp_featured),
                'additional' => $post_field['order'],
                'type' => 'slider',
                'created_at' => $this->current_datetime,
                'created_by' => $this->session->userdata('id'),
            );

            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            if ($this->widget_model->insert($data))
            {
                $message = 'Slider berhasil dibuat!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Slider gagal dibuat. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // insert
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field); // repopulate posf field
        }
        redirect_back();
    }

    /*
    * GET: Single Ads by ID
    */
    // public function single()
    // {
    //     // check if empty id
    //     $id = $this->input->get('id');
    //     if (empty($id))
    //         redirect_back();

    //     // check if data exist
    //     $where = array(
    //         'id' => $id,
    //     );
    //     $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id,content');
    //     if (!$data_check)
    //         redirect_back();

    //     $content = json_decode($data_check->content);
    //     $data = array(
    //         'id' => $data_check->id,
    //         'name' => $content->name,
    //         'name_id' => $content->name_id,
    //         'post_id' => $content->post_id,
    //     );
    //     echo json_encode($data);
    // }

    /*
    * POST: Update Featured Post by ID
    */
    public function update()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['slider_id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        // check rules validation
        $rules = $this->widget_model->rules['slider']['update'];
        $this->form_validation->set_rules($rules);
        if ($this->form_validation->run() == FALSE)
        {
            $message = validation_errors();
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // if not error insert data
        {
            // image
            $image = NULL;
            if (!empty($post_field['image_big'])) {
                $image = array(
                    'image' => $post_field['image_big'],
                    'thumb' => $post_field['image_thumb'],
                    'title' => $post_field['image_title'],
                    'alt' => $post_field['image_alt']
                );
                // $image = json_encode($image);
            }

            $temp_featured = array(
                'title' => $post_field['title'],
                'image' => $image,
            );

            // check order
            // $this->widget_model->get_by

            $temp_data = array(
                'content' => json_encode($temp_featured),
                'additional' => $post_field['order'],
                'type' => 'slider',
                'created_at' => $this->current_datetime,
                'created_by' => $this->session->userdata('id'),
            );

            // XSS CLEAN
            $data = $this->security->xss_clean($temp_data);

            if ($this->widget_model->update($data, $where))
            {
                $message = 'Slider berhasil dirubah!';
                $this->errors = FALSE;
            }
            else
            {
                $message = 'Slider gagal dirubah. Silahkan ulangi lagi!';
                $this->errors = TRUE;
            }
        }

        if ($this->errors == FALSE) // insert
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
        }
        else
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            repopulate_post_field($post_field); // repopulate posf field
        }
        redirect_back();
    }

    /*
    * POST: Delete Featured POST by ID
    */
    public function delete()
    {
        // set all posts
        $post_field = $this->input->post();

        // check if empty id
        $id = $post_field['delete-id'];
        if (empty($id))
            redirect_back();

        // check if data exist
        $where = array(
            'id' => $id,
        );
        $data_check = $this->widget_model->get_by($where, NULL, NULL, TRUE, 'id');
        if (!$data_check)
            redirect_back();

        if ($this->widget_model->delete_by($where)) // delete
        {
            $message = 'Slider berhasil dihapus!';
            $this->errors = FALSE;
        }
        else
        {
            $message = 'Slider gagal dihapus. Silahkan ulangi lagi!';
            $this->errors = TRUE;
        }

        if ($this->errors == FALSE) // success
        {
            set_flash('class', 'success', TRUE);
            set_flash('status', 'success', TRUE);
            set_flash('success', $message, TRUE);
            redirect_back();
        }
        else // error
        {
            set_flash('class', 'danger', TRUE);
            set_flash('status', 'errors', TRUE);
            set_flash('errors', $message, TRUE);
            redirect_back();
        }
    }

    public function all($return = FALSE)
    {
        $field = $this->input->get('field');
        $select = $this->input->get('select');
        $search = $this->input->get('search');
        $type = $this->input->get('type');

        $data = array(
            'model' => 'widget_model',
            'where' => ($type != NULL) ? array('type' => $type) : array(),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => ($field) ? $field : 'id,content,additional'
        );

        $data['where_total'] = array();
        $recordsTotal = $this->widget_model->count_post($data['where_total'],''); // my_model
        $recordsFiltered = $recordsTotal;

        $records = $this->widget_model->get_by($data['where'], NULL, NULL,$data['single'], $data['select']); // category_model

        if ($select)
        {
            echo json_encode($records);
        }
        else
        {
            $record_data = array();
            foreach ($records as $record)
            {
                $content = json_decode($record->content);
                $image = $content->image;
                array_push($record_data, 
                    array(
                        "id" => $record->id,
                        "title" => (isset($content->title)) ? $content->title : '',
                        "image" => (isset($image)) ? $image : '',
                        "order" => (isset($record->additional)) ? $record->additional : '',
                        // "count" => $record->count
                    )
                );
            }

            $results = array(
                'draw' => 2,
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsFiltered,
                'data' => $record_data
            );
            if ($return)
                return $results;
            else
                echo json_encode($results);
        }
    }
}