<?php
defined('BASEPATH') OR exit('No direct access script allowed');

class Auth extends Auth_controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('user_model');

        $this->method = $this->uri->segment(3);

        /* generate csrf token */
        $this->data['csrf_token'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        );

        $this->errors = FALSE;
    }

    /* Remap all uri
     * 
     * @return  string $method process method
     */
    public function _remap()
    {
        switch($this->method)
        {
            case 'login':
                $this->index();
                break;
 
            case 'logout':
                $this->logout();
                break;

            default:
                show_404();
                break;
        }
    }


    /**
     * Show admin login form
     *
     */
    public function index()
    {
        $post_field = $this->input->post();

        if (isset($post_field['submit']))
        {
            // var_dump($post_field);
            // return;
            // check rules validation
            $rules = $this->user_model->rules['login'];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() == FALSE)
            {
                $this->errors = TRUE;
                // echo 'test';
            }

            if ($this->errors == FALSE)
            {
                // check field will be use.
                if (!strpos($post_field['username'], '@') > 0)
                {// if username post
                    $where = array (
                        'username' => $post_field['username'],
                    );
                }
                else
                {// if email post
                    if (valid_email($post_field['username'])) 
                    { // check valid email
                        $where = array (
                            'email' => $post_field['username'],
                        );           
                    }
                }
            }


            // if ($this->errors == FALSE)
            // {
                // $where['role'] = 'administrator';
                /* get by username then compare the password */
                $user_data = $this->user_model->get_by($where, NULL, NULL, TRUE);
                // var_dump($user_data);
                // return;
                if ($user_data) 
                {
                    if (verify_hash($post_field['password'], $user_data->password)) { // success
                        // echo $user_data->password;
                        $this->errors = FALSE;
                    } 
                    else 
                    { // wrong password
                        $this->errors = FALSE;
                    }
                }   
                else
                { // wrong user
                    $this->errors = TRUE;
                }
            // }

            // Check not errors
            if ($this->errors == FALSE)
            {
                $this->session->set_userdata(
                    array(
                        'logged_in' => TRUE,
                        'backend_privilege' => TRUE,
                        'id' => $user_data->id,
                        'role' => $user_data->role, 
                    )
                );

                set_flash('class', 'success', TRUE);
                set_flash('status', 'success', TRUE);
                set_flash('success', 'Login berhasil!', TRUE, ''); // set error message
                redirect('admin/dashboard');
            }
            else
            {
                set_flash('class', 'danger', TRUE);
                set_flash('status', 'errors', TRUE);
                set_flash('errors', 'Username atau Password salah!', TRUE);
                repopulate_post_field($post_field);
                redirect_back();
            }
        }
        else
        {
            $this->render('login', NULL);
        }
    }

    /**
     * Destroy all admin session 
     * 
     */
    public function logout()
    {
        $session_data = array('logged_in', 'backend_privilege', 'id', 'role');
        $this->session->unset_userdata($session_data);
        set_flash('success', 'Logout berhasil!', TRUE, ''); // set error message
        redirect('admin/auth/login');
    }

}