<?php
// header('Content-type: text/xml');

defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends Frontend_controller {

	public function __construct()
    {
        parent::__construct();

        $this->load->model(array('page_model','post_model'));
        $this->load->helper(array('time_helper'));

        // load page
        $order = array (
            'order_by' => 'ID',
            'order_by_type' => 'ASC'
        );
        $this->data['pages'] = $this->page_model->get_order(array('status' => 'publish'), NULL, NULL, FALSE, 'title,slug', $order);

        // Insert Visitor
        $this->insert_visitor($this->uri_2);
    }

    public function index()
    {
        $this->output->set_content_type('text/xml');
        
        $data = array(
            'where' => array('amp_post.published_at <=' => current_datetime(), 'amp_post.status <>' => 'draft'),
            'limit' => NULL,
            'offset' => NULL,
            'single' => FALSE,
            'select' => 'amp_post.title,amp_post.slug,amp_post.excerpt,p.slug as p_slug, u.first_name, u.last_name,amp_post.published_at'
        );
        $feeds = $this->post_model->get_by_user($data['where'], $data['limit'], $data['offset'], $data['single'], $data['select']); // post_model
        $this->data['data'] = array(
            'encoding' => 'utf-8',
            'feed_name' => $this->data['web_info']->site_title,
            'feed_url' => current_url(),
            'page_description' => $this->data['web_info']->site_description,
            'page_language' => 'ID-id',
            'creator_email' => $this->data['web_info']->site_email,
            'posts' => $feeds
        );

        $this->load->view('frontend/rss', $this->data['data']);
    }

    public function robots()
    {
        $this->output->set_content_type('text/plain');

        $this->data['data'] = array(
            'User-Agent' => '*',
            'Disallow' => '',
            'Sitemap' => site_url('sitemap.xml'),
            // 'RSS' => site_url('rss.xml'),
        );

        $this->load->view('frontend/robots', $this->data);
    }

    public function sitemap()
    {
        $this->output->set_content_type('text/xml');

        $this->data['data'] = array(
            'encoding' => 'utf-8',
            'site_url' => site_url(),
            'sitemap' => $this->data['pages']
        );

        $this->load->view('frontend/sitemap', $this->data['data']);
    }
}