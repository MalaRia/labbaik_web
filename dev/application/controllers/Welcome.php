<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// $this->load->helper('form');
		// var_dump($this->input->post());
		// echo "tes";
		// var_dump($this->session->all_userdata());
		// echo "</pre>";
		// $this->load->view('welcome_message');
		// echo base_url();
		// print_r($this->data);
		// $this->site->view('index');
		$this->data['page_title'] = 'Ini Page title lho';
		$this->data['isi'] = 'Bagaimana caranya';
		// $this->data['content'] = 'content2';
		$this->render('content2');
	}

	public function bench()
	{
		$this->benchmark->mark('dog');

		$c = "";
		// Some code happens here
		for ($i = 0; $i < 1000; $i++)
		{
			if ($i == 1000 - 1)
				$c = "cat";
			$this->benchmark->mark($c);
		}

		// $this->benchmark->mark($c);

		// More code happens here

		$this->benchmark->mark('bird');

		echo $this->benchmark->elapsed_time('dog', $c);
		echo "<br>";
		echo $this->benchmark->elapsed_time($c, 'bird');
		echo "<br>";
		echo $this->benchmark->elapsed_time('dog', 'bird');
	}

	// public function upl()
	// {
	// 	echo "<form action='http://localhost/experimental/CMS-Exp-CI/welcome/' method='POST' enctype='multipart/form-data'>
	// 		<input type='file' name='upload-file'>
	// 		<input type='text' name='".$this->security->get_csrf_token_name()."' value='".$this->security->get_csrf_hash()."'>
	// 		<input type='submit'>
	// 	</form> <a href='http://localhost/experimental/CMS-Exp-CI/welcome/'> link</a>";
	// }

	function redirect_app(){
		// Include and instantiate the class.
		require_once 'Mobile_Detect.php';
		$detect = new Mobile_Detect;
		// Check for a specific platform with the help of the magic methods:
		if( $detect->isiOS() ){
			header("Location: http://bit.ly/labbaik-ios");
		}else if( $detect->isAndroidOS() ){
			header("Location: http://bit.ly/labbaik-android");
		}else{
			header("Location: http://labbaik.id");
		}
	}
}


