<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends Frontend_controller {

	public function __construct() {
		parent::__construct();

		$this->load->model(array('page_model','post_model','user_model','category_model','post_category_model'));
		$this->load->helper(array('time_helper'));

		/* generate csrf token */
		$this->data['csrf_token'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash(),
		);

		// load page
		$order = array (
			'order_by'      => 'ID',
			'order_by_type' => 'ASC'
		);
		$this->data['pages'] = $this->page_model->get_order(array('status' => 'publish'), NULL, NULL, FALSE, 'id,title,slug', $order);

		// Insert Visitor
		$this->insert_visitor($this->uri_2);

		$this->errors = FALSE;
	}

	/* Remap all uri
	 * 
	 * @return  string $method process method
	 */
	public function _remap() {
		switch($this->uri_1)
		{
			case 'index':
				$this->index();
				break;
			case 'blog':
				$this->blog();
				break;
			/*case 'page':
				$this->page();
				break;*/
			case 'about-me':
				$this->about_me();
				break;
			/*case 'contact':
				$this->contact();
				break;*/
			case 'schedule':
				$this->schedule();
				break;
			case 'album':
				$this->album();
				break;
			case 'preview':
				$this->preview();
				break;
			default:
				$this->index();
				break;
		}
	}

	/* Index */
	public function index() {

		if ($this->uri_1 !== 'admin') {

			/* Search Page */
			if (!empty($this->input->get('search'))) {

				if ( $this->uri_1 == 'page' ) {
					if (!empty($this->uri_2) && is_numeric($this->uri_2)) {
						$this->_search($this->input->get('search'), $this->uri_2);
					} else {
						$this->_404();
					}
				} else {
					$this->_search($this->input->get('search'));
				}
				return;
			}

			if (!empty($this->uri_1)) {

				/* Index Paged */
				if ( $this->uri_1 == 'page' ) {
					if (!empty($this->uri_2) && is_numeric($this->uri_2)) {
						$this->_archive('', $this->uri_2);
					} else {
						$this->_404();
					}
					return;
				}

				/* Author Page */
				if ( $this->uri_1 == 'author' && !empty($this->uri_2) ) {
					if ( $this->uri_3 == 'page' ) {
						if (!empty($this->uri_4) && is_numeric($this->uri_4)) {
							$this->_author($this->uri_2, $this->uri_4);
						} else {
							$this->_404();
						}
					} else {
						$this->_author($this->uri_2);
					}
					return;
				}

				/* Authors Page */
				if ( $this->uri_1 == 'authors' ) {
					if ( $this->uri_2 == 'page' ) {
						if (!empty($this->uri_3) && is_numeric($this->uri_3)) {
							$this->_authors($this->uri_3);
						} else {
							$this->_404();
						}
					} else {
						$this->_authors();
					}
					return;
				}

				/* Contact Page */
				if ( ($this->uri_1 == 'contact' || $this->uri_1 == 'contact-us') && empty($this->uri_2) ) {
					$this->_contact($this->uri_1);
					return;
				}

				// API
				// if ($this->uri_1 == 'api') {
				// 	$this->api($this->uri_2);
				// 	return;
				// }

				/* Page / Archive / Post */
				$page_now = $this->_conditional($this->uri_1);
				if ($page_now == 'page') {

					$this->_page($this->uri_1);
				} elseif ($page_now == 'category') {
					
					$this->_category($this->uri_1, $this->uri_3);
				} elseif ($page_now == 'archive') {

					/* Archive Paged */
					if ( $this->uri_2 == 'page' ) {
						if (!empty($this->uri_3) && is_numeric($this->uri_3)) {
							$this->_archive($this->uri_1,$this->uri_3);
						} else {
							$this->_404();
						}
					} else {
						$this->_archive($this->uri_1);
					}
				} elseif ($page_now == 'post') {

					$this->_single($this->uri_1);
				} else {

					$this->_404();
				}
			} else {

				$this->homepage();
			}
		}
	}

	/* Home Page */
	public function homepage() {

		/* Featured Post */
		$this->load->model(array('widget_model'));
		$feat_a   = $this->widget_model->get();
		$feat_all = array();
		for ($i=0; $i < count($feat_a); $i++) { 
			$get_page  = json_decode($feat_a[$i]->content);
			$list_page = $this->page_model->get_by(array('title' => $get_page->name),1, NULL, TRUE, 'id,title,slug');
			if (!empty($list_page)) {
				$list_post = $this->post_model->get_by(array('page_id' => $list_page->id), NULL, NULL, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
				$feat_all[$i] = array(
					'page' => array(
						'id'    => $list_page->id,
						'title' => $list_page->title,
						'slug'  => $list_page->slug
					),
					'post' => $list_post
				);
			}
		}

		/* Data Storing */
		$this->data['data']['current']        = 'home';
		$this->data['data']['header_title']   = 'All Stories';
		$this->data['data']['user']           = $this->_user();
		$this->data['data']['recent_posts']   = $this->post_model->get_by(array('status' => 'publish'), 6, NULL, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
		$this->data['data']['featured_posts'] = array_reverse(array_filter($feat_all));
		$this->data['data']['pagination']     = $this->_paginasi( site_url('page/'), site_url(), $this->post_model->count_post(array('status' => 'publish'), 'id') );
		$this->data['data']['jumbotron']      = TRUE;
		$this->data['data']['recent_posts_jumbotron']  = $this->post_model->get_by(array('status' => 'publish', 'page_id' => 2), NULL, NULL, TRUE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
		$this->data['data']['featured_image'] = $this->data['web_info']->image->image;
		$this->data['data']['video_page']     = TRUE;

		/* Meta - Site Info */
		$meta_data = array(
			'title'        => $this->data['web_info']->site_title,
			'description'  => $this->data['web_info']->site_description,
			'keywords'     => $this->data['web_info']->site_description,
			'published_at' => '',
			'image'        => $this->data['web_info']->logo->image,
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('home');
	}

	/* Archive Page */
	public function _archive($slug='', $paged='') {

		if (!empty($slug)) {

			/* Variables */
			$var_page     = $this->page_model->get_by(array('slug' => $slug), NULL, NULL, TRUE, 'id,title,slug,content,seo,published_at');
			$page_id      = $var_page->id;
			$page_title   = $var_page->title;
			$page_content = $var_page->content;
			$offs         = (!empty($paged)) ? 6*($paged-1) : 0;
			$recent_posts = $this->post_model->get_by(array('status' => 'publish','page_id' => $page_id), 6, $offs, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
			
			/* No Data */
			if (empty($recent_posts)) {
				$this->_404();
				return;
			}

			/* Data Storing */
			$this->data['data']['current']         = $slug;
			$this->data['data']['header_title']    = $page_title;
			$this->data['data']['header_subtitle'] = $page_content;
			$this->data['data']['user']            = $this->_user();
			$this->data['data']['recent_posts']    = $recent_posts;
			$this->data['data']['pagination']      = $this->_paginasi( site_url($slug.'/page/'), site_url($slug), $this->post_model->count_post(array('status' => 'publish','page_id' => $page_id), 'id'));
			$this->data['data']['jumbotron']       = FALSE;

			/* Meta - Site Info */
			$current = (!empty($paged)) ? 'Page '.$paged.' &#8211; ' : '';
			$seo     = json_decode($var_page->seo);
			$meta_data = array(
				'title'        => (!empty($seo->title)) ? $seo->title : $page_title . ' &#8211; ' . $current . $this->data['web_info']->site_title,
				'description'  => (!empty($seo->description)) ? strip_tags($seo->description) : $this->data['web_info']->site_description,
				'keywords'     => (!empty($seo->keywords)) ? $seo->keywords : $this->data['web_info']->site_description,
				'published_at' => '',
				'image'        => (!empty($var_page->image)) ? json_decode($var_page->image)->image : $this->data['web_info']->logo->image,
			);
			$this->data['data']['meta'] = $this->meta_tag($meta_data);


		} else {

			/* Index Paged */

			/* Variables */
			$offs = (!empty($paged)) ? 6*($paged-1) : 0;
			$recent_posts = $this->post_model->get_by(array('status' => 'publish'), 6, $offs, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
			$recent_posts_jumbotron = $this->post_model->get_by(array('status' => 'publish', 'page_id' => 2), NULL, NULL, TRUE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
			
			/* No Data */
			if (empty($recent_posts)) {
				$this->_404();
				return;
			}

			/* Data Storing */
			$this->data['data']['current']        = 'home';
			$this->data['data']['header_title']   = 'All Stories';
			$this->data['data']['user']           = $this->_user();
			$this->data['data']['recent_posts']   = $recent_posts;
			$this->data['data']['pagination']     = $this->_paginasi( site_url('page/'), site_url(), $this->post_model->count_post(array('status' => 'publish'), 'id') );
			$this->data['data']['jumbotron']      = TRUE;
			$this->data['data']['recent_posts_jumbotron']   = $recent_posts_jumbotron;
			$this->data['data']['featured_image'] = $this->data['web_info']->image->image;

			/* Meta - Site Info */
			$meta_data = array(
				'title'        => 'Page '.$paged.' &#8211; ' . $this->data['web_info']->site_title,
				'description'  => $this->data['web_info']->site_description,
				'keywords'     => $this->data['web_info']->site_description,
				'published_at' => '',
				'image'        => $this->data['web_info']->logo->image,
			);
			$this->data['data']['meta'] = $this->meta_tag($meta_data);
		}

		$this->render('archive');
	}

	/* Author Page */
	public function _author($name='', $paged='') {

		$this->load->model(array('post_comment_model'));

		/* Author Detail */
		$var_author = $this->_user($name);
		$auth_id    = $var_author->id;
		$auth_fname = $var_author->first_name;
		$auth_lname = $var_author->last_name;

		/* Variables */
		$offs         = (!empty($paged)) ? 6*($paged-1) : 0;
		$recent_posts = $this->post_model->get_by(array('status' => 'publish','created_by' => $auth_id), 6, $offs, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by,comment');
		
		/* No Data */
		if (empty($recent_posts)) {
			$this->_404();
		}

		/* Data Storing */
		$this->data['data']['current']      = 'author';
		$this->data['data']['title']        = ucwords($auth_fname . ' ' . $auth_lname);
		$this->data['data']['total']        = $this->post_model->count_post(array('status' => 'publish','created_by' => $auth_id), 'id');
		$this->data['data']['comment']      = $this->post_comment_model;
		$this->data['data']['author']       = $var_author;
		$this->data['data']['recent_posts'] = $recent_posts;
		$this->data['data']['pagination']   = $this->_paginasi( site_url('author/'.$name.'/page/'), site_url('author/'.$name), $this->data['data']['total']);
		
		/* Meta - Site Info */
		$current = (!empty($paged)) ? 'Page '.$paged.' &#8211; ' : '';
		$meta_data = array(
			'title'        => ucwords($auth_fname . ' ' . $auth_lname) . ' &#8211; ' . $current . $this->data['web_info']->site_title,
			'description'  => $this->data['web_info']->site_description,
			'keywords'     => $this->data['web_info']->site_description,
			'published_at' => '',
			'image'        => $this->data['web_info']->logo->image,
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('author');
	}

	/* Authors Page */
	public function _authors($paged='') {

		/* Check If has Post */
		$var_author  = $this->user_model->get('', FALSE);
		$var_authors = array();
		for ($i=0; $i < count($var_author); $i++) { 
			$author_id    = $var_author[$i]->id;
			$author_posts = $this->post_model->get_by(array('status' => 'publish','created_by' => $author_id), NULL, NULL, FALSE, 'id');
			if (!empty($author_posts)) {
				$var_authors[] = $this->_user($var_author[$i]->username);
			}
		}

		/* Variable */
		$offs        = (!empty($paged)) ? 6*($paged-1) : 0;
		$var_authors = array_slice($var_authors, $offs, 6);
		$var_page    = $this->page_model->get_by(array('slug' => 'authors'), NULL, NULL, TRUE, 'id,title,seo');

		/* Data Storing */
		$this->data['data']['current']      = 'authors';
		$this->data['data']['header_title'] = 'Authors';
		$this->data['data']['list_author']  = $var_authors;
		$this->data['data']['pagination']   = $this->_paginasi( site_url('page/'), site_url(), $this->post_model->count_post(array('status' => 'publish'), 'id') );

		/* Meta - Site Info */
		$current = (!empty($paged)) ? 'Page '.$paged.' &#8211; ' : '';
		$seo     = json_decode($var_page->seo);
		$meta_data = array(
			'title'        => (!empty($seo->title)) ? $seo->title : 'Authors '.$current.' &#8211; ' . $this->data['web_info']->site_title,
			'description'  => (!empty($seo->description)) ? strip_tags($seo->description) : $this->data['web_info']->site_description,
			'keywords'     => (!empty($seo->keywords)) ? $seo->keywords : $this->data['web_info']->site_description,
			'published_at' => '',
			'image'        => (!empty($var_page->image)) ? json_decode($var_page->image)->image : $this->data['web_info']->logo->image,
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('authors');
	}

	/* Search Page */
	public function _search($query='', $paged='') {

		/* Variables */
		$offs         = (!empty($paged)) ? 6*($paged-1) : 0;
		$recent_posts = $this->post_model->get_search($query, 6, $offs, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
		$total_posts  = count($this->post_model->get_search($query, 0, 0, FALSE, 'id,title,image,slug,excerpt,additional,published_at,created_by'));
		
		/* No Data */
		if (empty($recent_posts)) {
			$this->_404();
			return;
		}

		/* Data Storing */
		$this->data['data']['current']        = 'search';
		$this->data['data']['header_title']   = 'Search results for: ' . ucwords($query);
		$this->data['data']['user']           = $this->_user();
		$this->data['data']['recent_posts']   = $recent_posts;
		$this->data['data']['pagination']     = $this->_paginasi( site_url('/page/'), site_url('?search='.urlencode($query)), $total_posts, TRUE, '/?search='.urlencode($query));
		$this->data['data']['jumbotron']      = TRUE;
		$this->data['data']['recent_posts_jumbotron']  = $this->post_model->get_by(array('status' => 'publish', 'page_id' => 2), NULL, NULL, TRUE, 'id,title,image,slug,excerpt,additional,published_at,created_by');
		$this->data['data']['featured_image'] = $this->data['web_info']->image->image;

		/* Meta - Site Info */
		$current = (!empty($paged)) ? 'Page '.$paged.' &#8211; ' : '';
		$meta_data = array(
			'title'        => 'Search Results for &#8220;' . ucwords($query) . '&#8221; &#8211; ' . $current . $this->data['web_info']->site_title,
			'description'  => $this->data['web_info']->site_description,
			'keywords'     => $this->data['web_info']->site_description,
			'published_at' => '',
			'image'        => $this->data['web_info']->logo->image,
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('search');
	}

	/* Single Post */
	public function _single($slug) {

		$this->load->model(array('post_comment_model','comment_model'));
		$this->load->library('form_validation');

		/* Get Post Detail */
		$var_post = $this->post_model->get_by(array('status' => 'publish','slug' => $slug), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,clap,type,comment,published_at,created_by');

		/* Clap Button */
		if ($this->uri_2 == 'clap' && is_numeric($this->uri_3)) {
			$current_clap = $var_post->clap;
			$count_clap   = $current_clap+1;
			$plus_clap    = $this->post_model->update(array('clap'=>$count_clap), array('id' => $var_post->id));
			if ($plus_clap) {
				$return = json_encode(array('status' => true, 'message' => $count_clap));
			} else {
				$return = json_encode(array('status' => false, 'message' => $current_clap));
			}
			echo $return;
			return;
		}

		$this->data['data']['claps_ids'] = '0';
		$get_clap_cookies = $this->input->cookie('wp_claps_applause_ids', TRUE);
		$var_clap_cookies = (strpos($get_clap_cookies, ',') !== false) ? explode(',', $get_clap_cookies) : $get_clap_cookies;
		if (is_array($var_clap_cookies)) {
			for ($ico=0; $ico < count($var_clap_cookies); $ico++) {
				$clapid = str_replace('post', '', $var_clap_cookies[$ico]);
				if ($clapid == $var_post->id) {
					$this->data['data']['claps_ids'] = '1';
				}
			}
		} else {
			$clapid = str_replace('post', '', $var_clap_cookies);
			if ($clapid == $var_post->id) {
				$this->data['data']['claps_ids'] = '1';
			}
		}

		/* Related Post */
		$related_post = $this->post_model->get_by(array('page_id' => $var_post->page_id), 3, NULL, FALSE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,type,comment,published_at,created_by');
		
		/* Author Detail */
		$var_author   = $this->user_model->get_by(array('id' => $var_post->created_by), NULL, NULL, TRUE, 'id, username, first_name, last_name, avatar, description');
		
		/* Get Page/Category */
		$var_cat      = $this->category_model->have_posts(array('post_id' => $var_post->id), NULL, NULL, TRUE, 'amp_cat.id,amp_cat.name,amp_cat.slug');
		
		/* Next Prev */
		$prev_id      = $var_post->id+1;
		$next_id      = $var_post->id-1;
		$prev_post    = $this->post_model->get_by(array('id' => $prev_id), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,type,comment,published_at,created_by');
		$next_post    = ($next_id > 0) ? $this->post_model->get_by(array('id' => $next_id), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,type,comment,published_at,created_by') : '';

		/* Data Storing */
		$this->data['data']['current']       = $get_clap_cookies;
		$this->data['data']['detail']        = $var_post;
		$this->data['data']['author']        = $var_author;
		$this->data['data']['category']      = $var_cat;
		$this->data['data']['comments']      = $this->comment_model->comment_by_post_id(array('po.id' => $var_post->id, 'co.status' => 'accepted'), NULL, NULL, FALSE, 'amp_co.id as c_id,amp_co.name as c_name,amp_co.comment as c_comment,amp_co.created_at as c_created_at');
		$this->data['data']['count_comment'] = count($this->data['data']['comments']);
		$this->data['data']['prev_post']     = $prev_post;
		$this->data['data']['next_post']     = $next_post;
		$this->data['data']['related_post']  = $related_post;
		$this->data['data']['user']          = $this->_user();

		// var_dump($this->data['data']['category']);
		// return;

		$post_field = $this->input->post();
		if (isset($post_field['submit']) && $var_post->comment == 1) {
			$temp_data = array(
				'name'       => $post_field['name'],
				'email'      => $post_field['email'],
				'comment'    => $post_field['comment'],
				'url'        => $post_field['url'],
				'created_at' => $this->current_datetime,
				'created_by' => $post_field['email'],
			);
			$data = $this->security->xss_clean($temp_data);

			$rules = $this->comment_model->rules['create'];
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data['data']['comment_message'] = array('status' => 'error', 'message' => validation_errors());
				$this->errors = TRUE;
			}

			if ($this->errors == FALSE) {
				$comment_id = $this->comment_model->insert($data);
				if ($comment_id) {
					$this->post_comment_model->insert(array('post_id' => $var_post->id, 'comment_id' => $comment_id));
					$this->data['data']['comment_message'] = array('status' => 'success', 'message' => '<p>Your comment is awaiting moderation</p>');
					$this->errors = FALSE;
				} else {
					$this->data['data']['comment_message'] = array('status' => 'error', 'message' => '<p>ERROR. Please try again!</p>');
					$this->errors = TRUE;
				}
			}

			if ($this->errors == FALSE) {
				set_flash('class', 'success', TRUE);
				set_flash('status', 'success', TRUE);
				set_flash('success', $this->data['data']['comment_message'], TRUE);
			} else {
				set_flash('class', 'danger', TRUE);
				set_flash('status', 'errors', TRUE);
				set_flash('errors', $this->data['data']['comment_message'], TRUE);
				repopulate_post_field($post_field);
			}
		}

		/* Meta - Site Info */
		$seo = json_decode($var_post->seo);
		$meta_data = array(
			'title'        => (!empty($seo->title)) ? $seo->title : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'description'  => (!empty($seo->description)) ? strip_tags($seo->description) : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'keywords'     => (!empty($seo->keywords)) ? $seo->keywords : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'published_at' => '',
			'image'        => (!empty($var_post->image)) ? json_decode($var_post->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('single');
	}

	/* Single Page */
	public function _page($slug) {

		/* Get Post Detail */
		$var_post = $this->page_model->get_by(array('status' => 'publish','slug' => $slug), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,parent_id,image,seo,clap,comment,published_at,created_by');

		/* Clap Button */
		if ($this->uri_2 == 'clap' && is_numeric($this->uri_3)) {
			$current_clap = $var_post->clap;
			$count_clap   = $current_clap+1;
			$plus_clap    = $this->page_model->update(array('clap'=>$count_clap), array('id' => $var_post->id));
			if ($plus_clap) {
				$return = json_encode(array('status' => true, 'message' => $count_clap));
			} else {
				$return = json_encode(array('status' => false, 'message' => $current_clap));
			}
			echo $return;
			return;
		}

		$this->data['data']['claps_ids'] = '0';
		$get_clap_cookies = $this->input->cookie('wp_claps_applause_ids', TRUE);
		$var_clap_cookies = (strpos($get_clap_cookies, ',') !== false) ? explode(',', $get_clap_cookies) : $get_clap_cookies;
		if (is_array($var_clap_cookies)) {
			for ($ico=0; $ico < count($var_clap_cookies); $ico++) {
				$clapid = str_replace('page', '', $var_clap_cookies[$ico]);
				if ($clapid == $var_post->id) {
					$this->data['data']['claps_ids'] = '1';
				}
			}
		} else {
			$clapid = str_replace('page', '', $var_clap_cookies);
			if ($clapid == $var_post->id) {
				$this->data['data']['claps_ids'] = '1';
			}
		}

		/* Related Post */
		$related_post = $this->page_model->get_by(array('status' => 'publish', 'slug <>' => $slug), 3, NULL, FALSE, 'id,title,content,slug,excerpt,parent_id,image,seo,clap,comment,published_at,created_by');
		
		/* Author Detail */
		$var_author   = $this->user_model->get_by(array('id' => $var_post->created_by), NULL, NULL, TRUE, 'id, username, first_name, last_name, avatar, description');

		/* Next Prev */
		$prev_id      = $var_post->id-1;
		$next_id      = $var_post->id+1;
		$prev_post    = $this->page_model->get_by(array('id' => $prev_id), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,parent_id,image,seo,clap,comment,published_at,created_by');
		$next_post    = ($next_id > 0) ? $this->page_model->get_by(array('id' => $next_id), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,parent_id,image,seo,clap,comment,published_at,created_by') : '';
		// var_dump("prev_post", $prev_post);
		// var_dump("next_post", $next_post);
		// return;

		/* Data Storing */
		$this->data['data']['current']       = 'page';
		$this->data['data']['detail']        = $var_post;
		$this->data['data']['author']        = $var_author;
		$this->data['data']['prev_post']     = $prev_post;
		$this->data['data']['next_post']     = $next_post;
		$this->data['data']['related_post']  = $related_post;
		$this->data['data']['user']          = $this->_user();

		/* Meta - Site Info */
		$seo = json_decode($var_post->seo);
		$meta_data = array(
			'title'        => (!empty($seo->title)) ? $seo->title : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'description'  => (!empty($seo->description)) ? strip_tags($seo->description) : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'keywords'     => (!empty($seo->keywords)) ? $seo->keywords : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'published_at' => '',
			'image'        => (!empty($var_post->image)) ? json_decode($var_post->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('page');
	}

	/* Category */
	public function _category($slug, $paged='') {
		/* Variables */
		$var_page     = $this->category_model->get_by(array('slug' => $slug), NULL, NULL, TRUE, 'id,name,slug,description');
		$page_id      = $var_page->id;
		$page_title   = $var_page->name;
		$page_content = $var_page->description;
		$offs         = (!empty($paged)) ? 6*($paged-1) : 0;
		$recent_posts = $this->category_model->have_posts(array('amp_cat.slug' => $slug,'p.status' => 'publish'), 6, $offs, FALSE, 'p.id,p.title,p.content,p.slug,p.excerpt,p.additional,p.page_id,p.image,p.seo,p.clap,p.comment,p.published_at,p.created_by');
		
		/* No Data */
		if (empty($recent_posts)) {
			$this->_404();
			return;
		}

		/* Data Storing */
		$this->data['data']['current']         = $slug;
		$this->data['data']['header_title']    = $page_title;
		$this->data['data']['header_subtitle'] = $page_content;
		$this->data['data']['user']            = $this->_user();
		$this->data['data']['recent_posts']    = $recent_posts;
		$this->data['data']['pagination']      = $this->_paginasi( site_url($slug.'/page/'), site_url($slug), $this->post_category_model->count_post(array('category_id' => $page_id), 'id'));
		$this->data['data']['jumbotron']       = FALSE;

		/* Meta - Site Info */
		$current = (!empty($paged)) ? 'Page '.$paged.' &#8211; ' : '';
		// $seo     = json_decode($var_page->seo);
		$meta_data = array(
			'title'        => $page_title . ' &#8211; ' . $current . $this->data['web_info']->site_title,
			'description'  => $var_page->description,
			'keywords'     => '',
			'published_at' => '',
			'image'        => '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('archive');
	}

	/* Contact Page */
	public function _contact($slug) {
		$this->load->model('contact_model');
		$this->load->library('form_validation');

		$post_field = $this->input->post();
		if (isset($post_field['submit'])) {
			$temp_data = array(
				'full_name'   => $post_field['full_name'],
				'email'       => $post_field['email'],
				'description' => $post_field['description'],
				'created_at'  => $this->current_datetime,
				'created_by'  => $post_field['email'],
			);
			$data = $this->security->xss_clean($temp_data);

			$rules = $this->contact_model->rules['create'];
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE) {
				$this->data['data']['comment_message'] = array('status' => 'error', 'message' => validation_errors());
				$this->errors = TRUE;
			}

			if ($this->errors == FALSE) {

				if ($this->contact_model->insert($data)) {
					$this->data['data']['comment_message'] = array('status' => 'success', 'message' => '<p>Thank you for your message. It has been sent.</p>');
					$this->errors = FALSE;
				} else {
					$this->data['data']['comment_message'] = array('status' => 'error', 'message' => '<p>ERROR. Please try again!</p>');
					$this->errors = TRUE;
				}
			}

			if ($this->errors == FALSE) {
				set_flash('class', 'success', TRUE);
				set_flash('status', 'success', TRUE);
				set_flash('success', $this->data['data']['comment_message'], TRUE);
			} else {
				set_flash('class', 'danger', TRUE);
				set_flash('status', 'errors', TRUE);
				set_flash('errors', $this->data['data']['comment_message'], TRUE);
				repopulate_post_field($post_field);
			}
		}

		/* Get Post Detail */
		$var_post = $this->page_model->get_by(array('status' => 'publish','slug' => $slug), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,parent_id,image,seo,clap,comment,published_at,created_by');

		/* Data Storing */
		$this->data['data']['current'] = 'page';

		/* Meta - Site Info */
		$seo = json_decode($var_post->seo);
		$meta_data = array(
			'title'        => (!empty($seo->title)) ? $seo->title : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'description'  => (!empty($seo->description)) ? strip_tags($seo->description) : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'keywords'     => (!empty($seo->keywords)) ? $seo->keywords : $var_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
			'published_at' => '',
			'image'        => (!empty($var_post->image)) ? json_decode($var_post->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('contact');
	}

	/* 404 Page */
	public function _404() {


		$this->data['data']['current'] = 'Page Not Found';

		/* Meta - Site Info */
		$meta_data = array(
			'title'        => 'Page Not Found &#8211; ' . $this->data['web_info']->site_title,
			'description'  => $this->data['web_info']->site_description,
			'keywords'     => $this->data['web_info']->site_description,
			'published_at' => '',
			'image'        => $this->data['web_info']->logo->image,
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('404');
	}



	/* Pagination */
	public function _paginasi($base_url='', $first_url='', $total='', $search=FALSE, $search_query='') {

		$this->load->library('pagination');
		
		$config['base_url']          = $base_url;
		$config['total_rows']        = $total;
		$config['first_url']         = $first_url;
		$config['search']            = $search;
		$config['search_query']      = $search_query;
		$config['per_page']          = 6;
		$config["num_links"]         = 2;
		$config['use_page_number']   = TRUE;
		$config['display_prev_link'] = TRUE;
		$config['display_next_link'] = TRUE;
		$config['first_link']        = 'First';
		$config['last_link']         = 'Last';
		$config['next_link']         = '<i class="fa fa-angle-double-right"></i>';
		$config['prev_link']         = '<i class="fa fa-angle-double-left"></i>';
		$config['full_tag_open']     = '<div class="bottompagination"><span class="navigation">';
		$config['full_tag_close']    = '</span></div>';
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['cur_tag_open']      = '<li class="active">';
		$config['cur_tag_close']     = '</li>';
		$config['next_tag_open']     = '<li>';
		$config['next_tagl_close']   = '</li>';
		$config['prev_tag_open']     = '<li>';
		$config['prev_tagl_close']   = '</li>';
		$config['first_tag_open']    = '<li class="previous">';
		$config['first_tagl_close']  = '</li>';
		$config['last_tag_open']     = '<li class="next">';
		$config['last_tagl_close']   = '</li>';
		
		$this->pagination->initialize($config);
		
		return $this->pagination->create_links();
	}

	/* Conditional */
	public function _conditional($slug) {

		/* Check Page */
		$c_page = $this->page_model->get_by(array('slug' => $slug), 1, NULL, TRUE, 'id');
		if (!empty($c_page)) {

			/* Check Archive */
			$c_archive = $this->post_model->get_by(array('status' => 'publish', 'page_id' => $c_page->id), 1, NULL, TRUE, 'id');
			if (!empty($c_archive)) {
				return 'archive';
			}

			return 'page';
		} else {
			// Check Category
			$c_category = $this->category_model->get_by(array('slug' => $slug), 1, NULL, TRUE, 'id');
			if (!empty($c_category)) {
				return 'category';
			} else {
				/* Check Post */
				$c_post = $this->post_model->get_by(array('status' => 'publish', 'slug' => $slug), 1, NULL, TRUE, 'id');
				if (!empty($c_post)) {
					return 'post';
				}
			}

			return '404';
		}
	}

	/* Author */
	public function _user($name='') {
		$this->load->model(array('user_model'));
		if (!empty($name)) {
			$var_user = $this->user_model->get_by(array('username' => $name), 1, NULL, TRUE, 'id, username, first_name, last_name, website, email, avatar, description, facebook, twitter, gplus, instagram, linkedin');
		} else {
			$var_user = $this->user_model->get_by(NULL, NULL, NULL, FALSE, 'id, username, first_name, last_name, avatar, description, facebook, twitter, gplus, instagram, linkedin');
		}
		return $var_user;
	}




	// Blog
	public function blog() {

		/* Author */
		$this->load->model(array('user_model'));
		$user_a   = $this->user_model->get();
		$user_all = $this->user_model->get_by(NULL, NULL, NULL, false, 'id, username, first_name, last_name, avatar');

		/* Data Storing */
		$this->data['data'] = array(
			'current'        => 'blog',
			'user'           => $user_all,
			'featured_image' => $this->data['web_info']->image->image,
			'single'         => FALSE
		);

		/* Variable */
		$uri_1 = $this->uri_1;
		$uri_2 = $this->uri_2;
		$uri_3 = $this->uri_3;
		$uri_4 = $this->uri_4;
		$uri_5 = $this->uri_5;

		/* Check Page */
		if (($uri_1 == 'blog') && empty($uri_2)) show_404();
		if (($uri_2 == 'category') && empty($uri_3))
		if (($uri_2 == 'category') && empty($uri_3)) show_404();

		/* Paged */
		if ( $uri_1 == 'blog' || ($uri_2 == 'page' && is_numeric($uri_3)) ) {
			$offs = (!empty($uri_3) && is_numeric($uri_3)) ? 6*($uri_3-1) : 0;
			
			$this->data['data']['header_title'] = 'All Stories';
			$this->data['data']['recent_posts'] = $this->post_model->get_by(array('status' => 'publish'), 6, $offs, FALSE, 'id,title,image,slug,excerpt,published_at,created_by');
			$this->data['data']['pagination']   = $this->paginasi(site_url('blog/page/'),site_url(),count($this->post_model->get()));
			$this->data['data']['jumbotron']    = TRUE;

			/* Meta - Site Info */
			$paged = (!empty($uri_3)) ? 'Page '.$uri_3.' &#8211; ' : '';
			$meta_data = array(
				'title'        => 'Blog &#8211; ' . $paged . $this->data['web_info']->site_title,
				'description'  => $this->data['web_info']->site_description,
				'keywords'     => $this->data['web_info']->site_description,
				'published_at' => '',
				'image'        => $this->data['web_info']->logo->image,
			);
			$this->data['data']['meta'] = $this->meta_tag($meta_data);
		}

		/* Category */
		if ( $uri_1 == 'blog' && ($uri_2 == 'category' && !is_numeric($uri_3)) ) {

			$page_slug    = $this->uri_3;
			$var_page     = $this->page_model->get_by(array('slug' => $page_slug), NULL, NULL, TRUE, 'id,title,slug,content,seo,published_at');
			$page_id      = $var_page->id;
			$page_title   = $var_page->title;
			$page_content = $var_page->content;
			$offs         = ($uri_4 == 'page' && is_numeric($uri_5)) ? 6*($uri_5-1) : 0;
			
			$this->data['data']['header_title']    = $page_title;
			$this->data['data']['header_subtitle'] = $page_content;
			$this->data['data']['recent_posts']    = $this->post_model->get_by(array('status' => 'publish','page_id' => $page_id), 6, $offs, FALSE, 'id,title,image,slug,excerpt,published_at,created_by');
			$this->data['data']['pagination']      = $this->paginasi(site_url('blog/category/'.$page_slug.'/page/'),site_url('blog/category/'.$page_slug), $this->post_model->count_post(array('status' => 'publish','page_id' => $page_id), 'id'));
			$this->data['data']['jumbotron']       = FALSE;

			/* Meta - Site Info */
			$paged = (!empty($uri_5)) ? 'Page '.$uri_5.' &#8211; ' : '';
			$meta_data = array(
				'title'        => $page_title . ' &#8211; ' . $paged . $this->data['web_info']->site_title,
				'description'  => $this->data['web_info']->site_description,
				'keywords'     => $this->data['web_info']->site_description,
				'published_at' => '',
				'image'        => $this->data['web_info']->logo->image,
			);
			$this->data['data']['meta'] = $this->meta_tag($meta_data);
		}

		/* Single */
		if (!empty($uri_2) && !is_numeric($uri_2) && $uri_2 !== 'page' && $uri_2 !== 'category') {

			$get_post = $this->post_model->get_by(array('status' => 'publish','slug' => $uri_2), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,type,comment,published_at,created_by');
			$var_cat  = $this->page_model->get_by(array('id' => $get_post->page_id), NULL, NULL, TRUE, 'id,title,slug');

			/* Next Prev */
			$prev_id = $get_post->id-1;
			$next_id = $get_post->id+1;
			$prev_post = ($prev_id > 0) ? $this->post_model->get_by(array('status' => 'publish','id' => $prev_id), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,type,comment,published_at,created_by') : '';
			$next_post = $this->post_model->get_by(array('id' => '27'), NULL, NULL, TRUE, 'id,title,content,slug,excerpt,page_id,image,additional,seo,type,comment,published_at,created_by');

			$this->data['data']['jumbotron'] = FALSE;
			$this->data['data']['single']    = TRUE;
			$this->data['data']['detail']    = $get_post;
			$this->data['data']['category']  = $var_cat;
			$this->data['data']['prev_post'] = $prev_post;
			$this->data['data']['next_post'] = $next_post;

			$seo = json_decode($get_post->seo);
			$meta_data = array(
				'title'        => (!empty($seo->title)) ? $seo->title : $get_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
				'description'  => (!empty($seo->description)) ? strip_tags($seo->description) : $get_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
				'keywords'     => (!empty($seo->keywords)) ? $seo->keywords : $get_post->title . ' &#8211; ' . $this->data['web_info']->site_title,
				'published_at' => '',
				'image'        => (!empty($get_post->image)) ? json_decode($get_post->image)->image : '',
			);
			$this->data['data']['meta'] = $this->meta_tag($meta_data);
		}

		$this->render('blog');
	}

	// Single Post
	public function single_post($uri, $layout = '', $preview = FALSE) {

		$this->render($layout);

		return;

		$this->load->model(array('comment_model','post_comment_model'));
		$this->load->library('form_validation');

		$this->breadcrumb->add($uri);
		$this->data['breadcrumb'] = $this->breadcrumb->output();

		if ($preview)
			$where = array('amp_post.id' => $uri, 'p.slug' => $this->uri_2);
		else
			$where = array('amp_post.slug' => $uri);
			
		$this->data['data'] = array(
			'current' => $this->uri_1,
			'info' => $this->post_model->get_by_user($where, NULL, NULL, TRUE, 'amp_post.id,amp_post.title,amp_post.content,amp_post.slug,amp_post.excerpt,amp_post.image,amp_post.additional,amp_post.seo,amp_post.type,amp_post.comment,amp_post.published_at')
		);

		if ($this->data['data']['info'] == NULL)
			redirect('/');

		$post_id = $this->data['data']['info']->id;
		// get comment by post id
		$this->data['data']['meta'] = $this->meta_tag();
		$this->data['data']['comments'] = $this->comment_model->comment_by_post_id(array('po.id' => $post_id, 'co.status' => 'accepted'), NULL, NULL, FALSE, 'amp_co.id as c_id,amp_co.name as c_name,amp_co.comment as c_comment,amp_co.created_at as c_created_at');
		// get visitor by post id
		$this->data['data']['visitors'] = $this->visitor_model->count_post(array('post_id' => $post_id), 'id');

		// POST
		$post_field = $this->input->post();
		if (isset($post_field['submit']) && $this->data['data']['info']->comment == 1) // CREATE COMMENT
		{
			// CREATE COMMENT
			$temp_data = array(
				'name' => $post_field['name'],
				'email' => $post_field['email'],
				'comment' => $post_field['comment'],
				'created_at' => $this->current_datetime,
				'created_by' => $post_field['email'],
			);
			// XSS CLEAN
			$data = $this->security->xss_clean($temp_data);

			// check rules validation
			$rules = $this->comment_model->rules['create'];
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE)
			{
				$message = validation_errors();
				$this->errors = TRUE;
			}

			if ($this->errors == FALSE) // if not error insert data
			{
				if ($comment_id = $this->comment_model->insert($data))
				{
					// Insert to Post Category
					$this->post_comment_model->insert(array('post_id' => $post_id, 'comment_id' => $comment_id));
					$message = 'Komentar berhasil dibuat, silahkan tunggu di moderasi oleh admin!';
					// $message = 'Komentar berhasil dibuat!';
					$this->errors = FALSE;
				}
				else
				{
					$message = 'Komentar gagal dibuat. Silahkan ulangi lagi!';
					$this->errors = TRUE;
				}
			}

			if ($this->errors == FALSE) // insert
			{
				set_flash('class', 'success', TRUE);
				set_flash('status', 'success', TRUE);
				set_flash('success', $message, TRUE);
			}
			else
			{
				set_flash('class', 'danger', TRUE);
				set_flash('status', 'errors', TRUE);
				set_flash('errors', $message, TRUE);
				repopulate_post_field($post_field); // repopulate posf field
			}
			redirect_back();
		}

		// META TAG
		$seo = json_decode($this->data['data']['info']->seo);
		$meta_data = array(
			'title' => ($seo) ? $seo->title : $this->data['data']['info']->title,
			'description' => ($seo) ? strip_tags($seo->description) : strip_tags($this->data['data']['info']->excerpt),
			'keywords' => ($seo) ? $seo->keywords : '',
			'published_at' => ($this->data['data']['info']) ? $this->data['data']['info']->published_at : '',
			'image' => ($this->data['data']['info']->image) ? json_decode($this->data['data']['info']->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);
		
		// GET PAGE
		$this->render($layout);
	}

	// About Me
	public function about_me()
	{
		$this->breadcrumb->add('About Me');
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['data'] = array(
			'current' => 'about-me',
			'info' => $this->page_model->get_by(array('slug' => 'about-me'), NULL, NULL, TRUE, 'title,slug,content,excerpt,image,seo,published_at'),
		);

		// META TAG
		// var_dump($this->data['data']['info']);
		// return;
		$seo = json_decode($this->data['data']['info']->seo);
		$meta_data = array(
			'title' => ($seo) ? $seo->title : $this->data['data']['info']->title,
			'description' => ($seo) ? strip_tags($seo->description) : strip_tags($this->data['data']['info']->excerpt),
			'keywords' => ($seo) ? $seo->keywords : '',
			'published_at' => ($this->data['data']['info']) ? $this->data['data']['info']->published_at : '',
			'image' => ($this->data['data']['info']->image) ? json_decode($this->data['data']['info']->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('my-story');
	}

	// Contact
	public function contact()
	{
		$this->load->model('contact_model');
		$this->load->library('form_validation');

		// POST
		$post_field = $this->input->post();
		if (isset($post_field['submit']))
		{
		  // CREATE CONTACT HERE
			$temp_data = array(
				'full_name' => $post_field['full_name'],
				'email' => $post_field['email'],
				'description' => $post_field['description'],
				'created_at' => $this->current_datetime,
				'created_by' => $post_field['email'],
			);
			// XSS CLEAN
			$data = $this->security->xss_clean($temp_data);

			// check rules validation
			$rules = $this->contact_model->rules['create'];
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE)
			{
				$message = validation_errors();
				$this->errors = TRUE;
			}

			if ($this->errors == FALSE) // if not error insert data
			{

				if ($this->contact_model->insert($data))
				{
					$message = 'Pesan berhasil dikirim!';
					$this->errors = FALSE;
				}
				else
				{
					$message = 'Pesan gagal dikirim. Silahkan ulangi lagi!';
					$this->errors = TRUE;
				}
			}

			if ($this->errors == FALSE) // insert
			{
				set_flash('class', 'success', TRUE);
				set_flash('status', 'success', TRUE);
				set_flash('success', $message, TRUE);
			}
			else
			{
				set_flash('class', 'danger', TRUE);
				set_flash('status', 'errors', TRUE);
				set_flash('errors', $message, TRUE);
				repopulate_post_field($post_field); // repopulate posf field
			}
			redirect_back();
		}
		
		// GET PAGE
		$this->breadcrumb->add('Contact');
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		$this->data['data'] = array(
			'current' => 'contact',
			'info' => $this->page_model->get_by(array('slug' => 'contact'), NULL, NULL, TRUE, 'title,slug,content,image,excerpt,seo,published_at')
		);

		// META TAG
		$seo = json_decode($this->data['data']['info']->seo);
		$meta_data = array(
			'title' => ($seo) ? $seo->title : $this->data['data']['info']->title,
			'description' => ($seo) ? strip_tags($seo->description) : strip_tags($this->data['data']['info']->excerpt),
			'keywords' => ($seo) ? $seo->keywords : '',
			'published_at' => ($this->data['data']['info']) ? $this->data['data']['info']->published_at : '',
			'image' => ($this->data['data']['info']->image) ? json_decode($this->data['data']['info']->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('contact');
	}
	
	// Schedule
	public function schedule($preview = FALSE)
	{
		$this->breadcrumb->add('Schedule');
		$this->data['breadcrumb'] = $this->breadcrumb->output();
		
		$this->data['data'] = array(
			'current' => 'schedule',
			'info' => $this->page_model->get_by(array('slug' => 'schedule'), NULL, NULL, TRUE, 'id,title,slug,content,excerpt,seo,image,published_at'),
		);
		$this->data['data']['schedules'] = $this->post_model->get_by(array('page_id' => $this->data['data']['info']->id), NULL, NULL, FALSE, 'title,slug,content,image');

		// META TAG
		$seo = json_decode($this->data['data']['info']->seo);
		$meta_data = array(
			'title' => ($seo) ? $seo->title : $this->data['data']['info']->title,
			'description' => ($seo) ? strip_tags($seo->description) : strip_tags($this->data['data']['info']->excerpt),
			'keywords' => ($seo) ? $seo->keywords : '',
			'published_at' => ($this->data['data']['info']) ? $this->data['data']['info']->published_at : '',
			'image' => ($this->data['data']['info']->image) ? json_decode($this->data['data']['info']->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);

		$this->render('schedule');
	}

	// Album
	public function album($preview = FALSE)
	{
		$this->breadcrumb->add('Album');

		if ($preview) // only for page
			$this->uri_2 = '';
		
		if ($this->uri_2 && !is_numeric($this->uri_2)) // detail
		{
			$this->detail($this->uri_2, 'gallery-detail');
		}
		else
		{
			// echo $this->uri_1;
			$this->data['breadcrumb'] = $this->breadcrumb->output();
			$this->data['data'] = array(
				'current' => 'album',
				'info' => $this->page_model->get_by(array('slug' => 'album'), NULL, NULL, TRUE, 'id,title,content,excerpt,slug,seo,image,published_at'),
				'loadmore' => site_url($this->uri_1).'/',
				'meta' => $this->meta_tag()
			);
			// var_dump($this->data['data']['info']);
			if ($this->data['data']['info'])
			{
				$where = array(
					'amp_post.published_at <=' => current_datetime(), 
					'amp_post.status <>' => 'draft',
					'amp_post.page_id' => $this->data['data']['info']->id
				);
				$where_rows = array(
					'published_at <=' => current_datetime(), 
					'status <>' => 'draft',
					'page_id' => $this->data['data']['info']->id
				);

				$limit = $this->limit;
				$paging = ($this->uri_2 > 1) ? $this->uri_2 : 1;
				$offset = $limit * ($paging - 1);
				$total_rows = $this->post_model->count_post($where_rows, 'title');
				
				$this->data['data']['is_empty'] = ($offset + $limit >= $total_rows) ? TRUE : FALSE;
				$this->data['data']['galleries'] = $this->post_model->get_by_user($where, $limit, $offset, FALSE, 'amp_post.title,amp_post.slug,amp_post.content,amp_post.excerpt,amp_post.image,amp_post.published_at');
				// $this->data['data']['galleries'] = $this->post_model->get_by(array('page_id' => $this->data['data']['info']->id), NULL, NULL, FALSE, 'title,slug,content,excerpt,image,published_at');
				
				// AJAX REQUEST PAGINATION
				if ($this->is_ajax_request)
				{
					$this->data['data'] = array(
						'base_url' => $this->upload_location,
						'galleries' => $this->data['data']['galleries'],
						'paging' => $paging + 1,
						'is_empty' => $this->data['data']['is_empty']
					);
					echo json_encode($this->data['data']);
					return false;
				}
			}

			// META TAG
			$seo = json_decode($this->data['data']['info']->seo);
			$meta_data = array(
				'title' => ($seo) ? $seo->title : $this->data['data']['info']->title,
				'description' => ($seo) ? strip_tags($seo->description) : strip_tags($this->data['data']['info']->excerpt),
				'keywords' => ($seo) ? $seo->keywords : '',
				'published_at' => ($this->data['data']['info']) ? $this->data['data']['info']->published_at : '',
				'image' => ($this->data['data']['info']->image) ? json_decode($this->data['data']['info']->image)->image : '',
			);
			$this->data['data']['meta'] = $this->meta_tag($meta_data);

			$this->render('gallery');
		}
	}

	// Detail
	public function detail($uri, $layout = '', $preview = FALSE)
	{
		$this->load->model(array('comment_model','post_comment_model'));
		$this->load->library('form_validation');

		$this->breadcrumb->add($uri);
		$this->data['breadcrumb'] = $this->breadcrumb->output();

		if ($preview)
			$where = array('amp_post.id' => $uri, 'p.slug' => $this->uri_2);
		else
			$where = array('amp_post.slug' => $uri);
			
		$this->data['data'] = array(
			'current' => $this->uri_1,
			'info' => $this->post_model->get_by_user($where, NULL, NULL, TRUE, 'amp_post.id,amp_post.title,amp_post.content,amp_post.slug,amp_post.excerpt,amp_post.image,amp_post.additional,amp_post.seo,amp_post.type,amp_post.comment,amp_post.published_at')
		);

		if ($this->data['data']['info'] == NULL)
			redirect('/');

		$post_id = $this->data['data']['info']->id;
		// get comment by post id
		$this->data['data']['meta'] = $this->meta_tag();
		$this->data['data']['comments'] = $this->comment_model->comment_by_post_id(array('po.id' => $post_id, 'co.status' => 'accepted'), NULL, NULL, FALSE, 'amp_co.id as c_id,amp_co.name as c_name,amp_co.comment as c_comment,amp_co.created_at as c_created_at');
		// get visitor by post id
		$this->data['data']['visitors'] = $this->visitor_model->count_post(array('post_id' => $post_id), 'id');

		// POST
		$post_field = $this->input->post();
		if (isset($post_field['submit']) && $this->data['data']['info']->comment == 1) // CREATE COMMENT
		{
			// CREATE COMMENT
			$temp_data = array(
				'name' => $post_field['name'],
				'email' => $post_field['email'],
				'comment' => $post_field['comment'],
				'created_at' => $this->current_datetime,
				'created_by' => $post_field['email'],
			);
			// XSS CLEAN
			$data = $this->security->xss_clean($temp_data);

			// check rules validation
			$rules = $this->comment_model->rules['create'];
			$this->form_validation->set_rules($rules);
			if ($this->form_validation->run() == FALSE)
			{
				$message = validation_errors();
				$this->errors = TRUE;
			}

			if ($this->errors == FALSE) // if not error insert data
			{
				if ($comment_id = $this->comment_model->insert($data))
				{
					// Insert to Post Category
					$this->post_comment_model->insert(array('post_id' => $post_id, 'comment_id' => $comment_id));
					$message = 'Komentar berhasil dibuat, silahkan tunggu di moderasi oleh admin!';
					// $message = 'Komentar berhasil dibuat!';
					$this->errors = FALSE;
				}
				else
				{
					$message = 'Komentar gagal dibuat. Silahkan ulangi lagi!';
					$this->errors = TRUE;
				}
			}

			if ($this->errors == FALSE) // insert
			{
				set_flash('class', 'success', TRUE);
				set_flash('status', 'success', TRUE);
				set_flash('success', $message, TRUE);
			}
			else
			{
				set_flash('class', 'danger', TRUE);
				set_flash('status', 'errors', TRUE);
				set_flash('errors', $message, TRUE);
				repopulate_post_field($post_field); // repopulate posf field
			}
			redirect_back();
		}

		// META TAG
		$seo = json_decode($this->data['data']['info']->seo);
		$meta_data = array(
			'title' => ($seo) ? $seo->title : $this->data['data']['info']->title,
			'description' => ($seo) ? strip_tags($seo->description) : strip_tags($this->data['data']['info']->excerpt),
			'keywords' => ($seo) ? $seo->keywords : '',
			'published_at' => ($this->data['data']['info']) ? $this->data['data']['info']->published_at : '',
			'image' => ($this->data['data']['info']->image) ? json_decode($this->data['data']['info']->image)->image : '',
		);
		$this->data['data']['meta'] = $this->meta_tag($meta_data);
		
		// GET PAGE
		$this->render($layout);
	}

	public function preview()
	{
		if ($this->session->userdata('id') == NULL)
			show_404();    

		$uri_2 = $this->uri->segment('2');
		$uri_3 = $this->uri->segment('3');
		if ($uri_2 == 'page') // PAGE
		{
			if ($uri_3 == 'about-me')
				$this->about_me(TRUE);
			elseif ($uri_3 == 'album')
				$this->album(TRUE);
			elseif ($uri_3 == 'blog')
				$this->blog(TRUE);
			elseif ($uri_3 == 'contact')
				$this->contact(TRUE);
			elseif ($uri_3 == 'schedule')
				$this->schedule(TRUE);
			else
				$this->blog(TRUE);
		}
		else // POST
		{
			$layout = '';
			if ($uri_2 == 'album')
				$this->detail($uri_3, 'gallery-detail', TRUE);
			elseif ($uri_2 == 'blog')
				$this->detail($uri_3, 'blog-detail', TRUE);
			elseif ($uri_2 == 'schedule')
				$this->schedule();
			else
				show_404();
		}
	}
}