<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'frontend';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* redirect to store */
$route['app']            = 'welcome/redirect_app';

/* ROUTE BACKEND */
$route['admin/dashboard']            = 'admin/dashboard';
$route['admin/page']                 = 'admin/page';
$route['admin/page/(:any)']          = 'admin/page/$i';
$route['admin/post']                 = 'admin/post';
$route['admin/post/(:any)']          = 'admin/post/$i';
$route['admin/comment']              = 'admin/comment';
$route['admin/comment/(:any)']       = 'admin/comment/$i';
$route['admin/category']             = 'admin/category';
$route['admin/category/(:any)']      = 'admin/category/$i';
$route['admin/tag']                  = 'admin/tag';
$route['admin/tag/(:any)']           = 'admin/tag/$i';
$route['admin/user']                 = 'admin/user';
$route['admin/user/(:any)']          = 'admin/user/$i';
$route['admin/featured-post']        = 'admin/featured_post';
$route['admin/featured-post/(:any)'] = 'admin/featured_post/$i';

// $route['admin/contact/(:any)']  = 'admin/contact/$i';


/* ROUTE LOGIN LOGOUT */
// $route['auth/(:any)'] = 'admin/auth/$i';
// $route['auth/login'] = 'admin/auth/login';

/* ROUTE FRONTEND */
// $route['index'] = 'public/frontend/index';
$route['api/feed/(:any)'] = 'api/index';
$route['api/feed/(:any)/(:any)'] = 'api/index/$i';

$route['(?!admin)(?!api)|(:any)']                      = 'frontend/$i';
$route['(?!admin)(?!api)|(:any)/(:any)']               = 'frontend/$i/$i';
$route['(?!admin)(?!api)|(:any)/(:any)/(:any)']        = 'frontend/$i/$i/$i';
$route['(?!admin)(?!api)|(:any)/(:any)/(:any)/(:any)'] = 'frontend/$i/$i/$i/$i';

/* ROUTE FEED */
$route['robots.txt'] = 'feed/robots';
$route['sitemap.xml'] = 'feed/sitemap';
$route['rss.xml'] = 'feed';