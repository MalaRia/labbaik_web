// Featured Image
$("#popup").click(function(e) {
  e.preventDefault();
  var href = $(this).attr("href");
  var multiple = $(this).attr('data-multiple');
  var type = $(this).attr('data-type');
  $("#myModal .modal-body #image_preview").attr("src", "");
  $(".meta-image").find("span").html("");
  $("#myModal .modal-body").find("input, textarea").val("");
  if (multiple == "true") {
    $("#set-image").attr('data-multiple', "true");
  } else {
    $("#set-image").attr('data-multiple', "false");
  }

  if (type != "undefined") {
    $("#set-image").attr('data-type', type);
  } else {
    $("#set-image").attr('data-type', '');
  }
  
  $("#myModal").modal("show");
});
// ICON
$("#popup-icon").click(function(e) {
  e.preventDefault();
  var href = $(this).attr("href");
  var multiple = $(this).attr('data-multiple');
  var type = $(this).attr('data-type');
  $("#myModal .modal-body #image_preview").attr("src", "");
  $(".meta-image").find("span").html("");
  $("#myModal .modal-body").find("input, textarea").val("");
  if (multiple == "true") {
    $("#set-image").attr('data-multiple', "true");
  } else {
    $("#set-image").attr('data-multiple', "false");
  }

  if (type != "undefined") {
    $("#set-image").attr('data-type', type);
  } else {
    $("#set-image").attr('data-type', '');
  }
  
  $("#myModal").modal("show");
});
// Logo
$("#popup-logo").click(function(e) {
  e.preventDefault();
  var href = $(this).attr("href");
  var multiple = $(this).attr('data-multiple');
  var type = $(this).attr('data-type');
  $("#myModal .modal-body #image_preview").attr("src", "");
  $(".meta-image").find("span").html("");
  $("#myModal .modal-body").find("input, textarea").val("");
  if (multiple == "true") {
    $("#set-image").attr('data-multiple', "true");
  } else {
    $("#set-image").attr('data-multiple', "false");
  }

  if (type != "undefined") {
    $("#set-image").attr('data-type', type);
  } else {
    $("#set-image").attr('data-type', '');
  }
  
  $("#myModal").modal("show");
});
// Gallery
$("#popupGallery").click(function(e) {
  e.preventDefault();
  var href = $(this).attr("href");
  var multiple = $(this).attr('data-multiple');
  $("#myModal .modal-body #image_preview").attr("src", "");
  $(".meta-image").find("span").html("");
  $("#myModal .modal-body").find("input, textarea").val("");
  if (multiple == "true") {
    $("#set-image").attr('data-multiple', "true");
  } else {
    $("#set-image").attr('data-multiple', "false");
  }
  $("#myModal").modal("show");
});
// When Choose File
$("#popup-upload").click(function(e) {
  e.preventDefault();
  var href = $(this).attr("href");
  $("#myModal-Upload .modal-body").html(
    "<iframe src='"+href+"' width='100%' height='600'></iframe>"
  );
  $("#myModal-Upload").modal("show");
});
// Set Image
$("#set-image").click(function(e) {
  e.preventDefault();
  var multiple = $(this).attr('data-multiple');
  var type = $(this).attr('data-type');
  var thumbnailClass = $('.box-body .thumbnail.gallery');
  var tmbClassLngth = thumbnailClass.length;
  
  if (multiple == "true")
  {
    // console.log(multiple);
    // console.log(tmbClassLngth);
    if (thumbnailClass.length == 1)
    {
      // console.log(thumbnailClass.find("#imageFileField").val());
      if (thumbnailClass.find("#imageGalleryFileField").val() == "")
      {
        $("#image-gallery-set").attr({
          "src": $("#url").val(),
          "alt": $("#image_alt").val(),
          "title": $("#image_title").val(),
        });
        $("#imageGalleryFileField").val($("#imageFile").val());
        $("#thumbGalleryFileField").val($("#thumbFile").val());
        $("#fullImageGallery").val($("#url").val());
        $("#imageGalleryTitle").val($("#image_title").val());
        $("#imageGalleryAlt").val($("#image_alt").val()); 

        // button reset value
        $("#image-gallery-set").parent().append('<button class="btn btn-danger reset-image"><span class="fa fa-trash"></span></button>');
      }
      else
      {
        // var thbClass = tmbClassLngth + 1;
        $("#img-gallery").append('<div class="col-sm-3"><div class="thumbnail gallery">'+
        '<button class="btn btn-danger trash"><span class="fa fa-trash"></span></button>'+
        '<img id="image-gallery-set" class="img-responsive" src="'+$("#url").val()+'" alt="'+$("#image_alt").val()+'" title="'+$("#image_title").val()+'">'+
        '<input type="hidden" name="fullImageGallery[]" class="fullImage" value="'+$("#url").val()+'">'+
        '<input type="hidden" name="image_gallery_big[]" class="imageFileField" value="'+$("#imageFile").val()+'">'+
        '<input type="hidden" name="image_gallery_thumb[]" class="thumbFileField" value="'+$("#thumbFile").val()+'">'+
        '<input type="hidden" name="image_gallery_title[]" class="imageTitle" id="imageTitle" value="'+$("#image_title").val()+'">'+
        '<input type="hidden" name="image_gallery_alt[]" class="imageAlt" id="imageAlt" value="'+$("#image_alt").val()+'">'+
        '</div></div>');  
      }
    }
    else
    {
      // var thbClass = tmbClassLngth + 1;
      $("#img-gallery").append('<div class="col-sm-3"><div class="thumbnail gallery">'+
        '<button class="btn btn-danger trash"><span class="fa fa-trash"></span></button>'+
        '<img id="image-gallery-set" class="img-responsive" src="'+$("#url").val()+'" alt="'+$("#image_alt").val()+'" title="'+$("#image_title").val()+'">'+
        '<input type="hidden" name="fullImageGallery[]" class="fullImage" value="'+$("#url").val()+'">'+
        '<input type="hidden" name="image_gallery_big[]" class="imageFileField" value="'+$("#imageFile").val()+'">'+
        '<input type="hidden" name="image_gallery_thumb[]" class="thumbFileField" value="'+$("#thumbFile").val()+'">'+
        '<input type="hidden" name="image_gallery_title[]" class="imageTitle" id="imageTitle" value="'+$("#image_title").val()+'">'+
        '<input type="hidden" name="image_gallery_alt[]" class="imageAlt" id="imageAlt" value="'+$("#image_alt").val()+'">'+
        '</div></div>');  
    }
  }
  else
  {
    if (type == 'favicon') {
      $("#image-icon-set").attr({
        "src": $("#url").val(),
        "alt": $("#image_alt").val(),
        "title": $("#image_title").val(),
      });
      $("#imageFileFieldIcon").val($("#imageFile").val());
      $("#thumbFileFieldIcon").val($("#thumbFile").val());
      $("#fullImageIcon").val($("#url").val());
      $("#imageTitleIcon").val($("#image_title").val());
      $("#imageAltIcon").val($("#image_alt").val());
      // button reset value
      $("#image-icon-set").parent().append('<button class="btn btn-danger reset-image"><span class="fa fa-trash"></span></button>');
    } else if (type == 'logo') {
      $("#image-logo-set").attr({
        "src": $("#url").val(),
        "alt": $("#image_alt").val(),
        "title": $("#image_title").val(),
      });
      $("#imageFileFieldLogo").val($("#imageFile").val());
      $("#thumbFileFieldLogo").val($("#thumbFile").val());
      $("#fullImageLogo").val($("#url").val());
      $("#imageTitleLogo").val($("#image_title").val());
      $("#imageAltLogo").val($("#image_alt").val());
      // button reset value
      $("#image-logo-set").parent().append('<button class="btn btn-danger reset-image"><span class="fa fa-trash"></span></button>');
    } else {
      $("#image-set").attr({
        "src": $("#url").val(),
        "alt": $("#image_alt").val(),
        "title": $("#image_title").val(),
      });
      $("#imageFileField").val($("#imageFile").val());
      $("#thumbFileField").val($("#thumbFile").val());
      $("#fullImage").val($("#url").val());
      $("#imageTitle").val($("#image_title").val());
      $("#imageAlt").val($("#image_alt").val());
      // button reset value
      $("#image-set").parent().append('<button class="btn btn-danger reset-image"><span class="fa fa-trash"></span></button>');
    }
  }
  $(this).attr('data-type', '');
  $("#myModal").modal("hide");
});

// filemanager callback
function responsive_filemanager_callback(field_id){
  var filename = jQuery("#"+field_id).val();
  
  // preview image
  var fullPath = uploadPath+filename;
  $("#image_preview").attr({
    "src": fullPath
  });
  
  // set meta image
  setTimeout(function() {
    setMeta("image_preview", fullPath);
    // add scroll to modal
    $("#myModal").css("overflow-y", "auto");
  }, 100);
}

// get filename
function getFilename(location) {
  // find the filename then split it
  var popit = location.replace(/^.*[\\\/]/, '').split(".");
  // remove last array
  popit.pop();
  // join by space
  return popit.join(" ");
}
// get filetype
function getFileType(location) {
  return location.split('.').pop();
}

// set meta
function setMeta(fileID, location) {
  // if id set
  if (typeof fileID !== 'undefined') {
    // get natural width and height of image
    var width = $("#"+fileID)[0].naturalWidth;
    var height = $("#"+fileID)[0].naturalHeight;

    // fill the meta image 
    $("#url").val(location);
    $("#fullname").text(location);
    $("#dimensions").text(width +" x "+ height);
    $("#imageFile").val(getFilename(location) + "." + getFileType(location));
    $("#thumbFile").val(getFilename(location) + "_thumb." + getFileType(location));
    $("#image_title").val(getFilename(location));
    $("#image_alt").val(getFilename(location));


    // get the size and last modified
    var xhr = new XMLHttpRequest();
    xhr.open('HEAD', $("#"+fileID).attr("src"), true);
    xhr.onreadystatechange = function(){
      if ( xhr.readyState == 4 ) {
        if ( xhr.status == 200 ) {
          // Get Last-Modified
          var dateModified = new Date(xhr.getResponseHeader("Last-Modified"));
          var days = ["Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"];
          var months = ["Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember"];
          var day = days[dateModified.getDay()];
          var month = months[dateModified.getMonth()];
          var year = dateModified.getFullYear();
          var modifiedDate = day +", "+ month +" "+year;
          $("#modified_at").text(modifiedDate);

          // Get Image Size
          var sizeKB = 0.001 * xhr.getResponseHeader('Content-Length');
          $("#size").text(sizeKB + "kb");
        } else {
          alert('ERROR');
        }
      }
    };
    xhr.send(null);
  }
}