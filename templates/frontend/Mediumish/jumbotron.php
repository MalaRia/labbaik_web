<?php $bg_jumbotron = json_decode($data['recent_posts_jumbotron']->image); ?>
<div class="jumbotron fortags mt-4" style="background-image:url(<?php echo base_url('assets/uploads/'.$bg_jumbotron->image); ?>);"  >
	<div class="row">
		<div class="col-md-4 align-self-center text-center">
			<h2 class="hidden-sm-down text-white">Explore &rarr;</h2>
		</div>
		<div class="col-md-8 align-self-center text-center">
			<?php
				$i = 0;
				$cat = $this->category_model->get_by(array(), NULL, NULL, FALSE, '');
				foreach($cat as $c) { ?>
				<a href="<?php echo site_url($c->slug); ?>" class="tag-cloud-link tag-link-position-<?php echo $i; ?>" style="font-size: 16.4pt;" aria-label="<?php echo $c->name; ?>"><?php echo $c->name; ?></a>
				<?php $i++; } ?>
		</div>
	</div>
</div>