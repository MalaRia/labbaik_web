<div class="container">
	<footer class="footer"> 
		<p class="pull-left"> &copy; <?php echo $web_info->site_title; ?>. All rights reserved.</p> 
		<p class="pull-right" style="display: none;"> Mediumish Theme by WowThemesNet.</p> 
		<div class="clearfix"></div>
		<a href="" class="back-to-top hidden-md-down"><i class="fa fa-angle-up"></i></a>
	</footer>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/ie10-viewport-bug-workaround.js') ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/masonry.pkgd.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/mediumish.js') ?>"></script>
<?php if (!empty($data['detail'])) {
?>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/js.cookie.js') ?>"></script>
<script type='text/javascript'>
var clapsapplause = {"ajax_url":"<?php echo current_url().'/clap/'.$data['detail']->id; ?>","lovedText":"","loveText":""};
</script>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/claps-applause.js') ?>"></script>
<?php
} ?>
<?php if (isset($data['video_page'])) {
?>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/ytv.js') ?>"></script>
<script type='text/javascript'>
    window.onload = function(){
        window.controller = new YTV('frame', {
            channelId: 'UCQHTo2vPGsfVpbMxxZwD9XA',
            accent: 'yellow',
            responsive: true
        });
    };
</script>
<?php
} ?>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js?ver=3.0.20"></script>
<script type="text/javascript">WebFont.load({google:{families:['PT Sans:400,400,400i,700i,700,400,400i,700i', 'Shadows Into Light:400,400', 'Merriweather:700,300i,400,400i,700i,900i,300i,400,400i,700i,900i', 'Lato:400,100i,300i,400,400i,700i,900i']}});</script>
</body>
</html>