<div class="site-content">
	<div class="mainheading homecover forauthor">
		<div class="row post-top-meta authorpage justify-content-md-center justify-content-lg-center">
			<div class="col-md-8 col-md-8 col-xs-12 text-center">
				<h1><?php echo $data['title']; ?></h1>
				<?php if (!empty($data['author']->website)) {
					$author_website = $data['author']->website;
					$author_weburl  = (strpos($author_website, 'http') !== false) ? $author_website : 'http://'.$author_website;
					?>
					<i class="fa fa-globe"></i> <span class="bull">•</span> <a target="_blank" href="<?php echo $author_weburl; ?>"><?php echo $author_website; ?></a>
				<?php } ?>
				<span class="author-description text-white mt-3 mb-3 d-block"><?php echo $data['author']->description; ?></span>
				<p class="d-block">

					<?php if (!empty($data['author']->facebook)) { ?>
						<a target="_blank" href="<?php echo $data['author']->facebook; ?>"><i class="fa fa-facebook"></i></a> &nbsp;
					<?php } ?>

					<?php if (!empty($data['author']->twitter)) { ?>
						<a target="_blank" href="<?php echo $data['author']->twitter; ?>"><i class="fa fa-twitter"></i></a> &nbsp;
					<?php } ?>

					<?php if (!empty($data['author']->gplus)) { ?>
						<a target="_blank" href="<?php echo $data['author']->gplus; ?>"><i class="fa fa-google-plus"></i></a> &nbsp;
					<?php } ?>

					<?php if (!empty($data['author']->instagram)) { ?>
						<a target="_blank" href="<?php echo $data['author']->instagram; ?>"><i class="fa fa-instagram"></i></a> &nbsp;
					<?php } ?>

					<?php if (!empty($data['author']->linkedin)) { ?>
						<a target="_blank" href="<?php echo $data['author']->linkedin; ?>"><i class="fa fa-linkedin"></i></a> &nbsp;
					<?php } ?>

					<?php if (!empty($data['author']->email)) { ?>
						<a href="mailto:<?php echo $data['author']->email; ?>"><i class="fa fa-send-o"></i></a> &nbsp;
					<?php } ?>
				</p>
				<p class="margbotneg100"><img class="author-thumb" src="<?php
					$author_image = json_decode($data['author']->avatar);
					$author_image = (!empty($author_image)) ? base_url('assets/uploads/'.$author_image->image) : 'http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=100&d=mm&r=g';
					echo $author_image;
				?>"></p>
			</div>
		</div>
	</div>
	<br/><br/>
	<div class="container">
		<section class="recent-posts">
			<div class="row listrecent justify-content-md-center">
				<div class="col-md-8">
					<div class="section-title text-center">
						<h2><?php echo $data['total']; ?> Stories by <span> <span class="vcard"><?php echo $data['title']; ?></span></span></h2>
					</div>

					<?php
					if (!empty($data['recent_posts'])) {
						$post_data = $data['recent_posts'];

						for ($ip=0; $ip < count($post_data); $ip++) {
							if (str_word_count($post_data[$ip]->title) <= 9) {
								$post_title = $post_data[$ip]->title;
							} else {
								$post_title = explode(" ", $post_data[$ip]->title);
								$post_title = implode(" ", array_splice($post_title, 0, 9)) . '...';
							}
							if (str_word_count($post_data[$ip]->excerpt) <= 19) {
								$post_excerpt = strip_tags($post_data[$ip]->excerpt);
							} else {
								$post_excerpt = explode(" ", strip_tags($post_data[$ip]->excerpt));
								$post_excerpt = implode(" ", array_splice($post_excerpt, 0, 19));
							}
							$post_slug    = $post_data[$ip]->slug;
							$post_date    = date('M j, Y', strtotime($post_data[$ip]->published_at));
							$post_image   = json_decode($post_data[$ip]->image);
							$post_image   = (!empty($post_image)) ? base_url('assets/uploads/'.$post_image->image) : get_template_directory(dirname(__FILE__), 'img/image-default.jpg');
							$post_comment = $this->post_comment_model->count_post(array('post_id' => $post_data[$ip]->id), 'id');

							?>
							<div class="card post authorpost">
								<a class="thumbimage" href="<?php echo site_url($post_slug); ?>" style="background-image:url(<?php echo $post_image; ?>);"></a>
								<div class="card-block">
									<h2 class="card-title"><a href="<?php echo site_url($post_slug); ?>"><?php echo $post_title; ?></a></h2>
									<span class="card-text d-block"><?php echo $post_excerpt; ?>...</span>
									<div class="metafooter">
										<div class="wrapfooter">
											<span class="author-meta">
												<span class="post-date"><?php echo $post_date; ?></span>
												<span class="dot" style="display:none"></span>    
												<span class="muted" style="display:none"><i class="fa fa-comments"></i> <?php echo $post_comment; ?></span>
												<span class="dot" style="display:none"></span>
												<span class="readingtime" style="display:none">1 min read</span>
											</span>
											<span class="post-read-more" style="display:none">
												<a href="<?php echo site_url($post_slug); ?>" title="<?php echo $post_title; ?>">
													<svg class="svgIcon-use" width="25" height="25" viewBox="0 0 25 25">
														<path d="M19 6c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v14.66h.012c.01.103.045.204.12.285a.5.5 0 0 0 .706.03L12.5 16.85l5.662 4.126a.508.508 0 0 0 .708-.03.5.5 0 0 0 .118-.285H19V6zm-6.838 9.97L7 19.636V6c0-.55.45-1 1-1h9c.55 0 1 .45 1 1v13.637l-5.162-3.668a.49.49 0 0 0-.676 0z" fill-rule="evenodd"></path>
													</svg>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						<?php }
					} ?>

					<?php echo $data['pagination']; ?>
				</div>
			</div>
		</section>
	</div>
</div>