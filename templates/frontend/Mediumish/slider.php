<div id="main-slider" class="carousel slide margb-2" data-ride="carousel">
	<ol class="carousel-indicators">
		<?php for ($i=0; $i < count($data['recent_posts']); $i++) {
		$cactive = ($i==0) ? 'active': ''; ?>
		<li data-target="#main-slider" data-slide-to="<?php echo $i; ?>" class="<?php echo $cactive; ?>"></li>
		<?php } ?>
	</ol>
	<div class="carousel-inner" role="listbox">
		<?php $i = 0;
		foreach ($data['recent_posts'] as $blog) { 
			$title        = $blog->title;
			$link         = site_url($blog->slug);
			$excerpt      = strip_tags($blog->excerpt);
			$image        = ($blog->image) ? json_decode($blog->image) : '';
			$image        = (!empty($image)) ? $this->upload_location . '/' . $image->image : get_template_directory(dirname(__FILE__), 'img/image-default.jpg');
			$published_at = indonesian_date($blog->published_at);
			$cactive      = ($i==0) ? 'active': '';
			$video_id     = ((!empty($blog->additional) && preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $blog->additional, $match))) ? $match[1] : '';
			$img          = (!empty($video_id)) ? 'https://i.ytimg.com/vi/'.$video_id.'/hqdefault.jpg' : $image;
			?>
			<div class="carousel-item <?php echo $cactive; ?>" style="background-image: url(<?php echo $img; ?>); background-size: cover; background-repeat: no-repeat; background-position: center;">
				<a href="<?php echo $link; ?>">
					<img width="1110" height="300" style="visibility: hidden;" src="<?php echo $img; ?>" class="d-block wp-post-image" alt="<?php echo $title; ?>" data-no-lazy="1" />
					<div class="carousel-caption d-flex h-100 align-items-center justify-content-center">
						<h3 class="carousel-excerpt d-block">
							<span class="title d-block"><?php echo $title; ?></span>
							<span class="fontlight d-block hidden-md-down"><?php echo $excerpt; ?>...</span>
							<span class="btn btn-simple">Read More</span>
						</h3>
					</div>
				</a>
			</div><?php
			$i++;
		} ?>
	</div>
	<a href="#main-slider" class="carousel-control-prev" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>                
	</a>
	<a href="#main-slider" class="carousel-control-next" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>                
	</a>
</div>