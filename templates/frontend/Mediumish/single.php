<?php

$title           = $data['detail']->title;
$permalink       = site_url($data['detail']->slug);

// var_dump($data['detail']);
// return;
$cat_title       = $data['category']->name;
$cat_slug        = $data['category']->slug;
$content         = $data['detail']->content;
$clap            = $data['detail']->clap;
$total_comment   = $data['count_comment'];
$date            = date('F j, Y', strtotime($data['detail']->published_at));
$image           = json_decode($data['detail']->image);
$image           = (!empty($image)) ? base_url('assets/uploads/'.$image->image) : get_template_directory(dirname(__FILE__), 'img/image-default.jpg');
$video_id        = ((!empty($data['detail']->additional) && preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['detail']->additional, $match))) ? $match[1] : '';
$image           = (!empty($video_id)) ? 'https://i.ytimg.com/vi/'.$video_id.'/hqdefault.jpg' : $image;
$post_user_id    = $data['author']->id;
$post_user_name  = $data['author']->username;
$post_user_fname = $data['author']->first_name;
$post_user_lname = $data['author']->last_name;
$post_user_image = json_decode($data['author']->avatar);
$post_user_image = (!empty($post_user_image)) ? base_url('assets/uploads/'.$post_user_image->image) : 'http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=100&d=mm&r=g';

?>
<div class="site-content">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 col-md-2 col-xs-12">
				<div class="share hidden-xs-down">
					<div class="sidebarapplause">
						<div id="pt-claps-applause-<?php echo $data['detail']->id; ?>" class="pt-claps-applause<?php if (!empty($data['claps_ids'])) { echo ' has_rated';} ?>">
							<?php if (!empty($data['claps_ids']) && $data['claps_ids'] == '1') { ?>
								<a class="claps-button" data-id="<?php echo $data['detail']->id; ?>"><span class="lovedit">Already applauded!</span></a>
							<?php } else { ?>
								<a class="claps-button" href="<?php echo site_url($permalink); ?>" data-id="<?php echo $data['detail']->id; ?>" data-token-name="<?php echo $csrf_token['name'] ?>" data-token-hash="<?php echo $csrf_token['hash'] ?>" data-type="post"></a>
							<?php } ?>
							<span id="claps-count-<?php echo $data['detail']->id; ?>" class="claps-count"><?php echo $clap; ?></span>
						</div>
					</div>
					<p class="sharecolour">Share</p>
					<ul class="shareitnow">
						<li>
							<a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo rawurlencode($title); ?>&amp;url=<?php echo rawurlencode($permalink); ?>"><i class="fa fa-twitter"></i></a>
						</li>
						<li>
							<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo rawurlencode($permalink); ?>"><i class="fa fa-facebook"></i></a>
						</li>
						<li>
							<a target="_blank" href="https://plus.google.com/share?url=<?php echo rawurlencode($permalink); ?>"><i class="fa fa-google"></i></a>
						</li>
					</ul>
					<div class="sep"></div>
					<div class="hidden-xs-down reply-sidebar">
						<p>Reply</p>
						<ul>
							<li>
								<a class="smoothscroll" href="#comments">
									<?php echo $total_comment; ?><br/>
									<svg class="svgIcon-use" width="29" height="29" viewBox="0 0 29 29">
										<path d="M21.27 20.058c1.89-1.826 2.754-4.17 2.754-6.674C24.024 8.21 19.67 4 14.1 4 8.53 4 4 8.21 4 13.384c0 5.175 4.53 9.385 10.1 9.385 1.007 0 2-.14 2.95-.41.285.25.592.49.918.7 1.306.87 2.716 1.31 4.19 1.31.276-.01.494-.14.6-.36a.625.625 0 0 0-.052-.65c-.61-.84-1.042-1.71-1.282-2.58a5.417 5.417 0 0 1-.154-.75zm-3.85 1.324l-.083-.28-.388.12a9.72 9.72 0 0 1-2.85.424c-4.96 0-8.99-3.706-8.99-8.262 0-4.556 4.03-8.263 8.99-8.263 4.95 0 8.77 3.71 8.77 8.27 0 2.25-.75 4.35-2.5 5.92l-.24.21v.32c0 .07 0 .19.02.37.03.29.1.6.19.92.19.7.49 1.4.89 2.08-.93-.14-1.83-.49-2.67-1.06-.34-.22-.88-.48-1.16-.74z"></path>
									</svg>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-lg-8 col-lg-offset-2 col-md-10 col-xs-12 post type-post status-publish format-standard has-post-thumbnail hentry">
				<div class="mainheading">
					<div class="row post-top-meta hidden-md-down">
						<div class="col-md-2 col-xs-12">
							<a href="<?php echo site_url('author/'.$post_user_name); ?>">
								<img alt='<?php echo $post_user_fname .' '. $post_user_lname; ?>' src='<?php echo $post_user_image; ?>' class='avatar avatar-72 photo imgavt' height='72' width='72' />
							</a>
						</div>
						<div class="col-md-10 col-xs-12"> 
							<a class="text-capitalize link-dark" href="<?php echo site_url('author/'.$post_user_name); ?>">
								<?php echo $post_user_fname .' '. $post_user_lname; ?> <span class="btn follow">Follow</span>
							</a>
							<span class="author-description d-block"><?php echo $data['author']->description; ?></span>
						</div>
					</div>
					<h1 class="posttitle"><?php echo $title; ?></h1>
					<p>
						<span class="post-date"><time class="post-date"><?php echo $date; ?></time></span>
						<span class="dot"></span>
						<span class="readingtime">1 min read</span>
					</p>
				</div>

				<?php if (!empty($video_id)) { ?>
					<iframe id="vid_frame" class="vid_frame featured-image" src="https://www.youtube.com/embed/<?php echo $video_id; ?>?rel=0&showinfo=0&autohide=1" frameborder="0" allowFullScreen="allowFullScreen" width="100%" height="450"></iframe>
				<?php } else { ?>
					<img width="1400" height="700" src="<?php echo $image; ?>" class="featured-image img-fluid wp-post-image" alt="<?php echo $title; ?>" />
				<?php } ?>
				
				<article class="article-post"><div class="clearfix"></div><?php echo $content; ?><div class="clearfix"></div></article>
				<div class="hidden-lg-up">
					<div id="pt-claps-applause-<?php echo $data['detail']->id; ?>" class="pt-claps-applause<?php if (!empty($data['claps_ids'])) { echo ' has_rated';} ?>">
						<?php if (!empty($data['claps_ids']) && $data['claps_ids'] == '1') { ?>
							<a class="claps-button" data-id="<?php echo $data['detail']->id; ?>"><span class="lovedit">Already applauded!</span></a>
						<?php } else { ?>
							<a class="claps-button" href="<?php echo site_url($permalink); ?>" data-id="<?php echo $data['detail']->id; ?>" data-token-name="<?php echo $csrf_token['name'] ?>" data-token-hash="<?php echo $csrf_token['hash'] ?>" data-type="post"></a>
						<?php } ?>
						<span id="claps-count-<?php echo $data['detail']->id; ?>" class="claps-count"><?php echo $clap; ?></span>
					</div>
				</div>
				<div class="hidden-lg-up share-horizontal">
					<p>Share</p>
					<ul class="shareitnow">
						<li>
							<a target="_blank" href="https://twitter.com/intent/tweet?text=<?php echo rawurlencode($title); ?>&amp;url=<?php echo rawurlencode($permalink); ?>">
							<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo rawurlencode($permalink); ?>">        
							<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li>
							<a target="_blank" href="https://plus.google.com/share?url=<?php echo rawurlencode($permalink); ?>">
							<i class="fa fa-google"></i>
							</a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="row post-top-meta hidden-lg-up">
					<div class="col-md-2 col-xs-4">
						<img alt='<?php echo $post_user_fname .' '. $post_user_lname; ?>' src='<?php echo $post_user_image; ?>' class='avatar avatar-72 photo' height='72' width='72' />
					</div>
					<div class="col-md-10 col-xs-8">
						<a class="text-capitalize link-dark" href="<?php echo site_url('author/'.$post_user_name); ?>">
							<?php echo $post_user_fname .' '. $post_user_lname; ?> <span class="btn follow">Follow</span>
						</a>
						<span class="author-description d-block"><?php echo $data['author']->description; ?></span>
					</div>
				</div>
				<div class="after-post-tags">
					<ul class="post-categories">
						<li><a href="<?php echo site_url($cat_slug); ?>" rel="category tag"><?php echo $cat_title; ?></a></li>
					</ul>
				</div>
				<div class="row mb-5 prevnextlinks justify-content-center align-items-center">
					<div class="col-md-6 col-xs-12 rightborder pl-0">
						<?php if (!empty($data['prev_post'])) { ?>
							<div class="thepostlink">&laquo; <a href="<?php echo site_url($data['prev_post']->slug); ?>/" rel="prev"><?php echo $data['prev_post']->title; ?></a></div>
						<?php } ?>
					</div>
					<div class="col-md-6 col-xs-12 text-right pr-0">
						<?php if (!empty($data['next_post'])) { ?>
							<div class="thepostlink"><a href="<?php echo site_url($data['next_post']->slug); ?>/" rel="next"><?php echo $data['next_post']->title; ?></a> &raquo;</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="hideshare"></div>
	<div class="graybg">
		<div class="container">
			<?php if (!empty($data['related_post'])) { ?>
				<div class="row justify-content-center listrecent listrelated">
					<?php
					for ($ir=0; $ir < count($data['related_post']); $ir++) {
						if (str_word_count($data['related_post'][$ir]->title) <= 9) {
							$rel_title = $data['related_post'][$ir]->title;
						} else {
							$rel_title = explode(" ", $data['related_post'][$ir]->title);
							$rel_title = implode(" ", array_splice($rel_title, 0, 9)) . '...';
						}
						if (str_word_count($data['related_post'][$ir]->excerpt) <= 19) {
							$rel_excerpt = strip_tags($data['related_post'][$ir]->excerpt);
						} else {
							$rel_excerpt = explode(" ", strip_tags($data['related_post'][$ir]->excerpt));
							$rel_excerpt = implode(" ", array_splice($rel_excerpt, 0, 19));
						}
						$rel_permalink = site_url($data['related_post'][$ir]->slug);
						$rel_date      = date('M j, Y', strtotime($data['related_post'][$ir]->published_at));
						$rel_image     = json_decode($data['related_post'][$ir]->image);
						$rel_image     = (!empty($rel_image)) ? base_url('assets/uploads/'.$rel_image->image) : get_template_directory(dirname(__FILE__), 'img/image-default.jpg');
						$video_id      = ((!empty($data['related_post'][$ir]->additional) && preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $data['related_post'][$ir]->additional, $match))) ? $match[1] : '';
						$rel_image     = (!empty($video_id)) ? 'https://img.youtube.com/vi/'.$video_id.'/maxresdefault.jpg' : $rel_image;

						/* Get User */
						$data_user = $data['user'];
						$rel_user = $data['related_post'][$ir]->created_by;
						for ($iu=0; $iu < count($data_user); $iu++) { 
							if ($data_user[$iu]->id == $rel_user) {
								$rel_user_id    = $data_user[$iu]->id;
								$rel_user_name  = $data_user[$iu]->username;
								$rel_user_fname = $data_user[$iu]->first_name;
								$rel_user_lname = $data_user[$iu]->last_name;
								$rel_user_image = json_decode($data_user[$iu]->avatar);
								$rel_user_image = (!empty($rel_user_image)) ? base_url('assets/uploads/'.$rel_user_image->thumb) : 'http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=100&d=mm&r=g';
								break;
							}
						} ?>
						<div class="col-lg-4 col-md-4 col-sm-4">
							<div class="card post highlighted">
								<a class="thumbimage" href="<?php echo $rel_permalink; ?>" style="background-image:url(<?php echo $rel_image; ?>);"></a>
								<div class="card-block">
									<h2 class="card-title">
										<a href="<?php echo $rel_permalink; ?>"><?php echo $rel_title; ?></a>
									</h2>
									<div class="metafooter">
										<div class="wrapfooter">
											<span class="meta-footer-thumb"> 
												<a href="<?php echo site_url('author/'.$rel_user_name); ?>">
													<img alt='<?php echo $rel_user_fname .' '. $rel_user_lname; ?>' src='<?php echo $rel_user_image; ?>' class='avatar avatar-40 photo author-thumb' height='40' width='40' />
												</a>
											</span>
											<span class="author-meta"> 
												<span class="post-name">
													<a href="<?php echo site_url('author/'.$rel_user_name); ?>"><?php echo $rel_user_fname .' '. $rel_user_lname; ?></a>
												</span>
												<br>
												<span class="post-date"><?php echo $rel_date; ?></span>
												<span class="dot"></span>
												<span class="readingtime">1 min read</span>
											</span>
											<span class="post-read-more">
												<a href="<?php echo $rel_permalink; ?>" title="<?php echo $rel_title; ?>">
													<svg class="svgIcon-use" width="25" height="25" viewBox="0 0 25 25">
														<path d="M19 6c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v14.66h.012c.01.103.045.204.12.285a.5.5 0 0 0 .706.03L12.5 16.85l5.662 4.126a.508.508 0 0 0 .708-.03.5.5 0 0 0 .118-.285H19V6zm-6.838 9.97L7 19.636V6c0-.55.45-1 1-1h9c.55 0 1 .45 1 1v13.637l-5.162-3.668a.49.49 0 0 0-.676 0z" fill-rule="evenodd"></path>
													</svg>
												</a>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
					} ?>
				</div>
				<div class="clearfix"></div>
			<?php } ?>

			<?php if ($data['detail']->comment == 1) { ?>
				<div class="row justify-content-center">
					<div class="col-md-8">
						<div id="comments" class="comments-area">
							<?php if (!empty($data['comment_message'])) {
								echo '<div class="alert-message '.$data['comment_message']['status'].'">' . $data['comment_message']['message'] . '</div>';
							} ?>
							<?php if ($data['count_comment'] > 0) { ?>
								<h2 class="comments-title"><?php if ($data['count_comment'] == '1') {echo 'One';} else {echo $data['count_comment'];};?> Reply to &ldquo;<?php echo $title; ?>&rdquo;</h2>
								<ol class="comment-list">
									<?php for ($ic=0; $ic < count($data['comments']); $ic++) { ?>
									<li class="comment even thread-odd thread-alt depth-1">
										<article class="comment-body">
											<footer class="comment-meta">
												<div class="comment-author vcard">
													<img alt='' src='http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=36&#038;d=mm&#038;r=g' srcset='http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=72&#038;d=mm&#038;r=g 2x' class='avatar avatar-36 photo' height='36' width='36' />
													<b class="fn"><?php echo $data['comments'][$ic]->c_name; ?></b> <span class="says">says:</span>					
												</div>
												<div class="comment-metadata">
													<time datetime="2018-04-16T18:58:51+00:00">April 16, 2018 at 6:58 pm | <?php echo time_ago($data['comments'][$ic]->c_created_at) ?></time>
												</div>
											</footer>
											<div class="comment-content">
												<p><?php echo $data['comments'][$ic]->c_comment; ?></p>
											</div>
										</article>
									</li>
									<?php } ?>
								</ol>
							<?php } ?>
							<div id="respond" class="comment-respond">
								<h3 id="reply-title" class="comment-reply-title">Leave a Reply</h3>
								<form action="<?php echo current_url(); ?>/#comments" method="POST" class="comment-form" novalidate>
									<p class="comment-notes">
										<span id="email-notes">Your email address will not be published.</span> Required fields are marked <span class="required">*</span>
									</p>
									<p class="comment-form-comment">            
										<textarea required id="comment" name="comment" placeholder="Write a response..." cols="45" rows="8" aria-required="true"><?php echo get_flash('comment') ?></textarea>
									</p>
									<div class="row">
										<p class="comment-form-author col-md-4">
											<input required id="name" name="name" type="text" placeholder="Name" value="<?php echo get_flash('name') ?>" size="30" aria-required='true' />
										</p>
										<p class="comment-form-email col-md-4">
											<input required id="email" name="email" type="email" placeholder="E-mail address" value="<?php echo get_flash('email') ?>" size="30" aria-required='true' />
										</p>
										<p class="comment-form-url col-md-4">
											<input id="url" name="url" type="url"  placeholder="Website Link" value="<?php echo get_flash('url') ?>" size="30" />
										</p>
									</div>
									<p class="form-submit">
										<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
										<input name="submit" type="submit" id="submit" class="submit" value="Post Comment" />
									</p>
								</form>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>