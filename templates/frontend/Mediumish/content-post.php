<section class="recent-posts">
	<div class="section-title">
		<?php if (!empty($data['header_subtitle'])) { ?>
			<h5 class="font400"><p><?php echo $data['header_subtitle']; ?></p></h5>
		<?php } ?>
		<h2><span><?php echo $data['header_title']; ?></span></h2> 
	</div>
	<div class="masonrygrid row listrecent">
		<?php
		$data_user = $data['user'];
		$post_data = array_slice($data['recent_posts'], 0, 6);

		for ($ip=0; $ip < count($post_data); $ip++) {
			if (str_word_count($post_data[$ip]->title) <= 9) {
				$post_title = $post_data[$ip]->title;
			} else {
				$post_title = explode(" ", $post_data[$ip]->title);
				$post_title = implode(" ", array_splice($post_title, 0, 9)) . '...';
			}
			if (str_word_count($post_data[$ip]->excerpt) <= 19) {
				$post_excerpt = strip_tags($post_data[$ip]->excerpt);
			} else {
				$post_excerpt = explode(" ", strip_tags($post_data[$ip]->excerpt));
				$post_excerpt = implode(" ", array_splice($post_excerpt, 0, 19));
			}
			$post_slug    = $post_data[$ip]->slug;
			$post_date    = date('M j, Y', strtotime($post_data[$ip]->published_at));
			$post_image   = json_decode($post_data[$ip]->image);
			$post_image   = (!empty($post_image)) ? base_url('assets/uploads/'.$post_image->thumb) : get_template_directory(dirname(__FILE__), 'img/image-default.jpg');
			$video_id     = ((!empty($post_data[$ip]->additional) && preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $post_data[$ip]->additional, $match))) ? $match[1] : '';
			$post_image   = (!empty($video_id)) ? 'https://i.ytimg.com/vi/'.$video_id.'/hqdefault.jpg' : $post_image;

			/* Get User */
			$post_user    = $post_data[$ip]->created_by;
			for ($iu=0; $iu < count($data_user); $iu++) { 
				if ($data_user[$iu]->id == $post_user) {
					$post_user_id    = $data_user[$iu]->id;
					$post_user_name  = $data_user[$iu]->username;
					$post_user_fname = $data_user[$iu]->first_name;
					$post_user_lname = $data_user[$iu]->last_name;
					$post_user_image = json_decode($data_user[$iu]->avatar);
					$post_user_image = (!empty($post_user_image)) ? base_url('assets/uploads/'.$post_user_image->thumb) : 'http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=100&d=mm&r=g';
					break;
				}
			} ?>
			<div class="col-lg-4 col-md-4 col-sm-6 grid-item" id="post-165">
				<div class="card post highlighted">
					<a class="thumbimage" href="<?php echo site_url($post_slug); ?>" style="background-image:url(<?php echo $post_image; ?>);"></a>
					<div class="card-block">
						<h2 class="card-title"><a href="<?php echo site_url($post_slug); ?>"><?php echo $post_title; ?></a></h2>
						<span class="card-text d-block"><?php echo $post_excerpt; ?>...</span>
						<div class="metafooter">
							<div class="wrapfooter"> 
								<span class="meta-footer-thumb"> 
									<a href="<?php echo site_url('author/'.$post_user_name); ?>">
										<img alt='<?php echo $post_user_fname .' '. $post_user_lname; ?>' src='<?php echo $post_user_image; ?>' class='avatar avatar-40 photo author-thumb' height='40' width='40' />
									</a>
								</span>
								<span class="author-meta"> 
									<span class="post-name">
										<a href="<?php echo site_url('author/'.$post_user_name); ?>"><?php echo $post_user_fname .' '. $post_user_lname; ?></a>
									</span>
									<br> 
									<span class="post-date"><?php echo $post_date; ?></span>
									<span class="dot"></span>
									<span class="readingtime">1 min read</span> 
								</span> 
								<span class="post-read-more">
									<a href="<?php echo site_url($post_slug); ?>" title="<?php echo $post_title; ?>">
									<svg class="svgIcon-use" width="25" height="25" viewBox="0 0 25 25">
										<path d="M19 6c0-1.1-.9-2-2-2H8c-1.1 0-2 .9-2 2v14.66h.012c.01.103.045.204.12.285a.5.5 0 0 0 .706.03L12.5 16.85l5.662 4.126a.508.508 0 0 0 .708-.03.5.5 0 0 0 .118-.285H19V6zm-6.838 9.97L7 19.636V6c0-.55.45-1 1-1h9c.55 0 1 .45 1 1v13.637l-5.162-3.668a.49.49 0 0 0-.676 0z" fill-rule="evenodd"></path>
									</svg>
									</a>
								</span>
							</div>  
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>
	<?php echo $data['pagination']; ?>
</section>