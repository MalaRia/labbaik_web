<div class="site-content">
	<div class="container">
		<div class="section-title">
			<h2><span>Contact</span></h2>
		</div>
		<div class="row justify-content-center">
			<div class="col-md-12">
				<article id="post-190" class="post-190 page type-page status-publish hentry">
					<div class="entry-content">
						<div role="form" class="wpcf7" id="wpcf7-f8-p190-o1" lang="en-US" dir="ltr">
							<div class="screen-reader-response">
							<?php if (!empty($data['comment_message'])) {
								echo '<div class="alert-message '.$data['comment_message']['status'].'">' . $data['comment_message']['message'] . '</div>';
							} ?>
							</div>
							<form action="" method="POST" class="wpcf7-form" novalidate="novalidate">
								<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
								<style>.wpcf7 input, .wpcf7  textarea {width:100%;}
									.wpcf7 input[type="submit"] {width:auto;}
								</style>
								<div class="row">
									<div class="col-md-6 col-lg-6 mb-1">
										<span class="wpcf7-form-control-wrap your-name">
											<input type="text" name="full_name" value="<?php echo get_flash('full_name') ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Name" />
										</span>
									</div>
									<div class="col-md-6 col-lg-6 mb-1">
										<span class="wpcf7-form-control-wrap your-email">
											<input type="email" name="email" value="<?php echo get_flash('email') ?>" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="E-mail" />
										</span>
									</div>
								</div>
								<div class="mt-3">
									<span class="wpcf7-form-control-wrap your-message">
										<textarea name="description" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Message"><?php echo get_flash('description') ?></textarea>
									</span>
								</div>
								<div class="mt-3">
									<input name="submit" type="submit" value="Send" class="wpcf7-form-control wpcf7-submit" />
								</div>
							</form>
						</div>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>