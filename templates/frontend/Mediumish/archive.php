<div class="site-content">
	<div class="container">

		<?php

			if (!empty($data['recent_posts'])) get_template('content-post');

			if ($data['jumbotron']) get_template('jumbotron');

		?>

	</div>
</div>