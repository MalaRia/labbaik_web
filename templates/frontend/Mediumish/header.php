<?php

// print_r($data['list_author']); die();

?><!DOCTYPE html>
<html lang="en-US">
<head>
<?php if ($data['meta']['ga_verify']): ?>
<meta name="google-site-verification" content="<?php echo $data['meta']['ga_verify'] ?>">
<?php endif;?>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $data['meta']['title'] ?></title>
<meta name="description" content="<?php echo $data['meta']['description'] ?>"/>
<meta name="keyword" content="<?php echo $data['meta']['keywords'] ?>"/>
<meta property="og:locale" content="id_ID" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo $data['meta']['title'] ?>" />
<meta property="og:description" content="<?php echo $data['meta']['description'] ?>" />
<meta property="og:site_name" content="<?php echo $data['meta']['site_title'] ?>" />
<meta property="article:published_time" content="<?php echo $data['meta']['published_at'] ?>" />
<meta property="twitter:card" content="summary" />
<meta property="twitter:description" content="<?php echo $data['meta']['description'] ?>" />
<meta property="twitter:title" content="<?php echo $data['meta']['title'] ?>" />
<meta property="DC.date.issued" content="<?php echo $data['meta']['published_at'] ?>" />
<meta property="og:title" content="<?php echo $data['meta']['title'] ?>"/>
<meta property="og:image" content="<?php echo $data['meta']['image'] ?>"/>
<meta property="og:type" content="web"/>
<meta property="og:url" content="<?php echo $data['meta']['url'] ?>"/>
<meta property="og:description" content="<?php echo $data['meta']['description'] ?>"/>
<meta name="twitter:card" content="web">
<meta name="twitter:url" content="<?php echo $data['meta']['url'] ?>">
<meta name="twitter:title" content="<?php echo $data['meta']['title'] ?>">
<meta name="twitter:image" content="<?php echo $data['meta']['image'] ?>">
<meta name="twitter:description" content="<?php echo $data['meta']['description'] ?>">
<link rel="dns-prefetch" href="//cdnjs.cloudflare.com" />
<link rel="dns-prefetch" href="//maxcdn.bootstrapcdn.com" />
<link rel="alternate" href="<?php echo site_url($data['meta']['rss']) ?>" type="application/rss+xml" title="<?php echo $web_info->site_title ?> &raquo; Feed" />
<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), 'css/bootstrap.min.css') ?>" type="text/css" media="all" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css" media="all" />
<?php if (!empty($data['detail'])) {
    ?>
<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), 'css/claps-applause.css') ?>" type="text/css" media="all" />
<?php
}?>
<?php if (isset($data['video_page'])) {
    ?>
<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), 'css/ytv.css') ?>" type="text/css" media="all" />
<?php
}?>
<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), 'css/style.css') ?>" type="text/css" media="all" />
<style type="text/css">.btn-simple{background-color:#1C9963;border-color:#1C9963;}.prevnextlinks a, .article-post a, .post .btn.follow, .post .post-top-meta .author-description a, article.page a, .alertbar a{color:#1C9963;}.post .btn.follow, .alertbar input[type="submit"]{border-color:#1C9963;}blockquote{border-color:#1C9963;}.entry-content input[type=submit], .alertbar input[type="submit"]{background-color:#1C9963;border-color:#1C9963;}p.sharecolour{color:#999999;}.shareitnow ul li a svg, .shareitnow a{fill:#b3b3b3;}.shareitnow li a{color:#b3b3b3;border-color:#d2d2d2;}#comments a{color:#1C9963;}.comment-form input.submit{background-color:#1C9963;border-color:#1C9963;}footer.footer a{color:#1C9963;}.carousel-excerpt .fontlight,body{font-family:"PT Sans", Helvetica, Arial, sans-serif;font-size:15px;font-weight:400;line-height:1.5;color:#666666;}.h1,.h2,.h3,.h4,.h5,.h6,h1,h2,h3,h4,h5,h6{font-family:"PT Sans", Helvetica, Arial, sans-serif;font-weight:700;color:#111111;}.carousel-excerpt .title{font-family:"Shadows Into Light", "Comic Sans MS", cursive, sans-serif;font-size:30px;font-weight:400;text-transform:none;color:#ffffff;}.carousel-excerpt .fontlight{font-size:18px;font-weight:400;color:#ffffff;}.mediumnavigation .navbar-brand{font-family:Merriweather, Georgia, serif;font-size:26px;font-weight:700;}.navbar-toggleable-md .navbar-collapse{font-family:Lato, Helvetica, Arial, sans-serif;font-weight:400;}.article-post{font-family:Merriweather, Georgia, serif;font-weight:400;line-height:1.8;color:#222222;}.mediumnavigation, .dropdown-menu, .dropdown-item{background-color:rgba(255,255,255,0.97);}.mediumnavigation, .mediumnavigation a, .navbar-light .navbar-nav .nav-link{color:#999999;}.navbar-light .navbar-brand{color:#111111;}.navbar-light .navbar-brand:hover{color:#02b875;}.customarea .btn.follow{border-color:#02b875;color:#02b875;}.search-form .search-submit{background-color:#02b875;}.search-form .search-field{border-color:#f9f9f9;}.search-form .search-submit .fa{color:#ffffff;}.search-form .search-field, .search-form .search-field::placeholder{color:#b2b2b2;}@media (max-width: 767px){.navbar-collapse{background-color:rgba(255,255,255,0.97);}}</style>
<style type="text/css">.listfeaturedtag .card {overflow:initial;}@media (min-width: 992px){.navbar-toggleable-sm .navbar-nav .nav-link {font-size: 0.80rem;font-weight: 600;letter-spacing: 0.8px;}}</style>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/jquery.js') ?>"></script>
<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), 'js/jquery-migrate.min.js') ?>"></script>

<script type="text/javascript">
	/*! http://mrclay.org/index.php/2010/11/14/using-jquery-before-its-loaded/ */ (function(a){if(a.$)return;var b=[];a.$=function(c){b.push(c)};a.defer$=function(){while(f=b.shift())$(f)}})(window);
</script>

<?php if ($data['meta']['ga_analytics']): ?>
<!-- Google Analytics -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', '<?php echo $data['meta']['ga_analytics'] ?>', 'auto');
ga('send', 'pageview');
</script>
<!-- End Google Analytics -->
<?php endif;?>

</head>
<body class="home blog">

<header class="navbar-light bg-white fixed-top mediumnavigation">
	<div class="container">
		<div class="row justify-content-center align-items-center brandrow">
			<div class="col-lg-4 col-md-4 col-xs-12 hidden-xs-down customarea">
				<a target="_blank" href="<?php echo 'https://www.instagram.com/labbaik.id' ?>" class="btn follow"><i class="fa fa-instagram"></i> Follow</a>
				<a target="_blank" href="<?php echo $social_media->twitter ?>"> <i class="fa fa-twitter social"></i></a>
				<a target="_blank" href="<?php echo $social_media->facebook ?>"> <i class="fa fa-facebook social"></i></a>
			</div>
			<div class="col-lg-4 col-md-4  col-xs-12 text-center logoarea">
				<?php if (!empty($web_info->logo->image)) {
    ?>
				<a class="navbar-brand brand-image" href="<?php echo site_url() ?>"><img src="<?php echo $this->upload_location . '/' . $web_info->logo->image; ?>" alt="<?php echo $web_info->site_title ?>"></a>
				<?php } else {?>
				<a class="navbar-brand" href="<?php echo site_url() ?>"><?php echo $web_info->site_title; ?></a>
				<?php }?>
			</div>
			<div class="col-lg-4 col-md-4 mr-auto col-xs-12 text-right searcharea">
				<form role="search" method="get" class="search-form" action="<?php echo site_url() ?>">
					<input type="search" class="search-field" placeholder="Search..." value="" name="search" title="Search for:" />
					<button type="submit" class="search-submit">
					<i class="fa fa-search"></i>
					</button>
				</form>
			</div>
		</div>
		<div class="navarea">
			<nav class="navbar navbar-toggleable-sm">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div id="bs4navbar" class="collapse navbar-collapse">
					<ul id="menu-top-menu" class="navbar-nav col-md-12 justify-content-center">
						<?php foreach ($pages as $page) {?>
						<li class="nav-item<?php echo ($data['current'] == $page->slug || empty($data['current'])) ? ' active' : '' ?>">
							<a class="nav-link<?php echo ($data['current'] == $page->slug) ? ' active' : ''; ?>" href="<?php echo site_url($page->slug); ?>" itemprop="<?php echo site_url($page->slug); ?>">
								<?php echo $page->title ?>
							</a>
						</li>
						<?php }?>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>