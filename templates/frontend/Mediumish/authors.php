<div class="site-content">
	<div class="container">
		<div class="section-title">
			<h2>
				<span>Authors</span>
			</h2>
		</div>
		<article id="post-187" class="post-187 page type-page status-publish hentry">
			<div class="entry-content">
			</div>
		</article>
		<div class="row listrecent listrecent listauthor">

			<?php if (!empty($data['list_author'])) {
				for ($i=0; $i < count($data['list_author']); $i++) {

					$author_posts   = $this->post_model->count_post(array('status' => 'publish','created_by' => $data['list_author'][$i]->id), 'id');
					$author_website = $data['list_author'][$i]->website;
					$author_weburl  = (strpos($author_website, 'http') !== false) ? $author_website : 'http://'.$author_website;
					$author_image   = json_decode($data['list_author'][$i]->avatar);
					$author_image = (!empty($author_image)) ? base_url('assets/uploads/'.$author_image->image) : 'http://2.gravatar.com/avatar/ec7743afa99c0a4efdb855495341ac9b?s=100&d=mm&r=g';

					?>
					<div class="col-lg-4 col-md-4">
						<div class="card post author text-center mt-5 mb-2">
							<div class="card-block">
								<a href="<?php echo site_url('author/'.$data['list_author'][$i]->username); ?>">
									<img alt='<?php echo ucwords($data['list_author'][$i]->first_name .' '. $data['list_author'][$i]->last_name); ?>' src='<?php echo $author_image; ?>' class='avatar avatar-90 photo' height='90' width='90' />
								</a>
								<h2 class="card-title">
									<a href="<?php echo site_url('author/'.$data['list_author'][$i]->username); ?>" class="contributor-link"><?php echo ucwords($data['list_author'][$i]->first_name .' '. $data['list_author'][$i]->last_name); ?><br/><small><?php echo $author_posts; ?> posts</small></a>
								</h2>
								<span class="card-text d-block"><?php echo $data['list_author'][$i]->description; ?></span>
								<span class="card-text d-block mt-1">
									<?php if (!empty($data['list_author'][$i]->website)) { ?>
										<a target="_blank" href="<?php echo $author_weburl; ?>"><?php echo $author_website; ?></a>
									<?php } ?>
								</span>
								<div class="profile-icons mt-3">
									<?php if (!empty($data['list_author'][$i]->facebook)) { ?>
										<a target="_blank" href="<?php echo $data['list_author'][$i]->facebook; ?>"><i class="fa fa-facebook"></i></a> &nbsp;
									<?php } ?>

									<?php if (!empty($data['list_author'][$i]->twitter)) { ?>
										<a target="_blank" href="<?php echo $data['list_author'][$i]->twitter; ?>"><i class="fa fa-twitter"></i></a> &nbsp;
									<?php } ?>

									<?php if (!empty($data['list_author'][$i]->gplus)) { ?>
										<a target="_blank" href="<?php echo $data['list_author'][$i]->gplus; ?>"><i class="fa fa-google-plus"></i></a> &nbsp;
									<?php } ?>

									<?php if (!empty($data['list_author'][$i]->instagram)) { ?>
										<a target="_blank" href="<?php echo $data['list_author'][$i]->instagram; ?>"><i class="fa fa-instagram"></i></a> &nbsp;
									<?php } ?>

									<?php if (!empty($data['list_author'][$i]->linkedin)) { ?>
										<a target="_blank" href="<?php echo $data['list_author'][$i]->linkedin; ?>"><i class="fa fa-linkedin"></i></a> &nbsp;
									<?php } ?>

									<?php if (!empty($data['list_author'][$i]->email)) { ?>
										<a href="mailto:<?php echo $data['list_author'][$i]->email; ?>"><i class="fa fa-send-o"></i></a> &nbsp;
									<?php } ?>
								</div>
							</div>
						</div>
					</div>
					<?php
				}
			} ?>
		</div>
	</div>
</div>