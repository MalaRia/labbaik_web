<div class="site-content">
	<div class="container">

		<?php

			if (!empty($data['recent_posts'])) get_template('slider');

			if (!empty($data['featured_posts'])) get_template('featured-post');

			if (!empty($data['recent_posts'])) get_template('content-post');
			
			if ($data['jumbotron']) get_template('jumbotron');

		?>

	</div>
</div>