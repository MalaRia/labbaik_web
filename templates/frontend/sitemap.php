<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url>
        <loc><?php echo $site_url;?></loc> 
        <priority>1.0</priority>
    </url>

    <?php foreach($sitemap as $map) : ?>
    <url>
        <loc><?php echo site_url($map->slug); ?></loc>
        <priority>0.5</priority>
    </url>
    <?php endforeach; ?>
</urlset>