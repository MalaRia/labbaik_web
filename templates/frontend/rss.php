<rss version="2.0"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:admin="http://webns.net/mvcb/"
xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
xmlns:media="http://search.yahoo.com/mrss/"
xmlns:content="http://purl.org/rss/1.0/modules/content/">

    <channel>
        <title><?php echo $feed_name; ?></title>
        <link><?php echo $feed_url; ?></link>
        <description><?php echo $page_description; ?></description>
        <dc:language><?php echo $page_language; ?></dc:language>
        <dc:creator><?php echo $creator_email; ?></dc:creator>
        <dc:rights>Copyright <?php echo $this->data['web_info']->site_title .' '. current_datetime(); ?></dc:rights>
        <admin:generatorAgent rdf:resource="<?php echo site_url() ?>" />
        <?php foreach($posts as $post):?>
        <item>
            <title><?php echo $post->title; ?></title>
            <link><?php echo site_url($post->p_slug."/".$post->slug); ?></link>
            <guid><?php echo site_url($post->p_slug."/".$post->slug); ?></guid>
            <pubDate><?php echo $post->published_at;?></pubDate>
            <description><![CDATA[  <?php echo word_limiter(strip_tags($post->excerpt),50); ?>     ]]></description>
            <creator><?php echo ucfirst($post->first_name) .' '. ucfirst($post->last_name); ?></creator>
        </item>
        <?php endforeach; ?>
    </channel>
</rss>