        <!-- BEGIN CONTENT -->
        <?php 
        $image = ($data['info']->image) ? json_decode($data['info']->image) : ''; 
				$img = ($image) ? $image->image : '';
				
				$galleries = '';
				$video = '';
				if ($data['info']->type == 'image')
					$galleries = ($data['info']->additional) ? json_decode($data['info']->additional) : '';
				if ($data['info']->type == 'video')
				$video = $data['info']->additional;
        
        $title = $data['info']->title;
        $content = $data['info']->content;
        $published_at = indonesian_date($data['info']->published_at);
        ?>
        <div class="page-description" id="gallery-detail">
          <div class="container">
            <div class="blog-header">
              <h3 class="blog-title" itemprop="title"><?php echo $title ?></h3>
              <div class="blog-info">
                <label class="blog-publish"><time itemprop="startDate" datetime="<?php echo $published_at ?>"><?php echo $published_at ?></label>
                <span class="view" itemprop="view">viewed <i class="fa fa-eye" ></i><i class="text" itemprop="text"> <?php echo $data['visitors'] ?></i></span>
              </div>
              <div itemprop="description"><?php echo $content; ?></div>
             </div>
            <div class="media-box">
                <!-- if image -->
							<?php if ($galleries) : foreach($galleries as $gallery): ?>
							<div class="col-lg-3 col-sm-3 col-xs-4"><a href="<?php echo base_url('assets/uploads/images/'.$gallery->image) ?>" data-lightbox="roadtrip" class="thumbnail" style="background-image: url('<?php echo base_url('assets/uploads/thumbs/'.$gallery->thumb) ?>')" itemprop="images"></a></div>
							<?php endforeach; endif; ?>
							<div class="col-lg-12 col-sm-12 col-xs-12">
								<iframe src="<?php echo $video; ?>" width="100%" height="500" frameborder="0"></iframe>
							</div>
							<?php if ($video) : ?>
							<?php endif; ?>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
        <!-- END CONTENT -->
