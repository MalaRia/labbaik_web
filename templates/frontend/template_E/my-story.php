         
        
        <!-- BEGIN CONTAINER -->
        <?php
          $image = ($data['info']->image) ? json_decode($data['info']->image) : '';
          $image = ($image) ? $image->image : '';

          $info = (strpos($data['info']->content,'==') > 1) ? explode('==', $data['info']->content) : '';
          $title = (isset($info[0])) ? $info[0] : '';
          $content = (isset($info[1])) ? $info[1] : '';
        ?>
        <div id="home-banner" style="background-image: url(<?php echo ($image) ? base_url('assets/uploads/images/'.$image) : get_template_directory(dirname(__FILE__), 'assets/img/gallery/book-1836380_1920.jpg') ?>);" itemprop="images">
          <!-- <div class="slider-gradient"></div> -->
        </div>
        <div class="container">
          <div class="col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class=" description">
              <h2 itemprop="title"><?php echo strip_tags($title); ?></h2>
              <?php echo html_entity_decode($content); ?>
              <!-- <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate ligula sem, non volutpat felis lobortis sed. Nunc congue id dolor nec ullamcorper. Maecenas vehicula erat non malesuada vestibulum. Praesent nec turpis quis tortor dapibus cursus. Praesent pharetra tincidunt molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate ligula sem, non volutpat felis lobortis sed. Nunc congue id dolor nec ullamcorper. Maecenas vehicula erat non malesuada vestibulum. Praesent nec turpis quis tortor dapibus cursus. Praesent pharetra tincidunt molestie. Maecenas urna urna, lobortis nec erat non, vehicula laoreet ipsum. Quisque nec vestibulum velit. Quisque ultrices neque quis lectus ullamcorper elementum. Curabitur vel nulla in libero dignissim consectetur. Duis dictum sagittis eros eget suscipit. Praesent in eros a odio fringilla aliquet in imperdiet nibh.</p>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate ligula sem, non volutpat felis lobortis sed. Nunc congue id dolor nec ullamcorper. Maecenas vehicula erat non malesuada vestibulum. Praesent nec turpis quis tortor dapibus cursus. Praesent pharetra tincidunt molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate ligula sem, non volutpat felis lobortis sed. Nunc congue id dolor nec ullamcorper. Maecenas vehicula erat non malesuada vestibulum. Praesent nec turpis quis tortor dapibus cursus. Praesent pharetra tincidunt molestie. Maecenas urna urna, lobortis nec erat non, vehicula laoreet ipsum. Quisque nec vestibulum velit. Quisque ultrices neque quis lectus ullamcorper elementum. Curabitur vel nulla in libero dignissim consectetur. Duis dictum sagittis eros eget suscipit. Praesent in eros a odio fringilla aliquet in imperdiet nibh.</p>
              <div class="media-box">
                
                <iframe src="https://www.youtube.com/embed/o9F22nwFMhU" width="100%" height="400" frameBorder="0" itemprop="movie"></iframe>
              </div>
              <p itemprop="description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate ligula sem, non volutpat felis lobortis sed. Nunc congue id dolor nec ullamcorper. Maecenas vehicula erat non malesuada vestibulum. Praesent nec turpis quis tortor dapibus cursus. Praesent pharetra tincidunt molestie. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vulputate ligula sem, non volutpat felis lobortis sed. Nunc congue id dolor nec ullamcorper. Maecenas vehicula erat non malesuada vestibulum. Praesent nec turpis quis tortor dapibus cursus. Praesent pharetra tincidunt molestie. Maecenas urna urna, lobortis nec erat non, vehicula laoreet ipsum. Quisque nec vestibulum velit. Quisque ultrices neque quis lectus ullamcorper elementum. Curabitur vel nulla in libero dignissim consectetur. Duis dictum sagittis eros eget suscipit. Praesent in eros a odio fringilla aliquet in imperdiet nibh.</p> -->
            </div>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END CONTAINER -->
