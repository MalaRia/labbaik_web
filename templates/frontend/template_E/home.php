<!--  BEGIN CONTENT  -->  
    <!-- BEGIN BANNER -->
    <?php
        $image = (isset($data['about_me']->image)) ? json_decode($data['about_me']->image) : '';
        $thumb = ($image) ? $image->thumb : '';
        $link = site_url($data['about_me']->slug);

        $about_me = (strpos($data['about_me']->content,'==') > 1) ? explode('==', $data['about_me']->content) : '';
        $title = (isset($info[0])) ? $about_me[0] : '';
        $excerpt = $data['about_me']->excerpt;
    ?>
    <div id="home-banner" style="background-image: url(<?php echo (isset($web_info->image)) ? $this->upload_location .'images/'.$web_info->image->image : get_template_directory(dirname(__FILE__), 'assets/img/gallery/book-1836380_1920.jpg') ?>);" itemprop="images">
        <div class="slider-gradient"></div>
        <div class="container">
            <div class="col-md-12 col-sm-12 col-xs-12 mycontact">
				<div class="col-md-6 col-sm-6 detailContact" style="background-image: url('<?php echo ($image) ? base_url('assets/uploads/thumbs/'.$thumb) : get_template_directory(dirname(__FILE__), 'assets/img/gallery/book-1836380_1920.jpg')?>');" itemprop="images"></div>
				<div class="col-md-6 col-sm-6 kontenContact">
					<h3 itemprop="title"><?php echo $title ?></h3>
					<p class="visible-xs" itemprop="description"><?php echo strip_tags($excerpt) ?></p>
					<p class="hidden-xs" itemprop="description"><?php echo strip_tags($excerpt) ?></p>
					<a href="<?php echo $link ?>" class="readAll pull-right">Read More</a>
				</div>
            </div>
        </div>
    </div>
    <div class="scedule-slider">
        <div class="container">
			<div class="title-schedule visible-xs">
				<h2><a href="#" class="blog-title" itemprop="link">My scedule</a></h2>
				<p><a href="#">see all</a></p>
			</div>
			<div class="lazy">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 schedule-box hidden-xs">
					<div class="scedule-content">
						<div class="link-info">
							<h3><a href="#" class="blog-title">My scedule</a></h3>
							<p itemprop="description"><a href="<?php echo site_url('schedule') ?>" style="text-decoration:none">see all</a></p>
						</div>
					</div>
				</div>
				<?php
				foreach ($data['featured_schedule'] as $schedule) :
					$title = $schedule->title;
					$content = (strpos($schedule->content, '==') > 1) ? explode('==', $schedule->content) : '';
					$time = ($content) ? $content[0] : '';
					$address = ($content) ? $content[1] : '';
				?>
				<div class="col-lg-3 col-md-3 col-sm-6 schedule-box">
					<div class="scedule-content" style="background-color:#f07ac0;">
						<div class="scedule-info">
							<h3><a class="practice-docter" itemprop="day"><?php echo $time ?></a></h3>
							<h3 class="nameRS" itemprop="nameRs"><?php echo $title ?></h3>
							<p itemprop="alamat"><?php echo $address ?></p>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
				<div class="clearfix"></div>
			</div>
        </div>
    </div>
    <div class="container">
        <div class="artikel">
			<?php $i = 1;
			foreach ($data['featured_blog'] as $blog) :
				$title = $blog->title;
				$link = site_url('blog/'.$blog->slug);
				$excerpt = strip_tags($blog->excerpt);
				$image = ($blog->image) ? json_decode($blog->image) : '';
				$img = $image->image;
				$thumb = $image->thumb;
				$published_at = indonesian_date($blog->published_at);
			?>

			<?php if ($i == 1) : ?>
			<div class="col-lg-6 col-md-6 col-sm-6 blogView">
				<img src="<?php echo ($image) ? base_url('assets/uploads/images/'.$img) : get_template_directory(dirname(__FILE__), 'assets/img/home/pexels-photo-70497.jpeg')?>" itemprop="images">
				<h2 itemprop="title"><a href="<?php echo $link ?>"><?php echo $title ?></a></h2>
				<p itemprop="description"><?php echo $excerpt ?></p>
				<label class="publish"><time itemprop="startDate" datetime="<?php echo $published_at ?>"><?php echo $published_at ?></label>
			</div>
			<?php else : ?>
			<div class="col-lg-6 col-md-6 col-sm-6 blogMore">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 konten-explore">
					<div class="col-lg-5 col-md-5 col-sm-6 col-xs-4 imageKonten" style="background-image:url('<?php echo ($image) ? base_url('assets/uploads/thumbs/'.$thumb) : get_template_directory(dirname(__FILE__), 'assets/img/home/kopi.png')?>');" itemprop="images">
						<a href="<?php echo $link ?>" itemprop="link"></a>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-6 col-xs-8 post-explore">
						<a href="<?php echo $link ?>" itemprop="title"><?php echo $title ?></a>
						<p class="hidden-xs" itemprop="description"><?php echo $excerpt ?></p>
						<label class="publish"><time itemprop="startDate" datetime="<?php echo $published_at ?>"><?php echo $published_at ?> </label>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php endif; $i++; endforeach; ?>
			<h5><a href="<?php echo site_url('blog') ?>" class="readAll pull-right" itemprop="readAll">Show More</a></h5>
			<div class="clearfix"></div>
        </div>
    </div>
    <div class="gallery">
        <div class="container">
			<div class="gallery-slider">	
				<?php foreach ($data['featured_gallery'] as $gallery) :
					$title = $gallery->title;
					$link = site_url('album/'.$gallery->slug);
					$excerpt = strip_tags($gallery->excerpt);
					$image = ($gallery->image) ? json_decode($gallery->image) : '';
					$thumb = $image->thumb;
					$published_at = indonesian_date($gallery->published_at);
				?>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 gallery-box">
						<div class="gallery-content" style="background-image: url(<?php echo ($thumb) ? base_url('assets/uploads/thumbs/'.$thumb) : get_template_directory(dirname(__FILE__), 'assets/img/home/sliderCenter.png')?>);" itemprop="images">
						<!-- <div class="transparent"></div> -->
						</div>
						<div class="konten-gallery">
							<h3><a href="<?php echo $link; ?>" itemprop="title"><?php echo word_limiter($title, 4); ?></a></h3>
							<label class="publish"><time itemprop="startDate" datetime="<?php echo $published_at; ?>"><?php echo $published_at; ?></label> 
						</div>
					</div>
				<?php endforeach; ?>
			</div>
        </div>
    </div>
    <!-- END BANNER -->
    <!--  END CONTENT  -->