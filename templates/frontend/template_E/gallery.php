			<!-- BEGIN CONTENT -->
			<div class="gallery" id="gallery-result">
				<div class="container">
					<div class="gallery-slider">
						<div class="col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8" id="result">
							<h3 class="title visible-xs">ALBUM</h3>
							<?php 
							if (!isset($data['galleries'])) :
								echo '<p>Galeri belum ada!</p>';
							else :
							foreach ($data['galleries'] as $gallery) : 
								// var_dump($gallery->image);
								$title = $gallery->title; 
								$link = site_url('album/'.$gallery->slug);
								$excerpt = strip_tags($gallery->excerpt);
								$image = ($gallery->image) ? json_decode($gallery->image) : '';
								$thumb = ($image) ? $image->thumb : '';
								$published_at = format_date($gallery->published_at);
							?>
							<div class="col-lg-4 col-md-4 col-sm-4 gallery-box">
								<div class="gallery-content" style="background-image: url(<?php echo ($thumb) ? base_url('assets/uploads/thumbs/'.$thumb) : base_url('assets/images/default.png') ?>);" itemprop="images">
								<!-- <div class="transparent"></div> -->
								<!-- <span class="fa fa-video-camera camera" aria-hidden="true"></span>         -->
								</div>
								<div class="konten-gallery">
									<h3><a href="<?php echo $link; ?>" itemprop="title"><?php echo word_limiter($title, 4); ?></a></h3>
									<label class="publish"><time itemprop="startDate" datetime="<?php echo $published_at ?>"><?php echo $published_at ?></label> 
								</div>
							</div>
							<?php endforeach; endif; ?>
						</div>
						<div class="clearfix"></div>
							<?php if ($data['is_empty'] == FALSE) : ?>
							<div class="loadmore">
								<button id="btnLoad">LoadMore</button>
							</div>
							<?php endif; ?>
						</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- END CONTENT -->
		
			<script type="text/javascript">
			$(function() {
				var paging = 2;
				$('#btnLoad').click(function(){
					$.ajax({
						url: "<?php echo $data['loadmore'] ?>" + paging,
						type: "GET",
						dataType: "json",
						success: function(data){
							var loop = '';
							for(var i = 0; i < data.galleries.length; i++) {
								var image = '';
								if (data.galleries[i].image.length > 1) { 
									image = JSON.parse(data.galleries[i].image);
									image = data.base_url+'thumbs/'+image.thumb;
								} else {
									image = data.base_url+'../images/default.png';
								}
								loop += '<div class="col-lg-4 col-md-4 col-sm-4 gallery-box">'
									+'<div class="gallery-content" style="background-image: url('+image+');" itemprop="images">'
										+'<div class="transparent">'
										+'</div>'
										// +'<span class="'+d[i].camera+'" aria-hidden="true">'
										// +'</span>'
									+'</div>'
									+'<div class="konten-gallery">'
										+'<h3>'
										+'<a href="'+data.galleries[i].slug+'" itemprop="title">'+data.galleries[i].title
										+'</a>'
										+'</h3>'
										+'<label class="publish"><time itemprop="startDate" datetime="'+data.galleries[i].published_at+'">'+data.galleries[i].published_at
										+'</label>' 
									+'</div>'
									+'</div>';
						}
						$('#result').append(loop);
							paging += 1;
							if (data.is_empty) {
								$('.loadmore').remove();
							}
						}
					});
				});
			});
			</script>