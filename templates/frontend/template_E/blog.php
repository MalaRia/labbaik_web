
			<!-- BEGIN CONTENT -->
			<div id="blog-result">
				<div class="container">
					<!-- <div class="blog-slider"> -->
					<div class="col-md-offset-2 col-sm-offset-2 col-lg-8 col-md-8 col-sm-8 col-xs-12">
						<div class="col-lg-12 col-md-12 col-sm-12 blogMore" id="result">
							<h3 class="title visible-xs">BLOG</h3>
							<?php 
							if (!$data['blogs']) {
								echo '<p>Blog belum ada!</p>';
							}
							foreach ($data['blogs'] as $blog) : 
								$title = $blog->title;
								$link = site_url('blog/'.$blog->slug);
								$excerpt = strip_tags($blog->excerpt);
								$image = ($blog->image) ? json_decode($blog->image) : '';
								$thumb = $image->thumb;
								$published_at = indonesian_date($blog->published_at);
							?>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 konten-explore">
								<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 imageKonten" style="background-image:url('<?php echo ($thumb) ? base_url('assets/uploads/thumbs/'.$thumb) : get_template_directory(dirname(__FILE__), 'assets/img/home/kopi.png') ?>');" itemprop="images">
								<a href="<?php echo $link; ?>" itemprop="link"></a>
								</div>
								<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 post-explore">
								<a href="<?php echo $link; ?>" itemprop="title"><?php echo $title; ?></a>
								<p itemprop="description"><?php echo $excerpt; ?></p>
								<label class="publish"><time itemprop="startDate" datetime="<?php echo $published_at; ?>"><?php echo $published_at; ?> </label>
								</div>
								<div class="clearfix"></div>
							</div>
							<?php endforeach; ?>
							<div class="clearfix"></div>
						</div>
						<?php if ($data['is_empty'] == FALSE) : ?>
						<div class="loadmore">
							<button id="btnLoad">LoadMore</button>
						</div>
						<?php endif; ?>
					</div>
					<!-- </div>         -->
				</div>
			</div>
			<!-- END CONTENT -->

			<script type="text/javascript">
				$(function() {
					var paging = 2;
					$('#btnLoad').click(function(){
						$.ajax({
							url: "<?php echo $data['loadmore'] ?>" + paging,
							type: "GET",
							dataType: "json",
							success: function(data){
								var loop = '';
								for(var i = 0; i < data.blogs.length; i++) {
									var image = '';
									if (data.blogs[i].image.length > 1) { 
										image = JSON.parse(data.blogs[i].image);
										image = data.base_url+'thumbs/'+image.thumb;
									} else {
										image = data.base_url+'../images/default.png';
									}
									// var thumb = if (image.thumb == 'undefined')
									// console.log(image);
									loop += '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 konten-explore">'
											+'<div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 imageKonten" style="background-image:url('+image+');" itemprop="images">'
												+'<a href="blog-detail.html" itemprop="link">'
												+'</a>'
											+'</div>'
											+'<div class="col-lg-7 col-md-7 col-sm-6 col-xs-12 post-explore">'
												+'<a href="blog-detail.html" itemprop="title">'+data.blogs[i].title
												+'</a>'
												+'<p itemprop="description">'+data.blogs[i].excerpt
												+'</p>'
												+'<label class="publish"><time itemprop="startDate" datetime="'+data.blogs[i].published_at+'">'+data.blogs[i].published_at
												+'</label>'
											+'</div>'
											+'<div class="clearfix">'
											+'</div>'
											+'</div>';
								}
								$('#result').append(loop);
								paging += 1;
								if (data.is_empty) {
									$('.loadmore').remove();
								}
							}
						});
					});
				});
			</script>