

        <!-- BEGIN CONTENT -->
        <div  id="schedule-result">
          <div class="scedule-slider">
            <div class="container">
              <h3 class="title visible-xs">SCHEDULE</h3>
              <div id="tempat_konten">
                <?php foreach ($this->data['data']['schedules'] as $schedule) : 
                  $title = $schedule->title;
                  $content = (strpos($schedule->content, '==') > 1) ? explode('==', $schedule->content) : '';
                  $time = ($content) ? $content[0] : '';
                  $address = ($content) ? $content[1] : '';
                ?>
                <div class="col-lg-3 col-md-3 col-sm-6 schedule-box">
                  <div class="scedule-content" style="background-color:#f57fc5;">
                      <div class="scedule-info">
                        <h3><a href="#" class="practice-docter" itemprop="day"><?php echo $time ?></a></h3>
                        <h3 class="nameRS" itemprop="title"><?php echo $title ?></h3>
                        <p itemprop="address"><?php echo $address ?></p>
                      </div>
                  </div>
                </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
              </div>
            </div>
            <!-- <div class="loadmore">
              <button id="btnLoad">LoadMore</button>
            </div> -->
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END CONTAINER -->
        <script type="text/javascript">
            $(function(){
              //  $('#btnLoad').click(function(){
              //    alert('test');
              //  })
            });
            // $(document).ready(function() {
                // $('#btnLoad').click(function(){
                //   $.ajax({
                //       url: "schedule-ajax.json",
                //       type: "get",
                //       success: function(mn){
                //         var c = JSON.parse(mn);
                //         var d = c.data;
                //         var loop = "";
                //         for(var i = 0; i < d.length; i++) {
                //           loop += '<div class="col-lg-3 col-md-3 col-sm-6 schedule-box">'
                //                     +'<div class="scedule-content" style="background-color:#'+d[i].color+';">'
                //                         +'<div class="scedule-info">'
                //                           +'<h3>'
                //                               +'<a href="blog-detail.html" class="practice-docter" itemprop="day">'+d[i].day
                //                               +'</a>'
                //                             +'</h3>'
                //                           +'<h3 class="nameRS" itemprop="title">'+d[i].name
                //                           +'</h3>'
                //                           +'<p itemprop="address">'+d[i].address
                //                           +'</p>'
                //                         +'</div>'
                //                     +'</div>'
                //                   +'</div>';
                //         }
                //         // console.log(test);
                //         $('#tempat_konten').append(loop);
                //       }
                //   });
                // });
                // $('#btnLoad').click(function(){
                //   $.ajax({
                //       url: "schedule-ajax.json",
                //       type: "get",
                //       success: function(nambah){
                //         var c = JSON.parse(nambah);
                //         var d = c.data;
                //         var loop = "";
                //         for(var i = 0; i < d.length; i++) {
                //           loop += '<div class="col-lg-3 col-md-3 col-sm-6 schedule-box">'
                //                     +'<div class="scedule-content" style="background-color:#'+d[i].color+';">'
                //                         +'<div class="scedule-info">'
                //                           +'<h3>'
                //                               +'<a href="blog-detail.html" class="practice-docter" itemprop="day">'+d[i].day
                //                               +'</a>'
                //                             +'</h3>'
                //                           +'<h3 class="nameRS" itemprop="title">'+d[i].name
                //                           +'</h3>'
                //                           +'<p itemprop="address">'+d[i].address
                //                           +'</p>'
                //                         +'</div>'
                //                     +'</div>'
                //                   +'</div>';
                //         }
                //         $('#tempat_konten').append(loop);
                //       }
                //   });
                // });
            // });
        </script>