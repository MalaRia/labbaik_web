<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title><?php echo $data['meta']['title'] ?></title>

        <?php if ($data['meta']['ga_verify']) : ?>
        <meta name="google-site-verification" content="<?php echo $data['meta']['ga_verify'] ?>">
        <?php endif; ?>

        <link rel="shortcut icon" href="<?php echo ($data['meta']['favicon']) ? $this->upload_location .'images/'.$data['meta']['favicon'] : get_template_directory(dirname(__FILE__), '../assets/img/logo.png') ?>"/>
        <link rel="canonical" href=""/>
        
        <!-- <meta name="author" content=""> -->
        <meta name="description" content="<?php echo $data['meta']['description'] ?>"/>
        <meta name="keyword" content="<?php echo $data['meta']['keywords'] ?>"/>
        <!-- <meta name="robots" content="noodp"/> -->
        <meta property="og:locale" content="id_ID" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content="<?php echo $data['meta']['title'] ?>" />
        <meta property="og:description" content="<?php echo $data['meta']['description'] ?>" />
        <meta property="og:site_name" content="<?php echo $data['meta']['site_title'] ?>" />
        <!-- <meta property="article:publisher" content="..." /> -->
        <!-- <meta property="article:section" content="kategori..." /> -->
        <meta property="article:published_time" content="<?php echo $data['meta']['published_at'] ?>" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:description" content="<?php echo $data['meta']['description'] ?>" />
        <meta property="twitter:title" content="<?php echo $data['meta']['title'] ?>" />
        <meta property="DC.date.issued" content="<?php echo $data['meta']['published_at'] ?>" />
        <!-- Opengraph -->
        <meta property="og:title" content="<?php echo $data['meta']['title'] ?>"/>
        <meta property="og:image" content="<?php echo $data['meta']['image'] ?>"/>
        <meta property="og:type" content="web"/>
        <meta property="og:url" content="<?php echo $data['meta']['url'] ?>"/>
        <meta property="og:description" content="<?php echo $data['meta']['description'] ?>"/>
        <!-- <meta property="og:article:author" content="" /> -->

        <!-- Twitter Card -->
        <meta name="twitter:card" content="web">
        <meta name="twitter:url" content="<?php echo $data['meta']['url'] ?>">
        <meta name="twitter:title" content="<?php echo $data['meta']['title'] ?>">
        <meta name="twitter:image" content="<?php echo $data['meta']['image'] ?>">
        <meta name="twitter:description" content="<?php echo $data['meta']['description'] ?>">

        <!-- RSS -->
        <link rel="alternate" type="application/rss+xml" 
            title="RSS Feed for <?php echo site_url() ?>" 
            href="<?php echo site_url($data['meta']['rss']) ?>" />

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo get_template_directory(dirname(__FILE__), '../assets/bootstrap-3.3.7-dist/css/bootstrap.min.css') ?>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo get_template_directory(dirname(__FILE__), '../assets/css/haidokter.css') ?>" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../assets/fonts/font-awesome-4.5.0/css/font-awesome.min.css') ?>">
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory(dirname(__FILE__), '../assets/slick/slick.css') ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory(dirname(__FILE__), '../assets/slick/slick-theme.css') ?>"/>
        <link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../assets/lightbox2/css/lightbox.css')?>">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script type="text/javascript">
            /*! http://mrclay.org/index.php/2010/11/14/using-jquery-before-its-loaded/ */ (function(a){if(a.$)return;var b=[];a.$=function(c){b.push(c)};a.defer$=function(){while(f=b.shift())$(f)}})(window);
        </script>

        <?php if ($data['meta']['ga_analytics']) : ?>
        <!-- Google Analytics -->
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '<?php echo $data['meta']['ga_analytics'] ?>', 'auto');
        ga('send', 'pageview');
        </script>
        <!-- End Google Analytics -->
        <?php endif; ?>
    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar template" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="fa fa-bars visible-xs" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!-- If logo is not set -->
                    <a class="navbar-brand" href="<?php echo site_url() ?>" itemprop="link"><?php echo $web_info->site_title ?></a>
                    <!-- If logo set -->
                    <!-- <a class="brand-image" href="index.html"><img src="assets/img/logo.png"></a> -->
                </div>

                <!-- HEADER -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <?php foreach ($pages as $page) : ?>
                        <li class="<?php echo ($data['current'] == $page->slug) ? 'active' : '' ?>"><a href="<?php echo site_url($page->slug) ?>" itemprop="<?php echo site_url($page->slug) ?>"><?php echo $page->title ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        <!-- /.navbar-collapse -->
        <!-- /.END HEADER -->
        </nav>
        <!-- /.END NAV -->
        <div id="hidden">
            <div id="progress-bar"></div><div id="loading"></div>
        </div>