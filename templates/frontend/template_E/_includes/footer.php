				<!-- BEGIN FOOTER -->
				<div class="footer">
					<div class="container">
						<div class="col-sm-6 kontenContact">
							<h3 class="nameDok" itemprop="nameDr"><?php echo $web_info->site_title ?></h3>
							<p itemprop="Email">Email : <?php echo $web_info->site_email ?></p>
							<p itemprop="phone">Phone : <?php echo $web_info->site_telephone ?></p>
							<a href="tel:<?php echo $web_info->site_telephone ?>" class="visible-xs" itemprop="call">CALL</a>
						</div>
						<div class="col-sm-6 kontenSosial">
							<ul class="icon-social">
								<li><a href="<?php echo $social_media->facebook ?>" target="_blank" class="facebook"><i class="fa fa-facebook-official"></i></a></li>
								<li><a href="<?php echo $social_media->twitter ?>" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a></li>
								<li><a href="<?php echo $social_media->instagram ?>" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a></li>
								<li><a href="<?php echo $social_media->linkedin ?>" target="_blank" class="linked"><i class="fa fa-linkedin-square"></i></a></li>
								</ul>
								<p class="copyright" itemprop="copyright">&copy; 2016 haiDokter.com</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- END FOOTER -->

				<!-- MODAL STATUS -->
				<div class="modal fade" id="myModal-status" data-status="<?php echo (get_flash('status') != NULL) ? "true" : "false"; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content box box-solid">
							<div class="modal-header box-header label-<?php echo get_flash('class') ?>">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel" style="color:#fff"><?php echo ucfirst(get_flash('status')); ?></h4>
							</div>
							<div class="modal-body box-body">
								<ul class="error-list">
									<?php echo (get_flash('status') == 'success') ? get_flash('success') : get_flash('errors'); ?>
								</ul>
							</div>
						</div>
					</div>
				</div>

        <!-- SCRIPT --> 
				<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), '../assets/js/jquery.js') ?>"></script>
				<!-- Defer jQuery -->
				<script>defer$()</script> 
        <script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), '../assets/bootstrap-3.3.7-dist/js/bootstrap.js') ?>"></script>
				<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), '../assets/slick/slick.min.js') ?>"></script>
				<script type="text/javascript" src="<?php echo get_template_directory(dirname(__FILE__), '../assets/lightbox2/js/lightbox.js')?>"></script>
        <!-- <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDHbBZQknYfatc2qxaTG8exAu5XY1J895o"></script> -->
       
        <script type="text/javascript">
            /* Begin Progress-Bar */
            $(document).ready(function() {
                $("#hidden").css('display', 'block');
                $("#progress-bar").animate({width:"65%"}, 1000);

            });
            $(window).bind('load', function() {
                $("#progress-bar").stop().animate({width:"100%"}, 1000, function() {
                    $("#hidden").fadeOut(3000); 
                }); 
            });
            $(document).ready(function() {
                // $('.lazy').slick({
                //   lazyLoad: 'ondemand',
                //   slidesToShow: 4,
                //   slidesToScroll: 0,
                //   // centerPadding: '80px',
                //   responsive: [
                //     {
                //       breakpoint: 1024,
                //       settings: {
                //         slidesToShow: 4,
                //         slidesToScroll: 0,
                //         infinite: true,
                //         dots: true
                //       }
                //     },
                //     {
                //       breakpoint: 600,
                //       settings: {
                //         slidesToShow: 2,
                //         slidesToScroll: 1,
                //         dots: true
                //       }
                //     },
                //     {
                //       breakpoint: 480,
                //       settings: {
                //       slidesToShow: 1,
                //       slidesToScroll: 1,
                //       dots: true

                //       }
                //     }
                //   ]
                // });
                // $(function() {
                  $(".phonecell").click(function(){
                      var PhoneNumber = $(this).text();
                      PhoneNumber=PhoneNumber.replace("Phone:","");
                      window.location.href="tel://"+PhoneNumber;
                  });
                // });
            });
            /* End Progress-Bar */
						
						// Show modal message
						if ($("#myModal-status").attr("data-status") == "true") {
							$("#myModal-status").modal("show");
						}
        </script>
    </body>
</html>