
    <!-- konten blog -->
    <div class="page-description" id="contact">
        <div class="container">
            <h3 class="title visible-xs" itemprop="title">CONTACT</h3>
            <div class="row">
                <div class="contact-box">
                    <div class="col-md-8 col-sm-8 col-xs-12 mycontact">
                        <div class="">
                        <?php 
                            $image = ($data['info']->image) ? json_decode($data['info']->image) : '';
                            $thumb = ($image) ? $image->thumb : '';

                            $content = (strpos($data['info']->content,'==') > 1) ? explode('==', $data['info']->content) : '';
                            $name = (isset($content[0])) ? $content[0] : '';
                            $address = (isset($content[1])) ? $content[1] : '';
                            $email = (isset($content[2])) ? $content[2] : '';
                            $phone = (isset($content[3])) ? $content[3] : '';
                        ?>
                            <div class="col-md-6 col-sm-6 detailContact" style="background-image: url('<?php echo ($thumb) ? base_url('assets/uploads/thumbs/'.$thumb) : get_template_directory(dirname(__FILE__), 'assets/img/gallery/book-1836380_1920.jpg') ?>');" itemprop="images"></div>
                            <div class="col-md-6 col-sm-6 kontenContact">
                                <h3  itemprop="nameDok"><?php echo $name ?></h3>
                                <p class="street"  itemprop="address"><?php echo $address ?></p>
                                <p  itemprop="loadMore">Email : <?php echo $email ?></p>
                                <p  itemprop="phone">Phone : <?php echo $phone ?></p>
                                <div class="showme"><a target="_blank" href="https://map.google.com/?q=<?php echo strip_tags($address) ?>"  itemprop="direction">Get Direction</a></div>
                            </div>
                          <!-- </div> -->
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4 col-xs-12 contact-form">
                        <div>
                            <form action="" method="POST">
                            <input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
                                <h4  itemprop="title">KONSULTASI</h4>
                                <div class="form-group">
                                  <!-- <label for="nama">Nama</label> -->
                                    <input class="form-control right-kontrol" id="full_name" name="full_name" type="text" value="<?php echo get_flash('full_name') ?>" placeholder="Nama">
                                </div>
                                <div class="form-group">
                                  <!-- <label for="email">Email</label> -->
                                    <input class="form-control right-kontrol" id="email" name="email" type="email" value="<?php echo get_flash('email') ?>" placeholder="Email">
                                </div>
                                <div class="form-group">
                                  <textarea class="form-control right-kontrol input-textarea" id="description" rows="7" name="description" cols="50"><?php echo get_flash('description') ?></textarea>
                                </div>
                                <div class="button-wrapper">
                                    <button type="submit" name="submit" class="btn btn-contact pull-right"  itemprop="submit">SUBMIT</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>