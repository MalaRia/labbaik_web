			<!-- BEGIN CONTENT -->
			<?php 
			$image = ($data['info']->image) ? json_decode($data['info']->image) : ''; 
			$img = ($image) ? $image->image : '';
			
			$title = $data['info']->title;
			$content = $data['info']->content;
					$published_at = indonesian_date($data['info']->published_at);
			?>
			<div id="home-banner" style="background-image: url(<?php echo ($image) ? base_url('assets/uploads/images/'.$img) : 'assets/img/gallery/book-1836380_1920.jpg'?>);">
				<div class="slider-gradient"></div>
			</div>
			<div  id="blog-detail">
				<div class="container">
					<!-- <div class="page-description" id="blog-detail">
						<div class="col-md-10 col-sm-10"> -->
					<div class="col-md-offset-1 col-sm-offset-1 col-lg-10 col-md-10 col-sm-10">
						<div class="col-lg-12 col-md-12 col-sm-12 blogMore">
							<div class="blog-header">
								<h1 class="blog-title" itemprop="title"><?php echo $title ?></h1>
								<label class="blog-publish"><time itemprop="startDate" datetime="<?php echo $published_at ?>"><?php echo $published_at ?></label>
							</div>

							<div itemprop="description">
								<?php echo $content ?>
							</div>
							<ul class="icon-share">
								<li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo current_url() ?>" class="facebook">FACEBOOK</a></li>
								<li><a href="https://twitter.com/share?url=<?php echo current_url() ?>" class="twitter">TWITTER</a></li>
								<li><a href="https://plus.google.com/share?url=<?php echo current_url() ?>" class="gplus"> GOOGLE + </a></li>
								<!-- <li><a href="<?php echo current_url() ?>" class="copyLink"> COPY LINK</a></li> -->
							</ul>

							<?php if ($data['info']->comment == 1) : ?>
							<!-- BEGIN COMMENTS -->
							<div class="comments-box">
								<div class="comment-info">
									<div class="col-sm-12 hidden-xs">
										<label class="">Comment <span class="comment-total"><?php echo count($data['comments']) ?></span></label>
									</div>
									<div class="clearfix"></div>
								</div>
								<ul class="comment-lists">
									<?php if ($data['comments']) : foreach ($data['comments'] as $comment) : ?>
									<li>
										<img src="<?php echo get_template_directory(dirname(__FILE__), 'assets/img/avatar.png') ?>" class="comment-avatar img-responsive" itemprop="images">
										<div class="comment-detail">
											<p><span class="comment-name" itemprop="comment-name"><?php echo $comment->c_name ?></span> - <span class="fa fa-time" itemprop="time"><?php echo time_ago($comment->c_created_at) ?></span></p>
											<p itemprop="description"><?php echo $comment->c_comment ?></p>
										</div>                                
										<div class="clearfix"></div>
									</li>
									<?php endforeach; endif; ?>
								</ul>
								<div class="comment-form">
									<h3 itemprop="subtitle">Comment here!</h3>
									<form action="" method="POST">
										<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
										<div class="col-sm-6 form-group text-name ">
											<input required="required" type="text" class="form-control right-kontrol" id="name" name="name" type="text" value="<?php echo get_flash('name') ?>" placeholder="Name">
										</div>
										<div class="col-sm-6 form-group text-email ">
											<input required="required" type="email" class="form-control right-kontrol" id="email" name="email" type="text" value="<?php echo get_flash('email') ?>" placeholder="Email">
										</div>
										<div class="form-group text-name ">
											<textarea required="required" class="form-control right-kontrol input-textarea" id="comment" rows="7" name="comment" cols="50"><?php echo get_flash('comment') ?></textarea>
										</div>
										<div class="button-wrapper">
											<button type="submit" name="submit" class="btn btn-comment" itemprop="btn-comment">KIRIM</button>
										</div>
										<div class="clearfix"></div>
									</form>
								</div>
							</div>
							<!-- END COMMENTS -->
							<?php endif; ?>
						</div>
						<div class="col-md-4 col-sm-4"></div>
					</div>
				</div>
			</div>
			<!-- END CONTENT -->
