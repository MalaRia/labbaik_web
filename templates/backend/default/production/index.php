<?php get_template('_includes/header') ?>

  <div class="col-md-3 left_col">
    <div class="left_col scroll-view">
      <div class="navbar nav_title" style="border: 0;">
        <a href="index.html" class="site_title"><i class="fa fa-paw"></i> <span>Gentellela Alela!</span></a>
      </div>

      <div class="clearfix"></div>

      <!-- menu profile quick info -->
      <div class="profile">
        <div class="profile_pic">
          <img src="<?php echo get_image($user_data->avatar); ?>" alt="<?php echo $user_data->display_name; ?>" class="img-circle profile_img">
        </div>
        <div class="profile_info">
          <span>Welcome,</span>
          <h2><?php echo $user_data->display_name; ?></h2>
        </div>
      </div>
      <!-- /menu profile quick info -->

      <br />

      <!-- sidebar menu -->
      <?php get_template('_includes/sidebar') ?>
      <!-- /sidebar menu -->

      <!-- /menu footer buttons -->
      <?php get_template('_includes/footer-button') ?>
      <!-- /menu footer buttons -->
    </div>
  </div>

  <!-- top navigation -->
  <?php get_template('_includes/top-navigation') ?>
  <!-- /top navigation -->

  <!-- page content -->
  <?php get_template($layout) ?>
  <!-- /page content -->
<?php get_template('_includes/footer') ?> <!-- footer -->
