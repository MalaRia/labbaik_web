<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h1><?php echo current_page(); ?></h1>
          <?php echo $breadcrumb; ?>
        </div>
        <div class="x_content">
          <form class="form-horizontal">
              <div class="col-sm-9">
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" name="title" id="title" value="<?php echo $data['content']->title; ?>" class="form-control" readonly="readonly" placeholder="Title" >
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" name="name" id="name" value="<?php echo $data['content']->name; ?>" class="form-control" readonly="readonly" placeholder="Name" >
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="email" name="email" id="email" value="<?php echo $data['content']->email; ?>" class="form-control" readonly="readonly" placeholder="Email" >
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="content" id="content" readonly="readonly" placeholder="Content"><?php echo $data['content']->content; ?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-sm-9">
                <div class="form-group">
                  <div class="col-sm-12">
                    <a href="<?php echo site_url('admin/comment/'.$redirect) ?>" id="submit" class="btn btn-info pull-right"><span class="fa fa-chevron-left"></span> Back</a>
                  </div>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
