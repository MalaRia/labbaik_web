
<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h1><?php echo current_page(); ?></h1>
          <?php echo $breadcrumb; ?>
        </div>
        <div class="x_content">
          <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/post/insert-post') ?>">
              <div class="col-sm-3">
                <div class="form-group form-upload">
                  <div class="thumbnail" id="thumbnail">
                    <?php if (!empty($data['user_profile']->image) && $data['user_profile']->image != 'null') : ?>
                    <img src="<?php echo get_image($data['user_profile']->image, FALSE) ?>">
                    <?php else: ?>
                    <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>">
                    <?php endif; ?>
                  </div>
                  <label class="label-upload btn btn-info col-sm-12" for="banner"><span class="fa fa-upload"></span> Change Banner</label>
                  <input type="file" name="banner" id="banner" class="form-control hidden">
                </div>
                <!-- seo -->
                <fieldset>
                  <legend>SEO</legend>
                  <div class="form-group">
                  <input type="text" name="seo_title" class="form-control" placeholder="Title"  value="<?php echo get_flash('seo_title'); ?>">
                  </div>
                  <div class="form-group">
                  <textarea name="seo_content" class="form-control" placeholder="Content" rows="5"><?php echo get_flash('seo_content'); ?></textarea>
                  </div>
                  <div class="form-group">
                  <textarea name="seo_keywords" class="form-control" placeholder="Keywords"><?php echo get_flash('seo_keywords'); ?></textarea>
                  </div>
                </fieldset>
              </div>
              <div class="col-sm-9">
                <input type="hidden" id="csrf" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" name="title" id="title" value="<?php echo get_flash('title'); ?>" class="form-control" required="required" placeholder="Title" >
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="content" id="content" placeholder="Content"><?php echo get_flash('content'); ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <select name="parent_id" id="parent_id" class="select2_single form-control" required="required">
                      <option value="0">- Parent -</option>
                      <?php foreach ($pages as $page) : ?>
                      <option value="<?php echo $page->id ?>" <?php echo ($page->id == get_flash('parent_id')) ? 'selected' : ''; ?>><?php echo $page->title ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group media" style="<?php echo (get_flash('parent_id') != 3) ? 'display: none' : ''; ?>">
                  <div class="col-sm-12">
                    <select name="media_type" id="media_type" class="select2_single form-control" required="required">
                      <option>- Media Type -</option>
                      <option value="1" <?php echo (1 == get_flash('media_type')) ? 'selected' : ''; ?>>Image</option>
                      <option value="2" <?php echo (2 == get_flash('media_type')) ? 'selected' : ''; ?>>Video</option>
                    </select>
                  </div>
                </div>

                <!-- Media Content -->
                <?php // var_dump(get_flash('image_title')) ?>
                <!-- Image -->
                <div class="form-group media_image" style="<?php echo (get_flash('media_type') != 1) ? 'display: none' : ''; ?>">
                  <div class="col-sm-12">
                    <div class="row">
                      <div class="image-block">
                        <div class="col-sm-4">
                          <div class="thumbnail">
                            <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>" class="img-responsive image_thumbnail">
                            <input type="text" name="image_title[]" value="<?php get_flash('image_title') ?>" class="form-control image_title" placeholder="Image Title">
                            <label for="image_upload" class="btn btn-warning image_upload"><span class="fa fa-upload"></span></label>
                            <input type="file" name="userfile" id="image_upload" class="image_upload hidden">
                            <input type="hidden" name="image_path[]" id="image_path" class="image_path" value="">
                          </div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                      <div class="col-sm-12">
                        <button type="button" class="btn btn-warning pull-right" id="add-image"><span class="fa fa-plus"></span> Add Image</button>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Video -->
                <div class="form-group media_video" style="<?php echo (get_flash('media_type') != 2) ? 'display: none' : ''; ?>">
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="media_content" id="media_content" placeholder="Youtube Link"><?php echo get_flash('media_content'); ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="additional_info" id="additional_info" placeholder="Info"><?php echo get_flash('additional_info'); ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <select name="status" id="status" class="select2_single form-control" required="required">
                      <option>- Status -</option>
                      <option value="1" <?php echo (1 == get_flash('status')) ? 'selected' : ''; ?>>Draft</option>
                      <option value="2" <?php echo (2 == get_flash('status')) ? 'selected' : ''; ?>>Publish</option>
                      <option value="3" <?php echo (3 == get_flash('status')) ? 'selected' : ''; ?>>Schedule</option>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="display: none;">
                  <div class="col-sm-12">
                    <input type="text" name="published_at" id="published_at" value="<?php echo get_flash('published_at'); ?>" class="form-control calendar" placeholder="Publish At" >
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
                  </div>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
