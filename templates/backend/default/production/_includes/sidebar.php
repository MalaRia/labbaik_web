<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
  <div class="menu_section">
    <h3>General</h3>
    <ul class="nav side-menu">
      <li><a href="<?php echo site_url('admin/dashboard') ?>"><i class="fa fa-home"></i> Dashboard </a></li>
      <li><a><i class="fa fa-newspaper-o"></i> Pages <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="<?php echo site_url('admin/page/add-page') ?>">Add Page</a></li>
          <li><a href="<?php echo site_url('admin/page/all-pages') ?>">All Pages</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-edit"></i> Posts <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="<?php echo site_url('admin/post/add-post') ?>">Add Post</a></li>
          <li><a href="<?php echo site_url('admin/post/all-posts') ?>">All Post</a></li>
          <?php // foreach($pages as $page) : ?>
          <!-- <li><a href="<?php // echo site_url('admin/post/list/'.$page->slug) ?>"><?php // echo $page->title ?></a></li> -->
          <?php // endforeach; ?>
        </ul>
      </li>
      <li><a href="<?php echo site_url('admin/comment/all-comments') ?>"><i class="fa fa-comments"></i> Comments </a></li>
      <li><a href="<?php echo site_url('admin/contact/all-contacts') ?>"><i class="fa fa-phone"></i> Contacts </a></li>
      <li><a><i class="fa fa-folder"></i> Categories <span class="fa fa-chevron-down"></span> </a>
        <ul class="nav child_menu">
          <li><a href="<?php echo site_url('admin/category/add-category') ?>">Add Category</a></li>
          <li><a href="<?php echo site_url('admin/category/all-categories') ?>">All Categories</a></li>
        </ul>
      </li>
      <li><a><i class="fa fa-tags"></i> Tags <span class="fa fa-chevron-down"></span> </a>
        <ul class="nav child_menu">
          <li><a href="<?php echo site_url('admin/tag/add-tag') ?>">Add Tag</a></li>
          <li><a href="<?php echo site_url('admin/tag/all-tags') ?>">All Tags</a></li>
        </ul>
      </li>
      <!-- <li><a href="<?php echo site_url('admin/visitors') ?>"><i class="fa fa-line-chart"></i> Visitors </a></li> -->
      <li><a><i class="fa fa-gears"></i> Settings <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="<?php echo site_url('admin/settings/web-profile') ?>">Web Profile</a></li>
          <li><a href="<?php echo site_url('admin/settings/web-social-media') ?>">Web Social Media</a></li>
          <li><a href="<?php echo site_url('admin/settings/web-seo') ?>">Web SEO</a></li>
          <!-- <li><a href="<?php echo site_url('admin/settings/web-setting') ?>">Web Setting</a></li> -->
      </li>
  </div>

</div>