<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>
    <link rel="icon" href="<?php echo get_template_directory(dirname(__FILE__), '../images/favicon.png') ?>">

    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- jquery ui -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jquery-ui/jquery-ui.min.css'); ?>">
    <!-- NProgress -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
    <!-- bootstrap-progressbar -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css'); ?>" rel="stylesheet">
    <!-- JQVMap -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jqvmap/dist/jqvmap.min.css'); ?>" rel="stylesheet"/>

    <!-- Custom Theme Style -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../build/css/custom.min.css'); ?>" rel="stylesheet">

    <!-- Custom Style -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../build/css/style.css'); ?>" rel="stylesheet">

    <!-- PNotify -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">

    <!-- datatables -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

    <?php if (strpos(current_url(), 'add') > 0 || strpos(current_url(), 'edit') > 0) : ?>
    <!-- Select2 -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
    <!-- ckeditor -->
    <script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/ckeditor/ckeditor.js'); ?>"></script>
    <script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/ckeditor/config.js'); ?>"></script>
    <?php endif; ?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">