<!-- footer content -->
    <footer>
      <div class="pull-right">
        Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
      </div>
      <div class="clearfix"></div>
    </footer>
    <!-- /footer content -->
  </div>
</div>

<!-- jQuery -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jquery/dist/jquery.min.js'); ?>"></script>
<!-- Bootstrap -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
<!-- FastClick -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/fastclick/lib/fastclick.js'); ?>"></script>
<!-- NProgress -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/nprogress/nprogress.js'); ?>"></script>
<!-- Chart.js -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/Chart.js/dist/Chart.min.js'); ?>"></script>
<!-- gauge.js -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/gauge.js/dist/gauge.min.js'); ?>"></script>
<!-- bootstrap-progressbar -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/bootstrap-progressbar/bootstrap-progressbar.min.js'); ?>"></script>
<!-- iCheck -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/iCheck/icheck.min.js'); ?>"></script>
<!-- Skycons -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/skycons/skycons.js'); ?>"></script>
<!-- Flot -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/Flot/jquery.flot.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/Flot/jquery.flot.pie.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/Flot/jquery.flot.time.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/Flot/jquery.flot.stack.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/Flot/jquery.flot.resize.js'); ?>"></script>
<!-- Flot plugins -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/flot.orderbars/js/jquery.flot.orderBars.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/flot-spline/js/jquery.flot.spline.min.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/flot.curvedlines/curvedLines.js'); ?>"></script>
<!-- DateJS -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/DateJS/build/date.js'); ?>"></script>
<!-- JQVMap -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jqvmap/dist/jquery.vmap.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jqvmap/dist/maps/jquery.vmap.world.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js'); ?>"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../js/moment/moment.min.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../js/datepicker/daterangepicker.js'); ?>"></script>

<!-- Custom Theme Scripts -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../build/js/custom.min.js'); ?>"></script>

<!-- Pnotify -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/pnotify/dist/pnotify.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/pnotify/dist/pnotify.buttons.js'); ?>"></script>

<!-- Flot -->
<script>
  $(document).ready(function() {
    var data1 = [
      [gd(2012, 1, 1), 17],
      [gd(2012, 1, 2), 74],
      [gd(2012, 1, 3), 6],
      [gd(2012, 1, 4), 39],
      [gd(2012, 1, 5), 20],
      [gd(2012, 1, 6), 85],
      [gd(2012, 1, 7), 7]
    ];

    var data2 = [
      [gd(2012, 1, 1), 82],
      [gd(2012, 1, 2), 23],
      [gd(2012, 1, 3), 66],
      [gd(2012, 1, 4), 9],
      [gd(2012, 1, 5), 119],
      [gd(2012, 1, 6), 6],
      [gd(2012, 1, 7), 9]
    ];
    $("#canvas_dahs").length && $.plot($("#canvas_dahs"), [
      data1, data2
    ], {
      series: {
        lines: {
          show: false,
          fill: true
        },
        splines: {
          show: true,
          tension: 0.4,
          lineWidth: 1,
          fill: 0.4
        },
        points: {
          radius: 0,
          show: true
        },
        shadowSize: 2
      },
      grid: {
        verticalLines: true,
        hoverable: true,
        clickable: true,
        tickColor: "#d5d5d5",
        borderWidth: 1,
        color: '#fff'
      },
      colors: ["rgba(38, 185, 154, 0.38)", "rgba(3, 88, 106, 0.38)"],
      xaxis: {
        tickColor: "rgba(51, 51, 51, 0.06)",
        mode: "time",
        tickSize: [1, "day"],
        //tickLength: 10,
        axisLabel: "Date",
        axisLabelUseCanvas: true,
        axisLabelFontSizePixels: 12,
        axisLabelFontFamily: 'Verdana, Arial',
        axisLabelPadding: 10
      },
      yaxis: {
        ticks: 8,
        tickColor: "rgba(51, 51, 51, 0.06)",
      },
      tooltip: false
    });

    function gd(year, month, day) {
      return new Date(year, month - 1, day).getTime();
    }
  });
</script>
<!-- /Flot -->

<!-- JQVMap -->
<script>
  $(document).ready(function(){
    $('#world-map-gdp').vectorMap({
        map: 'world_en',
        backgroundColor: null,
        color: '#ffffff',
        hoverOpacity: 0.7,
        selectedColor: '#666666',
        enableZoom: true,
        showTooltip: true,
        values: sample_data,
        scaleColors: ['#E6F2F0', '#149B7E'],
        normalizeFunction: 'polynomial'
    });
  });
</script>
<!-- /JQVMap -->

<!-- Skycons -->
<script>
  $(document).ready(function() {
    var icons = new Skycons({
        "color": "#73879C"
      }),
      list = [
        "clear-day", "clear-night", "partly-cloudy-day",
        "partly-cloudy-night", "cloudy", "rain", "sleet", "snow", "wind",
        "fog"
      ],
      i;

    for (i = list.length; i--;)
      icons.set(list[i], list[i]);

    icons.play();
  });
</script>
<!-- /Skycons -->

<!-- Doughnut Chart -->
<script>
  $(document).ready(function(){
    var options = {
      legend: false,
      responsive: false
    };

    new Chart(document.getElementById("canvas1"), {
      type: 'doughnut',
      tooltipFillColor: "rgba(51, 51, 51, 0.55)",
      data: {
        labels: [
          "Symbian",
          "Blackberry",
          "Other",
          "Android",
          "IOS"
        ],
        datasets: [{
          data: [15, 20, 30, 10, 30],
          backgroundColor: [
            "#BDC3C7",
            "#9B59B6",
            "#E74C3C",
            "#26B99A",
            "#3498DB"
          ],
          hoverBackgroundColor: [
            "#CFD4D8",
            "#B370CF",
            "#E95E4F",
            "#36CAAB",
            "#49A9EA"
          ]
        }]
      },
      options: options
    });
  });
</script>
<!-- /Doughnut Chart -->

<!-- bootstrap-daterangepicker -->
<script>
  $(document).ready(function() {

    var cb = function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    };

    var optionSet1 = {
      startDate: moment().subtract(29, 'days'),
      endDate: moment(),
      minDate: '01/01/2012',
      maxDate: '12/31/2015',
      dateLimit: {
        days: 60
      },
      showDropdowns: true,
      showWeekNumbers: true,
      timePicker: false,
      timePickerIncrement: 1,
      timePicker12Hour: true,
      ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      opens: 'left',
      buttonClasses: ['btn btn-default'],
      applyClass: 'btn-small btn-primary',
      cancelClass: 'btn-small',
      format: 'MM/DD/YYYY',
      separator: ' to ',
      locale: {
        applyLabel: 'Submit',
        cancelLabel: 'Clear',
        fromLabel: 'From',
        toLabel: 'To',
        customRangeLabel: 'Custom',
        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        firstDay: 1
      }
    };
    $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
    $('#reportrange').daterangepicker(optionSet1, cb);
    $('#reportrange').on('show.daterangepicker', function() {
      console.log("show event fired");
    });
    $('#reportrange').on('hide.daterangepicker', function() {
      console.log("hide event fired");
    });
    $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
      console.log("apply event fired, start/end dates are " + picker.startDate.format('MMMM D, YYYY') + " to " + picker.endDate.format('MMMM D, YYYY'));
    });
    $('#reportrange').on('cancel.daterangepicker', function(ev, picker) {
      console.log("cancel event fired");
    });
    $('#options1').click(function() {
      $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
    });
    $('#options2').click(function() {
      $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
    });
    $('#destroy').click(function() {
      $('#reportrange').data('daterangepicker').remove();
    });
  });
</script>
<!-- /bootstrap-daterangepicker -->

<!-- gauge.js -->
<script>
  var opts = {
      lines: 12,
      angle: 0,
      lineWidth: 0.4,
      pointer: {
          length: 0.75,
          strokeWidth: 0.042,
          color: '#1D212A'
      },
      limitMax: 'false',
      colorStart: '#1ABC9C',
      colorStop: '#1ABC9C',
      strokeColor: '#F0F3F3',
      generateGradient: true
  };
  var target = document.getElementById('foo'),
      gauge = new Gauge(target).setOptions(opts);

  gauge.maxValue = 6000;
  gauge.animationSpeed = 32;
  gauge.set(3200);
  gauge.setTextField(document.getElementById("gauge-text"));
</script>
<!-- /gauge.js -->

<!-- Notify -->
<?php 
if (!empty(get_flash('errors'))) : ?>
<?php $exp_errors = explode('!', get_flash('errors'));
  for ($i = 0; $i < count($exp_errors) - 1; $i++) :

?>
<script type="text/javascript">
  new PNotify({
      title: 'Error',
      text: "<?php echo trim($exp_errors[$i]) . '!' ?>",
      type: 'error',
      styling: 'bootstrap3'
  });
</script>
<?php 
  endfor;
elseif (!empty(get_flash('success'))): ?>
<script type="text/javascript">
  new PNotify({
      title: 'Success',
      text: "<?php echo get_flash('success') ?>",
      type: 'success',
      styling: 'bootstrap3'
  });
</script>
<?php endif; ?>

<!-- CKEDITOR -->
<?php if (strpos(current_url(), 'add') > 0 || strpos(current_url(), 'edit') > 0) : ?>
<!-- Select2 -->
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/select2/dist/js/select2.full.min.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>
<script type="text/javascript">
  // select
  $(".select2_single").select2({
    placeholder: "Select a state",
    allowClear: true
  });
  // ckeditor
  CKEDITOR.replace('content');
  // calendar
  $('.calendar').datepicker({
    'dateFormat': 'yy-mm-dd'
  });
  // status onclick
  $('#status').change(function() {
    var val = $(this).val();
    // console.log(val);
    if (val == 3) {
      $('#published_at').parent().parent().slideDown();
    } else {
      $('#published_at').parent().parent().slideUp();
    }
    // alert($(this).val());
  });

  // select page onclick
  $('#parent_id').change(function() {
    var val = $(this).val();
    // console.log(val);
    if (val == 3) {
      $('.media').slideDown();
    } else {
      $('.media').slideUp();
    }
  });

  // select media type onclick
  $('#media_type').change(function() {
    var val = $(this).val();
    // console.log(val);
    if (val == 1) {
      $('.media_image').slideDown();
      $('.media_video').slideUp();
    } else if (val == 2) {
      $('.media_image').slideUp();
      $('.media_video').slideDown();
    }
  });
</script>
<?php endif; ?>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo get_template_directory(dirname(__FILE__), '../../vendors/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script type="text/javascript">
  // datatable
  $('table').dataTable();

  // delete data
  $(document).on('click', '.modal-delete', function(e) {
    e.preventDefault();
    var pageID = $(this).attr('data-id');
    $('#modal-delete').find('input[name=id]').val(pageID);
    $('#modal-delete').modal('show');
  })
</script>

<!-- Ajax Upload -->
<script src="<?php echo site_url('assets/js/ajaxfileupload.js'); ?>"></script>
<script type="text/javascript">
  function unEntity(str){
     return str.replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">");
  }
  // Handle Upload
  jQuery.extend({
    handleError: function( s, xhr, status, e ) {
        // If a local callback was specified, fire it
        if ( s.error )
            s.error( xhr, status, e );
        // If we have some XML response text (e.g. from an AJAX call) then log it in the console
        else if(xhr.responseText)
          
          // remove p close tag
          var xhrFilter = xhr.responseText.split('</p>');

          // parse to Object
          var parse = JSON.parse(xhrFilter[0]);
          // console.log(parse);
          if (parse.error) {
              alert(unEntity(parse.content));
          }
          $('#csrf').attr('name', parse.csrf.name);
          $('#csrf').val(parse.csrf.hash);
    }
  });

  // add image
  $('#add-image').click(function(e) {
      e.preventDefault();
      var i = $('.image-block .thumbnail').length + 1;
      $('.image-block').append(
          '<div class="col-sm-4">'+
            '<div class="thumbnail">'+
              '<img src="<?php echo base_url('assets/images/thumbnail.png') ?>" class="img-responsive image_thumbnail">'+
              '<input type="text" name="image_title[]" class="form-control image_title" placeholder="Image Title">'+
              '<label for="image_upload'+i+'" class="btn btn-warning image_upload"><span class="fa fa-upload"></span></label>'+
              '<input type="file" name="userfile" id="image_upload'+i+'" class="image_upload hidden">'+
              '<input type="hidden" name="image_path[]" id="image_path'+i+'" class="image_path" value="">'+
            '</div>'+
          '</div>'
      );
  });

  var siteURL = "<?php echo site_url(); ?>";
  var csrf_name = $('#csrf').attr('name');
  var csrf_hash = $('#csrf').val();

  $(document).on('change', '.image_upload', function() {
    var $this = $(this);
    var ID = $this.attr('id');
    var imgPath = $this.siblings('.image_path').val();

    $.ajaxFileUpload({
        url: siteURL+'admin/upload/insert',
        type: 'POST',
        fileElementId: ID,
        dataType: 'json',
        data: {'csrf_token_haidokter': $('#csrf').val()},
        success: function(data) {
            $('#csrf').attr('name', data.csrf.name);
            $('#csrf').val(data.csrf.hash);
            
            console.log(data.content);
            $('#'+ID)
                .siblings('.image_path')
                    .val(data.content.file+','+data.content.thumb)
                .siblings('.image_thumbnail')
                    .attr('src', siteURL + 'assets/uploads/thumbs/'+data.content.thumb);

                    JSON.stringify
        }
    });
  });

  // preview image before upload
  function readBGURL(input) {
      // console.log(el);
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#web_bg').siblings('.thumbnail').find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readFAURL(input) {
      // console.log(el);
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#web_favicon').siblings('.thumbnail').find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readLGURL(input) {
      // console.log(el);
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#web_logo').siblings('.thumbnail').find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readAVURL(input) {
      // console.log(el);
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#avatar').siblings('.thumbnail').find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    function readPSURL(input) {
      // console.log(el);
      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $('#banner').siblings('.thumbnail').find('img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
      }
    }

    $("[type=file]").change(function() {
      if ($(this).attr('id') == 'web_bg') {
        readBGURL(this);
      } else if($(this).attr('id') == 'web_favicon') {
        readFAURL(this);
      } else if($(this).attr('id') == 'web_logo') {
        readLGURL(this);
      } else if($(this).attr('id') == 'avatar') {
        readAVURL(this);
      } else {
        readPSURL(this);
      }
    });
</script>
</body>
</html>