<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <?php echo $breadcrumb; ?>
          <div class="col-md-6">
            <h1><?php echo current_page(); ?></h1>
          </div>
        </div>
        <div class="x_content">
          <div class="row">
            <table id="table" class="display" cellspacing="0" width="100%">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>slug</th>
                  <th>Order</th>
                  <th>Description</th>
                  <th>Icon</th>
                  <th>Created At</th>
                  <th>Actions</th>
                </tr>
              </thead>

              <tbody>
                <?php 
                $no = 1;
                if (!empty($data['tags'])) :
                foreach ($data['tags'] as $tag) : ?>
                <tr>
                  <td><?php echo $no++ ?></td>
                  <td><?php echo $tag->tag_name ?></td>
                  <td><?php echo $tag->slug ?></td>
                  <td><?php echo $tag->order_tag ?></td>
                  <td><?php echo $tag->description ?></td>
                  <td><?php echo $tag->icon?></td>
                  <td><?php echo $tag->created_at ?></td>
                  <td><a href="<?php echo site_url('admin/tag/edit-tag/'.$tag->id); ?>" class="btn btn-info"><span class="fa fa-edit"></span></a>
                  <a data-id="<?php echo $tag->id; ?>" class="btn btn-danger modal-delete"><span class="fa fa-trash"></span></a>
                  </td>
                </tr>
                <?php endforeach; 
                endif;
                ?>
              </tbody>
    
              <tfoot>
                <tr>
                  <th>No</th>
                  <th>Name</th>
                  <th>slug</th>
                  <th>Order</th>
                  <th>Description</th>
                  <th>Icon</th>
                  <th>Created At</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- delete -->
<div class="modal fade" tabindex="-1" role="dialog" id="modal-delete">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Pesan !</h4>
      </div>
      <form action="<?php echo site_url('admin/tag/delete-tag') ?>" method="POST">
        <input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
        <input type="hidden" name="id" value="">
        <div class="modal-body">
          <p>Apakah anda yakin akan menghapusnya?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-danger">Ya</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->