<div class="right_col" role="main">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	      	<div class="x_panel">
	        	<div class="x_title">
	          		<div class="col-md-6">
	            		<h1><?php echo current_page(); ?></h1>
          				<?php echo $breadcrumb; ?>
	          		</div>
	          		<div class="clearfix"></div>
	        	</div>
	        	<div class="x_content">
	        		<!-- flash message -->
	        		<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/settings/update-web-profile') ?>">
	        			<div class="col-sm-3">
	        				<div class="form-group form-upload">
	        					<div class="thumbnail">
	        						<?php if (!empty($data['profile']->web_logo) && $data['profile']->web_logo != 'null') : ?>
				                      <img src="<?php echo get_image($data['profile']->web_logo, FALSE) ?>">
				                    <?php else: ?>
				                    <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>">
				                    <?php endif; ?>
	        					</div>
		        				<label class="label-upload btn btn-info col-sm-12" for="web_logo"><span class="fa fa-upload"></span> Change Logo</label>
	        					<input type="file" name="web_logo" id="web_logo" class="form-control hidden">
		        			</div>
		        			<div class="form-group form-upload">
	        					<div class="thumbnail">
	        						<?php if (!empty($data['profile']->web_icon) && $data['profile']->web_icon != 'null') : ?>
				                      <img src="<?php echo get_image($data['profile']->web_icon, FALSE) ?>">
				                    <?php else: ?>
				                    <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>">
				                    <?php endif; ?>
	        					</div>
		        				<label class="label-upload btn btn-info col-sm-12" for="web_favicon"><span class="fa fa-upload"></span> Change Favicon</label>
		        				<input type="file" name="web_favicon" id="web_favicon" class="form-control hidden">
		        				<span class="help-block">*Ideal image size is 155 x 50 px </span>
		        			</div>
	        			</div>
	        			<div class="col-sm-9">
	        				<input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<input type="text" name="web_name" id="web_name" value="<?php echo $data['profile']->web_name ?>" class="form-control" required="required" placeholder="Your Web Name" >
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<input type="text" name="web_meta_title" id="web_meta_title" value="<?php echo $data['profile']->web_meta_title ?>" class="form-control" placeholder="Your Meta Title" >
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<textarea class="form-control" rows="3" name="web_description" id="web_description"><?php echo $data['profile']->web_description ?></textarea>
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<input type="email" name="web_email" id="web_email" value="<?php echo $data['profile']->web_email ?>" class="form-control" required="required" placeholder="Your Email">
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<textarea class="form-control" rows="3" name="web_address" placeholder="Web Address"><?php echo $data['profile']->web_address ?></textarea>
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<input type="text" name="web_phone" id="web_phone" value="<?php echo $data['profile']->web_phone ?>" class="form-control" required="required" placeholder="Phone Number">
		        				</div>
		        			</div>

		        			<div class="form-group">
		        				<div class="col-sm-12">
			        				<fieldset>
				        				<legend>Set Logo</legend>
				        				<div class="row">
				        					<div class="col-sm-3">
					        					<select name="web_slogan" class="form-control">
					        						<option value="1" <?php echo ($data['profile']->web_slogan == 1) ? 'selected' : '' ?>>Logo</option>
					        						<option value="2" <?php echo ($data['profile']->web_slogan == 2) ? 'selected' : '' ?>>Text</option>
					        					</select>
					        				</div>
				        				</div>
				        			</fieldset>
				        		</div>
		        			</div>

		        			<div class="form-group form-upload">
		        				<div class="col-sm-12">
			        				<fieldset>
				        				<legend>Set Background</legend>
			        					<div class="thumbnail">
			        						<?php if (!empty($data['profile']->web_bg) && $data['profile']->web_bg != 'null') : ?>
						                      <img src="<?php echo get_image($data['profile']->web_bg, FALSE) ?>">
						                    <?php else: ?>
						                    <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>">
						                    <?php endif; ?>
			        					</div>
				        				<label class="label-upload btn btn-info col-sm-12" for="web_bg"><span class="fa fa-upload"></span> Change Background</label>
				        				<input type="file" name="web_bg" id="web_bg" class="form-control hidden">
				        			</fieldset>
				        		</div>
		        			</div>
	        			</div>
	        			<div class="col-sm-12">
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
		        				</div>
		        			</div>
	        			</div>
	        		</form>
	        	</div>
	      	</div>
	    </div>
	</div>
</div>