<!-- add category -->
<div class="right_col" role="main">
	<div class="row">
	    <div class="col-md-6 col-sm-6 col-xs-12">
	      	<div class="x_panel">
	        	<div class="x_title">
	          		<div class="col-md-12">
	            		<h1><?php echo current_page(); ?></h1>
          				<?php echo $breadcrumb; ?>
	          		</div>
	          		<div class="clearfix"></div>
	        	</div>
	        	<div class="x_content">
	        		<!-- flash message -->
	        		<div class="col-md-12">
	        			<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/category/insert-category') ?>">
	        				<div class="form-group">
								<input type="text" name="category_name" class="form-control" required="required" placeholder="Category Name"  value="<?php echo get_flash('category_name'); ?>">
	        				</div>
	        				<div class="form-group">
								<input type="text" name="slug" class="form-control" placeholder="Slug"  value="<?php echo get_flash('slug'); ?>">
	        				</div>
	        				<div class="form-group">
								<input type="text" name="order_category" class="form-control" placeholder="Order"  value="<?php echo get_flash('order_category'); ?>">
	        				</div>
	        				<div class="form-group">
								<textarea name="icon" class="form-control" placeholder="icon" placeholder="Icon"><?php echo get_flash('icon'); ?></textarea>
								<span class="help-block">For Classic Theme: Find yor font code <a href="http://fontawesome.io/icons/">here</a>. Example code: <code><?php echo htmlentities('<i class="fa fa-pencil-square-o"></i>') ?></code></span>
	        				</div>
	        				<div class="form-group">
								<textarea name="description" class="form-control" placeholder="description" placeholder="Description"><?php echo get_flash('description'); ?></textarea>
	        				</div>
		        			<div class="form-group">
	        					<input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
	        					<button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
		        			</div>
		        		</form>
	        		</div>
	        	</div>
	        </div>
	    </div>
	</div>
</div>