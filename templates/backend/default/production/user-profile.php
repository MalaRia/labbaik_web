<div class="right_col">
	<div class="row">
	    <div class="col-md-12 col-sm-12 col-xs-12">
	      	<div class="x_panel">
	        	<div class="x_title">
		          <?php echo $breadcrumb; ?>
		          <div class="col-md-6">
		            <h1><?php echo current_page(); ?></h1>
		          </div>
		        </div>
	        	<div class="x_content">
	        		<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/settings/update-user-profile') ?>">
	        			<div class="col-sm-3">
	        				<div class="form-group form-upload">
	        					<div class="thumbnail" id="thumbnail">
	        						<?php if (!empty($data['user_profile']->avatar) && $data['user_profile']->avatar != 'null') : ?>
                      <img src="<?php echo get_image($data['user_profile']->avatar, FALSE) ?>">
	                    <?php else: ?>
	                    <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>">
	                    <?php endif; ?>
	        					</div>
		        				<label class="label-upload btn btn-info col-sm-12" for="avatar"><span class="fa fa-upload"></span> Change Avatar</label>
	        					<input type="file" name="avatar" id="avatar" class="form-control hidden">
		        			</div>
	        			</div>
	        			<div class="col-sm-9">
	        				<input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
		        			<div class="form-group">
		        				<label class="control-label col-sm-3" for="username">Username*</label>
		        				<div class="col-sm-9">
		        					<input type="text" name="username" id="username" value="<?php echo $data['user_profile']->username ?>" class="form-control" readonly="readonly" placeholder="Your Username" >
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<label class="control-label col-sm-3" for="email">Email*</label>
		        				<div class="col-sm-9">
		        					<input type="email" name="email" id="email" value="<?php echo $data['user_profile']->email ?>" class="form-control" readonly="readonly" placeholder="Your Email">
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<label class="control-label col-sm-3" for="display_name">Name*</label>
		        				<div class="col-sm-9">
		        					<input type="text" name="display_name" id="display_name" class="form-control" placeholder="Your Name" value="<?php echo $data['user_profile']->display_name; ?>">
		        				</div>
		        			</div>
		        			<div class="form-group">
		        				<label class="control-label col-sm-3" for="work_place">Work Place*</label>
		        				<div class="col-sm-9">
		        					<input type="text" name="work_place" id="work_place" class="form-control" placeholder="Your Work Place" value="<?php echo $data['user_profile']->work_place; ?>">
		        				</div>
		        			</div>
		        			<fieldset>
		        				<legend>User Authentication</legend>
		        				<div class="form-group">
			        				<label class="control-label col-sm-3" for="new_password">New Password</label>
			        				<div class="col-sm-9">
			        					<input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password">
			        					<span class="help-block">Minimum characters length is 6</span>
			        				</div>
			        			</div>
			        			<div class="form-group">
			        				<label class="control-label col-sm-3" for="passconf">Password Confirmation</label>
			        				<div class="col-sm-9">
			        					<input type="password" name="passconf" id="passconf" class="form-control" placeholder="Password Confirmation">
			        				</div>
			        			</div>
			        			<div class="form-group">
			        				<label class="control-label col-sm-3" for="current_password">Current Password</label>
			        				<div class="col-sm-9">
			        					<input type="password" name="current_password" id="current_password" class="form-control" placeholder="Password">
			        				</div>
			        			</div>
		        			</fieldset>
	        			</div>
	        			<div class="col-sm-12">
		        			<div class="form-group">
		        				<div class="col-sm-12">
		        					<button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
		        				</div>
		        			</div>
	        			</div>
	        		</form>
	        	</div>
	      	</div>
	    </div>
	</div>
</div>