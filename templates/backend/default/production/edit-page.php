<div class="right_col" role="main">
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h1><?php echo current_page(); ?></h1>
          <?php echo $breadcrumb; ?>
        </div>
        <div class="x_content">
          <form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/page/update-page/'.$data['content']->id) ?>">
              <div class="col-sm-3">
                <div class="form-group form-upload">
                  <div class="thumbnail" id="thumbnail">
                    <?php if (!empty($data['content']->image) && $data['content']->image != 'null') : ?>
                    <img src="<?php echo get_image($data['content']->image, FALSE) ?>">
                    <?php else: ?>
                    <img src="<?php echo get_template_directory(dirname(__FILE__), 'images/thumbnail.png') ?>">
                    <?php endif; ?>
                  </div>
                  <label class="label-upload btn btn-info col-sm-12" for="banner"><span class="fa fa-upload"></span> Change Banner</label>
                  <input type="file" name="banner" id="banner" class="form-control hidden">
                </div>
                <!-- seo -->
                <fieldset>
                  <legend>SEO</legend>
                  <div class="form-group">
                  <?php $seo = seo($data['content']->seo); ?>
                  <input type="text" name="seo_title" class="form-control" placeholder="Title"  value="<?php echo (get_flash('seo_title')) ? get_flash('seo_title') : $seo['title']; ?>">
                  </div>
                  <div class="form-group">
                  <textarea name="seo_content" class="form-control" placeholder="Content" rows="5"><?php echo (get_flash('seo_content')) ? get_flash('seo_content') : $seo['content']; ?></textarea>
                  </div>
                  <div class="form-group">
                  <textarea name="seo_keywords" class="form-control" placeholder="Keywords"><?php echo (get_flash('seo_keywords')) ? get_flash('seo_keywords') : $seo['keywords']; ?></textarea>
                  </div>
                </fieldset>
              </div>
              <div class="col-sm-9">
                <input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
                <div class="form-group">
                  <div class="col-sm-12">
                    <input type="text" name="title" id="title" value="<?php echo (get_flash('title')) ? get_flash('title') : $data['content']->title; ?>" class="form-control" required="required" placeholder="Title" >
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="content" id="content" placeholder="Content"><?php echo (get_flash('content')) ? get_flash('content') : $data['content']->content; ?></textarea>
                  </div>
                </div>
                <!-- <div class="form-group">
                  <div class="col-sm-12">
                    <select name="parent_id" id="parent_id" class="select2_single form-control">
                      <option value="0">- Parent -</option>
                      <?php foreach ($data['pages'] as $page) : ?>
                      <option value="<?php echo $page->id ?>" <?php echo ($page->id == get_flash('parent_id')) ? 'selected' : ''; ?>><?php echo $page->title ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div> -->
                <div class="form-group">
                  <div class="col-sm-12">
                    <select name="status" id="status" class="select2_single form-control" required="required">
                      <option>- Status -</option>
                      <option value="1" <?php echo (1 == get_flash('status') || 1 == $data['content']->status) ? 'selected' : ''; ?>>Draft</option>
                      <option value="2" <?php echo (2 == get_flash('status') || 2 == $data['content']->status) ? 'selected' : ''; ?>>Publish</option>
                      <option value="3" <?php echo (3 == get_flash('status') || 3 == $data['content']->status) ? 'selected' : ''; ?>>Schedule</option>
                    </select>
                  </div>
                </div>
                <div class="form-group" style="<?php echo ($data['content']->status != 3 ? 'display: none;' : '') ?>">
                  <div class="col-sm-12">
                    <input type="text" name="published_at" id="published_at" value="<?php echo (get_flash('published_at')) ? get_flash('published_at') : $data['content']->published_at; ?>" class="form-control calendar" placeholder="Publish At" >
                  </div>
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <div class="col-sm-12">
                    <button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
                  </div>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
