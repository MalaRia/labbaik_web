<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>
    <link rel="icon" href="<?php echo get_template_directory(dirname(__FILE__), '../images/favicon.png') ?>">

    <!-- Bootstrap -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../vendors/nprogress/nprogress.css'); ?>" rel="stylesheet">
    <!-- Animate.css -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../build/css/animate.css'); ?>" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../build/css/custom.min.css'); ?>" rel="stylesheet">
    <!-- PNotify -->
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
    <link href="<?php echo get_template_directory(dirname(__FILE__), '../vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form action="<?php echo site_url('auth/login-process') ?>" method="POST">
              <input type='hidden' name='<?php echo $csrf['name'] ?>' value='<?php echo $csrf['hash'] ?>'>
              <h1>Login Form</h1>
              <div>
                <input type="text" class="form-control" name="username" placeholder="Username" required="required" value="<?php echo get_flash('username'); ?>" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="Password" required="required" autocomplete="off" />
              </div>
              <div>
                <button type="submit" class="btn btn-default submit">Log in</button>
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div>
                  <h1><i class="fa fa-paw"></i> <?php echo $web_info->web_name ?></h1>
                  <p>©2016 <?php echo $web_info->web_description ?></p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    <!-- jQuery -->
    <script src="<?php echo get_template_directory(dirname(__FILE__), '../vendors/jquery/dist/jquery.min.js'); ?>"></script>
    <!-- Pnotify -->
    <script src="<?php echo get_template_directory(dirname(__FILE__), '../vendors/pnotify/dist/pnotify.js'); ?>"></script>
    <script src="<?php echo get_template_directory(dirname(__FILE__), '../vendors/pnotify/dist/pnotify.buttons.js'); ?>"></script>

    <!-- Notify -->
    <?php 
    if (!empty(get_flash('errors'))) : ?>
    <?php $exp_errors = explode('!', get_flash('errors'));
      for ($i = 0; $i < count($exp_errors) - 1; $i++) :

    ?>
    <script type="text/javascript">
      new PNotify({
          title: 'Error',
          text: "<?php echo trim($exp_errors[$i]) . '!' ?>",
          type: 'error',
          styling: 'bootstrap3'
      });
    </script>
    <?php 
      endfor;
    elseif (!empty(get_flash('success'))): ?>
    <script type="text/javascript">
      new PNotify({
          title: 'Success',
          text: "<?php echo get_flash('success') ?>",
          type: 'success',
          styling: 'bootstrap3'
      });
    </script>
    <?php endif; ?>
  </body>
</html>