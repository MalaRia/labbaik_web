<div class="right_col">
    <!-- top tiles -->
    <style type="text/css">
    .tile_stats_count p {
        position: absolute;
        top: 22px;
        right: 15px;
    }
    </style>
    <div class="row tile_count">
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Visitors</span>
            <div class="count green"><?php echo $data['visitors']->visitors; ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-user"></i> Total Readers</span>
            <div class="count green"><?php echo $data['readers']->readers; ?></div>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i> Total Blogs</span>
            <div class="count green"><?php echo $data['blogs']->blogs; ?></div>
            <p>
                Publish: <i class="label label-success"><?php echo $data['publish_blogs']->blogs; ?></i> <br>
                Draft: <i class="label label-info"><?php echo $data['schedule_blogs']->blogs; ?></i> <br>
                Schedule: <i class="label label-warning"><?php echo $data['draft_blogs']->blogs; ?></i>
            </p>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i> Total Galleries</span>
            <div class="count green"><?php echo $data['galleries']->galleries; ?></div>
            <p>
                Publish: <i class="label label-success"><?php echo $data['publish_galleries']->galleries; ?></i> <br>
                Draft: <i class="label label-info"><?php echo $data['schedule_galleries']->galleries; ?></i> <br>
                Schedule: <i class="label label-warning"><?php echo $data['draft_galleries']->galleries; ?></i>
            </p>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-clock-o"></i> Total Schedules</span>
            <div class="count green"><?php echo $data['schedules']->schedules; ?></div>
            <p>
                Publish: <i class="label label-success"><?php echo $data['publish_schedules']->schedules; ?></i> <br>
                Draft: <i class="label label-info"><?php echo $data['schedule_schedules']->schedules; ?></i> <br>
                Schedule: <i class="label label-warning"><?php echo $data['draft_schedules']->schedules; ?></i>
            </p>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-phone"></i> Total Contacts</span>
            <div class="count green"><?php echo $data['contacts']->contacts; ?></div>
            <p>
                Read: <i class="label label-default"><?php echo $data['read_contacts']->contacts; ?></i> <br>
                Unread: <i class="label label-success"><?php echo $data['unread_contacts']->contacts; ?></i>
            </p>
        </div>
        <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
            <span class="count_top"><i class="fa fa-comments"></i> Total Comments</span>
            <div class="count green"><?php echo $data['comments']->comments; ?></div>
            <p>
                Read: <i class="label label-default"><?php echo $data['read_comments']->comments; ?></i> <br>
                Unread: <i class="label label-success"><?php echo $data['unread_comments']->comments; ?></i>
            </p>
        </div>
    </div>
    <!-- /top tiles -->

    <div class="row">
        <!-- Begin Popular Articles -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h2>Popular Articles</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <?php if ($data['popular_blogs']) : ?>
                        <?php foreach ($data['popular_blogs'] as $blog) : ?>
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span><?php echo $blog->title ?></span>
                            </div>
                            <div class="w_center w_55">
                                <div class="progress">
                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?php echo $blog->visitors ?>" aria-valuemin="0" aria-valuemax="<?php echo $blog->visitors / 100; ?>" style="width: <?php echo $blog->visitors / 100 ?>%;">
                                        <span class="sr-only"><?php echo $blog->visitors ?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="w_right w_20">
                                <span><?php echo $blog->visitors ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- End Popular Articles -->
        </div>
    </div>

    <div class="row">
        <!-- Begin Popular Galleries -->
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <div class="x_panel tile fixed_height_320">
                    <div class="x_title">
                        <h2>Popular Galleries</h2>
                        <ul class="nav navbar-right panel_toolbox">
                          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                          </li>
                          <li><a class="close-link"><i class="fa fa-close"></i></a>
                          </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    <?php if ($data['popular_galleries']) : ?>
                        <?php foreach ($data['popular_galleries'] as $gallery) : ?>
                        <div class="widget_summary">
                            <div class="w_left w_25">
                                <span><?php echo $gallery->title ?></span>
                            </div>
                            <div class="w_center w_55">
                                <div class="progress">
                                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="<?php echo $gallery->visitors ?>" aria-valuemin="0" aria-valuemax="<?php echo $gallery->visitors / 100 ?>" style="width: <?php echo $gallery->visitors / 100 ?>%;">
                                        <span class="sr-only"><?php echo $gallery->visitors ?></span>
                                    </div>
                                </div>
                            </div>

                            <div class="w_right w_20">
                                <span><?php echo $gallery->visitors ?></span>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                </div>
            </div>
            <!-- End Popular Galleries -->
        </div>
    </div>
</div>