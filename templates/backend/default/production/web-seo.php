<!-- seo -->
<div class="right_col" role="main">
	<div class="row">
	    <div class="col-md-6 col-sm-6 col-xs-12">
	      	<div class="x_panel">
	        	<div class="x_title">
	          		<div class="col-md-12">
	            		<h1><?php echo current_page(); ?></h1>
          				<?php echo $breadcrumb; ?>
	          		</div>
	          		<div class="clearfix"></div>
	        	</div>
	        	<div class="x_content">
	        		<!-- flash message -->
	        		<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/settings/update-web-seo') ?>">
		        		<fieldset>
	        				<legend>SEO</legend>
	        				<?php 
	        					$seo = profile_seo($data['seo']->web_seo);
	        				?>
	        				<div class="form-group">
    							<input type="text" name="web_seo[ga_analytics]" class="form-control" placeholder="Google Analytics"  value="<?php echo $seo['ga_analytics'] ?>">
	        				</div>
	        				<div class="form-group">
    							<textarea name="web_seo[keywords]" class="form-control" placeholder="Keywords" placeholder="Keywords"><?php echo $seo['keywords'] ?></textarea>
	        				</div>
	        			</fieldset>
	        			<div class="form-group">
	        				<div class="col-sm-12">
	        					<input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
	        					<button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
	        				</div>
	        			</div>
	        		</form>
	        	</div>
	        </div>
	    </div>
	</div>
</div>