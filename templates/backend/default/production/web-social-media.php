<!-- social media -->
<div class="right_col" role="main">
	<div class="row">
	    <div class="col-md-6 col-sm-6 col-xs-12">
	      	<div class="x_panel">
	        	<div class="x_title">
	          		<div class="col-md-12">
	            		<h1><?php echo current_page(); ?></h1>
          				<?php echo $breadcrumb; ?>
	          		</div>
	          		<div class="clearfix"></div>
	        	</div>
	        	<div class="x_content">
	        		<!-- flash message -->
	        		<form class="form-horizontal" enctype="multipart/form-data" method="post" action="<?php echo site_url('admin/settings/update-web-social-media') ?>">
		        		<fieldset>
	        				<legend>Social Media</legend>
	        				<?php $soc = profile_socmed($data['social_media']->web_socmed); ?>
	        				<div class="form-group">
	        					<div class="input-group">
	        						<span class="input-group-addon"><span class="fa fa-facebook"></span></span>
        							<input type="text" name="web_socmed[facebook]" class="form-control" placeholder="Facebook" value="<?php echo $soc['facebook'] ?>">
        						</div>
	        				</div>
	        				<div class="form-group">
	        					<div class="input-group">
	        						<span class="input-group-addon"><span class="fa fa-twitter"></span></span>
        							<input type="text" name="web_socmed[twitter]" class="form-control" placeholder="Twitter" value="<?php echo $soc['twitter'] ?>">
        						</div>
	        				</div>
	        				<div class="form-group">
	        					<div class="input-group">
	        						<span class="input-group-addon"><span class="fa fa-google-plus"></span></span>
        							<input type="text" name="web_socmed[gplus]" class="form-control" placeholder="Google Plus" value="<?php echo $soc['gplus'] ?>">
        						</div>
	        				</div>
	        				<div class="form-group">
	        					<div class="input-group">
	        						<span class="input-group-addon"><span class="fa fa-linkedin"></span></span>
        							<input type="text" name="web_socmed[linkedin]" class="form-control" placeholder="Linked In" value="<?php echo $soc['linkedin'] ?>">
        						</div>
	        				</div>
	        				<div class="form-group">
	        					<div class="input-group">
	        						<span class="input-group-addon"><span class="fa fa-instagram"></span></span>
        							<input type="text" name="web_socmed[instagram]" class="form-control" placeholder="Instagram" value="<?php echo $soc['instagram'] ?>">
        						</div>
	        				</div>
	        			</fieldset>
	        			<div class="form-group">
	        				<div class="col-sm-12">
	        					<input type="hidden" name="<?php echo $csrf['name'] ?>" value="<?php echo $csrf['hash'] ?>">
	        					<button type="submit" id="submit" class="btn btn-info pull-right"><span class="fa fa-save"></span> Submit</button>
	        				</div>
	        			</div>
	        		</form>
	        	</div>
	        </div>
	    </div>
	</div>
</div>