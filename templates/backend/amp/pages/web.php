			<style>
			.thumbnail.gallery img {
				height: 100px!important;
			}
			.thumbnail {
				position: relative;
			}
			.thumbnail .btn {
				top: 0;
				position: absolute;
			}
			</style>
			<form role="form" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body"> 
								<?php $web = json_decode($data['setting']['web']->content); ?>
								<!-- site title input -->
								<div class="form-group">
									<input type="text" class="form-control" name="site_title" id="site_title" value="<?php echo (isset($web->site_title)) ? $web->site_title : ''; ?>" placeholder="Site Title">
								</div>
								<!-- site url input -->
								<div class="form-group">
									<input type="text" class="form-control" name="site_url" id="site_url" value="<?php echo (isset($web->site_url)) ? $web->site_url : ''; ?>" placeholder="Site Url">
								</div>
								<!-- site description input -->
								<div class="form-group">
									<textarea class="form-control" name="site_description" id="site_description" placeholder="Site Description"><?php echo (isset($web->site_description)) ? $web->site_description : ''; ?></textarea>
								</div>
								<!-- site email input -->
								<div class="form-group">
									<input type="text" class="form-control" name="site_email" id="site_email" value="<?php echo (isset($web->site_email)) ? $web->site_email : ''; ?>" placeholder="Site Email">
								</div>
								<!-- site telephone input -->
								<div class="form-group">
									<input type="text" class="form-control" name="site_telephone" id="site_telephone" value="<?php echo (isset($web->site_telephone)) ? $web->site_telephone : ''; ?>" placeholder="Site Telephone">
								</div>
								<!-- site address input -->
								<div class="form-group">
									<textarea class="form-control" name="site_address" id="site_address" placeholder="Site Address" rows="3"><?php echo (isset($web->site_address)) ? $web->site_address : ''; ?></textarea>
								</div>
								<!-- site pagination input -->
								<div class="form-group">
									<input type="number" min="1" class="form-control" name="site_pagination" id="site_pagination" value="<?php echo (isset($web->site_pagination)) ? $web->site_pagination : 10; ?>" placeholder="Site Pagination Rows">
								</div>
								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
					<div class="col-xs-4">
						<!-- FEATURED IMAGE -->
						<div class="box">
							<div class="box-header">
								<h3>Featured Image</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<?php // SET IMAGE
								// var_dump($web->image);
								$image = (isset($web->image)) ? json_encode($web->image) : '';
								$meta_image = ($image != '""') ? set_image($image) : ""; 
								// var_dump($meta_image['fullImage']);	
								?>
								<div class="thumbnail">
									<img id="upl-image" class="img-responsive" src="<?php echo ($meta_image != '') ? $meta_image['fullImage'] : base_url('templates/backend/amp/dist/img/default.png'); ?>">
									<input type="file" id="upload_file" name="upload_file" class="hidden">
								</div>
								<label for="upload_file" class="btn btn-info pull-right" data-multiple="false">Upload File</label>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->

						<!-- FAVICON -->
						<div class="box">
							<div class="box-header">
								<h3>Favicon</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<?php // SET IMAGE
								$favicon = (isset($web->icon)) ? json_encode($web->icon) : '';
								$meta_favicon = set_image($favicon); ?>
								<div class="thumbnail">
									<img id="upload_file_icon_image"style="height: 100px;" class="img-responsive" src="<?php echo $meta_favicon['fullImage']; ?>">
									<input type="file" id="upload_file_icon" name="upload_file_icon" class="hidden">
								</div>
								<label for="upload_file_icon" class="btn btn-info pull-right" data-multiple="false" data-type="favicon">Upload File</label>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->

						<!-- LOGO -->
						<div class="box">
							<div class="box-header">
								<h3>LOGO</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<?php // SET IMAGE
								$logo = (isset($web->logo)) ? json_encode($web->logo) : '';
								$meta_logo = set_image($logo); ?>
								<div class="thumbnail">
									<img id="upload_file_logo_image" style="height: 150px;" class="img-responsive" src="<?php echo $meta_logo['fullImage']; ?>">
									<input type="file" id="upload_file_logo" name="upload_file_logo" class="hidden">
								</div>
								<label for="upload_file_logo" class="btn btn-info pull-right" data-multiple="false" data-type="logo">Upload File</label>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->

			<script>
			$(function(){
				// Reset image
				$(document).on("click", ".reset-image", function(e) {
					e.preventDefault();
					var parent = $(this).parent();
					parent.find("input").val("");
					parent.find("img").attr("src", "<?php echo get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>");
					parent.find("img").attr("alt", "");
					parent.find("img").attr("title", "");
					$(this).remove();
				});
			});
			</script>