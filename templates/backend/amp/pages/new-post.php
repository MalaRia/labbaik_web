			<style>
			.thumbnail.gallery img {
				height: 100px!important;
			}
			.thumbnail {
				position: relative;
			}
			.thumbnail .btn {
				top: 0;
				position: absolute;
			}
			</style>
			<form role="form" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
								<!-- title input -->
								<div class="form-group">
									<input type="text" name="title" value="<?php echo get_flash('title'); ?>" required="required" class="form-control" placeholder="Your Title...">
								</div>
								<!-- editor input -->
								<div class="form-group">
									<textarea id="editor" name="content" class="form-control" placeholder=""><?php echo get_flash('content'); ?></textarea>
								</div>
								<div class="form-group" id="gallery-box">
									<div class="col-sm-12">
										<label for="gallery">Video Gallery</label>
									</div>
									<div class="col-sm-12">
										<input type="text" class="form-control" name="video_gallery" id="videoGallery" value="<?php echo (get_flash('video_gallery')) ? get_flash('video_gallery') : '' ?>" placeholder="eg: https://www.youtube.com/embed/jBhxBLPuVbc">
										<p><code>Choose image or video can't use both(priority image if both was filled)</code></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<div class="box">
							<!-- /.box-header -->
							<div class="box-body">
								<!-- Tab panes -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs" role="tablist">
										<li role="post" class="active"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
										<li role="post"><a href="#setting" aria-controls="setting" role="tab" data-toggle="tab">Setting</a></li>
									</ul>
								</div>
								<div class="tab-content" style="padding-top: 15px;">
									<!-- SEO -->
									<div role="tabpanel" class="tab-pane active" id="seo"> 
										<div class="form-group">
											<input type="text" id="seo_title" name="seo_title" class="form-control" value="<?php echo get_flash('seo_title'); ?>" placeholder="Seo Title">
										</div>
										<div class="form-group">
											<textarea id="seo_description" name="seo_description" class="form-control" placeholder="Seo Description" rows="3"><?php echo get_flash('seo_description'); ?></textarea>
										</div>
										<div class="form-group">
											<textarea id="seo_keywords" name="seo_keywords" class="form-control" placeholder="Seo Keywords" rows="3"><?php echo get_flash('seo_description'); ?></textarea>
										</div>
									</div>
									<div role="tabpanel" class="tab-pane" id="setting">
										<!-- Allow Comments -->
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" checked name="allow_comments" value="1" id="allow_comments"> 
													Allow Comments
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
					<!-- Status, Attribute, Featured Image -->
					<div class="col-xs-4">
						<!-- STATUS -->
						<div class="box">
							<div class="box-header">
								<h3>Status</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- status input -->
								<div class="form-group">
									<select class="form-control select2" name="status" id="status" style="width: 100%;">
									<option value="draft" <?php echo (get_flash('status') == 'draft') ? 'selected' : '' ?>>Draft</option>
									<option value="publish" <?php echo (get_flash('status') == 'publish') ? 'selected' : '' ?>>Publish</option>
									<option value="schedule" <?php echo (get_flash('status') == 'schedule') ? 'selected' : '' ?>>Schedule</option>
									</select>
								</div>
								<!-- schedule input -->
								<div class="form-group publish-form"style="display:<?php echo (get_flash('schedule_date')) ? 'block' : 'none' ?>;">
									<input id="schedule_date" name="schedule_date" class="form-control datepicker" value="<?php echo (get_flash('schedule_date')) ? get_flash('schedule_date') : '' ?>" placeholder="Schedule Date">
								</div>
								
								<!-- api feed input -->
								<div class="form-group">
									<label for="api_feed">
										<input type="checkbox" name="api_feed" id="api_feed" value="1" class="form-control" > Set API Feed
									</label>&nbsp;&nbsp;
									<label for="api_popular">
										<input type="checkbox" name="api_popular" id="api_popular" value="1" class="form-control" > Set API Popular
									</label>
								</div>
								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- ATTRIBUTE -->
						<div class="box">
							<div class="box-header">
								<h3>Attribute</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- Parent -->
								<div class="form-group">
									<label for="page">Page</label>
									<select class="form-control select2" name="page" id="page" required="required" style="width: 100%;">
									<?php if (get_flash('category')) : 
									foreach (get_flash('category') as $cat) :
										$cat = explode('-', $cat);
									?>
									<option value="<?php echo $cat[0] ?>" selected><?php echo $cat[1] ?></option>
									<!-- <option value="<?php // echo $cat[0] ?>" selected><?php // echo $cat[1] ?></option> -->
									<?php 
									endforeach;
									endif; ?>
									</select>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- CATEGORIES -->
						<div class="box">
							<div class="box-header">
							<h3>Categories</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<!-- Parent -->
							<div class="form-group">
								<label for="category">Category</label>
								<select class="form-control select2" name="category[]" id="category" style="width: 100%;">
								<?php if (get_flash('category')) : 
								foreach (get_flash('category') as $cat) :
									$cat = explode('-', $cat);
								?>
								<option value="<?php echo $cat[0] ?>" selected><?php echo $cat[1] ?></option>
								<?php 
								endforeach;
								endif; ?>
								</select>
							</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- FEATURED IMAGE -->
						<div class="box">
							<div class="box-header">
								<h3>Featured Image</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="thumbnail">
									<img id="upl-image" class="img-responsive" src="<?php echo base_url('templates/backend/amp/dist/img/default.png')?>">
									<input type="file" id="upload_file" name="upload_file" class="hidden">
								</div>
								<label id="upl_file_label" for="upload_file" class="btn btn-info pull-right" data-multiple="false">Upload File</label>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->

			<!-- page script -->
			<script>
			$(function () {
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
				CKEDITOR.replace("editor", {
					height: 400,
					// filemanager
					filebrowserBrowseUrl : '<?php echo $data['filemanager']['ckeditor'] ?>',
					filebrowserUploadUrl : '<?php echo $data['filemanager']['ckeditor'] ?>',
					filebrowserImageBrowseUrl : '<?php echo $data['filemanager']['ckeditor'] ?>'
				});
				//Initialize Select2 Elements
				$(".select2").select2();
				
				$(".select2#page").select2({
					ajax: {
						url: "<?php echo $data['all_posts']['page']; ?>",
						dataType: "json",
						data: function (params) {
							var query = {
								search: params.term,
								type: 'page'
							}

							// Query parameters will be ?search=[term]&type=public
							return query;
						},
						processResults: function (data) {
							// Tranforms the top-level key of the response object from 'items' to 'results'
							var results = [];
							$.each(data, function (index, item) {
								results.push({
								id: item.id +'-'+ item.title,
								text: item.title
								});
							});

							return {
								results: results
							};
						}
					}
				});
				
				$(".select2#category").select2({
				  ajax: {
				    url: "<?php echo $data['all_posts']['category']; ?>",
				    dataType: "json",
				    data: function (params) {
				      var query = {
				        search: params.term,
				        type: 'category'
				      }

				      // Query parameters will be ?search=[term]&type=public
				      return query;
				    },
				    processResults: function (data) {
				      // Tranforms the top-level key of the response object from 'items' to 'results'
				      var results = [];
				      $.each(data, function (index, item) {
				        results.push({
				          id: item.id +'-'+ item.name,
				          text: item.name
				        });
				      });

				      return {
				        results: results
				      };
				    }
				  }
				});
				// $(".select2#tag").select2({
				//   ajax: {
				//     url: "<?php // echo $data['all_posts']['tag']; ?>",
				//     dataType: "json",
				//     data: function (params) {
				//       var query = {
				//         search: params.term,
				//         type: 'tag'
				//       }

				//       // Query parameters will be ?search=[term]&type=public
				//       return query;
				//     },
				//     processResults: function (data) {
				//       // Tranforms the top-level key of the response object from 'items' to 'results'
				//       var results = [];
				//       $.each(data, function (index, item) {
				//         results.push({
				//           id: item.id +'-'+ item.name,
				//           text: item.name
				//         });
				//       });

				//       return {
				//         results: results
				//       };
				//     }
				//   }
				// });
				//Date picker
				$('.datepicker').datepicker({
					autoclose: true
				});
				// Hide publish-form
				// $(".publish-form").hide();
				// Show publish date when select publish
				$(document.body).on("change","#page",function(){
					// show gallery
					var page = $(this).val();
					// console.log(page);
					if (page.search('Video') > 1) {
						$('#gallery-box').show()
					} else {
						$('#gallery-box').hide()
					}
				});
				$(document.body).on("change","#status",function(){
					var val = $(this).val();
					if (val != "schedule") {
						$(".publish-form").slideUp();
					} else {
						$(".publish-form").slideDown();
					}
				});
				// Reset image
				$(document).on("click", ".reset-image", function(e) {
					e.preventDefault();
					var parent = $(this).parent();
					parent.find("input").val("");
					parent.find("img").attr("src", "<?php echo get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>");
					parent.find("img").attr("alt", "");
					parent.find("img").attr("title", "");
					$(this).remove();
				});
				// Remove gallery
				$(document).on("click", ".trash", function(e) {
					e.preventDefault();
					$(this).parent().remove();
				});
			});
			</script>
