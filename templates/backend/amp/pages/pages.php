			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title"><a href="<?php echo $data['action']['create']; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> New Page</a></h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<table id="table" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th class="no-sort">No.</th>
										<!-- <th class="no-sort">
											<div class="checkbox">
											<label>
												<input type="checkbox" name="check_all" id="check-all">
												Select all
											</label>
											</div>
										</th> -->
										<th>Title</th>
										<th>Date</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- Running script before load jQuery script -->
			<script type="text/javascript">
			$(function(){
				var dataTable = $('#table').DataTable({
					// "paging": true,
					// "lengthChange": false,
					// "searching": false,
					// "ordering": true,
					// "processing": true,
					// "serverSide": true,
					"ajax": {
						"url" : "<?php echo $data['lists'] ?>",
						// "type" : "POST"
					},
					"columns": [
						{ "data": null, "orderable": false },
						{ "data": "title" },
						// { "data": "author" },
						{ "data": "published" },
						{ "data": "status" },
						{ "data": "id" }
					],
					"columnDefs": [
						{
							render: function(data, type, row) {
								return '<a href="<?php echo $data['action']['update']; ?>'+row.id+'" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Update Page"><span class="fa fa-edit"></span></a>&nbsp;'+
								'<a href="<?php echo $data['action']['preview']; ?>'+row.slug+'" target="_blank" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="Page" class="btn btn-warning view" data-toggle="tooltip" data-placement="top" title="Preview Page"><span class="fa fa-eye"></span></a>&nbsp;'+
								'<a href="#" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="Page" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Page"><span class="fa fa-trash"></span></a>';
							},
							targets: -1,
						},
						{
							render: function(data, type, row) {
								var label;
								if (row.status == 'draft') { 
									label = 'warning';
								} else if (row.status == 'schedule') {
									label = 'info';
								} else if (row.status == 'publish') {
									label = 'success';
								}
								return '<span class="label label-'+label+'">'+row.status+'</span>';
							},
							targets: -3,
						},
					]
				});
				// Numbering
				dataTable.on( 'order.dt search.dt', function () {
					dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
						cell.innerHTML = i+1;
					});
				}).draw();

				// DELETE
				$(document).on("click",".delete", function(e) {
					e.preventDefault();
					var attribute = $(this).data("attribute");
					var id = $(this).data("id");
					var title = $(this).data("title");
					$("#myModal-delete #delete-id").val(id);
					$("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
					$("#myModal-delete form").attr("action", "<?php echo $data['action']['delete']; ?>");

					$("#myModal-delete").modal("show");
				});
			});
			</script>