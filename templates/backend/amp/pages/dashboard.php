			<div class="row">
				<div class="col-sm-3">
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3><?php echo $data['content']['pages']['total'] ?></h3>
							<p>All <?php echo $data['content']['pages']['title'] ?></p>
						</div>
						<div class="icon">
							<i class="fa fa-files-o"></i>
						</div>
						<a href="<?php echo $data['content']['pages']['detail'] ?>" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-3"> 
					<div class="small-box bg-aqua">
						<div class="inner">
							<h3><?php echo $data['content']['posts']['total'] ?></h3>
							<p>All <?php echo $data['content']['posts']['title'] ?></p>
						</div>
						<div class="icon">
							<i class="fa fa-newspaper-o"></i>
						</div>
						<a href="<?php echo $data['content']['posts']['detail'] ?>" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="small-box bg-red">
						<div class="inner">
							<h3><?php echo $data['content']['users']['total'] ?></h3>
							<p>All <?php echo $data['content']['users']['title'] ?></p>
						</div>
						<div class="icon">
							<i class="fa fa-users"></i>
						</div>
						<a href="<?php echo $data['content']['users']['detail'] ?>" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-3"> 
					<div class="small-box bg-green">
						<div class="inner">
							<h3><?php echo $data['content']['comments']['total'] ?></h3>
							<p>All <?php echo $data['content']['comments']['title'] ?></p>
						</div>
						<div class="icon">
							<i class="fa fa-comment-o"></i>
						</div>
						<a href="<?php echo $data['content']['comments']['detail'] ?>" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-3"> 
					<div class="small-box bg-yellow">
						<div class="inner">
							<h3><?php echo $data['content']['messages']['total'] ?></h3>
							<p>All <?php echo $data['content']['messages']['title'] ?></p>
						</div>
						<div class="icon">
							<i class="fa fa-envelope-o"></i>
						</div>
						<a href="<?php echo $data['content']['messages']['detail'] ?>" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
				<div class="col-sm-3"> 
					<div class="small-box bg-aqua">
						<div class="inner">
							<h3><?php echo $data['content']['visitors']['total'] ?></h3>
							<p>All <?php echo $data['content']['visitors']['title'] ?></p>
						</div>
						<div class="icon">
							<i class="fa fa-line-chart"></i>
						</div>
						<a href="<?php echo $data['content']['visitors']['detail'] ?>" class="small-box-footer">
							More info <i class="fa fa-arrow-circle-right"></i>
						</a>
					</div>
				</div>
			</div><!-- /.row -->
			<div class="row" style="display:none">
				<div class="col-sm-3">
					<div class="info-box">
						<!-- Apply any bg-* class to to the icon to color it -->
						<span class="info-box-icon bg-blue"><i class="fa fa-tags"></i></span>
						<div class="info-box-content">
							<span class="info-box-text"><?php echo $data['content']['categories']['title'] ?></span>
							<span class="info-box-number"><?php echo $data['content']['categories']['total'] ?></span>
						</div><!-- /.info-box-content -->
					</div><!-- /.info-box -->
				</div>
				<div class="col-sm-3">
					<div class="info-box">
					<!-- Apply any bg-* class to to the icon to color it -->
					<span class="info-box-icon bg-green"><i class="fa fa-tag"></i></span>
					<div class="info-box-content">
					<span class="info-box-text"><?php echo $data['content']['tags']['title'] ?></span>
					<span class="info-box-number"><?php echo $data['content']['tags']['total'] ?></span>
					</div><!-- /.info-box-content -->
					</div><!-- /.info-box -->
				</div>
			</div><!-- /.row -->