			<div class="row">
				<!-- Title and Content -->
				<div class="col-xs-8">
					<div class="box">
						<!-- <div class="box-header">
						</div> -->
							<!-- /.box-header -->
						<div class="box-body">
							<?php  $active_theme = json_decode($data['setting']['theme']->content);
							foreach (get_list_directory() as $dir) : ?>
							<!-- username input -->
							<div class="col-sm-3">
								<div class="thumbnail text-center">
									<img src="<?php echo base_url('templates/frontend/'.$dir.'/screenshot.jpg'); ?>" alt="<?php echo $dir ?>" class="img-responsive" style="height:150px;">
									<div class="caption"><?php echo $dir; ?></div>
									<?php if ($active_theme->template == $dir) : ?>
									<label class="btn ">ACTIVE</label>
									<?php else: ?>
									<form role="form" action="<?php echo site_url('admin/setting/theme') ?>" method="POST">
										<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
										<input type="hidden" name="template" value="<?php echo $dir; ?>">
										<button type="submit" name="submit" class="btn btn-warning">Set Template</button>
									</form>
									<!-- /form -->
									<?php endif; ?>
								</div>
							</div>
							<?php endforeach; ?>
							<div class="clearfix"></div>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->
