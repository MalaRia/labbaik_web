
    <form role="form" id="form" data-id="<?php echo (get_flash('slider_id')) ? get_flash('slider_id') : ''; ?>" action="<?php echo (get_flash('form_action')) ? get_flash('form_action') : $data['action']['create'] ?>" method="POST">
      <input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
      <input type="hidden" name="form_action" id="form_action" value="<?php echo get_flash('form_action'); ?>">
      <input type="hidden" name="slider_id" id="slider_id" value="<?php echo get_flash('slider_id'); ?>">
      <div class="row">
        <!-- Title and Content -->
        <div class="col-xs-4">
          <div class="box">
            <!-- <div class="box-header">
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <!-- order input -->
              <div class="form-group">
                <input type="text" name="title" class="form-control" id="title" placeholder="Title">
              </div>
              <!-- order input -->
              <div class="form-group">
                <input type="number" min="1" class="form-control" name="order" id="order" placeholder="Order">
              </div>
              <div id="slider">
                <div class="form-group">
                  <div class="thumbnail">
                    <img id="image-set" class="img-responsive" src="<?php echo (get_flash('fullImage')) ? get_flash('fullImage') : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>">
                    <input type="hidden" name="fullImage" id="fullImage" value="<?php echo (get_flash('fullImage')) ? get_flash('fullImage') : '' ?>">
                    <input type="hidden" name="image_big" id="imageFileField" value="<?php echo (get_flash('image_big')) ? get_flash('image_big') : '' ?>">
                    <input type="hidden" name="image_thumb" id="thumbFileField" value="<?php echo (get_flash('image_thumb')) ? get_flash('image_thumb') : '' ?>">
                    <input type="hidden" name="image_title" id="imageTitle" value="<?php echo (get_flash('image_title')) ? get_flash('image_title') : '' ?>">
                    <input type="hidden" name="image_alt" id="imageAlt" value="<?php echo (get_flash('image_alt')) ? get_flash('image_alt') : '' ?>">
                  </div>
                  <a href="#" id="popup" class="btn btn-info pull-right">Upload File</a>
                  <div class="clearfix"></div>
                </div>
              </div>
              <!-- action input -->
              <div class="form-group">
                <select class="form-control select2" name="choose_action" id="choose_action" required="required" style="width: 100%;">
                  <option value="create" data-action="<?php echo $data['action']['create'] ?>" <?php echo get_flash('cat_id') ? '' : 'selected' ?>>Create</option>
                  <option value="update" data-action="<?php echo $data['action']['update'] ?>" <?php echo get_flash('cat_id') ? 'selected' : '' ?>>Update</option>
                </select>
                <!-- <span class="help-block">Action is required</span> -->
              </div>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
                <div class="clearfix"></div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- Status, Attribute, Featured Image -->
        <div class="col-xs-8">
          <!-- STATUS -->
          <div class="box">
            <div class="box-header">
              <h3>Sliders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th class="no-sort">No.</th>
                  <!-- <th class="no-sort">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="check_all" id="check-all">
                        Select all
                      </label>
                    </div>
                  </th> -->
                  <th>Title</th>
                  <th>Order</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </form>
    <!-- /form -->

<!-- Running script before load jQuery script -->
<script type="text/javascript">
  $(function(){
    // DATATABLE
    var dataTable = $('#table').DataTable({
      // "paging": true,
      // "lengthChange": false,
      // "searching": false,
      // "ordering": true,
      // "processing": true,
      // "serverSide": true,
      "ajax": {
        "url" : "<?php echo $data['lists'] ?>",
        // "type" : "POST"
      },
      "columns": [
        { "data": null, "orderable": false },
        { "data": "title" },
        { "data": "order" },
        { "data": "image" },
        { "data": "id" }
      ],
      "columnDefs": [
        {
          render: function(data, type, row) {
            return '<a href="#" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="Slider" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Slider"><span class="fa fa-edit"></span></a>&nbsp;'+
            '<a href="#" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="Slider" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Slider"><span class="fa fa-trash"></span></a>';
          },
          targets: -1,
        },
        {
          render: function(data, type, row) {
            return '<img src="<?php echo base_url('assets/uploads/thumbs/') ?>'+row.image.thumb+'" alt="'+row.image.alt+'" title="'+row.image.title+'" data-thumb="'+row.image.thumb+'" data-img="'+row.image.image+'" class="img-responsive">';
          },
          targets: -2,
        }
      ]
    });
    // Numbering
    dataTable.on( 'order.dt search.dt', function () {
      dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
          cell.innerHTML = i+1;
      });
    }).draw();

    // EDIT
    $(document).on("click",".edit", function(e) {
      e.preventDefault();
      var id = $(this).attr('data-id');
      var title = $(this).parent().siblings("td:eq(1)").html();
      var order = $(this).parent().siblings("td:eq(2)").html();
      var fullImage = $(this).parent().siblings("td:eq(3)").find('img').attr('src');
      var imageFileField = $(this).parent().siblings("td:eq(3)").find('img').attr('data-img');
      var thumbFileField = $(this).parent().siblings("td:eq(3)").find('img').attr('data-thumb');
      var imageTitle = $(this).parent().siblings("td:eq(3)").find('img').attr('title');
      var imageAlt = $(this).parent().siblings("td:eq(3)").find('img').attr('alt');
      // Set Form Value
      $("#title").val(title);
      $("#order").val(order);
      
      // Image
      // $("#link").val(data.link);
      // console.log(image);
      $("#image-set").attr('src', fullImage);
      $("#fullImage").val(fullImage);
      $("#imageFileField").val(imageFileField);
      $("#thumbFileField").val(thumbFileField);
      $("#imageTitle").val(imageTitle);
      $("#imageAlt").val(imageAlt);

      var frmForm = $("form#form");

      // console.log(id);
      var actionUpdate = "<?php echo $data['action']['update'] ?>";
      frmForm.attr("action", actionUpdate);
      frmForm.attr("data-id",id);
      $("#form_action").val(actionUpdate);
      $("#slider_id").val(id);

      $("#choose_action").val("update").trigger("change"); // set select2

      // hide tooltip
      $(this).tooltip('hide');
    });

    // DELETE
    $(document).on("click",".delete", function(e) {
      e.preventDefault();
      var attribute = $(this).data("attribute");
      var id = $(this).data("id");
      var title = $(this).data("title");
      $("#myModal-delete #delete-id").val(id);
      $("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
      $("#myModal-delete form").attr("action", "<?php echo $data['action']['delete'] ?>");

      $("#myModal-delete").modal("show");
    });

    // CHOOSE ACTION
    $("#choose_action").change(function() {
      var action = $(this).val();
      var frmForm = $("form#form");
      var id = "";
      if (action == "create") {
        frmForm.attr("data-id", "");
        frmForm.attr("action", "<?php echo $data['action']['create'] ?>");
        // field
        $("#form_action").val("<?php echo $data['action']['create'] ?>");
        $("#slider_id").val(id);
      } else if (action == "update") {;
        if (frmForm.attr("data-id") > 0) {
          var id = frmForm.attr("data-id");
          frmForm.attr("action", "<?php echo $data['action']['update'] ?>");
          // field
          $("#form_action").val("<?php echo $data['action']['update'] ?>");
          $("#slider_id").val(id);

        } if (frmForm.attr("data-id") == "") {
          $("#choose_action").val("create").trigger("change"); // set select2
          alert("Please choose the Slider to edit!");
          return false;
        }
      }
    });
  });

  // Meta Featured Image
  uploadPath = "<?php echo base_url('assets/uploads/') ?>";
</script>
