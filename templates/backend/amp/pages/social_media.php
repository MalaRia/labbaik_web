
			<form role="form" action="" method="POST">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
							<?php $social_media = json_decode($data['setting']['social_media']->content); ?>
								<!-- facebook input -->
								<div class="form-group">
									<input type="text" class="form-control" name="facebook" id="facebook" value="<?php echo (isset($social_media->facebook)) ? $social_media->facebook : ''; ?>" placeholder="Facebook">
								</div>
								<!-- twitter input -->
								<div class="form-group">
									<input type="text" class="form-control" name="twitter" id="twitter" value="<?php echo (isset($social_media->twitter)) ? $social_media->twitter : ''; ?>" placeholder="Twitter">
								</div>
								<!-- gplus input -->
								<div class="form-group">
									<input type="text" class="form-control" name="gplus" id="gplus" value="<?php echo (isset($social_media->gplus)) ? $social_media->gplus : ''; ?>" placeholder="Google Plus">
								</div>
								<!-- instagram input -->
								<div class="form-group">
									<input type="text" class="form-control" name="instagram" id="instagram" value="<?php echo (isset($social_media->instagram)) ? $social_media->instagram : ''; ?>" placeholder="Instagram">
								</div>
								<!-- linkedin input -->
								<div class="form-group">
									<input type="text" class="form-control" name="linkedin" id="linkedin" value="<?php echo (isset($social_media->linkedin)) ? $social_media->linkedin : ''; ?>" placeholder="LinkedIn">
								</div>
								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->