			<div class="row">
				<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<?php if ($this->uri->segment(3) != 'feed' && $this->uri->segment(3) != 'feed-popular') : ?>
						<h3 class="box-title"><a href="<?php echo $data['action']['create']; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> New Post</a></h3>
						<?php endif; ?>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="table" class="table table-bordered table-striped table-hover">
							<thead>
								<tr>
									<th class="no-sort">No.</th>
									<!-- <th class="no-sort">
										<div class="checkbox">
										<label>
											<input type="checkbox" name="check_all" id="check-all">
											Select all
										</label>
										</div>
									</th> -->
									<th>Title</th>
									<th>Author</th>
									<!-- <th>Categories</th> -->
									<th>Status</th>
									<th>Date</th>
									<th>Page</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody></tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- page script -->
			<script type="text/javascript">
			$(function(){
				var dataTable = $('#table').DataTable({
					// "paging": true,
					// "lengthChange": false,
					// "searching": false,
					// "ordering": true,
					// "processing": true,
					// "serverSide": true,
					"ajax": {
						"url" : "<?php echo $data['lists'] ?>",
						// "type" : "POST"
					},
					"columns": [
						{ "data": null, "orderable": false },
						{ "data": "title" },
						{ "data": "author" },
						// { "data": "category" },
						{ "data": "status" },
						{ "data": "published" },
						{ "data": "page_slug" },
						{ "data": "id" }
					],
					"columnDefs": [
						{
							render: function(data, type, row) {
								<?php if ($this->uri->segment(3) != 'feed' && $this->uri->segment(3) != 'feed-popular') : ?>
								return '<a href="<?php echo $data['action']['update']; ?>'+row.id+'" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="<?php echo $data['action']['update']; ?>" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Post"><span class="fa fa-edit"></span></a>&nbsp;'+
								'<a href="#" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="Post" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Post"><span class="fa fa-trash"></span></a>';
								<?php else: ?>
								return '<a href="<?php echo $data['action']['update']; ?>'+row.id+'" data-id="'+row.id+'" data-title="'+row.title+'" data-attribute="<?php echo $data['action']['update']; ?>" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Post"><span class="fa fa-edit"></span></a>&nbsp;';
								<?php endif; ?>
							},
							targets: -1,
						},
					]
				});
				// Numbering
				dataTable.on( 'order.dt search.dt', function () {
					dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
						cell.innerHTML = i+1;
					});
				}).draw();
				
				<?php if ($this->uri->segment(3) != 'feed' && $this->uri->segment(3) != 'feed-popular') : ?>
				// DELETE
				$(document).on("click",".delete", function(e) {
					e.preventDefault();
					var attribute = $(this).data("attribute");
					var id = $(this).data("id");
					var title = $(this).data("title");
					$("#myModal-delete #delete-id").val(id);
					$("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
					$("#myModal-delete form").attr("action", "<?php echo $data['action']['delete']; ?>");

					$("#myModal-delete").modal("show");
				});
				<?php endif; ?>
			});
			</script>
