
			<form role="form" action="" method="POST">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
								<?php $seo = json_decode($data['setting']['seo']->content); ?>
								<!-- title input -->
								<div class="form-group">
									<input type="text" name="title" id="title" class="form-control" value="<?php echo (isset($seo->title)) ? $seo->title : ''; ?>" placeholder="Title...">
								</div>
								<!-- description input -->
								<div class="form-group">
									<textarea name="description" id="description" class="form-control" rows="3" placeholder="Description..."><?php echo (isset($seo->description)) ? $seo->description : ''; ?></textarea>
								</div>
								<!-- keywords input -->
								<div class="form-group">
									<textarea class="form-control" name="keywords" id="keywords" value="" placeholder="Keywords" rows="3"><?php echo (isset($seo->keywords)) ? $seo->keywords : ''; ?></textarea>
								</div>
								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
					<!-- GA, RSS, SITEMAP, ROBOTS.TXT -->
					<div class="col-xs-4">
						<!-- SITEMAP -->
						<div class="box">
							<div class="box-header">
								<h3>Google Verification Code</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- sitemap input -->
								<div class="form-group">
									<textarea name="ga_verify" id="ga_verify" class="form-control" rows="3" placeholder="Google Verification Code"><?php echo (isset($seo->ga_verify)) ? $seo->ga_verify : ''; ?></textarea>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- GA -->
						<div class="box">
							<div class="box-header">
								<h3>Google Analytics</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- ga_analytics input -->
								<div class="form-group">
									<textarea class="form-control" name="ga_analytics" id="ga_analytics" placeholder="Google Analytics" rows="3"><?php echo (isset($seo->ga_analytics)) ? $seo->ga_analytics : ''; ?></textarea>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- RSS -->
						<div class="box">
							<div class="box-header">
								<h3>RSS</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- rss input -->
								<div class="form-group">
									<textarea name="rss" id="rss" class="form-control" rows="3" placeholder="RSS Url..."><?php echo (isset($seo->rss)) ? $seo->rss : ''; ?></textarea>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- SITEMAP -->
						<!-- <div class="box">
								<div class="box-header">
									<h3>SITEMAP</h3>
								</div> -->
								<!-- /.box-header -->
								<!-- <div class="box-body"> -->
								<!-- sitemap input -->
								<!-- <div class="form-group">
									<textarea name="sitemap" id="sitemap" class="form-control" rows="3" placeholder="SITEMAP Url..."><?php echo (isset($seo->sitemap)) ? $seo->sitemap : ''; ?></textarea>
								</div>
							</div> -->
							<!-- /.box-body -->
						<!-- </div> -->
						<!-- /.box -->
						<!-- ROBOTS.TXT -->
						<div class="box">
							<div class="box-header">
								<h3>ROBOTS.TXT</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<!-- robot input -->
								<div class="form-group">
									<textarea name="robots" id="robots" class="form-control" rows="3" placeholder="ROBOTS"><?php echo (isset($seo->robots)) ? $seo->robots : ''; ?></textarea>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->