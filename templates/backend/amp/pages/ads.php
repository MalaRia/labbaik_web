
    <form role="form" method="POST" id="form" data-id="<?php echo (get_flash('ads_id')) ? get_flash('ads_id') : ''; ?>" action="<?php echo $data['action']['create'] ?>">
      <input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
      <input type="hidden" name="form_action" id="form_action" value="<?php echo get_flash('form_action'); ?>">
      <input type="hidden" name="ads_id" id="ads_id" value="<?php echo get_flash('ads_id'); ?>">
      <div class="row">
        <!-- Title and Content -->
        <div class="col-xs-4">
          <div class="box">
            <!-- <div class="box-header">
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <!-- type input -->
              <div class="form-group">
                <select class="form-control select2" name="type" id="type" style="width: 100%;">
                  <option selected="selected" value="">-: Choose Type :-</option>
                  <option value="embedded-script">Embedded Script</option>
                  <option value="non-embedded-script">Non Embedded Script</option>
                </select>
              </div>
              <!-- position input -->
              <div class="form-group">
                <select class="form-control select2" name="position" id="position" style="width: 100%;">
                  <option selected="selected" value="">-: Choose Position :-</option>
                  <option value="long-top">Top (Long Width)</option>
                  <option value="long-bottom">Bottom (Long Width)</option>
                  <option value="long-left-side">Left Side (Long Height)</option>
                  <option value="long-right-side">Right Side (Long Height)</option>
                  <option value="short-left-side">Left Side (Short)</option>
                  <option value="short-right-side">Right Side (Short)</option>
                  <option value="short-post">Post (Short)</option>
                  <option value="long-post">Post (Long)</option>
                </select>
              </div>
              <div id="embedded-script" style="display: none;">
                <!-- script -->
                <div class="form-group">
                  <textarea rows="6" class="form-control" name="embedded_script" id="embedded_script" placeholder="Embedded Script" ></textarea>
                </div>
              </div>
              <div id="non-embedded-script" style="display: none;">
                <!-- url input -->
                <div class="form-group">
                  <input type="text" class="form-control" name="link" id="link" placeholder="Link...">
                </div>
                <div class="form-group">
                  <div class="thumbnail">
                    <img id="image-set" class="img-responsive" src="<?php echo (get_flash('fullImage')) ? get_flash('fullImage') : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>">
                    <input type="hidden" name="fullImage" id="fullImage" value="<?php echo (get_flash('fullImage')) ? get_flash('fullImage') : '' ?>">
                    <input type="hidden" name="image_big" id="imageFileField" value="<?php echo (get_flash('image_big')) ? get_flash('image_big') : '' ?>">
                    <input type="hidden" name="image_thumb" id="thumbFileField" value="<?php echo (get_flash('image_thumb')) ? get_flash('image_thumb') : '' ?>">
                    <input type="hidden" name="image_title" id="imageTitle" value="<?php echo (get_flash('image_title')) ? get_flash('image_title') : '' ?>">
                    <input type="hidden" name="image_alt" id="imageAlt" value="<?php echo (get_flash('image_alt')) ? get_flash('image_alt') : '' ?>">
                  </div>
                  <a href="#" id="popup" class="btn btn-info pull-right">Upload File</a>
                  <div class="clearfix"></div>
                </div>
              </div>
              <!-- action input -->
              <div class="form-group">
                <select class="form-control select2" name="choose_action" id="choose_action" required="required" style="width: 100%;">
                  <option value="create" data-action="<?php echo $data['action']['create'] ?>" <?php echo get_flash('cat_id') ? '' : 'selected' ?>>Create</option>
                  <option value="update" data-action="<?php echo $data['action']['update'] ?>" <?php echo get_flash('cat_id') ? 'selected' : '' ?>>Update</option>
                </select>
                <!-- <span class="help-block">Action is required</span> -->
              </div>
              <div class="form-group">
                <button type="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
                <div class="clearfix"></div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- Status, Attribute, Featured Image -->
        <div class="col-xs-8">
          <!-- STATUS -->
          <div class="box">
            <div class="box-header">
              <h3>Sliders</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th class="no-sort">No.</th>
                  <!-- <th class="no-sort">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="check_all" id="check-all">
                        Select all
                      </label>
                    </div>
                  </th> -->
                  <th>Type</th>
                  <!-- <th>Type 1</th> -->
                  <th>Position</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </form>
    <!-- /form -->

<!-- MODAL FEATURED IMAGE -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title col-sm-3" id="myModalLabel">
        <a href="<?php echo $data['filemanager']['standalone'] ?>" class="btn btn-warning" id="popup-upload"><span class="glyphicon glyphicon-picture"></span> Choose File</a>
      </h4>
    </div>
    <div class="modal-body">
      <div class="col-sm-8">
        <img id="image_preview" class="img-responsive" />
      </div>
      <div class="col-sm-4">
        <blockquote class="meta-image">
          <span id="fullname"></span> <br>
          <span id="modified_at"></span> <br>
          <span id="size"></span> <br>
          <span id="dimensions"></span>
        </blockquote>
        <input type="hidden" class="form-control" id="fieldID4">
        <div class="form-group">
          <input type="text" class="form-control" name="url" id="url" placeholder="Location">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="image_title" id="image_title" placeholder="Title">
          <input type="hidden" name="image_img" id="imageFile">
          <input type="hidden" name="image_thumb" id="thumbFile">
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="image_alt" id="image_alt" placeholder="Image Alt">
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="modal-footer">
      <div class="col-sm-4 pull-right">
        <div class="form-group">
        <a href="#" id="set-image" class="btn btn-success">Set Image</a>
      </div>
      </div>
    </div>
  </div>
</div>
</div>

<!-- MODAL FILEMANAGER -->
<div class="modal fade" id="myModal-Upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
<div class="modal-dialog modal-lg" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title" id="myModalLabel">FILEMANAGER</h4>
    </div>
    <div class="modal-body">
      ...
    </div>
  </div>
</div>
</div>


<!-- Running script before load jQuery script -->
<script type="text/javascript">
  $(function(){
    //Initialize Select2 Elements
    $(".select2").select2();
    // DATATABLE
    var dataTable = $('#table').DataTable({
      // "paging": true,
      // "lengthChange": false,
      // "searching": false,
      // "ordering": true,
      // "processing": true,
      // "serverSide": true,
      "ajax": {
        "url" : "<?php echo $data['lists'] ?>",
        // "type" : "POST"
      },
      "columns": [
        { "data": null, "orderable": false },
        { "data": "type" },
        // { "data": "type2", "visible": false },
        { "data": "position" },
        { "data": "id" }
      ],
      "columnDefs": [
        {
          render: function(data, type, row) {
            return '<a href="#" data-id="'+row.id+'" data-title="'+row.type+'" data-attribute="Ads" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Ads"><span class="fa fa-edit"></span></a>&nbsp;'+
            '<a href="#" data-id="'+row.id+'" data-title="'+row.type+'" data-attribute="Ads" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Ads"><span class="fa fa-trash"></span></a>';
          },
          targets: -1,
        }
      ]
    });
    // Numbering
    dataTable.on( 'order.dt search.dt', function () {
      dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
          cell.innerHTML = i+1;
      });
    }).draw();

    // EDIT
    $(document).on("click",".edit", function(e) {
      e.preventDefault();
      var id = $(this).attr('data-id');
      $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '<?php echo $data['single'] ?>'+id,
        success: function(data) {
          // console.log(data);
          // Set Form Value
          $("#type").val(data.type).trigger("change"); // select2
          $("#position").val(data.position).trigger("change"); // select2
          if (data.type == 'embedded-script')
          {
            $("#embedded_script").val(data.embedded_script);
          }
          else if(data.type == 'non-embedded-script')
          {
            $("#link").val(data.link); // select2
            $("#image-set").attr('src', '<?php echo base_url('assets/uploads/thumbs/') ?>'+data.image.thumb);
            $("#fullImage").attr('src', '<?php echo base_url('assets/uploads/thumbs/') ?>'+data.image.thumb);
            $("#imageFileField").attr('src', data.image.image);
            $("#thumbFileField").attr('src', data.image.thumb);
            $("#imageTitle").attr('src', data.image.title);
            $("#imageAlt").attr('src', data.image.alt);
          }

          var frmForm = $("form#form");

          // console.log(id);
          var actionUpdate = "<?php echo $data['action']['update'] ?>" + id;
          frmForm.attr("action", actionUpdate);
          frmForm.attr("data-id",id);
          $("#form_action").val(actionUpdate);
          $("#ads_id").val(id);

          $("#choose_action").val("update").trigger("change"); // set select2

          // hide tooltip
          $(this).tooltip('hide');
        }
      })
      // var type = $(this).parent().siblings("td:eq(1)").html();
      // var position = $(this).parent().siblings("td:eq(2)").html();
      // // Set Form Value
      // $("#type").val(type).trigger("change"); // select2
      // $("#position").val(position).trigger("change"); // select2

      // hide tooltip
      $(this).tooltip('hide');
    });

    // DELETE
    $(document).on("click",".delete", function(e) {
      e.preventDefault();
      var attribute = $(this).data("attribute");
      var id = $(this).data("id");
      var title = $(this).data("title");
      $("#myModal-delete #delete-id").val(id);
      $("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
      $("#myModal-delete form").attr("action", "<?php echo $data['action']['delete'] ?>");

      $("#myModal-delete").modal("show");
    });

    // Embedded or Non
    $("#type").change(function() {
      var type = $(this).val();
      if (type == "embedded-script") {
        $("#embedded-script").slideDown();
        $("#non-embedded-script").slideUp();
      } else {
        $("#embedded-script").slideUp();
        $("#non-embedded-script").slideDown();
      }
    });

    // CHOOSE ACTION
    $("#choose_action").change(function() {
      var action = $(this).val();
      var frmForm = $("form#form");
      var id = "";
      if (action == "create") {
        frmForm.attr("data-id", "");
        frmForm.attr("action", "<?php echo $data['action']['create'] ?>");
        // field
        $("#form_action").val("<?php echo $data['action']['create'] ?>");
        $("#ads_id").val(id);
      } else if (action == "update") {;
        if (frmForm.attr("data-id") > 0) {
          var id = frmForm.attr("data-id");
          frmForm.attr("action", "<?php echo $data['action']['update'] ?>" + id);
          // field
          $("#form_action").val("<?php echo $data['action']['update'] ?>" + id);
          $("#ads_id").val(id);

        } if (frmForm.attr("data-id") == "") {
          $("#choose_action").val("create").trigger("change"); // set select2
          alert("Please choose the Ads to edit!");
          return false;
        }
      }
    });
  });

  // Meta Featured Image
  uploadPath = "<?php echo base_url('assets/uploads/') ?>";
</script>
