
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"><a href="<?php echo $data['action']['create']; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> New User</a></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="table" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                  <th class="no-sort">No.</th>
                  <!-- <th class="no-sort">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox" name="check_all" id="check-all">
                        Select all
                      </label>
                    </div>
                  </th> -->
                  <th>Username</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Role</th>
                  <!-- <th>Posts</th> -->
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <!-- <td></td> -->
                  <!-- <td>
                    <div class="checkbox">
                      <label>
                        <input type="checkbox">
                      </label>
                    </div>
                  </td> -->
                  <!-- <td>Trident Title</td>
                  <td>Email</td>
                  <td>Role</td>
                  <td>Posts</td>
                  <td>
                    <a href="#" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Edit Page"><span class="fa fa-edit"></span></a>
                    <a href="#" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="View Page"><span class="fa fa-eye"></span></a>
                    <a href="#" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Delete Page"><span class="fa fa-trash"></span></a>
                  </td> -->
                </tr>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

<!-- Running script before load jQuery script -->
<script type="text/javascript">
  $(function(){
    var dataTable = $('#table').DataTable({
      // "paging": true,
      // "lengthChange": false,
      // "searching": false,
      // "ordering": true,
      // "processing": true,
      // "serverSide": true,
      "ajax": {
        "url" : "<?php echo $data['lists'] ?>",
        // "type" : "POST"
      },
      "columns": [
        { "data": null, "orderable": false },
        { "data": "username" },
        { "data": "name" },
        { "data": "email" },
        { "data": "role" },
        // { "data": "post" },
        { "data": "id" }
      ],
      "columnDefs": [
        {
          render: function(data, type, row) {
            var deleteButton = '';
            if (row.id != <?php echo $user_data->id ?>)
            {
              deleteButton = '<a href="#" data-id="'+row.id+'" data-title="'+row.username+'" data-attribute="User" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete User"><span class="fa fa-trash"></span></a>';
            }
            return '<a href="<?php echo $data['action']['update']; ?>'+row.id+'" data-id="'+row.id+'" data-title="'+row.username+'" data-attribute="User" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit User"><span class="fa fa-edit"></span></a>&nbsp;'+
            deleteButton;
          },
          targets: -1,
        }
      ]
    });
    // Numbering
    dataTable.on( 'order.dt search.dt', function () {
      dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
          cell.innerHTML = i+1;
      });
    }).draw();

    // DELETE
    $(document).on("click",".delete", function(e) {
      e.preventDefault();
      var attribute = $(this).data("attribute");
      var id = $(this).data("id");
      var title = $(this).data("title");
      $("#myModal-delete #delete-id").val(id);
      $("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
      $("#myModal-delete form").attr("action", "<?php echo $data['action']['delete']; ?>");

      $("#myModal-delete").modal("show");
    });
  });
</script>

<!-- page script -->
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
