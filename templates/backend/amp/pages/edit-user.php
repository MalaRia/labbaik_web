
			<form role="form" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
								<!-- username input -->
								<div class="form-group">
									<input type="text" class="form-control" name="username" id="username" value="<?php echo $data['all_users']['single']->username; ?>" required="required" placeholder="Username">
								</div>
								<!-- email input -->
								<div class="form-group">
									<input type="email" class="form-control" name="email" id="email" value="<?php echo $data['all_users']['single']->email; ?>" required="required" placeholder="Email">
								</div>
								<!-- firstname input -->
								<div class="form-group">
									<input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo $data['all_users']['single']->first_name; ?>" required="required" placeholder="Firstname">
								</div>
								<!-- lastname input -->
								<div class="form-group">
									<input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $data['all_users']['single']->last_name; ?>" required="required" placeholder="Lastname">
								</div>
								<!-- website input -->
								<div class="form-group">
									<input type="text" class="form-control" name="website" id="website" value="<?php echo $data['all_users']['single']->website; ?>" placeholder="website">
								</div>
								<!-- role input -->
								<div class="form-group">
									<select class="form-control select2" name="role" id="role" required="required" style="width: 100%;">
									<?php foreach ($roles as $role) : ?>
										<option <?php echo ($data['all_users']['single']->role == $role) ? 'selected' : '' ; ?> value="<?php echo $role ?>"><?php echo ucfirst($role) ?></option>
									<?php endforeach;?>
									</select>
								</div> 
								<!-- password input -->
								<div class="form-group">
									<input type="password" class="form-control" name="password" id="password" placeholder="Password">
								</div>
								<!-- password confirmation input -->
								<div class="form-group">
									<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Password Confirmation">
								</div>

								<!-- facebook input -->
								<div class="form-group">
									<input type="text" class="form-control" name="facebook" id="facebook" value="<?php echo $data['all_users']['single']->facebook; ?>" placeholder="facebook">
								</div>
								<!-- twitter input -->
								<div class="form-group">
									<input type="text" class="form-control" name="twitter" id="twitter" value="<?php echo $data['all_users']['single']->twitter; ?>" placeholder="twitter">
								</div>
								<!-- gplus input -->
								<div class="form-group">
									<input type="text" class="form-control" name="gplus" id="gplus" value="<?php echo $data['all_users']['single']->gplus; ?>" placeholder="gplus">
								</div>
								<!-- instagram input -->
								<div class="form-group">
									<input type="text" class="form-control" name="instagram" id="instagram" value="<?php echo $data['all_users']['single']->instagram; ?>" placeholder="instagram">
								</div>
								<!-- linkedin input -->
								<div class="form-group">
									<input type="text" class="form-control" name="linkedin" id="linkedin" value="<?php echo $data['all_users']['single']->linkedin; ?>" placeholder="linkedin">
								</div>

								<!-- description input -->
								<div class="form-group">
									<textarea class="form-control" name="description" id="description" cols="30" rows="10" required="required" placeholder="Biographical Info"><?php echo $data['all_users']['single']->description; ?></textarea>
								</div>
								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
					<!-- Status, Attribute, Featured Image -->
					<div class="col-xs-4">
						<!-- FEATURED IMAGE -->
						<div class="box">
							<div class="box-header">
								<h3>Featured Image</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<?php // SET IMAGE
								$meta_image = set_image($data['all_users']['single']->avatar); 
							?>
								<div class="thumbnail">
									<img id="upl-image" class="img-responsive" src="<?php echo $meta_image['fullImage'] ?>">
									<input type="file" id="upload_file" name="upload_file" class="hidden">
								</div>
								<label id="upl_file_label" for="upload_file" class="btn btn-info pull-right" data-multiple="false">Upload File</label>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->