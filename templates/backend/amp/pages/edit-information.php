
    <form role="form" action="" method="POST">
      <input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
      <div class="row">
        <!-- Title and Content -->
        <div class="col-xs-8">
          <div class="box">
            <!-- <div class="box-header">
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <!-- editor input -->
              <div class="form-group">
                <textarea id="editor" name="description" class="form-control" placeholder=""><?php echo $data['all_contacts']['single']->description; ?></textarea>
              </div>
              <!-- Save Button -->
              <div class="form-group">
                <button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
        <!-- Status, Attribute, Featured Image -->
        <div class="col-xs-4">
          <!-- FEATURED IMAGE -->
          <div class="box">
            <div class="box-header">
              <h3>Featured Image</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <?php 
                if ($data['all_contacts']['single']->image) :
                $image = json_decode($data['all_contacts']['single']->image);
                $fullImage = base_url('assets/uploads/images/'.$image->image); 
              ?>
                <div class="thumbnail">
                <img id="image-set" class="img-responsive" src="<?php echo ($image) ? $fullImage : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>">
                  <input type="hidden" name="fullImage" id="fullImage" value="<?php echo ($image) ? $fullImage : '' ?>">
                  <input type="hidden" name="image_big" id="imageFileField" value="<?php echo ($image) ? $image->image : '' ?>">
                  <input type="hidden" name="image_thumb" id="thumbFileField" value="<?php echo ($image) ? $image->thumb : '' ?>">
                  <input type="hidden" name="image_title" id="imageTitle" value="<?php echo ($image) ? $image->title : '' ?>">
                  <input type="hidden" name="image_alt" id="imageAlt" value="<?php echo ($image) ? $image->alt : '' ?>">
                </div>
                <a href="#" id="popup" class="btn btn-info pull-right">Upload File</a>
              </div>
              <?php else: ?>
              <div class="thumbnail">
                <img id="image-set" class="img-responsive" src="<?php echo (get_flash('fullImage')) ? get_flash('fullImage') : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>">
                <input type="hidden" name="fullImage" id="fullImage" value="<?php echo (get_flash('fullImage')) ? get_flash('fullImage') : '' ?>">
                <input type="hidden" name="image_big" id="imageFileField" value="<?php echo (get_flash('image_big')) ? get_flash('image_big') : '' ?>">
                <input type="hidden" name="image_thumb" id="thumbFileField" value="<?php echo (get_flash('image_thumb')) ? get_flash('image_thumb') : '' ?>">
                <input type="hidden" name="image_title" id="imageTitle" value="<?php echo (get_flash('image_title')) ? get_flash('image_title') : '' ?>">
                <input type="hidden" name="image_alt" id="imageAlt" value="<?php echo (get_flash('image_alt')) ? get_flash('image_alt') : '' ?>">
                </div>
                <a href="#" id="popup" class="btn btn-info pull-right">Upload File</a>
              <?php endif; ?>
              <div class="clearfix"></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </form>
    <!-- /form -->

<!-- page script -->
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace("editor", {
      height: 400,
    });
  });
  // Meta Featured Image
  uploadPath = "<?php echo base_url('assets/uploads/') ?>";
</script>
