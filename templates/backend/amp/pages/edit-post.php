			<style>
			.thumbnail.gallery img {
				height: 100px!important;
			}
			.thumbnail {
				position: relative;
			}
			.thumbnail .btn {
				top: 0;
				position: absolute;
			}
			</style>
			<form role="form" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
								<!-- title input -->
								<div class="form-group">
									<input type="text" name="title" value="<?php echo $data['all_posts']['single']->title; ?>" class="form-control" placeholder="Your Title...">
								</div>
								<!-- editor input -->
								<div class="form-group">
									<textarea id="editor" name="content" class="form-control" placeholder=""><?php echo $data['all_posts']['single']->content; ?></textarea>
								</div>
								<!-- Gallery input -->
								<div class="form-group" id="gallery-box">
									<div class="col-sm-12">
										<label for="gallery">Video Gallery</label>
									</div>
									<div class="col-sm-12">
									<?php // var_dump($data['all_posts']['single']->type); ?>
										<input type="text" class="form-control" name="video_gallery" id="videoGallery" value="<?php echo ($data['all_posts']['single']->type == 'video') ? $data['all_posts']['single']->additional : '' ?>" placeholder="eg: https://www.youtube.com/embed/jBhxBLPuVbc">
										<p><code>Choose image or video can't use both(priority image if both was filled)</code></p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<div class="box">
							<div class="box-body">
								<!-- Tab panes -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs" role="tablist">
										<li role="post" class="active"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
										<li role="post"><a href="#setting" aria-controls="setting" role="tab" data-toggle="tab">Setting</a></li>
									</ul>
								</div>
								<div class="tab-content" style="padding-top: 15px;">
									<!-- SEO -->
									<div role="tabpanel" class="tab-pane active" id="seo">
									<?php $seo = json_decode($data['all_posts']['single']->seo);
										$title = ($seo) ? $seo->title : '';
										$description = ($seo) ? $seo->description : '';
										$keywords = ($seo) ? $seo->keywords : '';
									?>
									<div class="form-group">
										<input type="text" id="seo_title" name="seo_title" class="form-control" value="<?php echo $title; ?>" placeholder="Seo Title">
									</div>
									<div class="form-group">
										<textarea id="seo_description" name="seo_description" class="form-control" placeholder="Seo Description" rows="3"><?php echo $description; ?></textarea>
									</div>
									<div class="form-group">
										<textarea id="seo_keywords" name="seo_keywords" class="form-control" placeholder="Seo Keywords" rows="3"><?php echo $description; ?></textarea>
									</div></div>
									<div role="tabpanel" class="tab-pane" id="setting">
										<!-- Allow Comments -->
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="allow_comments" <?php echo ($data['all_posts']['single']->comment == 1) ? 'checked' : ''?> value="<?php echo $data['all_posts']['single']->comment ?>" id="allow_comments"> 
													Allow Comments
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col -->
					<!-- Status, Attribute, Featured Image -->
					<div class="col-xs-4">
						<!-- STATUS -->
						<div class="box">
							<div class="box-header">
								<h3>Status</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- status input -->
								<div class="form-group">
									<select class="form-control select2" name="status" id="status" style="width: 100%;">
									<option selected="selected" value="draft" <?php echo ($data['all_posts']['single']->status == 'draft') ? 'selected' : '' ?>>Draft</option>
									<option value="publish" <?php echo ($data['all_posts']['single']->status == 'publish') ? 'selected' : '' ?>>Publish</option>
									<option value="schedule" <?php echo ($data['all_posts']['single']->status == 'schedule') ? 'selected' : '' ?>>Schedule</option>
									</select>
								</div>
								<!-- schedule input -->
								<div class="form-group publish-form" style="display:<?php echo ($data['all_posts']['single']->status == 'schedule') ? 'block' : 'none' ?>;">
									<input id="schedule_date" name="schedule_date" class="form-control datepicker" value="<?php echo $data['all_posts']['single']->published_at ?>" placeholder="Schedule Date">
								</div>

								<!-- api feed input -->
								<div class="form-group">
									<label for="api_feed">
										<input type="checkbox" name="api_feed" id="api_feed" value="1" class="form-control" <?php echo ($data['all_posts']['single']->feed) ? 'checked' : '' ?> > Set API Feed
									</label>&nbsp;&nbsp;
									<label for="api_popular">
										<input type="checkbox" name="api_popular" id="api_popular" value="1" class="form-control" <?php echo ($data['all_posts']['single']->popular) ? 'checked' : '' ?> > Set API Popular
									</label>
								</div>

								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- ATTRIBUTE -->
						<div class="box">
							<div class="box-header">
								<h3>Attribute</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- Parent -->
								<div class="form-group">
									<label for="page">Page</label>
									<select class="form-control select2" name="page" id="page" style="width: 100%;">
										<option value="<?php echo $data['all_posts']['page_detail']->id ?>-<?php echo $data['all_posts']['page_detail']->title ?>" selected><?php echo $data['all_posts']['page_detail']->title ?></option>
									</select>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- CATEGORIES -->
						<div class="box">
							<div class="box-header">
							<h3>Categories</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<!-- Parent -->
							<div class="form-group">
								<label for="category">Category</label>
								<select class="form-control select2" name="category[]" id="category" style="width: 100%;"> -->
								<!-- // set category -->
								<?php if ($data['all_posts']['category_detail']) :
								foreach ($data['all_posts']['category_detail'] as $category) :  
								?>
								<option value="<?php echo $category->id ?>-<?php echo $category->name ?>" selected><?php echo $category->name ?></option>
								<?php
								endforeach;
								endif;
								?>
								</select>
							</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- FEATURED IMAGE -->
						<div class="box">
							<div class="box-header">
								<h3>Featured Image</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<?php // SET IMAGE
								$meta_image = set_image($data['all_posts']['single']->image); 
							?>
								<div class="thumbnail">
									<img id="upl-image" class="img-responsive" src="<?php echo $meta_image['fullImage'] ?>">
									<input type="file" id="upload_file" name="upload_file" class="hidden">
								</div>
								<label id="upl_file_label" for="upload_file" class="btn btn-info pull-right" data-multiple="false">Upload File</label>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->


			<!-- page script -->
			<script>
			$(function () {
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
				CKEDITOR.replace("editor", {
					height: 400,
					// filemanager
					filebrowserBrowseUrl : '<?php echo $data['filemanager']['ckeditor'] ?>',
					filebrowserUploadUrl : '<?php echo $data['filemanager']['ckeditor'] ?>',
					filebrowserImageBrowseUrl : '<?php echo $data['filemanager']['ckeditor'] ?>'
				});

				// show gallery
				var page = $(".select2#page").val();
				if (page.search('Video') > 1) {
					$('#gallery-box').show()
				} else {
					$('#gallery-box').hide()
				}

				$(".select2#page").select2({
					ajax: {
						url: "<?php echo $data['all_posts']['page']; ?>",
						dataType: "json",
						data: function (params) {
							var query = {
								search: params.term,
								type: 'page'
							}

							// Query parameters will be ?search=[term]&type=public
							// console.log(query);
							return query;
						},
						processResults: function (data) {
							// Tranforms the top-level key of the response object from 'items' to 'results'
							var results = [];
							$.each(data, function (index, item) {
								results.push({
								id: item.id +'-'+ item.title,
								text: item.title
								});
								// console.log(item.title);
							});

							return {
								results: results
							};
						}
					}
				});
				$(".select2#category").select2({
					ajax: {
						url: "<?php echo $data['all_posts']['category']; ?>",
						dataType: "json",
						data: function (params) {
							var query = {
								search: params.term,
								type: 'category'
							}

							// Query parameters will be ?search=[term]&type=public
							return query;
							},
							processResults: function (data) {
							// Tranforms the top-level key of the response object from 'items' to 'results'
							var results = [];
							$.each(data, function (index, item) {
								results.push({
								id: item.id +'-'+ item.name,
								text: item.name
								});
							});

							return {
								results: results
							};
						}
					}
				});
				$(".select2#tag").select2({
					ajax: {
						url: "<?php echo $data['all_posts']['tag']; ?>",
						dataType: "json",
						data: function (params) {
						var query = {
							search: params.term,
							type: 'tag'
						}

						// Query parameters will be ?search=[term]&type=public
						return query;
						},
						processResults: function (data) {
						// Tranforms the top-level key of the response object from 'items' to 'results'
						var results = [];
						$.each(data, function (index, item) {
							results.push({
							id: item.id +'-'+ item.name,
							text: item.name
							});
						});

						return {
							results: results
						};
						}
					}
				});
				//Date picker
				$('.datepicker').datepicker({
					autoclose: true
				});
				// Hide publish-form
				// $(".publish-form").hide();
				// Show publish date when select publish
				$(document.body).on("change","#status",function(){
				var val = $(this).val();
				if (val != "schedule") {
					$(".publish-form").slideUp();
				} else {
					$(".publish-form").slideDown();
				}
				});

				// Reset image
				$(document).on("click", ".reset-image", function(e) {
					e.preventDefault();
					var parent = $(this).parent();
					parent.find("input").val("");
					parent.find("img").attr("src", "<?php echo get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>");
					parent.find("img").attr("alt", "");
					parent.find("img").attr("title", "");
					$(this).remove();
				});
				// Remove gallery
				$(document).on("click", ".trash", function() {
					$(this).parent().remove();
				});
			});
			</script>
