			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<!-- /.box-header -->
						<div class="box-body">
							<table id="table" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th class="no-sort">No.</th>
										<!-- <th class="no-sort">
											<div class="checkbox">
											<label>
												<input type="checkbox" name="check_all" id="check-all">
												Select all
											</label>
											</div>
										</th> -->
										<th>Email</th>
										<th>Fullname</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- Running script before load jQuery script -->
			<script type="text/javascript">
			$(function(){
				var dataTable = $('#table').DataTable({
					// "paging": true,
					// "lengthChange": false,
					// "searching": false,
					// "ordering": true,
					// "processing": true,
					// "serverSide": true,
					"ajax": {
						"url" : "<?php echo $data['lists'] ?>",
						// "type" : "POST"
					},
					"columns": [
						{ "data": null, "orderable": false },
						{ "data": "email" },
						{ "data": "fullname" },
						// { "data": "description" },
						{ "data": "id" }
					],
					"columnDefs": [
						{
							render: function(data, type, row) {
								// console.log(row.id);
								return '<a href="#" data-id="'+row.id+'" class="btn btn-warning view" data-toggle="tooltip" data-placement="top" title="View Message"><span class="fa fa-eye"></span></a>&nbsp;'+
								'<a href="#" data-id="'+row.id+'" data-title="'+row.fullname+'" data-attribute="Message" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Message"><span class="fa fa-trash"></span></a>';
							},
							targets: -1,
						},
						// {
						//   render: function(data, type, row) {
						//     return row.description;
						//   },
						//   targets: -2,
						// },
					]
				});
				// Numbering
				dataTable.on( 'order.dt search.dt', function () {
					dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
						cell.innerHTML = i+1;
					});
				}).draw();

				// DELETE
				$(document).on("click",".delete", function(e) {
					e.preventDefault();
					var attribute = $(this).data("attribute");
					var id = $(this).data("id");
					var title = $(this).data("title");
					$("#myModal-delete #delete-id").val(id);
					$("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
					$("#myModal-delete form").attr("action", "<?php echo $data['action']['delete']; ?>");

					$("#myModal-delete").modal("show");
				});

				// VIEW
				$(document).on("click",".view", function(e) {
					e.preventDefault();
					var id = $(this).attr('data-id');
					$.ajax({
						type: 'GET',
						dataType: 'json',
						url: '<?php echo $data['single'] ?>'+id,
						success: function(data) {
							// console.log(result);
							$("#myModal-view .modal-body").html("<div class='col-sm-12'><b>Email:</b> "+data.email+"</div>"+
								"<div class='col-sm-12'><b>Full Name:</b> "+data.full_name+"</div>"+
								"<div class='col-sm-12'><b>Message:</b> "+data.description+"</div>"
							);
						}
					});
					$("#myModal-view").modal("show");
				});
			});
			</script>