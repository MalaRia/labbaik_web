			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
						<h3>Comments</h3>
						</div>
						<!-- /.box-header -->
						<div class="box-body">
							<table id="table" class="table table-bordered table-striped table-hover">
								<thead>
									<tr>
										<th class="no-sort">No.</th>
										<th>Name</th>
										<th>Email</th>
										<th>Article</th>
										<th>Status</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
						<!-- /.box-body -->
					</div>
					<!-- /.box -->
				</div>
				<!-- /.col -->
			</div>
			<!-- /.row -->

			<!-- Running script before load jQuery script -->
			<script type="text/javascript">
			$(function(){
				// DATATABLE
				var dataTable = $('#table').DataTable({
					// "paging": true,
					// "lengthChange": false,
					// "searching": false,
					// "ordering": true,
					// "processing": true,
					// "serverSide": true,
					// "ajax": {
						"ajax" : "<?php echo $data['lists'] ?>",
						// "type" : "GET",
						// "data" : {
						
						// }
					// },
					"columns": [
						{ "data": null, "orderable": false },
						{ "data": "name" },
						{ "data": "email" },
						{ "data": "article" },
						{ "data": "status" },
						{ "data": "id" }
					],
					"columnDefs": [
						{
							// Action
							render: function(data, type, row) {
								console.log(row);
								return '<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Comment" class="btn btn-warning view" data-toggle="tooltip" data-placement="top" title="View Comment"><span class="fa fa-eye"></span></a>&nbsp;'+
								'<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Comment" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Comment"><span class="fa fa-trash"></span></a>';
							},
							targets: -1,
						},
						{
							// Status
							render: function(data, type, row) {
								// console.log(row.email);
								if (row.status == 'pending') {
									return "<span class='label label-warning'>"+row.status+"</span>";
								} else {
									return "<span class='label label-success'>"+row.status+"</span>";
								}
										
							},
							targets: 4,
						}
					]
				});
				// Numbering
				dataTable.on( 'order.dt search.dt', function () {
					dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
						cell.innerHTML = i+1;
					});
				}).draw();
					
				// VIEW
				$(document).on("click",".view", function(e) {
					e.preventDefault();
					var id = $(this).attr('data-id');
					$.ajax({
						type: 'GET',
						dataType: 'json',
						url: '<?php echo $data['single'] ?>'+id,
							success: function(data) {
							// console.log(result);
							var checked = '';
							if (data.status == 'accepted') {
								checked = 'checked';
							}
							$("#myModal-view .modal-body").html("<div class='col-sm-12'><b>Email:</b> "+data.email+"</div>"+
								"<div class='col-sm-12'><b>Full Name:</b> "+data.name+"</div>"+
								"<div class='col-sm-12'><b>Message:</b> "+data.comment+"</div>"+
								"<div class='col-sm-12'><label class='pull-right'><input type='checkbox' class='accepted' id='"+data.id+"' value='"+data.status+"' "+checked+"> Accepted</label></div>"
							);
						}
					});
					$("#myModal-view").modal("show");
				});

				// DELETE
				$(document).on("click",".delete", function(e) {
					e.preventDefault();
					var attribute = $(this).data("attribute");
					var id = $(this).data("id");
					var title = $(this).data("title");
					$("#myModal-delete #delete-id").val(id);
					$("#myModal-delete #delete-description").html("Are you sure want to delete this <b>"+ title +"'s</b> " + attribute + "?");
					$("#myModal-delete form").attr("action", "<?php echo $data['action']['delete'] ?>");

					$("#myModal-delete").modal("show");
				});
						
				// SHOW COMMENT IN FRONTEND
				$(document).on("click", ".accepted", function() {
					var id = $(this).attr('id');
					var value = $(this).val();
					if ($(this).is(':checked')) {
						value = 'accepted';
					} else {
						value = 'pending';
					}
					var csrfTokenName = $("#csrf_token").attr("name");
					var csrfTokenValue = $("#csrf_token").val();
					$.ajax({
						type: 'POST',
						dataType: 'json',
						data: {'csrf_token_haidokter': csrfTokenValue, id: id, status: value},
						url: '<?php echo $data['action']['update'] ?>'+id,
						success: function(data) {
							$("#csrf_token").val(data.csrf_token.hash);
							dataTable.ajax.reload();
						}
					})
				});
			});
			</script>