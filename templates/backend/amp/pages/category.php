<form role="form" id="form" data-id="<?php echo (get_flash('cat_id')) ? get_flash('cat_id') : ''; ?>" action="<?php echo (get_flash('form_action')) ? get_flash('form_action') : $data['action']['create'] ?>" method="POST">
  <input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
  <input type="hidden" name="form_action" id="form_action" value="<?php echo get_flash('form_action'); ?>">
  <input type="hidden" name="cat_id" id="cat_id" value="<?php echo get_flash('cat_id'); ?>">
  <div class="row">
    <!-- Title and Content -->
    <div class="col-xs-4">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
          <!-- title input -->
          <div class="form-group">
            <input type="text" id="name" name="name" class="form-control" required="required" value="<?php echo get_flash('name'); ?>" placeholder="Name...">
          </div>
          <!-- icon input -->
          <div class="form-group">
            <textarea id="icon" name="icon" class="form-control" placeholder="Icon Tag..."><?php echo get_flash('icon'); ?></textarea>
            <span class="help-block">*Optional. e.g: <code>&#x3C;i class=&#x22;fa fa-tag&#x22;&#x3E;&#x3C;/i&#x3E;<!-- <i class="fa fa-tag"></i> --></code> <br>Documentation see. <a href="http://fontawesome.io/icons/">Font Awesome</a></span>
          </div>
          <!-- description input -->
          <div class="form-group">
            <textarea id="description" name="description" class="form-control" placeholder="Description..."><?php echo get_flash('description'); ?></textarea>
            <span class="help-block">*Optional</span>
          </div>
          <!-- parent input -->
          <?php 
            $all_categories = $data['all_categories']['data'];
          ?>
          <div class="form-group">
            <select class="form-control select2" name="parent" id="parent" style="width: 100%;">
              <option value="" <?php echo (get_flash('parent') == '') ? 'selected' : '' ?>>:-Choose Parent-:</option>
              <?php foreach ($all_categories as $category) : ?>
              <option value="<?php echo $category->id ?>" <?php echo (get_flash('parent') == $category->id) ? 'selected' : '' ?>><?php echo $category->name ?></option>
              <?php endforeach; ?>
            </select>
            <span class="help-block">*Parent Category</span>
          </div>
          <!-- action input -->
          <div class="form-group">
            <select class="form-control select2" name="choose_action" id="choose_action" required="required" style="width: 100%;">
              <option value="create" data-action="<?php echo $data['action']['create'] ?>" <?php echo get_flash('cat_id') ? '' : 'selected' ?>>Create</option>
              <option value="update" data-action="<?php echo $data['action']['update'] ?>" <?php echo get_flash('cat_id') ? 'selected' : '' ?>>Update</option>
            </select>
            <!-- <span class="help-block">Action is required</span> -->
          </div>
          <div class="form-group">
            <button type="submit" class="btn btn-primary pull-right">Submit</button>
          </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
    <!-- Status, Attribute, Featured Image -->
    <div class="col-xs-8">
      <!-- STATUS -->
      <div class="box">
        <div class="box-header">
          <h3>Categories</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
              <th class="no-sort">No.</th>
              <th>Name</th>
              <th>Icon</th>
              <th>Description</th>
              <th><i class="fa fa-comments"></i></th>
              <th>Parent Name (ID)</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</form>
<!-- /form -->

<!-- Running script before load jQuery script -->
<script type="text/javascript">
  $(function(){
    // DATATABLE
    var dataTable = $('#table').DataTable({
      // "paging": true,
      // "lengthChange": false,
      // "searching": false,
      // "ordering": true,
      // "processing": true,
      // "serverSide": true,
      // "ajax": {
        "ajax" : "<?php echo $data['lists'] ?>",
        // "type" : "GET",
        // "data" : {
          
        // }
      // },
      "columns": [
        { "data": null, "orderable": false },
        { "data": "name" },
        { "data": "icon" },
        { "data": "description" },
        { "data": "count" },
        { "data": "parent_name" },
        { "data": "id" }
      ],
      "columnDefs": [
        {
          // Action
          render: function(data, type, row) {
            // console.log(row);
            return '<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Category" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Category"><span class="fa fa-edit"></span></a>&nbsp;'+
            '<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Category" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Category"><span class="fa fa-trash"></span></a>';
          },
          targets: -1,
        },
        {
          // Icon
          render: function(data, type, row) {
            return $("<i/>").html(row.icon).text();
          },
          targets: 2,
        }
      ]
    });
    // Numbering
    dataTable.on( 'order.dt search.dt', function () {
      dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
          cell.innerHTML = i+1;
      });
    }).draw();

    // EDIT
    $(document).on("click",".edit", function(e) {
      e.preventDefault();

      var id = $(this).data("id");
      var name = $(this).parent().siblings("td:eq(1)").html();
      var icon = $(this).parent().siblings("td:eq(2)").html();
      var description = $(this).parent().siblings("td:eq(3)").html();
      var prt = $(this).parent().siblings("td:eq(5)").text();
      var parent_id = "";
      if (prt != "") {
        parent_id = prt.match(/\(([^)]+)\)/);
      }

      var frmForm = $("form#form");

      // console.log(HtmlEntities.decode(icon));
      // Set Form Value
      $("#name").val(name);
      $("#icon").val(HtmlEntities.decode(icon));
      $("#description").val(description);
      $("#parent").val(parent_id).trigger("change");
      // console.log(id);
      var actionUpdate = "<?php echo $data['action']['update'] ?>" + id;
      frmForm.attr("action", actionUpdate);
      frmForm.attr("data-id",id);
      $("#form_action").val(actionUpdate);
      $("#cat_id").val(id);

      $("#choose_action").val("update").trigger("change"); // set select2

      // hide tooltip
      $(this).tooltip('hide');
    });

    // DELETE
    $(document).on("click",".delete", function(e) {
      e.preventDefault();
      var attribute = $(this).data("attribute");
      var id = $(this).data("id");
      var title = $(this).data("title");
      $("#myModal-delete #delete-id").val(id);
      $("#myModal-delete #delete-description").html("Are you sure want to delete this <b>"+ title +"'s</b> " + attribute + "?");
      $("#myModal-delete form").attr("action", "<?php echo $data['action']['delete'] ?>");

      $("#myModal-delete").modal("show");
    });

    // CHOOSE ACTION
    $("#choose_action").change(function() {
      var action = $(this).val();
      var frmForm = $("form#form");
      var id = "";
      if (action == "create") {
        frmForm.attr("data-id", "");
        frmForm.attr("action", "<?php echo $data['action']['create'] ?>");
        // field
        $("#form_action").val("<?php echo $data['action']['create'] ?>");
        $("#cat_id").val(id);
      } else if (action == "update") {;
        if (frmForm.attr("data-id") > 0) {
          var id = frmForm.attr("data-id");
          frmForm.attr("action", "<?php echo $data['action']['update'] ?>" + id);
          // field
          $("#form_action").val("<?php echo $data['action']['update'] ?>" + id);
          $("#cat_id").val(id);

        } if (frmForm.attr("data-id") == "") {
          $("#choose_action").val("create").trigger("change"); // set select2
          alert("Please choose the Category to edit!");
          return false;
        }
      }
    });
  });
</script>