			<form role="form" id="form" data-id="<?php echo (get_flash('featured_id')) ? get_flash('featured_id') : ''; ?>" action="<?php echo (get_flash('form_action')) ? get_flash('form_action') : $data['action']['create'] ?>" method="POST">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<input type="hidden" name="form_action" id="form_action" value="<?php echo get_flash('form_action'); ?>">
				<input type="hidden" name="featured_id" id="featured_id" value="<?php echo get_flash('featured_id'); ?>">
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-4">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
							<!-- name input -->
								<div class="form-group">
									<input type="text" name="name" class="form-control" id="name" placeholder="Name...">
								</div>
								<!-- name id input -->
								<div class="form-group">
									<input type="text" name="name_id" class="form-control" id="name_id" placeholder="ID Name...">
									<span class="help-block">*Allowed characters (<code>a-z</code>,<code>-</code>)</span>
								</div>
								<!-- post -->
								<div class="form-group">
									<select class="form-control select2" multiple="multiple" name="post_id[]" id="post_id" style="width: 100%;">
									</select>
								</div>
								<!-- action input -->
								<div class="form-group">
									<select class="form-control select2" name="choose_action" id="choose_action" required="required" style="width: 100%;">
									<option value="create" data-action="<?php echo $data['action']['create'] ?>" <?php echo get_flash('cat_id') ? '' : 'selected' ?>>Create</option>
									<option value="update" data-action="<?php echo $data['action']['update'] ?>" <?php echo get_flash('cat_id') ? 'selected' : '' ?>>Update</option>
									</select>
									<!-- <span class="help-block">Action is required</span> -->
									<code>If you create new you need to add it to the template</code>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary pull-right">Submit</button>
									<div class="clearfix"></div>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
					<!-- Status, Attribute, Featured Image -->
					<div class="col-xs-8">
						<!-- STATUS -->
						<div class="box">
							<div class="box-header">
							<h3>Featured</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<table id="table" class="table table-bordered table-striped table-hover">
									<thead>
									<tr>
									<th class="no-sort">No.</th>
									<!-- <th class="no-sort">
										<div class="checkbox">
										<label>
											<input type="checkbox" name="check_all" id="check-all">
											Select all
										</label>
										</div>
									</th> -->
									<th>Name</th>
									<th>Name ID</th>
									<th>Action</th>
									</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->
    
			<!-- Running script before load jQuery script -->
			<script type="text/javascript">
			$(function(){
				//Initialize Select2 Elements
				$(".select2#post_id").select2({
					ajax: {
						url: "<?php echo $data['all_featured']['posts']; ?>",
						dataType: "json",
						data: function (params) {
							var query = {
								search: params.term,
								type: 'post'
							}

							// Query parameters will be ?search=[term]&type=public
							return query;
						},
						processResults: function (data) {
							// Tranforms the top-level key of the response object from 'items' to 'results'
							var results = [];
							$.each(data, function (index, item) {
								results.push({
								id: item.id+'-'+item.title,
								text: item.title
								});
							});

							return {
								results: results
							};
						}
					}
				});
				// DATATABLE
				var dataTable = $('#table').DataTable({
					// "paging": true,
					// "lengthChange": false,
					// "searching": false,
					// "ordering": true,
					// "processing": true,
					// "serverSide": true,
					"ajax": {
						"url" : "<?php echo $data['lists'] ?>",
						// "type" : "POST"
					},
					"columns": [
						{ "data": null, "orderable": false },
						{ "data": "name" },
						{ "data": "name_id" },
						{ "data": "id" }
					],
					"columnDefs": [
						{
						render: function(data, type, row) {
							if (row.id < 4) {
								return '<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Featured Post" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Featured Post"><span class="fa fa-edit"></span></a>&nbsp;<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Featured Post" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Featured Post"><span class="fa fa-trash"></span></a>';
							} else {
								return '<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Featured Post" class="btn btn-primary edit" data-toggle="tooltip" data-placement="top" title="Edit Featured Post"><span class="fa fa-edit"></span></a>&nbsp;<a href="#" data-id="'+row.id+'" data-title="'+row.name+'" data-attribute="Featured Post" class="btn btn-danger delete" data-toggle="tooltip" data-placement="top" title="Delete Featured Post"><span class="fa fa-trash"></span></a>';
							}
						},
						targets: -1,
						}
					]
				});
				// Numbering
				dataTable.on( 'order.dt search.dt', function () {
					dataTable.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i) {
						cell.innerHTML = i+1;
					});
				}).draw();

				// EDIT
				$(document).on("click",".edit", function(e) {
					e.preventDefault();
					var id = $(this).attr('data-id');
					$.ajax({
						type: 'GET',
						dataType: 'json',
						url: '<?php echo $data['single'] ?>'+id,
						success: function(data) {
							// reset select2
							$('#post_id').html('');
							$('#post_id').val('').trigger('change');

							// Set Form Value
							$("#name").val(data.name);
							$("#name_id").val(data.name_id);

							// Set Select2
							$.each(data.post_id, function(key, val) {
								var splitVal = val.split('-');
								var intValueOfPost = val;
								var selectOption = new Option(splitVal[1], intValueOfPost, true, true);
								$('#post_id').append(selectOption).trigger('change');
							});
							
							var frmForm = $("form#form");
							frmForm.attr("data-id",id);

							$("#choose_action").val("update").trigger("change"); // set select2

						}
					});

					// hide tooltip
					$(this).tooltip('hide');
				});

				// DELETE
				$(document).on("click",".delete", function(e) {
					e.preventDefault();
					var attribute = $(this).data("attribute");
					var id = $(this).data("id");
					var title = $(this).data("title");
					$("#myModal-delete #delete-id").val(id);
					$("#myModal-delete #delete-description").html("Are you sure want to delete this <b>\"" + title +"'s\"</b> " + attribute + "?");
					$("#myModal-delete form").attr("action", "<?php echo $data['action']['delete'] ?>");

					$("#myModal-delete").modal("show");
				});

				// CHOOSE ACTION
				$("#choose_action").change(function() {
					var action = $(this).val();
					var frmForm = $("form#form");
					var id = "";
					if (action == "create") {
						frmForm.attr("data-id", "");
						frmForm.attr("action", "<?php echo $data['action']['create'] ?>");
						// field
						$("#form_action").val("<?php echo $data['action']['create'] ?>");
						$("#featured_id").val(id);
					} else if (action == "update") {;
						if (frmForm.attr("data-id") > 0) {
							var id = frmForm.attr("data-id");
							frmForm.attr("action", "<?php echo $data['action']['update'] ?>");
							// field
							$("#form_action").val("<?php echo $data['action']['update'] ?>");
							$("#featured_id").val(id);
						}
						if (frmForm.attr("data-id") == "") {
							$("#choose_action").val("create").trigger("change"); // set select2
							alert("Please choose the Featured Post to edit!");
							return false;
						}
					}
				});
			});
			</script>
