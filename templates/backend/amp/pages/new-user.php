
			<form role="form" action="" method="POST" enctype="multipart/form-data">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
								<!-- username input -->
								<div class="form-group">
									<input type="text" class="form-control" name="username" id="username" value="<?php echo get_flash('username'); ?>" required="required" placeholder="Username">
								</div>
								<!-- email input -->
								<div class="form-group">
									<input type="email" class="form-control" name="email" id="email" value="<?php echo get_flash('email'); ?>" required="required" placeholder="Email">
								</div>
								<!-- firstname input -->
								<div class="form-group">
									<input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo get_flash('first_name'); ?>" required="required" placeholder="Firstname">
								</div>
								<!-- lastname input -->
								<div class="form-group">
									<input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo get_flash('last_name'); ?>" required="required" placeholder="Lastname">
								</div>
								<!-- website input -->
								<div class="form-group">
									<input type="text" class="form-control" name="website" id="website" value="<?php echo get_flash('website'); ?>" placeholder="website">
								</div>
								<!-- role input -->
								<div class="form-group">
									<select class="form-control select2" name="role" id="role" required="required" style="width: 100%;">
									<?php foreach ($roles as $role) : ?>
										<option <?php echo (get_flash('role') == $role) ? 'selected' : '' ; ?> value="<?php echo $role ?>"><?php echo ucfirst($role) ?></option>
									<?php endforeach;?>
									</select>
								</div> 
								<!-- password input -->
								<div class="form-group">
									<input type="password" class="form-control" name="password" id="password" required="required" placeholder="Password">
								</div>
								<!-- password confirmation input -->
								<div class="form-group">
									<input type="password" class="form-control" name="password_confirmation" id="password_confirmation" required="required" placeholder="Password Confirmation">
								</div>

								<!-- facebook input -->
								<div class="form-group">
									<input type="text" class="form-control" name="facebook" id="facebook" value="" placeholder="facebook">
								</div>
								<!-- twitter input -->
								<div class="form-group">
									<input type="text" class="form-control" name="twitter" id="twitter" value="" placeholder="twitter">
								</div>
								<!-- gplus input -->
								<div class="form-group">
									<input type="text" class="form-control" name="gplus" id="gplus" value="" placeholder="gplus">
								</div>
								<!-- instagram input -->
								<div class="form-group">
									<input type="text" class="form-control" name="instagram" id="instagram" value="" placeholder="instagram">
								</div>
								<!-- linkedin input -->
								<div class="form-group">
									<input type="text" class="form-control" name="linkedin" id="linkedin" value="" placeholder="linkedin">
								</div>

								<!-- description input -->
								<div class="form-group">
									<textarea class="form-control" name="description" id="description" cols="30" rows="10" required="required" placeholder="Biographical Info"></textarea>
								</div>
								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col -->
					<!-- Status, Attribute, Featured Image -->
					<div class="col-xs-4">
						<!-- FEATURED IMAGE -->
						<div class="box">
							<div class="box-header">
								<h3>Featured Image</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="thumbnail">
									<img id="upl-image" class="img-responsive" src="<?php echo base_url('templates/backend/amp/dist/img/default.png')?>">
									<input type="file" id="upload_file" name="upload_file" class="hidden">
								</div>
								<label id="upl_file_label" for="upload_file" class="btn btn-info pull-right" data-multiple="false">Upload File</label>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col --> 
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->