			<style>
			.thumbnail.gallery img {
				height: 100px!important;
			}
			.thumbnail {
				position: relative;
			}
			.thumbnail .btn {
				top: 0;
				position: absolute;
			}
			</style>
			<form role="form" action="" method="POST">
				<input type="hidden" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
				<div class="row">
					<!-- Title and Content -->
					<div class="col-xs-8">
						<div class="box">
							<!-- <div class="box-header">
							</div> -->
							<!-- /.box-header -->
							<div class="box-body">
								<!-- title input -->
								<div class="form-group">
									<input type="text" name="title" class="form-control" value="<?php echo $data['all_pages']['single']->title; ?>" required="required" placeholder="Your Title...">
								</div>
								<!-- editor input -->
								<div class="form-group">
									<textarea id="editor" name="content" class="form-control" placeholder=""><?php echo $data['all_pages']['single']->content; ?></textarea>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<div class="box">
							<div class="box-body">
								<!-- Tab panes -->
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs" role="tablist">
										<li role="post" class="active"><a href="#seo" aria-controls="seo" role="tab" data-toggle="tab">SEO</a></li>
										<li role="post"><a href="#setting" aria-controls="setting" role="tab" data-toggle="tab">Setting</a></li>
									</ul>
								</div>
								<div class="tab-content" style="padding-top: 15px;">
									<!-- SEO -->
									<div role="tabpanel" class="tab-pane active" id="seo">
									<?php $seo = json_decode($data['all_pages']['single']->seo);
										$title = ($seo) ? $seo->title : '';
										$description = ($seo) ? $seo->description : '';
										$keywords = ($seo) ? $seo->keywords : '';
									?>
									<div class="form-group">
										<input type="text" id="seo_title" name="seo_title" class="form-control" value="<?php echo $title; ?>" placeholder="Seo Title">
									</div>
									<div class="form-group">
										<textarea id="seo_description" name="seo_description" class="form-control" placeholder="Seo Description" rows="3"><?php echo $description; ?></textarea>
									</div>
									<div class="form-group">
										<textarea id="seo_keywords" name="seo_keywords" class="form-control" placeholder="Seo Keywords" rows="3"><?php echo $description; ?></textarea>
									</div></div>
									<div role="tabpanel" class="tab-pane" id="setting">
										<!-- Allow Comments -->
										<div class="form-group">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="allow_comments" <?php echo ($data['all_pages']['single']->comment == 1) ? 'checked' : ''?> value="<?php echo $data['all_pages']['single']->comment ?>" id="allow_comments"> 
													Allow Comments
												</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.col -->
					<!-- Status, Attribute, Featured Image -->
					<div class="col-xs-4">
						<!-- STATUS -->
						<div class="box">
							<div class="box-header">
								<h3>Status</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- status input -->
								<div class="form-group">
									<select class="form-control select2" name="status" id="status" style="width: 100%;">
									<option selected="selected" value="draft" <?php echo ($data['all_pages']['single']->status == 'draft') ? 'selected' : '' ?>>Draft</option>
									<option value="publish" <?php echo ($data['all_pages']['single']->status == 'publish') ? 'selected' : '' ?>>Publish</option>
									<option value="schedule" <?php echo ($data['all_pages']['single']->status == 'schedule') ? 'selected' : '' ?>>Schedule</option>
									</select>
								</div>
								<!-- schedule input -->
								<div class="form-group publish-form" style="display:<?php echo ($data['all_pages']['single']->status == 'schedule') ? 'block' : 'none' ?>;">
									<input id="schedule_date" name="schedule_date" class="form-control datepicker" value="<?php echo $data['all_pages']['single']->published_at ?>" placeholder="Schedule Date">
								</div>

								<!-- Save Button -->
								<div class="form-group">
									<button type="submit" id="submit" name="submit" class="btn btn-primary pull-right">Submit</button>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- ATTRIBUTE -->
						<div class="box">
							<div class="box-header">
								<h3>Attribute</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<!-- parent input -->
								<?php 
									$all_pages = $data['all_pages']['data'];
								?>
								<div class="form-group">
									<select class="form-control select2" name="parent" id="parent" style="width: 100%;">
									<option value="" <?php echo ($data['all_pages']['single']->parent_id == '') ? 'selected' : '' ?>>:-Choose Parent-:</option>
									<?php foreach ($all_pages as $page) : ?>
									<option value="<?php echo $page->id ?>" <?php echo ($data['all_pages']['single']->parent_id == $page->id) ? 'selected' : '' ?>><?php echo $page->title ?></option>
									<?php endforeach; ?>
									</select>
									<span class="help-block">*Parent Page</span>
								</div>
								<!-- Order -->
								<!-- <div class="form-group">
									<label for="order">Order</label>
									<input type="number" id="order" name="order" class="form-control" placeholder="Order">
								</div> -->
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
						<!-- FEATURED IMAGE -->
						<div class="box">
							<div class="box-header">
								<h3>Featured Image</h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
							<?php // SET IMAGE
								$meta_image = set_image($data['all_pages']['single']->image); ?>
								<div class="thumbnail">
									<img id="image-set" class="img-responsive" src="<?php echo $meta_image['fullImage'] ?>">
									<input type="hidden" name="fullImage" id="fullImage" value="<?php echo $meta_image['fullImage'] ?>">
									<input type="hidden" name="image_big" id="imageFileField" value="<?php echo $meta_image['bigImage'] ?>">
									<input type="hidden" name="image_thumb" id="thumbFileField" value="<?php echo $meta_image['thumbImage'] ?>">
									<input type="hidden" name="image_title" id="imageTitle" value="<?php echo $meta_image['titleImage'] ?>">
									<input type="hidden" name="image_alt" id="imageAlt" value="<?php echo $meta_image['altImage'] ?>">
								</div>
								<a href="#" id="popup" class="btn btn-info pull-right">Upload File</a>
								<div class="clearfix"></div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col --> 
				</div>
				<!-- /.row -->
			</form>
			<!-- /form -->
    
			<!-- page script -->
			<script>
			$(function () {
				// Replace the <textarea id="editor1"> with a CKEditor
				// instance, using default configuration.
				CKEDITOR.replace("editor", {
					height: 400,
					// filemanager
					filebrowserBrowseUrl : '<?php echo $data['filemanager']['ckeditor'] ?>',
					filebrowserUploadUrl : '<?php echo $data['filemanager']['ckeditor'] ?>',
					filebrowserImageBrowseUrl : '<?php echo $data['filemanager']['ckeditor'] ?>'
				});
				//Initialize Select2 Elements
				$(".select2").select2();
				//Date picker
				$('.datepicker').datepicker({
					autoclose: true
				});
				// Hide publish-form
				// $(".publish-form").hide();
				// Show publish date when select publish
				$(document.body).on("change","#status",function(){
					var val = $(this).val();
					if (val != "schedule") {
						$(".publish-form").slideUp();
					} else {
						$(".publish-form").slideDown();
					}
				});

				// Reset image
				$(document).on("click", ".reset-image", function(e) {
					e.preventDefault();
					var parent = $(this).parent();
					parent.find("input").val("");
					parent.find("img").attr("src", "<?php echo get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>");
					parent.find("img").attr("alt", "");
					parent.find("img").attr("title", "");
					$(this).remove();
				});
			});
			</script>
