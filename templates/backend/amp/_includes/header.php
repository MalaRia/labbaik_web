<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>AdminLTE 2 | Dashboard</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.6 -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../bootstrap/css/bootstrap.min.css') ?>">
		<!-- Font Awesome -->
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"> -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../plugins/font-awesome/css/font-awesome.min.css') ?>">
		<!-- Ionicons -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		<!-- AdminLTE Skins. Choose a skin from the css/skins
			folder instead of downloading all of them to reduce the load. -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../dist/css/skins/_all-skins.min.css') ?>">
		<!-- DataTables -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../plugins/datatables/dataTables.bootstrap.css') ?>">
		
		<!-- Select2 -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../plugins/select2/select2.min.css') ?>">
		<!-- iCheck for checkboxes and radio inputs -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../plugins/iCheck/all.css') ?>">

		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo get_template_directory(dirname(__FILE__), '../dist/css/AdminLTE.min.css') ?>">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<script type="text/javascript">
			/*! http://mrclay.org/index.php/2010/11/14/using-jquery-before-its-loaded/ */ (function(a){if(a.$)return;var b=[];a.$=function(c){b.push(c)};a.defer$=function(){while(f=b.shift())$(f)}})(window);
		</script>

		<style>
			.user-panel>.image>img {
				height: 43px;
			}
		</style>
	</head>
	<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
	<!-- the fixed layout is not compatible with sidebar-mini -->
	<body class="hold-transition skin-blue fixed sidebar-mini">
		<!-- Site wrapper -->
		<div class="wrapper">

			<header class="main-header">
				<!-- Logo -->
				<a href="<?php echo site_url('admin/dashboard') ?>" class="logo">
					<!-- mini logo for sidebar mini 50x50 pixels -->
					<span class="logo-mini"><b>A</b>LT</span>
					<!-- logo for regular state and mobile devices -->
					<span class="logo-lg"><b>Admin</b>LTE</span>
				</a>
				<!-- Header Navbar: style can be found in header.less -->
				<nav class="navbar navbar-static-top"> 
					<!-- Sidebar toggle button-->
					<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</a>

					<div class="navbar-custom-menu">
					<?php $avatar = json_decode($user_data->avatar); ?>
						<ul class="nav navbar-nav">
						<!-- Messages: style can be found in dropdown.less-->
						<!-- <li class="dropdown messages-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-envelope-o"></i>
							<span class="label label-success">4</span>
							</a> -->
							<!-- <ul class="dropdown-menu">
							<li class="header">You have 4 messages</li>
							<li> -->
								<!-- inner menu: contains the actual data -->
								<!-- <ul class="menu"> -->
								<!-- <li>start message -->
									<!-- <a href="#">
									<div class="pull-left">
										<img src="<?php echo base_url('assets/uploads/thumbs/'.$avatar->thumb); ?>" class="img-circle" alt="User Image">
									</div>
									<h4>
										Support Team
										<small><i class="fa fa-clock-o"></i> 5 mins</small>
									</h4>
									<p>Why not buy a new awesome theme?</p>
									</a>
								</li> -->
								<!-- end message -->
								<!-- </ul>
							</li>
							<li class="footer"><a href="#">See All Messages</a></li>
							</ul>
						</li> -->
						<!-- Notifications: style can be found in dropdown.less -->
						<!-- <li class="dropdown notifications-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-bell-o"></i>
							<span class="label label-warning">10</span>
							</a>
							<ul class="dropdown-menu">
							<li class="header">You have 10 notifications</li>
							<li> -->
								<!-- inner menu: contains the actual data -->
								<!-- <ul class="menu">
								<li>
									<a href="#">
									<i class="fa fa-users text-aqua"></i> 5 new members joined today
									</a>
								</li>
								</ul>
							</li>
							<li class="footer"><a href="#">View all</a></li>
							</ul>
						</li> -->
						<!-- User Account: style can be found in dropdown.less -->
							<li class="dropdown user user-menu">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="<?php echo (isset($avatar->thumb)) ? base_url('assets/uploads/thumbs/'.$avatar->thumb) : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>" class="user-image" alt="User Image">
									<span class="hidden-xs"><?php echo $user_data->username ?></span>
								</a>
								<ul class="dropdown-menu">
									<!-- User image -->
									<li class="user-header">
										<img src="<?php echo (isset($avatar->thumb)) ? base_url('assets/uploads/thumbs/'.$avatar->thumb) : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>" class="img-circle" alt="User Image">

										<p>
										<?php echo $user_data->username ?> - <?php echo $user_data->role ?>
										<small><?php echo format_date($user_data->created_at) ?></small>
										</p>
									</li>
									<!-- Menu Footer-->
									<li class="user-footer">
										<div class="pull-left">
										<a href="<?php echo site_url('admin/user/profile') ?>" class="btn btn-default btn-flat">Profile</a>
										</div>
										<div class="pull-right">
										<a href="<?php echo site_url('admin/auth/logout') ?>" class="btn btn-default btn-flat">Sign out</a>
										</div>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</nav>
			</header>