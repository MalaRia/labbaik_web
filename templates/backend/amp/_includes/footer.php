			<footer class="main-footer">
				<div class="pull-right hidden-xs">
					<b>Version</b> 2.3.6
				</div>
				<strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
			</footer>
		</div>
		<!-- ./wrapper -->


		<!-- MODAL DELETE -->
		<div class="modal fade" id="myModal-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content box box-solid box-danger">
					<div class="modal-header box-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Delete !</h4>
					</div>
					<div class="modal-body box-body">
						<p id="delete-description"></p>
						<form role="form" action="" method="POST">
							<input type="hidden" id="csrf_token" name="<?php echo $csrf_token['name'] ?>" value="<?php echo $csrf_token['hash'] ?>" />
							<div class="form-group pull-right">
								<input type="hidden" id="delete-id" name="delete-id">
								<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
								<button type="submit" class="btn btn-danger">Yes</button>
							</div>
							<div class="clearfix"></div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL VIEW -->
		<div class="modal fade" id="myModal-view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content box box-solid box-warning">
					<div class="modal-header box-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">View</h4>
					</div>
					<div class="modal-body box-body">
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL STATUS -->
		<div class="modal fade" id="myModal-status" data-status="<?php echo (get_flash('status') != NULL) ? "true" : "false"; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog" role="document">
				<div class="modal-content box box-solid box-<?php echo get_flash('class') ?>">
					<div class="modal-header box-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel"><?php echo get_flash('status') ?></h4>
					</div>
					<div class="modal-body box-body">
						<ul class="error-list">
							<?php echo (get_flash('status') == 'success') ? get_flash('success') : get_flash('errors'); ?>
						</ul>
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL FEATURED IMAGE -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title col-sm-3" id="myModalLabel">
							<a href="<?php echo $data['filemanager']['standalone'] ?>" class="btn btn-warning" id="popup-upload"><span class="glyphicon glyphicon-picture"></span> Choose File</a>
						</h4>
					</div>
					<div class="modal-body">
						<div class="col-sm-8">
							<img id="image_preview" class="img-responsive" />
						</div>
						<div class="col-sm-4">
							<blockquote class="meta-image">
								<span id="fullname"></span> <br>
								<span id="modified_at"></span> <br>
								<span id="size"></span> <br>
								<span id="dimensions"></span>
							</blockquote>
							<input type="hidden" class="form-control" id="fieldID4">
							<div class="form-group">
								<input type="text" class="form-control" name="url" id="url" placeholder="Location">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="image_title_man" id="image_title" placeholder="Title">
								<input type="hidden" name="image_img_man" id="imageFile">
								<input type="hidden" name="image_thumb_man" id="thumbFile">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="image_alt_man" id="image_alt" placeholder="Image Alt">
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="modal-footer">
						<div class="col-sm-4 pull-right">
							<div class="form-group">
								<a href="#" id="set-image" data-multiple="false" class="btn btn-success">Set Image</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- MODAL FILEMANAGER -->
		<div class="modal fade" id="myModal-Upload" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">FILEMANAGER</h4>
					</div>
					<div class="modal-body">
						...
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery 2.2.3 -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/jQuery/jquery-2.2.3.min.js') ?>"></script>
		<!-- Defer jQuery -->
		<script>defer$()</script> 
		<!-- Bootstrap 3.3.6 -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../bootstrap/js/bootstrap.min.js') ?>"></script>
		<!-- SlimScroll -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>
		<!-- FastClick -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/fastclick/fastclick.js') ?>"></script>

		<!-- ChartJS 1.0.1 -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/chartjs/Chart.min.js') ?>"></script>

		<!-- DataTables -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/datatables/jquery.dataTables.min.js') ?>"></script>
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/datatables/dataTables.bootstrap.min.js') ?>"></script>

		<!-- Select2 -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/select2/select2.full.min.js') ?>"></script>

		<!-- Htmlentities -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/custom/htmlentities.js') ?>"></script>
		<!-- CK Editor -->
		<!-- <script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script> -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/ckeditor/ckeditor.js') ?>"></script>
		<!-- bootstrap datepicker -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/datepicker/bootstrap-datepicker.js') ?>"></script>
		<!-- iCheck 1.0.1 -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../plugins/iCheck/icheck.min.js') ?>"></script>
		<!-- Filemanager custom callback -->
		<script type="text/javascript" src="<?php echo base_url('templates/assets/filemanager/custom-callback.js') ?>"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo get_template_directory(dirname(__FILE__), '../dist/js/app.min.js') ?>"></script>

		<!-- AdminLTE for demo purposes -->
		<!-- <script src="<?php // echo get_template_directory(dirname(__FILE__), 'dist/js/demo.js') ?>"></script> -->
		<script type="text/javascript">
			//Initialize Select2 Elements
			$(".select2").select2();

			// Checkbox
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' // optional
			});

			// $('.checkbox').iCheck('update')[0].checked;
			$('#allow_comments').on('ifChanged', function(e) {
				if ($(this).prop('checked')) {
					$(this).val('1');
				} else {
					$(this).val('0');
				}
			});

			// show message myModal-status
			// console.log($("#myModal-status").attr("data-status"));
			if ($("#myModal-status").attr("data-status") == "true") {
				$("#myModal-status").modal("show");
			}
			// Meta Featured Image
			  uploadPath = "<?php echo base_url('assets/uploads/') ?>";
			  
			  // Preview image before load
			$("#upload_file, #upload_file_icon, #upload_file_logo").change(function() {
				// alert();
				readURL(this);
			})

			function readURL(input) {
				var idField = $(input).attr('id');
				// console.log(idField);
				if (input.files && input.files[0]) {
					var reader = new FileReader();
				
					reader.onload = function(e) {
						if (idField == 'upload_file_icon') {
							$('#upload_file_icon_image').attr('src', e.target.result);
						} else if (idField == 'upload_file_logo') {
							$('#upload_file_logo_image').attr('src', e.target.result);
						} else {
							$('#upl-image').attr('src', e.target.result);
						}
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
		</script>
	</body>
</html>