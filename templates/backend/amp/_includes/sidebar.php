			<!-- Left side column. contains the sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
					<!-- Sidebar user panel -->
					<div class="user-panel">
						<div class="pull-left image">
							<?php $avatar = json_decode($user_data->avatar); ?>
							<img src="<?php echo (isset($avatar->thumb)) ? base_url('assets/uploads/thumbs/'.$avatar->thumb) : get_template_directory(dirname(__FILE__), '../dist/img/default.png') ?>" class="img-circle" alt="User Image">
						</div>
						<div class="pull-left info">
							<p><?php echo $user_data->username ?></p>
							<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
						</div>
					</div>
					<!-- sidebar menu: : style can be found in sidebar.less -->
					<ul class="sidebar-menu">
						<li class="header">MAIN NAVIGATION</li>
						<li class="treeview <?php echo current_menu('dashboard') ?>">
							<a href="<?php echo site_url('admin/dashboard'); ?>">
								<i class="fa fa-dashboard"></i> <span>Dashboard</span>
							</a>
						</li>
						<li class="treeview <?php echo current_menu('page') ?>">
							<a href="#">
								<i class="fa fa-files-o"></i>
								<span>Pages</span>
								<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo current_menu('page', NULL, TRUE) ?>"><a href="<?php echo site_url('admin/page') ?>"><i class="fa fa-circle-o"></i> Pages</a></li>
								<li class="<?php echo current_menu('page', 'create', TRUE) ?>"><a href="<?php echo site_url('admin/page/create') ?>"><i class="fa fa-circle-o"></i> New Page</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo current_menu('post') ?> <?php echo current_menu('comment') ?>">
							<a href="#">
								<i class="fa fa-newspaper-o"></i>
								<span>Posts</span>
								<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo current_menu('post', NULL, TRUE) ?>"><a href="<?php echo site_url('admin/post') ?>"><i class="fa fa-circle-o"></i> Posts</a></li>
								<li class="<?php echo current_menu('post', 'create', TRUE) ?>"><a href="<?php echo site_url('admin/post/create') ?>"><i class="fa fa-circle-o"></i> New Post</a></li>
								<li class="<?php echo current_menu('comment') ?>"><a href="<?php echo site_url('admin/comment') ?>"><i class="fa fa-circle-o"></i> Comment</a></li>
								<li class="<?php echo current_menu('category') ?>"><a href="<?php echo site_url('admin/category') ?>"><i class="fa fa-circle-o"></i> Categories</a></li>
								<!-- <li class="<?php // echo current_menu('tag') ?>"><a href="<?php // echo site_url('admin/tag') ?>"><i class="fa fa-circle-o"></i> Tags</a></li> -->
								<li class="<?php echo current_menu('post', 'feed', TRUE) ?>"><a href="<?php echo site_url('admin/post/feed') ?>"><i class="fa fa-circle-o"></i> Feed Posts</a></li>
								<li class="<?php echo current_menu('post', 'feed-popular', TRUE) ?>"><a href="<?php echo site_url('admin/post/feed-popular') ?>"><i class="fa fa-circle-o"></i> Feed Popular Posts</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo current_menu('user') ?>">
							<a href="#">
								<i class="fa fa-users"></i>
								<span>Users</span>
								<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo current_menu('user', NULL, TRUE) ?>"><a href="<?php echo site_url('admin/user') ?>"><i class="fa fa-circle-o"></i> Users</a></li>
								<li class="<?php echo current_menu('user', 'create', TRUE) ?>"><a href="<?php echo site_url('admin/user/create') ?>"><i class="fa fa-circle-o"></i> New User</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo current_menu('contact') ?>">
							<a href="#">
								<i class="fa fa-phone"></i>
								<span>Contacts</span>
								<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<!-- <li class="<?php // echo active_menu('contact','information')['child'] ?>"><a href="<?php echo site_url('admin/contact/information') ?>"><i class="fa fa-circle-o"></i> Information</a></li> -->
								<li class="<?php echo current_menu('contact', 'message', TRUE) ?>"><a href="<?php echo site_url('admin/contact/message') ?>"><i class="fa fa-circle-o"></i> Messages</a></li>
							</ul>
						</li>
						<li class="treeview <?php echo current_menu('featured-post') ?>">
							<a href="#">
								<i class="fa fa-rocket"></i>
								<span>Widgets</span>
								<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<!-- <li class="<?php // echo active_menu('slider')['parent'] ?>"><a href="<?php echo site_url('admin/slider') ?>"><i class="fa fa-circle-o"></i> Sliders</a></li> -->
								<li class="<?php echo current_menu('featured-post', NULL, TRUE) ?>"><a href="<?php echo site_url('admin/featured-post') ?>"><i class="fa fa-circle-o"></i> Featured Posts</a></li>
								<!-- <li class="<?php // echo active_menu('ads')['parent'] ?>"><a href="<?php echo site_url('admin/ads') ?>"><i class="fa fa-circle-o"></i> Ads</a></li> -->
							</ul>
						</li>
						<li class="treeview <?php echo current_menu('setting') ?>">
							<a href="#">
								<i class="fa fa-gears"></i>
								<span>Settings</span>
								<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
								</span>
							</a>
							<ul class="treeview-menu">
								<li class="<?php echo current_menu('setting', 'theme', TRUE) ?>"><a href="<?php echo site_url('admin/setting/theme') ?>"><i class="fa fa-circle-o"></i> Theme</a></li>
								<li class="<?php echo current_menu('setting', 'web', TRUE) ?>"><a href="<?php echo site_url('admin/setting/web') ?>"><i class="fa fa-circle-o"></i> Web</a></li>
								<li class="<?php echo current_menu('setting', 'seo', TRUE) ?>"><a href="<?php echo site_url('admin/setting/seo') ?>"><i class="fa fa-circle-o"></i> SEO</a></li>
								<li class="<?php echo current_menu('setting', 'social-media', TRUE) ?>"><a href="<?php echo site_url('admin/setting/social-media') ?>"><i class="fa fa-circle-o"></i> Social Media</a></li>
							</ul>
						</li>
					</ul>
				</section>
				<!-- /.sidebar -->
			</aside>