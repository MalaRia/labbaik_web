<?php get_template('_includes/header') ?>
  <!-- =============================================== -->

  <?php get_template('_includes/sidebar') ?>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo $page_title; ?>
        <!-- <small>Blank example to the fixed layout</small> -->
      </h1>
      <ol class="breadcrumb">
        <!-- <li class="active"><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> -->
        <?php echo $breadcrumb; ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <?php echo $layout; ?>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php get_template('_includes/footer') ?>