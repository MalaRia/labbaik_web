// update user
$data = array (
    'username' => 'user1',
    'password' => generate_hash('password'),
    'email' => 'user1@gmail.com',
    'display_name' => 'User 1',
    'avatar' => json_encode(array('image'=>'avatar.jpg', 'thumb'=>'avatar_thumb.jpg')),
    'gender' => 1,
    'info' => '',
    'role' => 3, // member
    'status' => 0,
    'updated_at' => $this->current_datetime,
    'updated_by' => 1,
);
$update = $this->user_model->update($data, array('id' => 2));
if ($update) {
    echo "update";
} else {
    echo "failed";
}