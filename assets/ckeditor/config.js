/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.pasteFromWordRemoveStyles = false;
    config.pasteFromWordRemoveFontStyles = false;
	config.height = 500;
	config.line_height="1px;1.1px;1.2px;1.3px;1.4px;1.5px";
};
